#ifndef _SPARSE_MATRIX_H
#define _SPARSE_MATRIX_H
/** \file sparseMatrix.h
 *	\brief General functions for the use of sparse matrices, including
 *	constructor and destructor functions
 */

#include "definitions.h"

//Sets the pointers in the sparse matrix structure to NULL
void set_sparse_mat_to_null( SparseMatrix *mat ) ;

//Frees the memory assigned on the heap for the sparse matrix structure
void free_sparse_mat( SparseMatrix *mat ) ;

//Performs a sparse matrix-vector multiply
void sparse_matrix_vec_mult( const SparseMatrix *mat,
	const double *vec, double *res ) ;
	
//Performs the calculation of the transpose of a sparse matrix
void sparse_matrix_transpose( const SparseMatrix *mat,
	SparseMatrix *tMat, bool initMemory ) ;
	
//Copies the sparse matrix structure of a matrix into a new matrix, which is
//assumed to not have been initialised before
void copy_sparse_matrix_struct( const SparseMatrix *source,
	SparseMatrix *dest ) ;
	
//Useful function for testing purposes when small matrices can be written to the
//console to be inspected
void write_sparse_matrix_to_console( const SparseMatrix *mat ) ;

//Scales a vector by a given scalor factor
void scale_vec( double fact, unsigned int length, double *vec ) ;

//Scales the entries in the matrix by the given factor
void scale_mat( double fact, SparseMatrix *mat ) ;

//Calculates the infinity norm of the vector passed in
double dvec_infty_norm( const double *vect, unsigned int length ) ;

//Calculates the infinity norm of the difference in the vectors passed in
double dvec_diff_infty_norm( const double *vec1, const double *vec2,
	unsigned int length ) ;
	
//Calculates the dot product of two vectors
double vec_dot_prod( const double *vec1, const double *vec2,
	unsigned int numPoints ) ;
	
//Sets the entries in the sparse matrix to zero
void set_matrix_entries_to_zero( SparseMatrix *mat ) ;

//Returns the index in the matrix for the given column in the given row
unsigned int get_matrix_ind( const SparseMatrix *mat,
	unsigned int rowNum, unsigned int colNum ) ;
	
//Sets the entries in a vector t obe a given constant value
void set_const_dvec( unsigned int length, const double val, double *vec ) ;

//Perturbs each entry in the vector by the given constant value
void perturb_dvec( unsigned int length, const double pVal, double *vec ) ;

//Writes the vector as a space separated list on a single line to the console
void write_dvec_to_console( unsigned int length, unsigned int accuracy,
	const double *vec ) ;
	
//Calculates the discrete L2 inner product of two vectors
double discrete_l2_inner_prod( unsigned int length, const double *vec1,
	const double *vec2 ) ;
	
//Calculates the discrete L2 norm of a vector
double discrete_l2_norm( unsigned int length, const double *vec ) ;

//Writes a sparse matrix out to file in full matrix form for inspection. This is
//useful for debugging purposes
void write_sparse_matrix_to_file( const char *fName, const SparseMatrix *mat ) ;

//Writes the non-zero columns of a row to the console. This is useful for
//debugging purposes
void write_row_nonzero_cols_to_console( const SparseMatrix *mat,
	unsigned int row ) ;
	
//Removes the final n rows from a sparse matrix
void delete_sparse_matrix_last_n_rows( SparseMatrix *mat, unsigned int n ) ;

//Calculates the difference in the vectors passed in and stored the result in
//another vector
void dvec_diff( unsigned int length, const double *vec, const double *minusVec,
	double *res ) ;
	
//Normalises a vector such that its norm is one
void normalise_vec( unsigned int length, double *vec ) ;

#endif
