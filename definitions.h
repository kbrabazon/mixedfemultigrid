#ifndef _DEFINITIONS_H
#define _DEFINITIONS_H
/*! \file definitions.h
	\brief Definitions of useful constants, structs and functions
	
	Contains structures and functions that are not specific to a particular area
	of the implementation, and that will be used in (almost) all other files in
	the project
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdarg.h>
//Remove the next line in order to stop assertions from being compiled.
//Alternatively run the command:
//cmake -DCMAKE_BUILD_TYPE=Release ..
//from the current directory
//#define NDEBUG
#include <assert.h>

extern void dgesv_(int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b,
	int *ldb, int *info ) ;

/*! The perturbation to use when calculating the numerical derivative */
#define M_EPS (1e-6)
/*! \f$\pi\f$ to 18 decimal places */
#define PI (3.141592653589793238)
/*! The maximum number of linear smooths to perform when 'solving' on the
coarsest grid */
#define MAX_LIN_SMOOTHS (2000)
/*! The maximum number of nonlinear smooths to perform when 'solving' on the
coarsest grid */
#define MAX_NONLIN_SMOOTHS (500)
/*! The tolerance to 'solve' to in the linear case */
#define LIN_TOL	(1e-10)
/*!< The tolerance to 'solve' to in the nonlinear case */
#define NONLIN_TOL (1e-6)
/*! The tolerance to solve a nonlinear system to when performing a Newton
iteration */
#define NEWT_TOL (1e-3)
/*! The tolerance to solve a nonlinear system to when performing an FAS
iteration */
#define FAS_TOL (1e-3)
/*! The tolerance below which we assume an iteration to be 'solved' */
#define SOLVED_TOL (1e-10)
/*! The maximum number of linear iterations to perform per Newton iteration */
#define MAX_INNER_ITS (5)
/*! The scaling factor by which to increase a time step */
#define SCALE_UP_FACT (1.2)
/*! The scaling factor by which to decrease a time step */
#define SCALE_DOWN_FACT (0.85)
/*! The minimum scaling factor to use in a global Newton iteration */
#define MIN_NEWT_WEIGHT (0.0078125) //This is 1 / 128
//#define MIN_NEWT_WEIGHT (0.03125) //This is 1/32

/*! Enumerator for the type of algorithm to run. The choices are either Newton
	or FAS */
enum ALG_TYPE{
	ALG_FAS = 0,
	ALG_NEWTON
} ;

/*! Enumerator for the type of smoothing to use. The choices are either Jacobi
	or Gauss-Seidel iterations. Point-wise variants are used */
enum SMOOTH_TYPE{
	SMOOTH_JAC = 0,
	SMOOTH_GS,
	SMOOTH_SYM_GS
} ;

/*! Enumerator for the type of linear iteration to perform. The choices are
    either Multigrid, preconditioned CG or preconditioned GMRES */
enum LIN_IT{
	IT_LIN_MG = 0,
	IT_GMRES,
	IT_PGMRES,
	IT_PCG
} ;

/*! Defines a node type. We are interested in the distinction between interior
	grid nodes, Neumann boundary nodes and Dirichlet boundary nodes */
enum NODE_TYPE{
	NODE_INTERIOR = 0,
	NODE_DIR_BND,
	NODE_NEUM_BND
} ;

/*! Defines the orientation of an edge on an element */
enum EDGE_ORIENTATION{
	O_BACKWARD = 0,
	O_FORWARD
} ;

/*! Defines the orientation of edges on even numbered elements. Note that this
can only be used as we are on a regular grid for which the structure is known.
This is defined in file gridHandling.c */
extern const enum EDGE_ORIENTATION oEven[] ;
/*! Defines the orientation of edges on odd numbered elements. Note that this
can only be used as we are on a regular grid for which the structure is known.
This is defined in file gridHandling.c */
extern const enum EDGE_ORIENTATION oOdd[] ;
/*! Gives the filenames of the files from which to read the grids. The paths are
given relative to the current one. The grids are sought in folder ../Grids/ and
must be named mesh<d>.out, where 4 <= <d> <= 1024 is a power of 2 */
extern const char *gridFiles[] ;

/*! Defines the boundary types possible on the grid */
enum BND_TYPE{
	BND_TYPE_DIR = 1,
	BND_TYPE_NEUM
} ;

/*! \brief Defines the type of an edge

	We are interested in knowning if both nodes are on the Dirichlet boundary or
	not. If so the edge type will be KNOWN otherwise the edge will have some
	unknown values, so the type will be UNKNOWN */
enum EDGE_TYPE{
	EDGE_KNOWN,
	EDGE_UNKNOWN
} ;

/*! \brief A sparse matrix structure, which is stored in compressed row
	format */
typedef struct{
	unsigned int numNonZeros ; /*!< The number of nonzero entries in the
				matrix */
	unsigned int numRows ; /*!< The number of rows in the matrix */
	unsigned int numCols ; /*!< The number of columns in the matrix */
	double *globalEntry ; /*!< The vectorised form of the matrix stored in row
				order */
	unsigned int *rowStartInds ; /*!< The starting indices for the matrix rows
				in vector \p globalEntry */
	unsigned int *colNums ; /*!< The column numbers of the non-zero entries in
				the rows of the matrix */
}SparseMatrix ;

/*! \brief A wrapper for a coordinate with x- and z-components */
typedef struct{
	double x ; /*!< The x-coordinate */
	double z ; /*!< The z-coordinate */
}Vec2D ;

/*! \brief A structure that defines a bounding box for some geometric object */
typedef struct{
	double left ; /*!< The x-coordinate of the left hand side of a bounding
				  box */
	double bottom ; /*!< The z-coordinate of the bottom of a bounding box */
	double right ; /*!< The x-coordinate of the right hand side of a bounding
				   box */
	double top ; /*!< The z-coordinate of the top of a bounding box */
}BndBox ;

/*! \brief Define a structure to store information about a node on a grid */
typedef struct{
	unsigned int id ; /*!< Identifier of a node on a grid */
	enum NODE_TYPE type ; /*!< The type of node that this is (interior,
						  Neumann or Dirichlet boundary) */
	double x ; /*!< The x-coordinate of the node */
	double z ; /*!< The z-coordinate of the node */
}Node ;

//Give the type definition of Edge so that this can be used within the
//definition of itself
/*! \brief A structure to store information about an edge on a grid */
typedef struct Edge Edge ;

/*! \brief A structure to store information about an element on a grid */
typedef struct{
	const Edge *edges[3] ; /*!< Pointers to the edges on the element. These
				are ordered in an anti-clockwise manner */
	double zfacts[3] ; /*!< Constants, depending on the location of the edge
				and the element, used in the calculation of the residual and
				Jacobian for the mixed finite element approximation */
	const enum EDGE_ORIENTATION *edgeOrient ; /*!< The direction of the edges.
				This is either forwards or backwards, and lets us know which end
				point of an edge is at the 'start' and which at the 'end' of an
				edge */
	double area ; /*!< The area of the element */
	unsigned int soilType ; /*!< The type of soil that is present on this
				element */
	const double *invStiffMat ; /*!< The inverse of the stiffness matrix on the
				current element */
	double invMatRowSum ; /*!< The sum of the row in the inverse of the
				local stiffness matrix */
}Element ;

/*! \brief A structure to store information about an edge on a grid */
struct Edge{
	unsigned int id ; /*!< Unique identifier for the edge. This is also the
				position of the edge in the vector of unknowns */
	enum EDGE_TYPE type ; /*!< The type of edge that this is. If the type is
				UNKNOWN then the function on the edge is unknown. In the case
				that the type is KNOWN the function on the edge is already
				known. This is the case for the lowest order Raviart-Thomas
				elements when both edge end points are on the Dirichlet
				boundary */
	const Node *endPoints[2] ; /*!< Nodes at the start and end of the edge. The
				nodes are stored in ascending index order */
	const Edge *parentEdges[2] ; /*!< Edges on a coarser grid on which the end
				points of this edge lie */
	const Element *parentEls[2] ; /*!< The elements on the current grid to which
				this Edge is incident */
	double mag ; /*!< The magnitude of the edge */
} ;

/*! \brief A structure to store information about a regular grid required for a
	finite element implementation with lowest order Raviart-Thomas elements*/
typedef struct{
	unsigned int numRows ; /*!< The number of rows of nodes on the regular
				grid, including the boundary nodes */
	unsigned int numCols ; /*!< The number of columns of nodes on the regular
				grid, including the boundary nodes */
	unsigned int numNodes ; /*!< The number of nodes on the grid */
	unsigned int numEdges ; /*!< The number of edges on the grid */
	unsigned int numElements ; /*!< The number of elements on the grid */
	
	unsigned int numUnknowns ; /*!< The number of unknown edges on the grid */

	unsigned int *mapU2G ; /*!< Map from the ordering of the unknown edges to
				the edges on the grid */
	
	Node *nodes ; /*!< The nodes on the grid */
	Edge *edges ; /*!< The edges on the grid */
	Element *els ; /*!< The elements on the grid */
	
	enum BND_TYPE bndType[4] ; /*!< The type of the left, bottom, right and top
				boundary are stored (in that order) in this array */
}Grid ;

/*! \brief A structure storing the restriction and interpolation operators for a
	grid */
typedef struct{
	SparseMatrix r ; /*!< Matrix to perform restriction of edges to a coarser
				grid */
	SparseMatrix p ; /*!< Matrix to perform prolongation of edges to a finer
				grid */
	double *scaleFacts ; /*!< Scaling factors for the rows in the restriction
				matrix */
}Interpolation ;

/*! \brief Structure storing information regarding soil characteristics for the
	different soils that are used in a Richards equation type problem */
typedef struct{
	unsigned int numSoils ; /*!< The number of soils on the grid */
	double *thetaR ; /*!< The residual volumetric wetness for the soils on the
				grid */
	double *thetaS ; /*!< The saturated volumetric wetness for the soils on the
				grid */
	double *sCond ; /*!< The hydraulic conductivity at saturation of the soils
				on the grid */
	double *n ; /*!< Exponent in the van Genuchten-Mualem model */
	double *m ; /*!< Exponent in the van Genuchten-Mualem model */
	double *pSat ; /*!< The non-positive pressure at which to assume that a soil
				is saturated */
	double *satFact ; /*!< A scaling factor to move between the effective
				saturation calculated using the normal and modified
				van Genuchten-Mualem models */
	double *hCondConst ; /*!< A constant used in the calculation of the
				hydraulic conductivity. Storing this saves unnecessary
				recalculations */
	double *thetaM ; /*!< A perturbed saturated volumetric wetness defined such
				that the relative saturation definition remains unchanged in the
				van Genuchten-Mualem model */
	double *alpha ; /*!< Shape parameter for a soil on the grid, in the
				van Genuchten-Mualem model */
}REData ;

/*! \brief Stores the variables associated with an approximation to the mixed
	formulation of a Richards equation */
typedef struct{
	double *lM ; /*!< The Lagrange multipliers. These are defined on every edge
				 not on the Dirichlet boundary */
	double *press ; /*!< The pressure on the domain. This is defined on every
					element */
	double *prevTheta; /*!< The volumetric wetness at the previous time-step.
					   This acts as the right hand side for the original system
					   of equations, and is incorporated into the calculations
					   for the Lagrange multipliers */
	double *rhs ; /*!< The right hand side for the nonlinear system. This is
				  defined on every edge not on the Dirichlet boundary. For a
				  Newton iteration this will not be needed, and for the FAS it
				  will be needed only on the coarse grid levels */
	double *resid ; /*!< The residual in approximation for the nonlinear
					system of equations */
}REApproxVars ;

/*! \brief Stores the variables associated with an approximation to a linear
	system of equations */
typedef struct{
	double *approx ; /*!< The approximation to the solution of a linear system
					 of equations */
	double *rhs ; /*!< The right hand side of the linear system of equations */
	double *resid ; /*!< The residual in approixmation for the linear system
					of equations */
	SparseMatrix op ; /*!< Sparse matrix representation of the linear
					   operator */
}LinApproxVars ;

/*! \brief Structure containing parameters controlling the execution of the
	algorithm */
typedef struct{
	unsigned int testCase ; /*!< Indicator as to which test case to execute.
				This has an effect on the set-up of the problem */
				
	char approxFilePrefix[50] ; /*!< Prefix of a filename from which to read
				approximations, instead of initialising them to a default
				value */
				
	unsigned int maxIts ; /*!< The maximum number of nonlinear iterations to
				perform */
	enum ALG_TYPE algorithm ; /*!< The type of algorithm to run. This is going
				to be either Newton-MG or FAS */
	unsigned int fGridLevel ; /*!< The finest grid level on which the multigrid
				algorithm should be executed */
	unsigned int cGridLevel ; /*!< The coarsest grid level on which to
				operate */
	enum SMOOTH_TYPE smoother ; /*!< The type of smoothing to use */
	enum LIN_IT linIt ; /*!< The linear iteration to perform */
	bool precSymm ; /*!< Indicator as to whether just the symmetric part of the
				Jacobian should be used as a preconditioner for GMRES */
	unsigned int numLinIts ; /*!< The number of linear iterations to perform per
				Newton iteration */
	unsigned int preSmooth ; /*!< The number of pre-smoothing iterations to
				perform */
	unsigned int postSmooth ; /*!< The number of post-smoothing iterations to
				perform */
	unsigned int numMGCycles ; /*!< The number of multigrid cycles to perform
				per grid level - 1 performs a V-cycle, 2 a W-cycle, etc. */
	double smoothWeight ; /*!< Weighting factor to use in the smoothing
				operations */
	double newtWeight ; /*!< Weight factor to use in the Newton update */
	bool globalNewton ; /*!< Indicator as to whether a globalisation technique
				should be used or not */
	
	double dirConst ; /*!< A constant to set a Dirichlet boundary to
				initially */
	
	bool testing ; /*!< Indicator as to whether testing should be performed */
	bool writeToFile ; /*!< Indicator as to whether to write intermediate
					   functions to file or not */
	
	double timeStep ; /*!< The size of the time step */
	unsigned int numTSteps ; /*!< The number of time steps to take */
	double startTime ; /*!< The time at which to start */
	double currTime ; /*!< The current time */
	double endTime ; /*!< The target time to end at */
	bool adaptTStep ; /*!< Indicator as to whether the time step should be
				adapted or not */
	double minTStep ; /*!< The minimum allowable time step */
	double maxTStep ; /*!< The maximum allowable time step */
	
	double *sparePtr ; /*!< An array to be used locally by functions, if
				required. This is defined here to avoid the reallocation and
				deallocation of memory in the program */
}Params ;

/*! \brief Structure containing data structures required to perform a linear
	multigrid iteration */
typedef struct{
	Grid **gridHierarchy ; /*!< Hierarchy of grids to use in a linear multigrid
				iteration */
	LinApproxVars **linHierarchy ; /*!< Variables required to perform a linear
				solve on each grid in a hierarchy of grids */
	Interpolation **interpHierarchy ; /*!< Interpolation operators between grids
				in a hierarchy of grids */
}LinMGCtxt ;

/*! \brief Structure containing information required to solve the Richards
	equation using a Newton-Multigrid method */
typedef struct{
	REApproxVars reVars ; /*!< Variables associated with the solution of the
			mixed finite element formulation of the Richards equation on the
			finest grid level */
	Grid *grid ; /*!< The grid on which to perform a Newton iteration */
	LinApproxVars *linVars ; /*!< Variables required for a linear solve on the
			grid on which the Newton iteration is to be performed */
	void *linItCtxt ; /*!< Information required to perform the inner linear
			solve. This will change depending on which linear iteration is
			used */
			
	unsigned int numIts ; /*<! Counts the number of Newton iterations
			performed */
	double origResidNorm ; /*!< L2 norm of the residual for an initial
			approximation */
	double currResidNorm ; /*!< L2 norm of the residual for the most recent
			approximation */
}NewtonCtxt ;

/*! \brief Structure containing information required to solve the Richards
	equation using an FAS method */
typedef struct{
	Grid **gridHierarchy ; /*!< The hierarchy of grids to use */
	Interpolation **interpHierarchy ; /*!< The interpolation operators between
			all of the grids in the grid hierarchy */
	REApproxVars **reHierarchy ; /*!< Variables associated with the solution of
			the mixed finite element formulation of the Richards equation on the
			each grid in the hierarchy */
	double *jacDiags ; /*!< The diagonals of a Jacobian matrix. All grids will
			store this information in here, as it is required to be recomputed
			every time it is to be used. This saves on repeated allocation */
	double **initLM ; /*!< The initial approximation of the Lagrange multipliers
			on a coarse grid */
}FASCtxt ;

/*! \brief Structure containing information necessary to solve a least squares
	problem given in upper Hessenberg form */
typedef struct{
	double **hessMat ; /*!< The upper Hessenberg matrix used in the least
				squares problem */
	double **upTMat ; /*!< The upper triangular matrix to transform the upper
				Hessenberg matrix into */
	double *rhs ; /*!< The right hand side of the least squares problem */
	unsigned int dim ; /*!< The dimension of the least squares problem. This
				gives the number of entries in the right hand side, and the
				number of rows in the matrix */
}LeastSqCtxt ;

/*! \brief Structure containing information required to perform a GMRES
	iteration */
typedef struct{
	unsigned int size ; /*!< The current number of basis vectors calculated in
				the GMRES iteration */
	LinApproxVars vars ; /*!< Variables required to perform the solve of a
				linear system of equations */
	double residNorm ; /*<! The norm of the residual */
	double **basis ; /*!< The orthonormal basis for the Krylov subspace */
	LeastSqCtxt leastSq ; /*!< Variables required to solve a least squares
				problem in upper Hessenberg form */
}GMRESCtxt ;

/*! \brief Structure containing information required to perform a GMRES
	iteration preconditioned by multigrid */
typedef struct{
	unsigned int size ; /*!< The current number of basis vectors calculated in
				the GMRES iteration */
	Grid **gridHierarchy ; /*!< A hierarchy of grids on which to solve a linear
				problem */
	LinApproxVars **linHierarchy ; /*!< Variables required to perform a linear
				solve on each grid in a hierarchy of grids */
	Interpolation **interpHierarchy ; /*!< The interpolation operators between
				the grids in a grid hierarchy of grids */
	double residNorm ; /*!< The norm of the residual */
	double **basis ; /*!< The orthonormal basis for the Krylov subspace */
	double **precBasis ; /*!< Basis for the preconditioned Krylov subspace */
	LeastSqCtxt leastSq ; /*!< Variables required to solve a least squares
				problem in upper Hessenberg form */
				
	double *precMatrix ; /*!< The global entries in a matrix to use for the
				preonditioning */
}PrecGMRESCtxt ;

//Makes the console output bold on a linux system. As yet undefined on a windows
//system
void make_output_bold() ;
//Resets the console output style
void reset_output_style() ;
//Sets the conosle colour output style
void set_output_colour( const char *colName ) ;

//Copies a vector of double type of length 'length'
void copy_dvec( unsigned int length, const double *source, double *dest ) ;
//Copies a scaled source vector to a destination vector of length 'length'
void copy_scaled_dvec( unsigned int length, double fact, const double *source,
	double *dest ) ;
//Sets the entries in the vector passed in to zero
void set_dvec_to_zero( unsigned int length, double *vec ) ;

//Writes output to console indented by a specified amount
void print_indent( unsigned int indentLvl, const char *fms, ... ) ;

//Sets the string representation of the linear iteration type used in a Newton
//iteration
void lin_it_str_rep( const Params *params, char *str ) ;

#endif















