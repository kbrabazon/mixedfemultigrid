#ifndef _RICHARDS_TIME_STEPPING_H
#define _RICHARDS_TIME_STEPPING_H

#include "definitions.h"

/**	\file	richardsTimeStepping.h
 *	\brief	Performs a time dependent solve of the Richards equation
 */
 
//Performs a time dependent solve of the Richards equation
void solve_richards_eq( Params *params, REData *data ) ;
 
//Performs a time dependent solve of the Richards equation where the nonlinear
//solve is performed using a Newton iteration
void newton_solve_richards_eq( Params *params, REData *data ) ;

//Performs a time dependent solve of the Richards equation, and writes
//time depenendent function data to file. This is slower than the other method
//but will allow for the inspection of functions
void newton_solve_richards_eq_write_info( Params *params, REData *data ) ;

//Performs a time dependent solve of the Richards equation where the nonlinear
//solve is performed using a Newton iteration, and the linear solve is performed
//using only smoothing operations
void newton_smooth_solve_richards_eq( Params *params, REData *data ) ;

//Performs a time-dependent solve of the Richards equation using an FAS
//iteration and writes time dependent data to file. This is slower than the
//other method, but allows for the inspection of different functions
void fas_multigrid_solve_write_info( Params *params, const REData *data ) ;

//Performs a time-dependent solve of the Richards equation using an FAS
//iteration
void fas_multigrid_solve( Params *params, const REData *data ) ;

//Updates the time step when using an adaptive time stepping strategy
void update_time_step( unsigned int numIts, Params *params ) ;


#endif
