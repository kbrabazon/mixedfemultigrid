#include "interpolation.h"
#include "sparseMatrix.h"
#include "gridHandling.h"

/** Sets the pointers in an Interpolation structure to NULL
 *	\param [in] interp	The Interpolation structure for which to set the
 *						pointers to NULL
 */
void set_interp_ops_to_null( Interpolation *interp )
{
	set_sparse_mat_to_null( &(interp->r) ) ;
	set_sparse_mat_to_null( &(interp->p) ) ;
	interp->scaleFacts = NULL ;
}

/** Frees the memory assigned on the heap for the Interpolation structure passed
 *	in
 *	\param [in] interp	The Interpolation structure for which to free the memory
 */
void free_interp_ops( Interpolation *interp )
{
	free_sparse_mat( &(interp->r) ) ;
	free_sparse_mat( &(interp->p) ) ;
	if( interp->scaleFacts != NULL ){
		free( interp->scaleFacts ) ;
		interp->scaleFacts = NULL ;
	}
}

/** Sets the sparse matrix structure for the prolongation operator from a given
 *	a coarse grid to a given fine grid. The prolongation operator will be the
 *	operator on the coarse grid
 *	\param [in] fGrid	The fine grid to prolongate onto
 *	\param [in] cGrid	The coarse grid to prolongate from
 *	\param [out] interp	The Interpolation structure for which the matrix
 *						Interpolation::p is to be set
 */
void set_prolong_mat_on_grid( const Grid *fGrid, const Grid *cGrid,
	Interpolation *interp )
{
	unsigned int i, j, k ; //Loop counters
	unsigned int edgeInd ; //The index of an edge on an element
	unsigned int numEls ; //The number of elements incident to an edge
	int endInd ; //Index of the end point of a directed edge
	int fact ; //A factor by which to multiply an entry in the prolongation
			   //matrix
	const Edge *edge ; //An edge on the grid
	const Element *el ; //An element on the grid
	SparseMatrix *pMat ; //Placeholder for the prolongation matrix
	
	//Check that the edge neighbours on a coarse grid have been set
	assert( fGrid->edges[0].parentEdges[0] != NULL ) ;
	
	//Set the placeholder for the prolongation matrix
	pMat = &(interp->p) ;
	
	//Check that no memory has been assigned to this variable yet
	assert( pMat->rowStartInds == NULL ) ;
	
	//Set the number of rows in the matrix
	pMat->numRows = fGrid->numUnknowns ;
	//Set the number of columns in the matrix
	pMat->numCols = cGrid->numUnknowns ;
	
	//Initialise the row start indices to be the correct size
	pMat->rowStartInds = (unsigned int*)calloc( (pMat->numRows + 1),
		sizeof( unsigned int ) ) ;
	
	//The first task is to figure out the number of non-zero entries that are in
	//the prolongation matrix
	//I am only interested in the rows related to the unknowns on the grid, so
	//I loop over the unknowns
	pMat->numNonZeros = 0 ;
	for( i=0 ; i < fGrid->numUnknowns ; i++ ){
		//Get the current edge
		edge = &(fGrid->edges[ fGrid->mapU2G[i] ]) ;
		//The current edge knows which coarse grid edges it lies on
		//Check if the edge is not contained entirely on one coarse grid edge
		if( edge->parentEdges[0] != edge->parentEdges[1] ){
			//The comparison of the pointers above is valid, as the only way
			//that the edges should be the same is when they share the same
			//physical memory address
			for( j=0 ; j < 2 ; j++ ){
				if( edge->parentEdges[j]->type == EDGE_UNKNOWN ){
					pMat->rowStartInds[i+1]++ ;
					pMat->numNonZeros++ ;
				}
			}
		}
		else if( edge->parentEls[0] == edge->parentEls[1] ){
			//If the parent elements are the same it means that the edge is
			//contained only in a single element (i.e. it is on a boundary)
			el = edge->parentEdges[0]->parentEls[0] ;
			edgeInd = get_edge_el_index( edge->parentEdges[0], el ) ;
			//Count the entry for the coarse grid edge on which the fine grid
			//edge lies
			pMat->rowStartInds[i+1]++ ;
			pMat->numNonZeros++ ;
			//Count a column entry for the other unknown edges on the grid
			for( j=1 ; j < 3 ; j++ ){
				if( el->edges[(edgeInd+j)%3]->type == EDGE_UNKNOWN ){
					pMat->rowStartInds[i+1]++ ;
					pMat->numNonZeros++ ;
				}
			}
		}
		else{
			//Initially set the number of neighbours to one, as the current edge
			//will always be on its own row
			pMat->rowStartInds[i+1] = 1 ;
			pMat->numNonZeros++ ;
			//Loop through both of the elements incident to the parent edge
			for( j=0 ; j < 2 ; j++ ){
				//Get a handle to the current edge
				el = edge->parentEdges[0]->parentEls[j] ;
				//Get the index of the parent edge on the coarse grid element
				edgeInd = get_edge_el_index( edge->parentEdges[0], el ) ;
				for( k=1 ; k < 3 ; k++ ){
					if( el->edges[(edgeInd+k)%3]->type == EDGE_UNKNOWN ){
						pMat->rowStartInds[i+1]++ ;
						pMat->numNonZeros++ ;
					}
				}
			}
		}
	}
	
	//Make sure that the array of row start indices is set up properly
	for( i=1 ; i < fGrid->numUnknowns+1 ; i++ )
		pMat->rowStartInds[i] += pMat->rowStartInds[i-1] ;
		
	assert( pMat->rowStartInds[fGrid->numUnknowns] == pMat->numNonZeros ) ;
		
	//Set the memory for the global entries and for the column numbers
	pMat->globalEntry = (double*)malloc( pMat->numNonZeros *
		sizeof( double ) ) ;
	pMat->colNums = (unsigned int*)malloc( pMat->numNonZeros *
		sizeof( unsigned int ) ) ;
		
	//Loop through the unknown edges again
	for( i=0 ; i < fGrid->numUnknowns ; i++ ){
		//Get the current edge
		edge = &(fGrid->edges[ fGrid->mapU2G[i] ]) ;
		//If the edge has end points on two different coarse grid edges add the
		//coarse grid edges to the entry of columns
		if( edge->parentEdges[0] != edge->parentEdges[1] ){
			//Set the column numbers in the appropriate row. We are not
			//interested in the order of these
			for( j=0 ; j < 2 ; j++ ){
				if( edge->parentEdges[j]->type == EDGE_UNKNOWN ){
					//Set the global entry
					pMat->globalEntry[ pMat->rowStartInds[i] ] = 0.5 ;
					pMat->colNums[ pMat->rowStartInds[i]++ ] =
						edge->parentEdges[j]->id ;
				}
			}
		}
		else{
			//The fine grid edge is on a coarse grid edge, but we do not know
			//which half of the coarse grid edge this is. To find this out we
			//check if the starting points of the edges coincide, but we have to
			//do this on each element on which the edge is incident
			//Loop over the elements on which the edge exists
			//Add the parent edge value, which is one
			numEls = (edge->parentEdges[0]->parentEls[0] ==
				edge->parentEdges[0]->parentEls[1]) ? 1: 2 ;
			pMat->globalEntry[ pMat->rowStartInds[i] ] = 1.0 ;
			pMat->colNums[ pMat->rowStartInds[i]++ ] =
				edge->parentEdges[0]->id ;
			for( j=0 ; j < numEls ; j++ ){
				el = edge->parentEdges[0]->parentEls[j] ;
				//Get the index of the edge on the current element
				edgeInd = get_edge_el_index( edge->parentEdges[0], el ) ;
				//Check the orientation of the edge on the element, and check if
				//the start or end nodes are the same for the coarse and fine
				//edges, and set the appropriate values in the global entry and
				//the column numbers
				endInd = (int)el->edgeOrient[ edgeInd ] ;
				fact = nodes_eq( edge->endPoints[ endInd ],
					edge->parentEdges[0]->endPoints[ endInd ]) ? 1 : -1 ;
				//Check whether the first edge is on a Dirichlet boundary or not
				if( el->edges[(edgeInd+1)%3]->type == EDGE_UNKNOWN ){
					pMat->globalEntry[ pMat->rowStartInds[i] ] = fact * 0.25 ;
					pMat->colNums[ pMat->rowStartInds[i]++ ] =
						el->edges[(edgeInd+1)%3]->id ;
				}
				//Check whether the second edge is on a Dirichlet boundary or
				//not
				if( el->edges[(edgeInd+2)%3]->type == EDGE_UNKNOWN ){
					pMat->globalEntry[ pMat->rowStartInds[i] ] = -fact * 0.25 ;
					pMat->colNums[ pMat->rowStartInds[i]++ ] =
						el->edges[(edgeInd+2)%3]->id ;
				}
			}
		}
	}
	
	assert( pMat->rowStartInds[ fGrid->numUnknowns - 1 ] ==
		pMat->numNonZeros ) ;
	
	//We reset the row start indices
	for( i=fGrid->numUnknowns ; i > 0 ; i-- )
		pMat->rowStartInds[i] = pMat->rowStartInds[i-1] ;
	pMat->rowStartInds[0] = 0 ;
}

/** Sets the sparse matrix structure for the prolongation operator from a given
 *	a coarse grid to a given fine grid. The prolongation operator will be the
 *	operator on the coarse grid. Dirichlet boundary edges are also included
 *	in the matrix
 *	\param [in] fGrid	The fine grid to prolongate onto
 *	\param [in] cGrid	The coarse grid to prolongate from
 *	\param [out] interp	The Interpolation structure for which the matrix
 *						Interpolation::p is to be set
 */
void set_prolong_mat_on_full_grid( const Grid *fGrid, const Grid *cGrid,
	Interpolation *interp )
{
	unsigned int i, j ; //Loop counters
	unsigned int edgeInd ; //The index of an edge on an element
	unsigned int numEls ; //The number of elements incident to an edge
	int endInd ; //Index of the end point of a directed edge
	int fact ; //A factor by which to multiply an entry in the prolongation
			   //matrix
	const Edge *edge ; //An edge on the grid
	const Element *el ; //An element on the grid
	SparseMatrix *pMat ; //Placeholder for the prolongation matrix
	
	//Check that the edge neighbours on a coarse grid have been set
	assert( fGrid->edges[0].parentEdges[0] != NULL ) ;
	
	//Set the placeholder for the prolongation matrix
	pMat = &(interp->p) ;
	
	//Check that no memory has been assigned to this variable yet
	assert( pMat->rowStartInds == NULL ) ;
	
	//Set the number of rows in the matrix
	pMat->numRows = fGrid->numEdges ;
	//Set the number of columns in the matrix
	pMat->numCols = cGrid->numEdges ;
	
	//Initialise the row start indices to be the correct size
	pMat->rowStartInds = (unsigned int*)calloc( (pMat->numRows + 1),
		sizeof( unsigned int ) ) ;
	
	//The first task is to figure out the number of non-zero entries that are in
	//the prolongation matrix. I want to set up the matrix in the order of
	//unknowns first then the Dirichlet boundary nodes
	pMat->numNonZeros = 0 ;
	for( i=0 ; i < fGrid->numEdges ; i++ ){
		//Get the current edge
		edge = &(fGrid->edges[ fGrid->mapU2G[i] ]) ;
		//The current edge knows which coarse grid edges it lies on
		//Check if the edge is not contained entirely on one coarse grid edge
		if( edge->parentEdges[0] != edge->parentEdges[1] ){
			pMat->rowStartInds[i+1] = 2 ;
			pMat->numNonZeros += 2 ;
		}
		else if( edge->parentEls[0] == edge->parentEls[1] ){
			//If the parent elements are the same it means that the edge is
			//contained only in a single element (i.e. it is on a boundary)
			//Count an entry in the matrix for each edge on the element
			pMat->rowStartInds[i+1] = 3 ;
			pMat->numNonZeros += 3 ;
		}
		else{
			//The edge is incident to two elements. We count a contribution from
			//each unique edge on two elements
			pMat->rowStartInds[i+1] = 5 ;
			pMat->numNonZeros += 5 ;
		}
	}
	
	//Make sure that the array of row start indices is set up properly
	for( i=1 ; i < fGrid->numEdges+1 ; i++ )
		pMat->rowStartInds[i] += pMat->rowStartInds[i-1] ;
		
	assert( pMat->rowStartInds[fGrid->numEdges] == pMat->numNonZeros ) ;
		
	//Set the memory for the global entries and for the column numbers
	pMat->globalEntry = (double*)malloc( pMat->numNonZeros *
		sizeof( double ) ) ;
	pMat->colNums = (unsigned int*)malloc( pMat->numNonZeros *
		sizeof( unsigned int ) ) ;
		
	//Loop through the edges again
	for( i=0 ; i < fGrid->numEdges ; i++ ){
		//Get the current edge
		edge = &(fGrid->edges[ fGrid->mapU2G[i] ]) ;
		//If the edge has end points on two different coarse grid edges add the
		//coarse grid edges to the entry of columns
		if( edge->parentEdges[0] != edge->parentEdges[1] ){
			//Set the column numbers in the appropriate row. We are not
			//interested in the order of these
			for( j=0 ; j < 2 ; j++ ){
				//Set the global entry
				pMat->globalEntry[ pMat->rowStartInds[i] ] = 0.5 ;
				pMat->colNums[ pMat->rowStartInds[i]++ ] =
					edge->parentEdges[j]->id ;
			}
		}
		else{
			//The fine grid edge is on a coarse grid edge, but we do not know
			//which half of the coarse grid edge this is. To find this out we
			//check if the starting points of the edges coincide, but we have to
			//do this on each element on which the edge is incident
			//Add the parent edge value, which is one
			numEls = (edge->parentEdges[0]->parentEls[0] ==
				edge->parentEdges[0]->parentEls[1]) ? 1: 2 ;
			pMat->globalEntry[ pMat->rowStartInds[i] ] = 1.0 ;
			pMat->colNums[ pMat->rowStartInds[i]++ ] =
				edge->parentEdges[0]->id ;
			//Loop over the elements on which the edge exists
			for( j=0 ; j < numEls ; j++ ){
				el = edge->parentEdges[0]->parentEls[j] ;
				//Get the index of the edge on the current element
				edgeInd = get_edge_el_index( edge->parentEdges[0], el ) ;
				//Check the orientation of the edge on the element, and check if
				//the start or end nodes are the same for the coarse and fine
				//edges, and set the appropriate values in the global entry and
				//the column numbers
				endInd = (int)el->edgeOrient[ edgeInd ] ;
				fact = nodes_eq( edge->endPoints[ endInd ],
					edge->parentEdges[0]->endPoints[ endInd ]) ? 1 : -1 ;
				//Add the first neighbouring edge
				pMat->globalEntry[ pMat->rowStartInds[i] ] = fact * 0.25 ;
				pMat->colNums[ pMat->rowStartInds[i]++ ] =
					el->edges[(edgeInd+1)%3]->id ;
				//Add the second neighbouring edge
				pMat->globalEntry[ pMat->rowStartInds[i] ] = -fact * 0.25 ;
				pMat->colNums[ pMat->rowStartInds[i]++ ] =
					el->edges[(edgeInd+2)%3]->id ;
			}
		}
	}
	
	assert( pMat->rowStartInds[ fGrid->numEdges - 1 ] ==
		pMat->numNonZeros ) ;
	
	//We reset the row start indices
	for( i=fGrid->numEdges ; i > 0 ; i-- )
		pMat->rowStartInds[i] = pMat->rowStartInds[i-1] ;
	pMat->rowStartInds[0] = 0 ;
}

/** Sets the restriction matrix on the fine grid and the prolongation matrix
 *	on the coarse grid. The prolongation matrix is the transpose of the
 *	restriction matrix
 *	\param [in] fGrid	The fine grid on which to calculate the restriction
 *						operator
 *	\param [in] cGrid	The coarse grid on which to calculate the prolongation
 *						operator
 *	\param [out] fInterp	The interpolation operators on the fine grid. The
 *							member variable Interpolation::r is populated in
 *							this method
 *	\param [out] cInterp	The inerpolation operators on the coarse grid. The
 *							member variable Interpolation::p is populated in
 *							this method
 */
void set_restrict_prolong_mat_on_grid( const Grid *fGrid, const Grid *cGrid,
	Interpolation *fInterp, Interpolation *cInterp )
{
	//Check that the memory has not been set for the restriction operator
	assert( fInterp->scaleFacts == NULL ) ;
	
	//First set the prolongation operator on the coarse grid
	set_prolong_mat_on_grid( fGrid, cGrid, cInterp ) ;
	
	//Now set the restriction matrix on the fine grid to be the transpose of the
	//prolongation matrix on the coarse grid
	sparse_matrix_transpose( &(cInterp->p), &(fInterp->r), true ) ;
	//Now remove the rows in the interpolation and restriction operators that
	//give the values for the Dirichlet boundary nodes
/*	delete_sparse_matrix_last_n_rows( &(cInterp->p),*/
/*		fGrid->numEdges - fGrid->numUnknowns ) ;*/
/*	delete_sparse_matrix_last_n_rows( &(fInterp->r),*/
/*		cGrid->numEdges - cGrid->numUnknowns ) ;*/
/*		*/
	//The restriction needs to be scaled to reflect the fact that the
	//integration is performed on elements a quarter of the size on the fine
	//grid
	fInterp->scaleFacts = (double*)calloc( fInterp->r.numRows,
		sizeof( double ) ) ;
	set_restrict_scale_facts( &(fInterp->r), fInterp->scaleFacts ) ;
	scale_restrict_mat( fInterp->scaleFacts, &(fInterp->r) ) ;
	free( fInterp->scaleFacts ) ;
	fInterp->scaleFacts = NULL ;
}

/** Set the restriction and prolongation matrices between the given fine and
 *	coarse grids. These restriction and prolongation operators are to be used in
 *	an FAS iteration in which the Dirichlet boundary nodes need to be included
 *	in the columns
 *	\param [in] fGrid	The fine grid from which to restrict
 *	\param [in] cGrid	The coarse grid from which to prolong
 *	\param [out] fInterp	Interpolation operators on the fine grid
 *	\param [out] cInterp	Interpolation operators on the coarse grid
 */
void set_fas_restrict_prolong_mat_on_grid( const Grid *fGrid, const Grid *cGrid,
	Interpolation *fInterp, Interpolation *cInterp )
{
	//First set up the prolongation matrix on coarse grid
	set_prolong_mat_on_full_grid( fGrid, cGrid, cInterp ) ;
	
	//Now set the restriction matrix on the fine grid to be the transpose of the
	//prolongation matrix on the coarse grid
	sparse_matrix_transpose( &(cInterp->p), &(fInterp->r), true ) ;
	//Now remove the rows in the interpolation and restriction operators that
	//give the values for the Dirichlet boundary nodes
	delete_sparse_matrix_last_n_rows( &(cInterp->p),
		fGrid->numEdges - fGrid->numUnknowns ) ;
	delete_sparse_matrix_last_n_rows( &(fInterp->r),
		cGrid->numEdges - cGrid->numUnknowns ) ;
		
	//Set the scale factors for the restriction operator
	fInterp->scaleFacts = (double*)calloc( fInterp->r.numRows,
		sizeof( double ) ) ;
	set_restrict_scale_facts( &(fInterp->r), fInterp->scaleFacts) ;
	scale_restrict_mat( fInterp->scaleFacts, &(fInterp->r) ) ;
}

/** Sets the scaling factor for the rows in the restriction matrix. These have
 *	the effect of normalising the entries in a row of the restriction matrix,
 *	such that they add up to one
 *	\param [in]	rMat	The matrix representation of the restriction operator
 *	\param [out] scaleFacts	The scaling factors to use, which will be the row
 *							sums in the matrix passed in
 */
void set_restrict_scale_facts( const SparseMatrix *rMat, double* scaleFacts )
{
	unsigned int i, j ; //Loop counters
	
	//Loop through the rows of the matrix
	for( i=0 ; i < rMat->numRows ; i++ ){
		//Set the scaling factor to zero
		scaleFacts[i] = 0.0 ;
		//Loop through the non-zero columns in the current row
		for( j=rMat->rowStartInds[i] ; j < rMat->rowStartInds[i+1] ; j++ ){
			//Update the scaling factor with the entry in the matrix
			scaleFacts[i] += rMat->globalEntry[j] ;
		}
	}
}

/** Scales the rows in the restriction matrix passed in using the corresponding
 *	entry in the vector of scaling factors passed in
 *	\param [in] scaleFacts	Vector of scaling factors for the rows in the
 *							restriction matrix
 *	\param [out] rMat	The restriction matrix to apply the scaling factors to
 */
void scale_restrict_mat( const double *scaleFacts, SparseMatrix *rMat )
{
	unsigned int i, j ; //Loop counters
	double fact ; //A scaling factor

	//Loop through the rows of the matrix
	for( i = 0 ; i < rMat->numRows ; i++ ){
		//Get the scaling factor to use
		fact = scaleFacts[i] ;
		//Loop through the columns of the matrix
		for( j=rMat->rowStartInds[i] ; j < rMat->rowStartInds[i+1] ; j++ ){
			//Scale each entry by the appropriate scaling factor
			rMat->globalEntry[j] /= fact ;
		}
	}
}

/** Restricts an approximation of pointwise values from a fine grid to a coarse
 *	grid
 *	\param [in] fInterp	Interpolation operators on the fine grid
 *	\param [in] fApprox	The approximation on the fine grid
 *	\param [out] cApprox	The approximation on the coarse grid. This is
 *							populated in this method
 */
void restrict_approx( const Interpolation *fInterp, const double *fApprox,
	double *cApprox )
{
	//All that we do in here is perform the sparse matrix-vector product of the
	//restriction matrix and the fine approximation
	sparse_matrix_vec_mult( &(fInterp->r), fApprox, cApprox ) ;
}

/** Prolongates an approximation of pointwise values from a coarse grid to a
 *	fine grid
 *	\param [in] cInterp	Interpolation operators on the coarse grid
 *	\param [in] cApprox	The approximation on the coarse grid
 *	\param [out] fApprox	The approximation on the fine grid. This is
 *							populated in this method
 */
void prolong_approx( const Interpolation * cInterp,
	const double * cApprox, double * fApprox )
{
	//All that we do here is perform the sparse matrix-vector product of the
	//prolongation matrix and the coarse grid approximation
	sparse_matrix_vec_mult( &(cInterp->p), cApprox, fApprox ) ;
}

/** Restricts a residual, which is an integral over edges, from a fine to a
 *	coarse grid
 *	\param [in] fInterp	Interpolation operators on the fine grid
 *	\param [in] fResid	Residual on the fine grid
 *	\param [out] cResid	Residual on the coarse grid. This is populated in this
 *						method
 */
void restrict_resid( const Interpolation *fInterp, const double *fResid,
	double *cResid )
{
	//First perform the normal restriction
	sparse_matrix_vec_mult( &(fInterp->r), fResid, cResid ) ;
}

/** Restricts a residual in the case of an FAS iteration. In this iteration we
 *	require the restricted residual to be weighted to be the correct right hand
 *	side for the nonlinear problem
 *	\param [in] fInterp	Interpolation operators on the fine grid
 *	\param [in] fResid	The residual on the fine grid
 *	\param [out] cResid	The residual on the coarse grid
 */
void restrict_fas_resid( const Interpolation *fInterp, const double *fResid,
	double *cResid )
{
	//Perform the normal restriction
	sparse_matrix_vec_mult( &(fInterp->r), fResid, cResid ) ;
	//Now scale the restricted value
	scale_restrict_approx_by_facts( fInterp->r.numRows, fInterp->scaleFacts,
		cResid ) ;
}

/** Scales each entry in the vector \p approx by the corresponding entry in the
 *	vector \p scaleFacts
 *	\param [in] length	The length of the vector to scale
 *	\param [in] scaleFacts	The scaling factors for each entry of the residual
 *	\param [out] resid	The residual to scale
 */
void scale_restrict_approx_by_facts( unsigned int length,
	const double *scaleFacts, double *approx )
{
	unsigned int i ; //Loop counter
	
	//For each entry in the residual vector scale by the factor stored in the
	//vector of scaling factors
	for( i=0 ; i < length ; i++ )
		approx[i] /= scaleFacts[i] ;
}

/** Sets up a hierarchy of interpolation operators between the grids in a
 *	hierarchy of grids
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] gridHierarchy	The hierarchy of grids on which to set up the
 *								interpolation operators. Note that this does not
 *								change, but the const keyword causes problems in
 *								C so that this is left out in this case
 *	\return	The hierarchy of interpolation operators
 */ 
Interpolation** setup_interp_hierarchy( const Params *params,
	Grid **gridHierarchy )
{
	unsigned int i ; //Loop counter
	Interpolation **hierarchy ; //The hierarchy of interpolation operators to
								//create
								
	//Make sure that the grids have been initialised properly
	assert( gridHierarchy != NULL ) ;
	
	//Set the memory necessary for the pointers
	hierarchy = (Interpolation**)calloc( params->fGridLevel+1,
		sizeof( Interpolation* ) ) ;
		
	//Set the unassigned items in the hierarchy to NULL
	for( i=0 ; i < params->cGridLevel ; i++ )
		hierarchy[i] = NULL ;
		
	//For the assigned grids in the hierarchy assign the appropriate memory
	//for the interpolation object
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		hierarchy[i] = (Interpolation*)calloc( 1, sizeof( Interpolation ) ) ;
		set_interp_ops_to_null( hierarchy[i] ) ;
	}
		
	//Now set the interpolation operators
	for( i=params->fGridLevel ; i > params->cGridLevel ; i-- )
		set_restrict_prolong_mat_on_grid( gridHierarchy[i], gridHierarchy[i-1],
			hierarchy[i], hierarchy[i-1] ) ;
		
	//Return the hierarchy that was set up
	return hierarchy ;
}

/** Sets up a hierarchy of interpolation operators between the grids in a
 *	hierarchy of grids. This sets up the operators to take into account
 *	Dirichlet boundary information
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] gridHierarchy	The hierarchy of grids on which to set up the
 *								interpolation operators
 *	\return	The hierarchy of interpolation operators suitable to be used in an
 *			FAS iteration
 */
Interpolation** setup_fas_interp_hierarchy( const Params *params,
	Grid **gridHierarchy )
{
	unsigned int i ; //Loop counter
	Interpolation **hierarchy ; //The hierarchy of interpolation operators to
								//create
								
	//Make sure that the grids have been initialised properly
	assert( gridHierarchy != NULL ) ;
	
	//Set the memory necessary for the pointers
	hierarchy = (Interpolation**)calloc( params->fGridLevel+1,
		sizeof( Interpolation* ) ) ;
		
	//Set the unassigned items in the hierarchy to NULL
	for( i=0 ; i < params->cGridLevel ; i++ )
		hierarchy[i] = NULL ;
		
	//For the assigned grids in the hierarchy assign the appropriate memory
	//for the interpolation object
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		hierarchy[i] = (Interpolation*)calloc( 1, sizeof( Interpolation ) ) ;
		set_interp_ops_to_null( hierarchy[i] ) ;
	}
		
	//Now set the interpolation operators
	for( i=params->fGridLevel ; i > params->cGridLevel ; i-- )
		set_fas_restrict_prolong_mat_on_grid( gridHierarchy[i],
			gridHierarchy[i-1], hierarchy[i], hierarchy[i-1] ) ;
		
	//Return the hierarchy that was set up
	return hierarchy ;
}

/** Frees the memory assigned on the heap for the hierarchy of interpolation
 *	operators passed in
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] hierarchy	The hierarchy of interpolation operators to free the
 *							memory for
 *	\return	A NULL pointer, as all of the memory will have been unassigned
 */
Interpolation** free_interp_hierarchy( const Params *params,
	Interpolation **hierarchy )
{
	unsigned int i ; //Loop counter
	
	//Check that the hierarchy is not NULL. Otherwise we are done
	if( hierarchy == NULL ) return NULL ;
	
	//Loop through the assigned interpolation structures
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		if( hierarchy[i] == NULL ) continue ;
		//Free the memory assigned in the interpolation
		free_interp_ops( hierarchy[i] ) ;
		//Free the single pointer
		free( hierarchy[i] ) ;
		hierarchy[i] = NULL ;
	}
	
	//Now free the memory assigned to the outer pointer
	free( hierarchy ) ;
	hierarchy = NULL ;
	return NULL ;
}




















