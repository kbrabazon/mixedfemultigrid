#ifndef _FAS_ITERATION_H
#define _FAS_ITERATION_H
/** \file fasIteration.h
 *	\brief Defines functions to be used in an FAS cycle to solve the mixed
 *	finite element formulation of the Richards equation
 */

#include "definitions.h"

//Initialises the pointers i nthe context to be NULL
void set_fas_ctxt_to_null( FASCtxt *ctxt ) ;

//Frees the memory assigned on the heap for the given structure
void free_fas_ctxt( const Params *params, FASCtxt *ctxt ) ;

//Sets the memory required to perform an FAS iteration for the Richards equation
//on a hierarchy of grids
void set_up_fas_ctxt( const Params *params, const REData *data,
	FASCtxt *ctxt ) ;
	
//Performs a nonlinear smoothing iteration the specified number of times
void nonlin_smooth( const Params *params, const REData *data, const Grid *grid,
	unsigned int numSmooths, double *jacDiags, REApproxVars *vars ) ;
	
//Performs a nonlinear Jacobi iteration the specified number of times
void nonlin_jacobi( const Params *params, const REData *data, const Grid *grid,
	unsigned int numSmooths, double *jacDiags, REApproxVars *vars ) ;
	
//Performs a nonlinear smoothing iteration until convergence. It is assumed that
//the pressure, residual and the Jacobian diagonals are up-to-date when this
//method is called
void nonlin_smooth_to_converge( const Params *params, const REData *data,
	const Grid *grid, double *jacDiags, REApproxVars *vars ) ;
	
//Performs an FAS multigrid iteration for the Richards equation
void fas_multigrid_iteration( Params *params, const REData *data,
	unsigned int currLevel, FASCtxt *ctxt ) ;

#endif
