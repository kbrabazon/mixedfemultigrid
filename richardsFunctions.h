#ifndef _RICHARDS_FUNCTIONS_H
#define _RICHARDS_FUNCTIONS_H

/** \file richardsFunctions.h
 *	\brief Defines functions related to the approximation of a Richards equation
	using a mixed finite element formulation
 */

#include "definitions.h"

//Set all variables to some NULL state initially
void set_re_vars_to_null( REApproxVars *vars ) ;

//Free the memory on the heap
void free_re_vars( REApproxVars *vars ) ;

//Set the data related to the soil properties to NULL initially
void set_re_data_to_null( REData *data ) ;

//Assigns the appropriate amount of memory for the soil data
void assign_re_data_mem( REData *data ) ;

//Initialises the variables associated with the approximation of the mixed
//formulation of the Richards equation on the current grid
void setup_re_vars_on_grid( const Params *params, const Grid *grid,
	REApproxVars *vars ) ;

//Sets data that is dependent on parameters read in for the soil characteristics
void set_re_dependent_data( REData *data ) ;

//Writes the contents of the data structure to console. Used for debugging
void write_re_data_to_console( REData *data ) ;

//Calculates the fictitious value theta_{m} used in the modified
//van Genuchten-Mualem model of Vogel et al
double calc_theta_m( const REData *data, int soilID ) ;

//Free the memory on the heap
void free_re_data( REData *data ) ;

//Calculates the saturation given the pressure
double calc_theta( const REData *data, int soilID, double pressure ) ;

//Calculates the derivative of the saturation given a pressure
double calc_theta_deriv( const REData *data, int soilID, double pressure ) ;

//Functions used to calculate the hydraulic conductivity given a volumetric
//wetness
double calc_hydr_cond( const REData *data, int soilID, double theta ) ;
double hydr_cond_sub_func( const REData *data, int soilID, double effSat,
	double power ) ;
	
//Calculates the derivative of the hydraulic conductivity at the given pressure
double calc_hydr_cond_deriv( const REData *data, int soilID, double pressure ) ;
	
//Simultaneously calculates and sets the hydraulic conductivity and the
//volumetric wetness
void calc_hydr_cond_vol_wet( const REData *data, int soilID,
	double pressure, double *hydrCond, double *volWet ) ;
	
//Simultaneously calculates and sets the hydraulic conductivity, the volumetric
//wetness and the derivatives of those functions
void calc_hydr_cond_vol_wet_deriv( const REData *data, int soilID,
	double pressure, double *hydrCond, double *hydrCondD, double *volWet,
	double *volWetD ) ;
	

//Calculates the value of a row sum of the local matrix on element el which
//gives the transformation from the flux to the pressure and Lagrange
//multipliers
double el_inv_mat_row_sum( const Element *el ) ;

//Calculates the value of the function of the pressure on the given element, the
//inverse of which will give the sum of the Lagrange multipliers over the edges
double el_press_func( const Params *params, const REData *data, double pressure,
	double prevPressure, const Element *el ) ;
	
	
//Calculates the derivative of the function, the inverse of which gives the
//pressure on the current element
double el_press_func_deriv( const Params *params, const REData *data,
	double pressure, double prevPressure, const Element *el ) ;

//Calculates the derivative of the local function defining the relationship
//between the pressure and Lagrange multipliers given that part of the data has
//already been calculated	
double el_press_func_deriv_from_data( const Params *params,
	const REData *data, const double hCond, const double hCondDeriv,
	const double volWet, const double volWetDeriv, const double prevPress,
	const Element *el ) ;
	
//Calculates the value and the derivative of the function, the inverse of which
//gives the pressure on the current element
void el_press_func_and_deriv( const Params *params, const REData *data,
	double pressure, double prevPressure, const Element *el, double *func,
	double *funcDeriv ) ;
	
//Returns a constant used in the inverse of a local matrix on each element
double local_mat_inv_const( const Element *el, const double *edgeMagSquare ) ;
	
//Recovers the flux from the value of the Lagrange multipliers and the pressure
//stored in the structure 'vars'
void recover_flux( const Params *params, const REData *data, const Grid *grid,
	const REApproxVars *vars, double *flux ) ;

//Recovers the flux, assuming that the pressure and the Lagrange multipliers
//have been calculated correctly
void recover_flux_invert_loc_system( const Params *params, const REData *data,
	const Grid *grid, const REApproxVars *vars, double *flux ) ;

//Solves the system of equations for the flux on the given element. This gives
//an approximation to the flux on an edge for the current element only, not
//globally
void solve_local_flux_system( const REData *data, const Element *el,
	unsigned int elInd, const REApproxVars *vars, double *localFlux ) ;
	
//Recovers the pressure on each element using the Lagrange multipliers on the
//edges of the given grid
void recover_pressure( const Params *params, const REData *data,
	const Grid *grid, REApproxVars *vars ) ;
	
//Sets the Dirichlet boundary information for the Lagrange multipliers in the
//mixed finite element approximation of the Richards equation
void set_lagrange_mult_bnd_data( const Params *params, const Grid *grid,
	double *lM ) ;
	
//Calculates the inverse of the local stiffness matrix on the element passed in
void calc_loc_mat_inv( const Element *el, double *locMat ) ;

//Calculates the residual in the Richards equation, assuming that the pressure
//on each element has been recovered before calling this function
void calc_richards_resid( const Params *params, const Grid *grid,
	const REData *data, REApproxVars *vars ) ;
	
//Calculates the Jacobian matrix for the Richards equation
void calc_richards_jacobian( const Params *params, const Grid *grid,
	const REData *data, const REApproxVars *vars, SparseMatrix *jacobian ) ;
	
//Calculates the symmetric part of the Jacobian matrix for the Richards equation
void calc_richards_jacobian_symm_part( const Params *params,
	const Grid *grid, const REData *data,
	const REApproxVars *vars, SparseMatrix *jacobian ) ;
	
//Calcualtes the Jacobian for the Richards equation using numerical
//differentiation. This is *much* slower than using the explicit form of the
//derivative, but is useful for testing
void calc_richards_numeric_jacobian( const Params *params, const Grid *grid,
	const REData *data, REApproxVars *vars, SparseMatrix *jacobian ) ;
	
//Calculates the diagonals of the Jacobian matrix for  the Richards equation. It
//is assumed that the pressure has been recovered on the elements for the
//current approximation such that this does not need to be recalculated
void calc_richards_jacobian_diags( const Params *params, const Grid *grid,
	const REData *data, const REApproxVars *vars, double *jacDiags ) ;
	
//Calculates the residual and the diagonals of the Jacobian matrix for the
//Richards equation at the same time to save on computational expense
void calc_richards_resid_and_jacobian_diags( const Params *params,
	const Grid *grid, const REData *data, REApproxVars *vars,
	double *jacDiags ) ;
	
//Calculates the residual and the Jacobian for the Richards equation at the same
//time to save on computational expense
void calc_richards_resid_and_jacobian( const Params *params, const Grid *grid,
	const REData *data, REApproxVars *vars, SparseMatrix *jacobian ) ;
	
//Sets an initial approximation on a grid. It is assumed that the values of the
//pressure are constant on all of the domain, including the Dirichlet boundary.
//This then models the case that the Dirichlet boundary conditions are
//instantaneously imposed
void set_richards_initial_data( const Params *params, const REData *data,
	const Grid *grid, REApproxVars *vars ) ;
	
//Sets the previous value of the volumetric wetness under the assumption that
//the pressure stored in the REApproxVars structure is up to date
void set_richards_prev_theta( const Grid *grid, const REData *data,
	REApproxVars *vars ) ;
	
//Updates the approximation to the Richards equation using the correction
//calculated in the linear Newton system
void update_re_approx_from_newt_correct( const Params *params, const Grid *grid,
	const LinApproxVars *linVars, REApproxVars *reVars ) ;
	
//Updates the approximation to the Richards equation using the correction
//calculated in and FAS iteration
void update_re_approx_from_fas_correct( const Grid *grid, const double *correct,
	double *lM ) ;
	
//Sets the initial approximation of the Lagrange multipliers for the Richards
//equation, depending on the parameters passed in
void set_richards_init_lagrange_mult( const Params *params, const REData *data,
	const Grid *grid, REApproxVars *vars ) ;
	
//Sets the initial pressure profile for the Richards equation using the Lagrange
//multipliers. It is assumed that the Lagrange multipliers have already been
//initialised
void set_richards_init_pressure( const Params *params, const REData *data,
	const Grid *grid, REApproxVars *vars ) ;
	
//Sets the values of the Lagrange multipliers to be in hydrostatic equilibrium.
//This assumes that the grid is rectangular, and that the top of the grid is at
//elevation zero
void set_lagrange_mult_hydrostatic_eq( const Grid *grid, REApproxVars *vars,
	double offset ) ;
	
//Sets a constant value for the Lagrange multipliers on the unknown edges of a
//grid
void set_lagrange_mult_const( const Grid *grid, REApproxVars *vars,
	double constVal ) ;
	
//Initialises the hierarchy of variables required to approximate the Richards
//equation on each grid in a hierarchy of grids. This does not set up the values
//but sets the memory required on the heap
REApproxVars** setup_re_vars_hierarchy( const Params *params,
	Grid** gridHierarchy ) ;
	
//Frees the memory assigned on the heap for the variables required to solve the
//mixed finite element formulation of the Richards equation on a hierarchy of
//grids
REApproxVars** free_re_vars_hierarchy( const Params *params,
	REApproxVars **reHierarchy ) ;
	
//Sets up the perturbed right hand side for an FAS iteration for the Richards
//equation. The steps to take are to restrict the approximation, the previous
//volumetric wetness, the current pressure and the current residual, and then to
//apply the nonlinear operator on the coarse grid
void calc_richards_fas_coarse_rhs( Params *params, const REData *data,
	const Interpolation *fInterp, const Grid *fGrid, const Grid *cGrid,
	const REApproxVars *fVars, REApproxVars *cVars ) ;

#endif















