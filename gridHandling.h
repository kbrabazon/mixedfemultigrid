#ifndef _GRID_HANDLING_H
#define _GRID_HANDLING_H

#include "definitions.h"
/** \file gridHandling.h
 *	\brief Functions associated with the set-up, maintenance and destruction of
	Grid structures
 */

//Reads the grid in from file
void read_grid_from_file( const char *fname, Grid *grid ) ;

//Initialises pointers in the grid structure to NULL
void set_grid_to_null( Grid *grid ) ;

//Frees the memory assigned on the heap for the grid structure
void free_grid( Grid *grid ) ;

//Calculates the magnitude of an edge
double calc_edge_mag( const Edge *edge ) ;

//Calculates the area of an element
double calc_el_area( const Element *el ) ;

//Calculates the mid-point of an edge
void get_edge_mid_point( const Edge *edge, double *midPoint ) ;

//Returns the z-coordinate of the node opposite the edge indentified by the 
//index 'eInd'
double get_z_op( const Element *el, const unsigned int eInd ) ;

//Sets the parents of the fine grid edges on the coarse grid
void set_edge_parents( const Grid *cGrid, Grid *fGrid ) ;

//Returns the local index of a given edge on an element under the assumption
//that the edge is on the element
unsigned int get_edge_el_index( const Edge *edge, const Element *el ) ;
	
//Returns the normal of the edge given, disregarding the orientation on an
//element
void get_edge_normal( const Edge *edge, Vec2D *normal ) ;
	
//Checks for equality of two different nodes. These are considered equal if 
//their coordinates are the same
bool nodes_eq( const Node *node1, const Node *node2 ) ;

//Gets the coordinates of a bounding box of the given element. The coordinates
//are given in the form [left, bottom, right, top]
void get_el_bnd_box( const Element *el, BndBox *bnd ) ;

//Sets the soils up on the grid. Each element is assigned a single soil type at
//the moment. This can cause problems if a grid does not resolve the geometry
//of the soils
void set_soil_type( const Params *params, const REData *data, Grid *grid ) ;

//Returns the list of nodes on the element, starting with the starting point of
//the first edge, going round in an anti-clockwise order
void get_el_nodes( const Element *el, const Node **nodes ) ;

//Calculates the centre of gravity ofa triangular element, which is simply an
//average of the nodal coordinates
void el_center_of_gravity( const Element *el, double *cog ) ;

//Prints a grid to the file spcified by the filename with the header that is
//given
void write_grid_paraview_format( const char *fname,
	const char *header, const Grid *grid ) ;
	
//Writes the mid-points of the edges to a file in vtk format
void write_grid_edge_mids_paraview_format( const char *fname,
	const char *header, const Grid *grid ) ;
	
//Writes a grid in such a format as to allow discontinuous data on elements
//to be represented
void write_grid_for_discont_func_paraview_format( const char *fname,
	const char *header, const Grid *grid ) ;
	
//Writes the grid edges to file in a format that can be read by paraview
void write_grid_edge_struct_paraview_format( const char *fname,
	const char *header, const Grid *grid ) ;
	
//Writes the grid edges which do not lie on the Dirichlet boundary to file in a
//format that can be read by paraview
void write_grid_interior_edge_struct_paraview_format( const char *fname,
	const char *header, const Grid *grid ) ;
	
//Prints a function which is piecewise constant on elements of a grid to file
void write_element_func_paraview_format( const char *fname, const Grid *grid,
	const double *elFunc ) ;
	
//Writes a function which is piecewise constant on the edges of a grid to file
void write_edge_func_paraview_format( const char *fname, const Grid *grid,
	const double *edFunc ) ;

//Writes a function which is piecewise constant on the non-Dirichlet edges of a
//grid to file. The function is extended by zero onto the Dirichlet boundary
void write_interior_edge_func_paraview_format( const char *fname,
	const Grid *grid, const double *edFunc ) ;
	
//Writes a function of vectors representing a flux over the edges of a grid to
//file in a format that can be read by paraview
void write_edge_vec_func_paraview_format( const char *fname, const Grid *grid,
	const Vec2D *edNormFunc ) ;
	
//Writes the normal components of the fluxes over the edges of a grid to file in
//a format readable by paraview. The vector passed in is the magnitude of the
//normal component of the flux over an edge
void write_edge_flux_func_paraview_format( const char *fname, const Grid *grid,
	const double *fluxMag ) ;
	
//Gets the vector representation of the edge. This does not take into account
//the orientation of the edge on an element
void get_edge_vec_rep( const Edge *edge, double *edgeVec ) ;

//Gets the normal component of a flux over the edge
void get_vec_edge_normal_comp( const Edge *edge, const Vec2D *flux,
	Vec2D *normFlux ) ;
	
//Indicates whether the given element is incident to a Dirichlet boundary or not
bool is_dir_bnd_el( const Element *el ) ;

//Reads a hiearchy of grids from file and saves them in an array of pointers
Grid** read_grid_hierarchy( const Params *params, const REData *data ) ;
	
//Frees the memory assigned on the heap for the hierarchy of grids passed in
Grid** free_grid_hierarchy( const Params *params, Grid **gridHierarchy ) ;
	
//Writes information about the grid to console. This is useful for debugging
//purposes
void write_grid_info_to_console( const char *heading, const Grid *grid ) ;
	
//Writes information about the element to console. This is useful for debugging
//purposes
void write_el_info_to_console( const char *heading, const Element *el ) ;
	
//Writes the entries in the given vector to file
void write_dvec_to_file( const char *fname, unsigned int length,
	const double *dvec ) ;
	
//Reads data from file and saves them in the given vector. It is assumed that
//both the file and the vector have at least 'length' entries in them, and that
//all of the data in the file are in a double format
void read_dvec_from_file( const char *fname, unsigned int length,
	double *dvec ) ;
	
//Calculates a factor required in the calculation of the residual and the
//Jacobian of the mixed finite element formulation of the Richards equation
double calc_zfact( const Element *el, unsigned int edgeInd ) ;

//Writes information about the nodes of the element to console. This is useful
//for debugging purposes
void write_el_nodes_to_console( const Element *el ) ;

//Performs restriction of a function that is piecewise constant on a grid
void restrict_const_el_func( const Grid *fGrid, const Grid *cGrid,
	const double *fFunc, double *cFunc ) ;

#endif






