#include "fasIteration.h"
#include "gridHandling.h"
#include "interpolation.h"
#include "richardsFunctions.h"
#include "sparseMatrix.h"

/** Initialises the pointers in the context to be NULL
 *	\param [out] ctxt	The context for which the pointers are to be set to
 *						NULL
 */
void set_fas_ctxt_to_null( FASCtxt *ctxt )
{
	ctxt->gridHierarchy = NULL ;
	ctxt->interpHierarchy = NULL ;
	ctxt->reHierarchy = NULL ;
	ctxt->jacDiags = NULL ;
	ctxt->initLM = NULL ;
}

/** Frees the memory assigned on the heap for the given structure
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] ctxt	The context for which to free the memory assigned on the
 *						heap
 */
void free_fas_ctxt( const Params *params, FASCtxt *ctxt )
{
	unsigned int i ; //Loop counter
	if( ctxt->gridHierarchy != NULL ){
		ctxt->gridHierarchy = free_grid_hierarchy( params,
			ctxt->gridHierarchy ) ;
	}
	if( ctxt->interpHierarchy != NULL ){
		ctxt->interpHierarchy = free_interp_hierarchy( params,
			ctxt->interpHierarchy ) ;
	}
	if( ctxt->reHierarchy != NULL ){
		ctxt->reHierarchy = free_re_vars_hierarchy( params,
			ctxt->reHierarchy ) ;
	}
	if( ctxt->jacDiags != NULL ){
		free( ctxt->jacDiags ) ;
		ctxt->jacDiags = NULL ;
	}
	if( ctxt->initLM != NULL ){
		for( i=params->cGridLevel ; i < params->fGridLevel ; i++ ){
			if( ctxt->initLM[i] != NULL ){
				free( ctxt->initLM[i] ) ;
				ctxt->initLM[i] = NULL ;
			}
		}
		free( ctxt->initLM ) ;
		ctxt->initLM = NULL ;
	}
}

/** Sets the memory required to perform an FAS iteration for the Richards
 *	equation on a hierarchy of grids
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [out] ctxt	Context containing variables required to perform an FAS
 *						iteration for the Richards equation on a hierarchy of
 *						grids
 */
void set_up_fas_ctxt( const Params *params, const REData *data, FASCtxt *ctxt )
{
	unsigned int i ; //Loop counter
	//Set up the memory on the grids
	//Read in the grid hierarchy
	ctxt->gridHierarchy = read_grid_hierarchy( params, data ) ;
	//Set up the hierarchy of variables for the Richards equation
	ctxt->reHierarchy = setup_re_vars_hierarchy( params,
		ctxt->gridHierarchy ) ;
	//Set up the hierarchy of interpolation operators
	ctxt->interpHierarchy = setup_fas_interp_hierarchy( params,
		ctxt->gridHierarchy ) ;
		
	//Set up the variable to store the diagonals of the Jacobian matrix on the
	//fine grid (although this will be used on all grids)
	ctxt->jacDiags = (double*)calloc(
		ctxt->gridHierarchy[ params->fGridLevel ]->numUnknowns,
		sizeof( double ) ) ;
		
	//Set the memory to store the initial approximation on the coarse grid so 
	//that the appropriate correction term can be calculated. This does not
	//require an array on the fine grid
	ctxt->initLM = (double**)calloc( params->fGridLevel+1, sizeof( double* ) ) ;
	for( i=0 ; i < params->cGridLevel ; i++ )
		ctxt->initLM[i] = NULL ;
	for( i=params->cGridLevel ; i < params->fGridLevel ; i++ ){
		ctxt->initLM[i] = (double*)calloc( ctxt->gridHierarchy[i]->numEdges,
			sizeof( double ) ) ;
	}
}

/** Performs a nonlinear smoothing iteration the given number of times
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\parma [in] grid	The grid on which smoothing is to be performed
 *	\param [in] numSmooths	The number of smoothing iterations to perform
 * 	\param [in] jacDiags	Used to store the diagonals of the Jacobian matrix.
 *							It is only required to make sure that this pointer
 *							has enough memory assigned, as this is updated in
 *							this method
 *	\param [out] vars	Variables associated with the approximation of the
 *						Richards equation
 */
void nonlin_smooth( const Params *params, const REData *data, const Grid *grid,
	unsigned int numSmooths, double *jacDiags, REApproxVars *vars )
{
	//At the moment we only perform nonlinear Jacobi iterations
	nonlin_jacobi( params, data, grid, numSmooths, jacDiags, vars ) ;
}

/** Performs a nonlinear Jacobi iteration the specified number of times. It is
 *	assumed when the method is called that the pressure, residual and the
 *	Jacobian diagonals are up-to-date
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] grid	The grid on which the smoothing is to be performed
 *	\param [in] numSmooths	The number of smoothing iterations to perform
 *	\param [in] jacDiags	Used to store the diagonals of the Jacobian matrix.
 *							It is only required to make sure that this pointer
 *							has enough memory assigned, as this is updated in
 *							this method
 *	\param [out] vars	Variables associated with the approximation of the
 *						Richards equation
 */
void nonlin_jacobi( const Params *params, const REData *data, const Grid *grid,
	unsigned int numSmooths, double *jacDiags, REApproxVars *vars )
{
	unsigned int smoothCnt, i ; //Loop counters
	
	//For each unknown on the grid update the value
	for( i=0 ; i < grid->numUnknowns ; i++ )
		vars->lM[i] += vars->resid[i] / jacDiags[i] ;
	
	//Perform the specified number of smooths
	for( smoothCnt = 1 ; smoothCnt < numSmooths ; smoothCnt++ ){
		//Update the value of the pressure and the residual
		recover_pressure( params, data, grid, vars ) ;
		
		//Calculate the residual and the diagonals of the Jacobian matrix
		calc_richards_resid_and_jacobian_diags( params, grid, data, vars,
			jacDiags ) ;
			
		//For each unknown on the grid update the value
		for( i=0 ; i < grid->numUnknowns ; i++ )
			vars->lM[i] -= params->smoothWeight * vars->resid[i] / jacDiags[i] ;
	}
}

/** Performs a nonlinear smoothing iteration until convergence. It is assumed
 *	that the pressure, residual and the Jacobian diagonals are up-to-date when
 *	this method is called
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [in] grid	The grid on which smoothing is to be performed
 *	\param [in] jacDiags	Used to store the diagonals of teh Jacobian matrix.
 *							It is only required to make sure that this pointer
 *							has enough memory assigned as this is updated in
 *							this method
 *	\param [out] vars	Variables required for the solve of the Richards
 *						equation on the given grid
 */
void nonlin_smooth_to_converge( const Params *params, const REData *data,
	const Grid *grid, double *jacDiags, REApproxVars *vars )
{
	unsigned int smoothCnt ; //Loop counter
	double origResid, resid ; //The original and current norm of the residual
	double numSmooths = 3 ;
	
	//Calculate the original residual norm
	origResid = resid = discrete_l2_norm( grid->numUnknowns, vars->resid ) ;
	
	smoothCnt = 0 ;
	while( resid / origResid > NONLIN_TOL && resid > SOLVED_TOL &&
		smoothCnt < MAX_NONLIN_SMOOTHS ){
		//Perform a small number of smooths before recalculating the residual
		nonlin_smooth( params, data, grid, numSmooths, jacDiags, vars ) ;
		smoothCnt += numSmooths ;
		
		//Recover the pressure
		recover_pressure( params, data, grid, vars ) ;
		//Calculate the residual and the diagonals of the Jacobian matrix
		calc_richards_resid_and_jacobian_diags( params, grid, data, vars,
			jacDiags ) ;
		resid = discrete_l2_norm( grid->numUnknowns, vars->resid ) ;
	}
}

/** Performs an FAS multigrid iteration for the Richards equation
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] currLevel	The current grid level on which we are operating
 *	\param [in,out] ctxt	Variables required to perform an FAS iteration for
 *							the Richards equation
 */
void fas_multigrid_iteration( Params *params, const REData *data,
	unsigned int currLevel, FASCtxt *ctxt )
{
	unsigned int mgCnt ; //Loop counter
	const Grid *fGrid, *cGrid ; //The current fine and coarse grid
	REApproxVars *fVars, *cVars ; //Variables required to solve the Richards
				//equation on the fine and coarse grids
	const Interpolation *fInterp, *cInterp ; //Interpolation operators on the
				//current fine and coarse grids
	double *initLM ; //Stores the initial value of the Lagrange multipliers on
					 //the coarse grid
	double *coarseCorrection ; //Stores the correction on the coarse grid
	double *fineCorrection ; //The correction on the fine grid
				
	fGrid = ctxt->gridHierarchy[currLevel] ;
	fVars = ctxt->reHierarchy[currLevel] ;
	
	//It is assumed that the pressure is up-to-date and that the residual
	//and the diagonal of the Jacobian matrix have been calculated
	//Calculate the residual and the diagonals of the Jacobian matrix
//	calc_richards_resid_and_jacobian_diags( params, fGrid, data, fVars,
//		ctxt->jacDiags ) ;
	
	//If we are on the coarsest grid level perform smooths until convergence
	if( currLevel == params->cGridLevel ){
		//Solve the nonlinear system of equations
		nonlin_smooth_to_converge( params, data, fGrid, ctxt->jacDiags,
			fVars ) ;
		//Return from the method
		return ;
	}
	
	//We are not on the coarsest level, so get a handle to the coarse grid
	cGrid = ctxt->gridHierarchy[currLevel-1] ;
	cVars = ctxt->reHierarchy[currLevel-1] ;
	
	fInterp = ctxt->interpHierarchy[currLevel] ;
	cInterp = ctxt->interpHierarchy[currLevel-1] ;
	
	//Perform pre-smoothing
	nonlin_smooth( params, data, fGrid, params->preSmooth, ctxt->jacDiags,
		fVars ) ;
		
	//Recover the current value of the pressure
	recover_pressure( params, data, fGrid, fVars ) ;
	//Calculate the residual
	calc_richards_resid( params, fGrid, data, fVars ) ;
	
	//Calculate the appropriate right hand side for the coarse grid
	calc_richards_fas_coarse_rhs( params, data, fInterp, fGrid, cGrid, fVars,
		cVars ) ;
		
	//Calculate the residual and the Jacobian diagonals on the coarse grid
	//ready to be used
	calc_richards_resid_and_jacobian_diags( params, cGrid, data, cVars,
		ctxt->jacDiags ) ;
		
	//Store the initial approximation to the coarse grid problem
	initLM = ctxt->initLM[currLevel-1] ;
	copy_dvec( cGrid->numUnknowns, cVars->lM, initLM ) ;
		
	//The approximation was restricted in the calculation of the right hand side
	//so we now perform a multigrid iteration
	for( mgCnt=0 ; mgCnt < params->numMGCycles ; mgCnt++ )
		fas_multigrid_iteration( params, data, currLevel-1, ctxt ) ;
		
	//Calculate the correction on the coarse grid
	coarseCorrection = ctxt->initLM[currLevel-1] ;
	dvec_diff( cGrid->numUnknowns, cVars->lM, initLM, coarseCorrection ) ;
	
	//Prolong the correction to the fine grid
	assert( params->sparePtr != NULL ) ;
	fineCorrection = params->sparePtr ;
	prolong_approx( cInterp, coarseCorrection, fineCorrection ) ;
	
	//Update the noninear approximation on the fine grid
	update_re_approx_from_fas_correct( fGrid, fineCorrection, fVars->lM ) ;
	
	//Recover the pressure
	recover_pressure( params, data, fGrid, fVars ) ;
	//Calculate the residual and the diagonals of the Jacobian matrix
	calc_richards_resid_and_jacobian_diags( params, fGrid, data, fVars,
		ctxt->jacDiags ) ;
	
	//Perform post-smoothing
	nonlin_smooth( params, data, fGrid, params->postSmooth, ctxt->jacDiags,
		fVars ) ;
	
	//Recover the pressure so that it is up-to-date with the approximation
	recover_pressure( params, data, fGrid, fVars ) ;
	
	//Calculate the residual and the diagonals of the Jacobian matrix
	calc_richards_resid_and_jacobian_diags( params, fGrid, data, fVars,
		ctxt->jacDiags ) ;
}














