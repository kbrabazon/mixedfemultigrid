#include "sparseMatrix.h"

/** Sets the pointers to NULL in the SparseMatrix structure passed in
 *	\param [in] mat	The matrix for which to set the pointers to NULL for
 */
void set_sparse_mat_to_null( SparseMatrix *mat )
{
	mat->numNonZeros = 0 ;
	mat->numRows = 0 ;
	mat->numCols = 0 ;
	mat->globalEntry = NULL ;
	mat->rowStartInds = NULL ;
	mat->colNums = NULL ;
}

/** Frees the memory assigned on the heap for the SparseMatrix structure passed
 *	in
 *	\param [in] mat	The matrix for which to free the memory
 */
void free_sparse_mat( SparseMatrix *mat )
{
	if( mat->globalEntry != NULL ){
		free( mat->globalEntry ) ;
		mat->globalEntry = NULL ;
	}
	if( mat->rowStartInds != NULL ){
		free( mat->rowStartInds ) ;
		mat->rowStartInds = NULL ;
	}
	if( mat->colNums != NULL ){
		free( mat->colNums ) ;
		mat->colNums = NULL ;
	}
}

/** Performs a sparse matrix-vector multiply and stores the result in variable
 *	\p res. It is assumed that none of the arrays are overlapping. It is the
 *	responsibility of the user to make sure that this is the case, as no checks
 *	are performed
 *	\param [in] mat	The SparseMatrix structure to use in the multiplication
 *	\param [in] vec	The vector to use in the multiplication
 *	\param [out] res	The result of the matrix-vector multiplication
 */
void sparse_matrix_vec_mult( const SparseMatrix *mat, const double *vec,
	double *res )
{
	unsigned int row, col ; //Loop counters
	
	//Perform the assertion that the vector and the result are not the same, as
	//this would give strange output
	assert( vec != res ) ;
	
	//Loop through the rows of the matrix
	for( row = 0 ; row < mat->numRows ; row++ ){
		res[row] = 0 ;
		//Loop through the columns in the current row
		for( col=mat->rowStartInds[row] ; col < mat->rowStartInds[row+1] ;
			col++ ){
			//Perform the vector-vector multiplication for the current row of
			//the matrix
			res[row] += mat->globalEntry[col] * vec[mat->colNums[col]] ;
		}
	}
}

/** Calculates the transpose of the sparse matrix \p mat, and saves the result
 *  in \p tMat. The algorithm is taken from [Bank and Douglas, Sparse Matrix
 *	Multiplication Package (SMMP), Advances in Computational Mathematics, 1993,
 *	Vol. 1, pp. 127-137]
 *  \param [in] mat	The matrix to transpose
 *	\param [out] tMat	The transpose of the matrix given by \p mat
 *	\param [in] initMemory	Indicates whether the memory should be initialised
 *							for the matrix that we are taking the transpose of
 */
void sparse_matrix_transpose( const SparseMatrix * mat,
	SparseMatrix * tMat, bool initMemory )
{
	unsigned int i, j ; //Loop counters
	unsigned int tRowInd ; //Index of a row in the transposed matrix
	//If we are initialising the memory for the transpose of the matrix we need
	//to assign the dimensions. Otherwise we assume that this has already been
	//done, but don't check that enough memory has assigned
	if( initMemory ){
		//Make sure that the memory has not been assigned for the matrix
		//transpose
		assert( tMat->rowStartInds == NULL ) ;
		
		//We know that the number of non-zero entries must be the same, and that
		//the number of rows and columns is swapped
		tMat->numNonZeros = mat->numNonZeros ;
		tMat->numRows = mat->numCols ;
		tMat->numCols = mat->numRows ;
		
		//Initialise the memory
		tMat->globalEntry = (double*)malloc( tMat->numNonZeros *
			sizeof( double ) ) ;
		tMat->colNums = (unsigned int*)malloc( tMat->numNonZeros *
			sizeof( unsigned int ) ) ;
		tMat->rowStartInds = (unsigned int*)malloc( (tMat->numRows+1) *
			sizeof( unsigned int ) ) ;
	}
	
	//Check that the memory is not null for the transposed matrix
	assert( tMat->rowStartInds != NULL ) ;
	
	//Set all of the row start indices in the transpose of the matrix to zero
	for( i=0 ; i < tMat->numRows+1 ; i++ )
		tMat->rowStartInds[i] = 0 ;
	
	//Loop through the rows of matrix mat
	for( i=0 ; i < mat->numRows ; i++ ){
		//Loop through the non-zero columns of the current row
		for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ ){
			//Increment a count of the number of times a given column occurs in
			//the rows of mat
			tMat->rowStartInds[mat->colNums[j]+1]++ ;
		}
	}
	
	//Update the row start indices for the matrix tMat
	for( i=1 ; i < tMat->numRows+1 ; i++ )
		tMat->rowStartInds[i] += tMat->rowStartInds[i-1] ;
			
	//Check that the last index for the row is equal to the number of non-zero
	//entries in the matrix
	assert( tMat->numNonZeros == tMat->rowStartInds[tMat->numRows] ) ;
	
	//Loop through the rows of matrix mat
	for( i=0 ; i < mat->numRows ; i++ ){
		//Loop through the non-zero columns of matrix mat
		for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ ){
			//Get the row of the transposed matrix into which to write a value
			tRowInd = mat->colNums[j] ;
			//Set the column number in the appropriate position of the
			//transposed matrix
			tMat->colNums[ tMat->rowStartInds[tRowInd] ] = i ;
			//Copy the value from the appropriate entry in mat to the matrix
			//tMat
			tMat->globalEntry[ tMat->rowStartInds[tRowInd]++ ] =
				mat->globalEntry[j] ;
		}
	}
	
	//We now reset the row start indices for matrix tMat
	for( i=tMat->numRows ; i > 0 ; i-- ){
		tMat->rowStartInds[i] = tMat->rowStartInds[i-1] ;
	}
	tMat->rowStartInds[0] = 0 ;
	
	//Check that the last index of the row is equal to the number of non-zero
	//entries in the matrix
	assert( tMat->rowStartInds[tMat->numRows] == tMat->numNonZeros ) ;
}

/** Copies the sparse matrix structure from a source matrix into a destination
 *	matrix
 *	\param [in] source	The sparse matrix from which to take the structure
 *	\param [out] dest	The sparse matrix to populate. It is assumed that no
 *						memory has been assigned for this matrix yet, and it is
 *						initialised in this method
 */
void copy_sparse_matrix_struct( const SparseMatrix *source,
	SparseMatrix *dest )
{
	unsigned int i ; //Loop counter
	//Check that no memory assignments have been made yet	
	assert( dest->rowStartInds == NULL ) ;
	
	//Set the number of non-zeros, rows and columns to be the same in both
	//matrices
	dest->numNonZeros = source->numNonZeros ;
	dest->numRows = source->numRows ;
	dest->numCols = source->numCols ;
	
	//Set the required memory for the sparse matrix
	dest->rowStartInds = (unsigned int*)malloc( (dest->numRows+1) *
		sizeof( unsigned int ) ) ;
	dest->colNums = (unsigned int*)malloc( dest->numNonZeros *
		sizeof( unsigned int ) ) ;
	dest->globalEntry = (double*)calloc( dest->numNonZeros,
		sizeof( double ) ) ;
		
	//Copy the row start indices and the column entries into the new matrix
	for( i=0 ; i < dest->numRows+1 ; i++ )
		dest->rowStartInds[i] = source->rowStartInds[i] ;
		
	for( i=0 ; i < dest->numNonZeros ; i++ )
		dest->colNums[i] = source->colNums[i] ;
}

/** Removes the final \p n rows from a sparse matrix
 *	\param [in,out] mat	The sparse matrix from which to remove the rows
 *	\param [in] n	The number of rows to delete from the end of the matrix
 */
void delete_sparse_matrix_last_n_rows( SparseMatrix *mat, unsigned int n )
{
	unsigned int i, j ; //Loop counters
	//We create a new sparse matrix structure to copy the information into
	SparseMatrix newMat ;
	
	//Set the pointers in the new matrix to NULL
	set_sparse_mat_to_null( &newMat ) ;
	
	//Set the number of columns in the new matrix
	newMat.numCols = mat->numCols ;
	newMat.numRows = mat->numRows - n ;
	newMat.numNonZeros = mat->rowStartInds[newMat.numRows] ;
	
	//Set the approrpriate memory for the new matrix
	newMat.rowStartInds = (unsigned int*)malloc( (newMat.numRows +1 ) *
		sizeof( unsigned int ) ) ;
	newMat.colNums = (unsigned int*)malloc( newMat.numNonZeros *
		sizeof( unsigned int ) ) ;
	newMat.globalEntry = (double*)malloc( newMat.numNonZeros *
		sizeof( double ) ) ;
	
	//Loop through the rows in the new matrix, and copy the entries from the
	//old matrix
	newMat.rowStartInds[0] = 0 ;
	for( i=0 ; i < newMat.numRows ; i++ ){
		//Set the row start index for the next row
		newMat.rowStartInds[i+1] = mat->rowStartInds[i+1] ;
		//Loop through the columns in the old matrix
		for( j = mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ ){
			newMat.globalEntry[j] = mat->globalEntry[j] ;
			newMat.colNums[j] = mat->colNums[j] ;
		}
	}
	
	//Delete the old matrix, and copy the information from the new matrix into
	//the old matrix
	free_sparse_mat( mat ) ;
	mat->numCols = newMat.numCols ;
	mat->numRows = newMat.numRows ;
	mat->numNonZeros = newMat.numNonZeros ;
	mat->globalEntry = newMat.globalEntry ;
	mat->colNums = newMat.colNums ;
	mat->rowStartInds = newMat.rowStartInds ;
}

/**	Writes a sparse matrix to the console. This is useful in test functions
 *	where small matrices can be inspected
 *	\param [in] mat	The matrix to print to screen
 */
void write_sparse_matrix_to_console( const SparseMatrix *mat )
{
	unsigned int i, j ; //Loop counters
	
	//Check that the memory has been assigned for the matrix
	assert( mat->rowStartInds != NULL ) ;
	assert( mat->colNums != NULL ) ;
	assert( mat->globalEntry != NULL ) ;
	
	//Loop through the rows of the matrix
	for( i=0 ; i < mat->numRows ; i++ ){
		printf( "Row %u:", i ) ;
		//Loop through the columns in the row. First print out the column
		//numbers followed by the entries in the columns
		for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ )
			printf( " %u", mat->colNums[j] ) ;
		printf( ","  ) ;
		for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ )
			printf( " %.3lf", mat->globalEntry[j] ) ;
		printf( "\n" ) ;
	}
}

/** Scales the given values in the vector by the given factor. The values in 
 *	variable \p vec are overwritten
 *	\param [in] fact	The factor by which to scale the entries in \p vec
 *	\param [in] length	The number of entries in the vector
 *	\param [in,out] vec	The vector to scale
 */
void scale_vec( double fact, unsigned int length, double *vec )
{
	unsigned int i ;
	for( i=0 ; i < length ; i++ )
		vec[i] *= fact ;
}

/** Scales the values in the global entry of the matrix by the given factor
 *	\param [in] fact	The constant factor by which to multiply the matrix
 *						entries
 *	\param [out] mat	The matrix for which to scale the values
 */
void scale_mat( double fact, SparseMatrix *mat )
{
	unsigned int i ; //Loop counter
	
	//Check that the matrix has been initialised
	assert( mat->numRows > 0 ) ;
	
	//Scale each entry in the matrix by the given number
	for( i=0 ; i < mat->numNonZeros ; i++ )
		mat->globalEntry[i] *= fact ;
}

/** Calculates the infinity norm of a vector that is passed in (i.e. the largest
 *	value in the vector)
 *	\param [in] vect	The vector for which to calculate the infinity norm
 *	\param [in] length	The length of the vector passed in
 *	\return \p double value containing the infinity norm of the vector
 */
double dvec_infty_norm( const double * vect, unsigned int length )
{
	unsigned int i ; //Loop counter
	double maxVal, compVal ; //The maximum value and a value to compare it to
	
	maxVal = fabs( vect[0] ) ;
	
	for( i=1 ; i < length ; i++ ){
		compVal = fabs(vect[i]) ;
		maxVal = (maxVal >= compVal) ? maxVal : compVal ;
	}
	
	return maxVal ;
}

/** Calculates the infinity norm of the difference between two vectors
 *	\param [in] vec1	The first vector to take the difference with
 *	\param [in] vec2	The second vector to take the difference with
 *	\param [in] length	The length of the vectors
 *	\return \p double value containing the infinity norm of the vector of
 *			differences between \p vec1 and \p vec2
 */
double dvec_diff_infty_norm( const double *vec1, const double *vec2,
	unsigned int length )
{
	unsigned int i ; //Loop counter
	double maxVal, compVal ; //The maximum value and a value to compare it to
	
	maxVal = fabs( vec1[0] - vec2[0] ) ;
	
	for( i=1 ; i < length ; i++ ){
		compVal = fabs( vec1[i] - vec2[i] ) ;
		maxVal = (maxVal >= compVal) ? maxVal : compVal ;
	}
	
	return maxVal ;
}

/** Calculates the dot product of two vectors
 *	\param [in] vec1	The first vector to be used in the calculation
 *	\param [in] vec2	The second vector to be used in the calculation
 *	\param [in] length	The length of the vectors. It is assumed that enough
 *						memory has been assigned for each vector
 *	\return \p double value containing the dot product of the vectors
 */
double vec_dot_prod( const double *vec1, const double *vec2,
	unsigned int length )
{
	unsigned int i ; //Loop counter
	double dotProd = 0.0 ; //The value to return
	
	//Loop through all of the points and add the elementwise multiplication to
	//the total
	for( i=0 ; i < length ; i++ )
		dotProd += vec1[i] * vec2[i] ;
		
	return dotProd ;
}

/** Set the entries in the sparse matrix to zero
 *	\param [out] mat	The matrix for which to set the entries to zero
 */
void set_matrix_entries_to_zero( SparseMatrix *mat )
{
	set_dvec_to_zero( mat->numNonZeros, mat->globalEntry ) ;
}

/** Sets the entries in a vector to be a given constant value
 *	\param [in] length	The length of the vector passed in
 *	\param [in] val	The value to assign in each position of the vector
 *	\param [out] vec	The vector to assign values to
 */
void set_const_dvec( unsigned int length, const double val, double *vec )
{
	unsigned int i ;
	for( i=0 ; i < length ; i++ )
		vec[i] = val ;
}

/** Perturbs each entry in the vector by the given constant value
 *	\param [in] length	The length of the vector passed in
 *	\param [in] pVal	The value to perturb the vector by
 *	\param [out] vec	The vector to perturb
 */
void perturb_dvec( unsigned int length, const double pVal, double *vec )
{
	unsigned int i ;
	for( i=0 ; i < length ; i++ )
		vec[i] += pVal ;
}

/** Returns the index in the matrix for the given column in the given row
 *	\param [in] mat	The matrix in which to look for the specified index
 *	\param [in] rowNum	The row number in the matrix
 *	\param [in] colNum	The column number in the matrix
 *	\return	The index in the matrix of the entry in the specifeid row and column
 */
unsigned int get_matrix_ind( const SparseMatrix * mat,
	unsigned int rowNum, unsigned int colNum )
{
	unsigned int i ;
	for( i=mat->rowStartInds[rowNum] ; i < mat->rowStartInds[rowNum+1] ; i++ )
		if( mat->colNums[i] == colNum ) return i ;
		
	fprintf( stderr, "Column %u not found in row %u\n", colNum, rowNum ) ;
	exit(0) ;
	return 0 ;
}

/** Writes the vector as a space separated list on a single line to the console.
 *	This method should be used to inspect short vectors only
 *	\param [in] length	The length of the vector to print. It is assumed that
 *						the input vector is of the correct length
 *	\param [in] accuracy	The floating point accuracy to which the vector
 *							should be output
 *	\param [in] vec	The vector to print on the console
 */
void write_dvec_to_console( unsigned int length, unsigned int accuracy,
	const double * vec )
{
	unsigned int i ;
	char formatString[20] ;
	
	sprintf( formatString, " %%.%ulf", accuracy ) ;
	for( i=0 ; i < length ; i++ ){
		printf( formatString, vec[i] ) ;
		if( i != length-1 ) printf( "," ) ;
	}
}

/** Calculates the discrete L2 inner product of two vectors
 *	\param [in] length	The length of the vectors of which to take the inner
 *						product
 *	\param [in] vec1	The first vector to use in the inner product
 *	\param [in] vec2	The second vector to use in the inner product
 *	\return The normal inner product of the vectors scaled by a factor of the 
 *			inverse of the length of the vector
 */
double discrete_l2_inner_prod( unsigned int length, const double *vec1,
	const double *vec2 )
{
	unsigned int i ; //Loop counter
	double total = 0.0 ;
	
	for( i=0 ; i < length ; i++ )
		total += vec1[i] * vec2[i] ;
		
	return total / length ;
}

/** Calculates the discrete L2 norm of a vector passed in
 *	\param [in] length	The length of the vector of which to take the L2 norm
 *	\param [in] vec	The vector for which to calculate the L2 norm
 *	\return The discrete L2 norm of the vector passed in
 */
double discrete_l2_norm( unsigned int length, const double *vec )
{
	return sqrt( discrete_l2_inner_prod( length, vec, vec ) ) ;
}

/** Writes a sparse matrix to file in full format. This is useful for debugging
 *	and should only be performed for small matrices
 *	\param [in] fName	The name of the file to write to
 *	\param [in] mat	The matrix to write out to file
 */
void write_sparse_matrix_to_file( const char *fName, const SparseMatrix *mat )
{
	unsigned int i, j ; //Loop counters
	double *row ; //Represents a row in the matrix
	FILE *fout ; //The file to write output to
	
	//Open the file for writing
	fout = fopen( fName, "w" ) ;
	
	if( fout == NULL ){
		printf( "Error opening file %s in function ", fName ) ;
		printf( "write_sparse_matrix_to_file\n" ) ;
		exit( 0 ) ;
	}
	
	//Set the memory for a row in the matrix
	row = (double*)calloc( mat->numCols, sizeof( double ) ) ;
	
	//Loop through the rows in the matrix
	for( i=0 ; i < mat->numRows ; i++ ){
		//Loop through the nonzero entries in the current row
		for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ ){
			//Set the appropriate value in the row
			row[ mat->colNums[j] ] = mat->globalEntry[j] ;
		}
		
		//Write the row to file
		for( j=0 ; j < mat->numCols-1 ; j++ ){
			fprintf( fout, "%.18lf, ", row[j] ) ;
			row[j] = 0.0 ;
		}
		fprintf( fout, "%.18lf\n", row[j] ) ;
		row[j] = 0.0 ;
	}
	
	//Close the file
	fclose( fout ) ;
	//Free the memory assigned on the heap
	free( row ) ;
}

/** Writes the non-zero columns of a row to the console
 *	\param [in] mat	The matrix from which to take a row
 *	\param [in] row	The row for which to pring the non-zero columns
 */
void write_row_nonzero_cols_to_console( const SparseMatrix *mat,
	unsigned int row )
{
	unsigned int j ; //Loop counter
	
	printf( "Row %u:", row ) ;
	//Loop through the non-zero columns in the row
	for( j=mat->rowStartInds[row] ; j < mat->rowStartInds[row+1] ; j++ )
		printf( " %u", mat->colNums[j] ) ;
	printf( "\n" ) ;
}

/** Calculates the difference in the vectors passed in and stores the result in
 *	another vector
 *	\param [in] length	The length of the vectors
 *	\param [in] vec	The vector from which to take another vector
 *	\param [in] minusVec	The vector that will be negative in the subtraction
 *	\param [out] res	The result of the computation \p vec - \p minusVec
 */
void dvec_diff( unsigned int length, const double *vec, const double *minusVec,
	double *res )
{
	unsigned int i ; //Loop counter
	
	for( i=0 ; i < length ; i++ )
		res[i] = vec[i] - minusVec[i] ;
}

/** Normalises a vector such that its norm is one
 *	\param [in] length	The length of the vector to normalise
 *	\param [in,out] vec	The vector to normalise
 */
void normalise_vec( unsigned int length, double *vec )
{
	double norm ; //The norm of the vector
	
	//Calculate the norm of the vector
	norm = discrete_l2_norm( length, vec ) ;
	
	//Now normalise the vector
	scale_vec( 1.0 / norm, length, vec ) ;
}


















