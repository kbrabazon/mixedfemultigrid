#include "definitions.h"
#include "gridHandling.h"
#include "params.h"
#include "linApprox.h"
#include "richardsFunctions.h"
#include "testing.h"
#include "richardsTimeStepping.h"

//Set the files from which to read the grids
const char *gridFiles[] = { "../Grids/mesh4.out", "../Grids/mesh8.out",
	"../Grids/mesh16.out", "../Grids/mesh32.out", "../Grids/mesh64.out",
	"../Grids/mesh128.out", "../Grids/mesh256.out", "../Grids/mesh512.out",
	"../Grids/mesh1024.out" } ;

/** Entry point for the program
*/
int main( int argc, char **argv )
{
	Params params ;
	REData data ;
	//Check that the right number of arguments is passed in
	if( argc != 2 ){
		printf( "Usage:\n  %s <paramFile>\n", argv[0] ) ;
		exit(0) ;
	}
	
	set_re_data_to_null( &data ) ;
	
	//Read the parameters in from file
	read_params_from_file( argv[1], &params, &data ) ;
	make_params_valid( &params, &data ) ;
	
	if( params.testing ){
		test( &params, &data ) ;
		//Clean up
		free_re_data( &data ) ;
		free_param_memory( &params ) ;
		return 0 ;
	}
	
	//Solve the Richards equation
	solve_richards_eq( &params, &data ) ;
	
	//Clean up
	free_re_data( &data ) ;
	free_param_memory( &params ) ;
}
