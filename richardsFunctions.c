#include "richardsFunctions.h"
#include "gridHandling.h"
#include "sparseMatrix.h"
#include "interpolation.h"

/** Sets the memory for the pointers to NULL, and sets default values for
 *	non-pointer data in the structure
 *	\param [out] vars	Variables associated with the mixed finite element
 *						approximation of a Richards equation
 */
void set_re_vars_to_null( REApproxVars *vars )
{
	vars->lM = NULL ;
	vars->press = NULL ;
	vars->prevTheta = NULL ;
	vars->rhs = NULL ;
	vars->resid = NULL ;
}

/** Frees the memory assigned on the heap for the variables associated with
 *	the mixed finite element apprxoimation of a Richards equation
 *	\param [out] vars	The structure for which to free the memory
 */
void free_re_vars( REApproxVars *vars )
{
	if( vars->lM != NULL ){
		free( vars->lM ) ;
		vars->lM = NULL ;
	}
	if( vars->press != NULL ){
		free( vars->press ) ;
		vars->press = NULL ;
	}
	if( vars->prevTheta != NULL ){
		free( vars->prevTheta ) ;
		vars->prevTheta = NULL ;
	}
	if( vars->rhs != NULL ){
		free( vars->rhs ) ;
		vars->rhs = NULL ;
	}
	if( vars->resid != NULL ){
		free( vars->resid ) ;
		vars->resid = NULL ;
	}
}

/** Initialises the data related to the soil properties of the current set up
 *	for the Richards equation to some NULL state
 *	\param [out] data	The REData structure to initialise to NULL
 */
void set_re_data_to_null( REData *data )
{
	data->numSoils = 0 ;
	data->thetaR = NULL ;
	data->thetaS = NULL ;
	data->sCond = NULL ;
	data->n = NULL ;
	data->m = NULL ;
	data->pSat = NULL ;
	data->satFact = NULL ;
	data->hCondConst = NULL ;
	data->thetaM = NULL ;
	data->alpha = NULL ;
}

/** Assigns the appropriate memory for the data of the soil properties
 *	\param [in,out] data	The structure for which to assign the memory
 */
void assign_re_data_mem( REData *data )
{
	//Make sure that the number of soils has been specified
	assert( data->numSoils != 0 ) ;
	//Check that the memory has not previously been assigned
	assert( data->n == NULL ) ;
	
	data->thetaR = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->thetaS = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->sCond = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->n = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->m = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->pSat = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->satFact = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->hCondConst = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->thetaM = (double*)calloc( data->numSoils, sizeof( double ) ) ;
	data->alpha = (double*)calloc( data->numSoils, sizeof( double ) ) ;
}

/** This populates the member variables of an REData structure that depend on
 *	the values of the variable elements read in
 *	\param [in,out] data	The structure for which to set the variables
 */
void set_re_dependent_data( REData *data )
{
	unsigned int i ; //Loop counter
	
	//Check that the dependent data has been set
	assert( data->numSoils != 0 ) ;
	assert( data->thetaR != NULL ) ;
	assert( data->thetaS != NULL ) ;
	assert( data->pSat != NULL ) ;
	assert( data->sCond != NULL ) ;
	assert( data->n != NULL ) ;
	assert( data->alpha != NULL ) ;
	
	//For each soil set the dependent variables
	for( i=0 ; i < data->numSoils ; i++ ){
		//First set the value of m
		data->m[i] = 1.0 - 1.0 / data->n[i] ;
		
		//We can now calculate theta_{m}
		data->thetaM[i] = calc_theta_m( data, i ) ;
		
		//Set the ratio between the modified and normal definition of the
		//effective saturation
		data->satFact[i] = (data->thetaS[i] - data->thetaR[i]) /
			(data->thetaM[i] - data->thetaR[i]) ;
			
		//Set a constant factor used in the calculations of the hydraulic
		//conductivity
		data->hCondConst[i] = 1.0 - hydr_cond_sub_func( data, i, 1.0,
			data->m[i] ) ;
	}
}

/** Initialises the variables associated with the approximation of the mixed
 *	finite element formulation of the Richards equation on the current grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics of the soils used for the current
 *						problem
 *	\param [in] grid	The grid on which to initialise the approximation
 *						information
 *	\param [out] vars	Variables associated with the approximation of the mixed
 *						formulation of the Richards equation
 */
void setup_re_vars_on_grid( const Params *params, const Grid *grid,
	REApproxVars *vars )
{
	//Check that memory has not already been assigned for the structure
	assert( vars->lM == NULL ) ;
	//Set the appropriate memory for the member variables
	vars->lM = (double*)calloc( grid->numEdges, sizeof( double ) ) ;
	vars->press = (double*)calloc( grid->numElements, sizeof( double ) ) ;
	vars->prevTheta = (double*)calloc( grid->numElements, sizeof( double ) ) ;
	if( params->algorithm == ALG_NEWTON ){
		//If the solution algorithm is a Newton algorithm we require the right
		//hand side and residual to only store information at unknown nodes.
		//This is because the grid transfer operators ignore Dirichlet
		//boundaries as all items transferred are guaranteed to have zero
		//Dirichlet boundaries
		vars->rhs = (double*)calloc( grid->numUnknowns, sizeof( double ) ) ;
		vars->resid = (double*)calloc( grid->numUnknowns, sizeof( double ) ) ;
	}
	else{
		//If we are performing FAS the number of entries in the right hand side
		//and residual vectors should be the number of edges on the grid. This
		//means that the restriction and prolongation operators do not need to
		//be defined differently if the Dirichlet conditions are homogeneous or
		//not
		vars->rhs = (double*)calloc( grid->numEdges, sizeof( double ) ) ;
		vars->resid = (double*)calloc( grid->numEdges, sizeof( double ) ) ;
	}
	
	//Set the Dirichlet boundary data for the Lagrange multipliers
	set_lagrange_mult_bnd_data( params, grid, vars->lM ) ;
}

/** Calculates the value of \f$\theta_{m}\f$ which is the fictional value of
 *	volumetric wetness which would be achieved at zero hydraulic head, if the
 *	Vogel, et al model would allow for the pressure to drop to zero
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [in] soilID	Identifier for the soil type for which to calculate the
 *						value of \f$\theta_{m}\f$
 *	\return \p double value of the value of \f$\theta_{m}\f$ for the modified
 *			van Genuchten-Mualem model by Vogel, et al
 */
double calc_theta_m( const REData *data, int soilID )
{
	return data->thetaR[soilID] + ( data->thetaS[soilID] -
		data->thetaR[soilID] ) * pow ( 1.0 +  pow( -data->alpha[soilID] *
		data->pSat[soilID], data->n[soilID] ), data->m[soilID] ) ;
}

/** Free the memory assigned on the heap for the soil properties of the current
 *	set up for the Richards equation
 *	\param [out] data	The REData structure to free
 */
void free_re_data( REData *data )
{
	if( data->thetaR != NULL ){
		free( data->thetaR ) ;
		data->thetaR = NULL ;
	}
	if( data->thetaS != NULL ){
		free( data->thetaS ) ;
		data->thetaS = NULL ;
	}
	if( data->sCond != NULL ){
		free( data->sCond ) ;
		data->sCond = NULL ;
	}
	if( data->n != NULL ){
		free( data->n ) ;
		data->n = NULL ;
	}
	if( data->m != NULL ){
		free( data->m ) ;
		data->m = NULL ;
	}
	if( data->pSat != NULL ){
		free( data->pSat ) ;
		data->pSat = NULL ;
	}
	if( data->satFact != NULL ){
		free( data->satFact ) ;
		data->satFact = NULL ;
	}
	if( data->hCondConst != NULL ){
		free( data->hCondConst ) ;
		data->hCondConst = NULL ;
	}
	if( data->thetaM != NULL ){
		free( data->thetaM ) ;
		data->thetaM = NULL ;
	}
	if( data->alpha != NULL ){
		free( data->alpha ) ;
		data->alpha = NULL ;
	}
}

/** Writes the contents of the REData structure to console. This is useful for
 *	debugging purposes
 *	\param [in] data	The REData structure to write to console
 */
void write_re_data_to_console( REData *data )
{
	unsigned int accuracy = 3 ;
	printf( "Number of soils: %u\n", data->numSoils ) ;
	printf( "thetaR:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->thetaR ) ;
	printf( "\nthetaS:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->thetaS ) ;
	printf( "\nsCond:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->sCond ) ;
	printf( "\nn:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->n ) ;
	printf( "\nm:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->m ) ;
	printf( "\npSat:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->pSat ) ;
	printf( "\nsatFact:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->satFact ) ;
	printf( "\nhCondConst:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->hCondConst ) ;
	printf( "\nthetaM:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->thetaM ) ;
	printf( "\nalpha:" ) ;
	write_dvec_to_console( data->numSoils, accuracy, data->alpha ) ;
	printf( "\n" ) ;
}

/** Calculates the value of the volumetric wetness \f$\theta\f$ given a pressure
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] soilID	Identifier for the soil type for which to calculate the
 *						value of the volumetric wetness
 *	\param [in] pressure	The pressure given in terms of hydraulic head
 *	\return \p double value of the volumetric wetness given the pressure
 */
double calc_theta( const REData *data, int soilID, double pressure )
{
	//If the pressure is larger than the pressure at saturation return the
	//saturated water volume
	if( pressure >= data->pSat[soilID] )
		return data->thetaS[soilID] ;
	else
		return data->thetaR[soilID] + (data->thetaM[soilID] -
			data->thetaR[soilID]) / pow( 1.0 + pow( -data->alpha[soilID] *
			pressure, data->n[soilID] ), data->m[soilID] ) ;
}

/** Calculates the analytic derivative of the volumetric wetness function at
 *	a given pressure
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] soilID	Identfier for the soil type for which to calculate the
 *						derivative of the volumetric wetness for
 *	\param [in] pressure	The pressure given in terms of hydraulic head
 *	\return \p double value of the derivative of the volumetric wetness function
 *			at a given value of the pressure
 */
double calc_theta_deriv( const REData *data, int soilID, double pressure )
{
	//If the pressure is high enough such that we are at saturation then the
	//derivative of the volumetric wetness as a function of the pressure is zero
	if( pressure >= data->pSat[soilID] ) return 0.0 ;
	
	return (data->thetaM[soilID] - data->thetaR[soilID]) * data->alpha[soilID] *
		(data->n[soilID] -1) * pow( -data->alpha[soilID] * pressure,
		data->n[soilID]-1 ) / pow( 1 + pow(-data->alpha[soilID] * pressure,
		data->n[soilID]), data->m[soilID]+1 ) ;
}

/** Calcualtes the hydraulic conductivity given the volumetric wetness
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] soilID	Identifier for the soil for which to calculate the
 *						hydraulic conductivity
 *	\param [in] theta	The volumetric wetness
 *	\return \p double value containing the value of the hydraulic conductivity
 *			given the volumetric wetness
 */
double calc_hydr_cond( const REData *data, int soilID, double theta )
{
	double effSat ; //The effective saturation for the given pressure
	
	//If we are at saturation, simply return the saturated hydraulic
	//conductivity
	if( theta == data->thetaS[soilID] ) return data->sCond[soilID] ;
	
	//Get the effective saturation for the given pressure
	effSat = (theta - data->thetaR[soilID]) /
		(data->thetaS[soilID] - data->thetaR[soilID]) ;
		
	return data->sCond[soilID] * sqrt( effSat ) *
		pow( (1 - hydr_cond_sub_func( data, soilID, effSat, data->m[soilID] )) /
		data->hCondConst[soilID], 2 ) ;
}

/** Function used in the calculation of the hydraulic conductivity. This is
 *	given its own function to make the code more readable
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] soilId	Identifier for the soil for which to calculate the
 *						hydraulic conductivity
 *	\param [in] effSat	The effective saturation at which this function should
 *						be evaluated
 *	\param [in] power	The power to which an expression is to be raised in this
 *						function
 *	\return \p double value to be used in the calculation of the hydraulic
 *			conductivity
 */
double hydr_cond_sub_func( const REData *data, int soilID, double effSat,
	double power )
{
	return pow( 1 - pow( data->satFact[soilID] * effSat,
		1.0/data->m[soilID] ), power ) ;
}

/** Calcualtes the derivative of the hydraulic conductivity with respect to
 *	the pressure (given in terms of hydraulic head)
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] soilID	Identifier for the soil type for which to calculate the
 *						derivative of the hydraulic conductivity
 *	\param [in] pressure	The pressure at a given point
 *	\return \p double value containing the value of the derivative of the
 *			hydraulic conductivity at the given pressure
 */
double calc_hydr_cond_deriv( const REData *data, int soilID, double pressure )
{
	double retVal ; //Value to return
	double effSat ; //The effective saturation given the pressure
	double effSatD ; //The derivative of the effective saturation at the given
					 //pressure
	double fSe ; //The value of the function 1 - F(S_{e}) as defined in
				 //Vogel, et al., 2001
	
	//If the pressure is high enough such that we are at saturation then the
	//derivative of the hydraulic conductivity as a function of the pressure
	//is zero
	if( pressure >= data->pSat[soilID] ) return 0.0 ;

	//Calculate the effective saturation to use. We will calculate the
	//derivative with respect to this value
	effSat = ( calc_theta( data, soilID, pressure ) - data->thetaR[soilID] ) /
		( data->thetaS[soilID] - data->thetaR[soilID] ) ;
	//Calculate the derivative of the effective saturation wrt the pressure
	effSatD = calc_theta_deriv( data, soilID, pressure ) /
		(data->thetaS[soilID] - data->thetaR[soilID]) ;
	
	fSe = 1.0 - hydr_cond_sub_func( data, soilID, effSat, data->m[soilID] ) ;
	
	retVal = effSatD * fSe * ( fSe / 2 + 2 * effSat * hydr_cond_sub_func( data,
		soilID, effSat, data->m[soilID] - 1) * pow( data->satFact[soilID] *
		effSat, 1 / (data->n[soilID] - 1) ) * data->satFact[soilID] ) / (
		pow( data->hCondConst[soilID], 2 ) * sqrt( effSat ) ) ;
		
	retVal *= data->sCond[soilID] ;
		
	return retVal ;
}

/** Calculates the hydraulic conductivity and the volumetric wetness at a given
 *	pressure and puts these values into the variables \p hydrCond and
 *	\p volWet
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] soilID	Identifier of the soil type to use in the calculation
 *	\param [in] pressure	The pressure (in terms of hydraulic head) at which
 *							to calculate the hydraulic conductivity and the
 *							volumetric wetness
 * 	\param [out] hydrCond	The hydraulic conductivity calculated
 *	\param [out] volWet		The volumetric wetness calculated
 */
void calc_hydr_cond_vol_wet( const REData * data, int soilID,
	double pressure, double * hydrCond, double * volWet )
{
	double effSat ;
	
	//If the pressure is larger than the pressure at saturation return the
	//saturated water volume
	if( pressure >= data->pSat[soilID] ){
		*volWet = data->thetaS[soilID] ;
		*hydrCond = data->sCond[soilID] ;
		return ;
	}
	
	*volWet = data->thetaR[soilID] + (data->thetaM[soilID] -
			data->thetaR[soilID]) / pow( 1.0 + pow( -data->alpha[soilID] *
			pressure, data->n[soilID] ), data->m[soilID] ) ;
			
	//Get the effective saturation for the given pressure
	effSat = (*volWet - data->thetaR[soilID]) /
		(data->thetaS[soilID] - data->thetaR[soilID]) ;
		
	*hydrCond = data->sCond[soilID] * sqrt( effSat ) *
		pow( (1 - hydr_cond_sub_func( data, soilID, effSat, data->m[soilID] )) /
		data->hCondConst[soilID], 2 ) ;
}

/** Calculates and sets the values of the hydraulic conductivity and volumetric
 *	wetness and the derivatives of those functions at a given pressure
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] soilID	Identifier of the soil type to use in the calculation
 *	\param [in] pressure	The pressure (in terms of hydraulic head) at which
 *							to calculate the desired values
 *	\param [out] hydrCond	The hydraulic conductivity calculated
 *	\param [out] hydrCondD	The derivative of the hydraulic conductivity
 *							calculated
 *	\param [out] volWet		The volumetric wetness calculated
 *	\param [out] volWetD	The derivative of the volumetric wetness calculated
 */
void calc_hydr_cond_vol_wet_deriv( const REData * data, int soilID,
	double pressure, double * hydrCond, double * hydrCondD,
	double * volWet, double * volWetD )
{
	double effSat ; //The effective saturation given the pressure
	double effSatD ; //The derivative of the effective saturation at the given
					 //pressure
	double fSe ; //The value of the function 1 - F(S_{e}) as defined in
				 //Vogel, et al., 2001
	//If the pressure is larger than the pressure at saturation return the
	//saturated water volume
	if( pressure >= data->pSat[soilID] ){
		*volWet = data->thetaS[soilID] ;
		*volWetD = 0 ;
		*hydrCond = data->sCond[soilID] ;
		*hydrCondD = 0 ;
		return ;
	}
	
	//Set the volumetric wetness at the current pressure
	*volWet = data->thetaR[soilID] + (data->thetaM[soilID] -
			data->thetaR[soilID]) / pow( 1.0 + pow( -data->alpha[soilID] *
			pressure, data->n[soilID] ), data->m[soilID] ) ;
			
	//Get the effective saturation for the given pressure
	effSat = (*volWet - data->thetaR[soilID]) /
		(data->thetaS[soilID] - data->thetaR[soilID]) ;
		
	fSe = 1.0 - hydr_cond_sub_func( data, soilID, effSat, data->m[soilID] ) ;
		
	//Set the hydraulic conductivity at the current effective saturation
	*hydrCond = data->sCond[soilID] * sqrt( effSat ) *
		pow( fSe / data->hCondConst[soilID], 2 ) ;
	
	//Set the derivative of the volumetric wetness
	*volWetD = (data->thetaM[soilID] - data->thetaR[soilID]) *
		data->alpha[soilID] * (data->n[soilID] -1) * pow( -data->alpha[soilID] *
		pressure, data->n[soilID]-1 ) / pow( 1 + pow(-data->alpha[soilID] *
		pressure, data->n[soilID]), data->m[soilID]+1 ) ;
		
	//Set the derivative of the effective saturation
	effSatD = *volWetD / (data->thetaS[soilID] - data->thetaR[soilID]) ;
	
	//Set the derivative of the hydraulic conductivity
	*hydrCondD = effSatD * fSe * ( fSe / 2 + 2 * effSat *
		hydr_cond_sub_func( data, soilID, effSat, data->m[soilID] - 1) *
		pow( data->satFact[soilID] * effSat, 1 / (data->n[soilID] - 1) ) *
		data->satFact[soilID] ) / ( pow( data->hCondConst[soilID], 2 ) *
		sqrt( effSat ) ) ;
		
	*hydrCondD *= data->sCond[soilID] ;
}

/** Calculates the value of a row sum of the local matrix which gives the
 *	transformation from the flux to the pressure and Lagrange multipliers
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] el	The element on which the local matrix is defined
 *	\return \p double value containing the row sum of a local matrix on the
 *			given element
 */
double el_inv_mat_row_sum( const Element *el )
{
	unsigned int i ; //Loop counter
	double edgeMagSum = 0.0 ;
	
	//Loop over the edges of the element and add the square of the edges to the
	//value to return
	for( i=0 ; i < 3 ; i++ )
		edgeMagSum += pow( el->edges[i]->mag, 2 )  ;
		
	return 48 * el->area / edgeMagSum ;
}

/** Calculates the value of the local function in the model by Bause and Knabner
 *	the inverse of which gives the pressure on the current element
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils defined for the
 *						current problem
 *	\param [in] pressure	The current approximation of the pressure on the
 *							element
 *	\param [in] prevTheta	The volumetric wetness at the previous time-step
 *	\param [in] el	The element on which the function should be calculated
 *	\return The value of the local function given in the model by Bause and
 *			Knabner evaluated at the current approximation of the pressure
 */
double el_press_func( const Params * params,
	const REData * data, double pressure, double prevTheta,
	const Element *el )
{
	double theta, hCond ; //The volumetric wetness and hydraulic conductivity
	
	//Calculate the volumetric wetness and the hydraulic conductivity on the
	//current element
	calc_hydr_cond_vol_wet( data, el->soilType, pressure, &hCond,
		&theta ) ;
	
	return 3 * pressure + el->area * (theta - prevTheta) /
		(params->timeStep * el_inv_mat_row_sum( el ) * hCond) ;
}

/** Calculates the derivative of the function, the inverse of which gives the
 *	pressure on the current element
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils defined for the
 *						current problem
 *	\param [in] pressure	The current approximation of the pressure on the
 *							element
 *	\param [in] prevTheta	The volumetric wetness at the previous time-step
 *	\param [in] el	The element on which the derivative is to be calculated
 */
double el_press_func_deriv( const Params * params,
	const REData * data, double pressure, double prevTheta,
	const Element *el )
{
	double theta, thetaD ; //The volumetric wetness and its derivative
	double hCond, hCondD ; //The hydraulic conductivity and its derivative
	
	//Calculate the volumetric wetness, hydraulic conductivity and their
	//derivatives
	calc_hydr_cond_vol_wet_deriv( data, el->soilType, pressure, &hCond, &hCondD,
		&theta, &thetaD ) ;
	
	return 3 + el->area * ( hCond * thetaD - hCondD * ( theta - prevTheta ) ) /
		( params->timeStep * el_inv_mat_row_sum( el ) * pow( hCond, 2 ) ) ;
}

/** Calculates the derivative of the local function defining the relationship
 *	between the pressure and Lagrange multipliers given that part of the data
 *	has already been calculated	
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for soils used in the current
 *						problem
 *	\param [in] hCond	The value of the hydraulic conductivity to use
 *	\param [in] hCondDeriv	The value of the derivative of the hydraulic
 *							conductivity to use
 *	\param [in] volWet	The value of the volumetric wetness to use
 *	\param [in] volWetDeriv	The value of the derivative of the volumetric
 *							wetness to use
 *	\param [in] prevTheta	The volumetric wetness at the previous time-step
 *	\return	The value of the derivative of the local function defining the
 *			relationship between the pressure and Lagrange multipliers
 */
double el_press_func_deriv_from_data( const Params * params,
	const REData * data, const double hCond, const double hCondDeriv,
	const double volWet, const double volWetDeriv, const double prevTheta,
	const Element *el )
{
	return 3 + el->area * ( volWetDeriv - hCondDeriv * ( volWet -
		prevTheta ) / hCond ) / ( params->timeStep * el_inv_mat_row_sum( el ) *
		hCond ) ;
}

/** Calculates the value and the derivative of the function, the inverse of
 *	which gives the pressure on the current element
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils defined for the
 *						current problem
 *	\param [in] pressure	The current approximation of the pressure on the
 *							element
 *	\param [in] prevTheta	The volumetric wetness at the previous time-step
 *	\param [in] el	The element on which the derivative is to be calculated
 *	\param [out] func	The function value
 *	\param [out] funcDeriv	The derivative of the function value
 */
void el_press_func_and_deriv( const Params * params,
	const REData * data, double pressure, double prevTheta,
	const Element *el, double *func, double *funcDeriv )
{
	double theta, thetaD ; //The volumetric wetness and its derivative
	double hCond, hCondD ; //The hydraulic conductivity and its derivative
	
	//Calculate the volumetric wetness, hydraulic conductivity and their
	//derivatives
	calc_hydr_cond_vol_wet_deriv( data, el->soilType, pressure, &hCond, &hCondD,
		&theta, &thetaD ) ;
		
	*func = 3 * pressure + el->area * (theta - prevTheta) /
		(params->timeStep * el->invMatRowSum * hCond) ;
		
	*funcDeriv = 3 + el->area * ( thetaD - hCondD * ( theta -
		prevTheta ) / hCond ) / ( params->timeStep * el->invMatRowSum *
		hCond ) ;
}

/** Returns a constant used in the inverse of a local matrix on each element.
 *	This is defined in Bause and Knabner, Advances in Water Resources
 *	\param [in]	el	The element on which the local matrix is defined
 *	\param [in] edgeMagSquare	The square of the magnitude of the edges on the
 *								element
 *	\return	The constant value used in the calculation of the inverse of the
 *			local matrix in the mixed formulation of the Richards equation
 */
double local_mat_inv_const( const Element * el,
	const double * edgeMagSquare )
{
	unsigned int i ; //Loop counters
	double edgeProd = 6.0 ;
	double retVal = 0.0 ;
		
	for( i=0 ; i < 3 ; i ++ ){
		edgeProd *= edgeMagSquare[i] ;
		retVal += pow( edgeMagSquare[i], 2 ) * ( edgeMagSquare[i] -
			edgeMagSquare[(i+1)%3] - edgeMagSquare[(i+2)%3] ) ;
	}
			
	return retVal - edgeProd ;
}

/** Calculates the inverse of the local stiffness matrix on the element passed
 *	in. It is assumed that the local matrix that is passed in is an array of
 *	length 9, which is enough to store the 3x3 local stiffness matrix
 *	\param [in] el	The element on which to calculate the inverse of the
 *					stiffness matrix
 *	\param [out] locMat	The inverse of the local stiffness matrix
 */
void calc_loc_mat_inv( const Element * el, double * locMat )
{
	unsigned int i, j ; //Loop counters
	double invConst ; //A constant used in the calculation of the inverse of
					  //the local stiffness matrix
	double sumSquareProds ; //Stores the sum of the pairwise products of the
							//squares of the edge magnitudes. This is required
							//repeatedly in the calculation
	double edgeMagSquare[3] ; //The square of the magnitudes of the edges on
							  //the current element
	unsigned int ind ; //The index into the local matrix
							  
	//Set the square of the edge magnitudes
	for( i=0 ; i < 3 ; i++ )
		edgeMagSquare[i] = pow( el->edges[i]->mag, 2 ) ;
		
	//Set the sum of the pairwise products of squares of edge magnitudes
	sumSquareProds = 2 * ( edgeMagSquare[0] * (edgeMagSquare[1] + 
		edgeMagSquare[2]) + edgeMagSquare[1] * edgeMagSquare[2] ) ;
	
	//Calculate the constant to use in the calculation of the matrix
	invConst = 16 * el->area / local_mat_inv_const( el, edgeMagSquare ) ;
	
	//Loop through the rows in the matrix
	for( i=0 ; i < 3 ; i++ ){
		//Set the diagonal index
		ind = 4 * i ;
		//Set the value on the diagonal
		locMat[ind] = 0 ;
		for( j=1 ; j < 3 ; j++ ){
			locMat[ind] += edgeMagSquare[(i+j)%3] *
				( edgeMagSquare[(i+j)%3] - edgeMagSquare[i] ) ;
		}
		locMat[ind] -= sumSquareProds ;
		//Now set the off-diagonal entries
		for( j=1 ; j < 3-i ; j++ ){
			locMat[ind+j] = locMat[ind+3*j] = ( 3 * ( pow(edgeMagSquare[i], 2) +
				pow(edgeMagSquare[i+j], 2) ) + 
				pow( edgeMagSquare[(2*j+i)%3], 2 ) ) / 2 + 
				edgeMagSquare[i] * edgeMagSquare[i+j] - sumSquareProds ;
		}
	}
	
	//Scale th entries in the matrix appropriately
	for( i=0 ; i < 9 ; i++ )
		locMat[i] *= invConst ;
}

/** Recovers the flux from the value of the Lagrange multipliers and the
 *	pressure stored in the structure \p vars
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characterstics of the soils used in the current
 *						problem
 *	\param [in] grid	The grid on which the flux is to be found
 *	\param [in] vars	Variables associated with the mixed finite element
 *						approximation of the Richards equation
 *	\param [out] flux	The normal components of the flux across the edges of
 *						the grid
 */
void recover_flux( const Params * params, const REData * data,
	const Grid *grid, const REApproxVars * vars, double * flux )
{
	unsigned int i, j, elCnt ; //Loop counters
	unsigned int ind ; //An index into the start of the row in the local matrix
	const Element *el ; //An element on the grid
	double press ; //The pressure on an element of the grid
	double hCond ; //Stores the hydraulic conductivity for the pressure
	double val ; //Stores intermediate values in the calculation of the flux
	
	//Set the flux vector to zero
	set_dvec_to_zero( grid->numEdges, flux ) ;
	
	//Loop over the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element on the grid
		el = &(grid->els[elCnt]) ;
		//Get a placeholder for the pressure on the current element
		press = vars->press[elCnt] ;
		//Calculate the hydraulic conductivity on the current element
		hCond = calc_hydr_cond( data, el->soilType,
			calc_theta( data, el->soilType, press ) ) ;
		
		//Loop through the edges of the element
		for( i=0 ; i < 3 ; i++ ){
			val = 0.0 ;
			//Set the index to the start of the row in the local matrix
			ind = 3*i ;
			//Loop through the edges again
			for( j=0 ; j < 3 ; j++ ){
				val += el->invStiffMat[ ind + j ] * ( press -
					vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
			}
			flux[ el->edges[i]->id ] = (2 * (int)el->edgeOrient[i] -1) *
				hCond * val ;
		}
	}
}

/** Recovers the flux from the approximation of the pressure and the Lagrange
 *	multipliers stored in the REApproxVars structure on the grid given
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils to be used in the 
 *						current problem
 *	\param [in] grid	The grid on which the function is to be recovered
 *	\param [in] vars	Variables associated with the mixed finite element
 *						approximation of the Richards equation
 *	\param [out] flux	The magnitude of the normal component of the function
 *						across an edge, which is used to construct the flux
 *						function as a combination of basis functions
 */
void recover_flux_invert_loc_system( const Params * params,
	const REData * data, const Grid *grid,
	const REApproxVars * vars, double * flux )
{
	unsigned int i, elCnt ; //Loop counters
	unsigned int ind ; //Index into the edges on the grid
	const Element *el ; //An element on the grid
	double localFlux[3] ; //The local approximations to the flux (i.e. on an
						 //element)
	
	//Set the flux vector to zero to start with
	for( i=0 ; i < grid->numEdges ; i++ )
		flux[i] = 0 ;
		
	//Loop over each element on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		//Solve the local problem on the current element
		solve_local_flux_system( data, el, elCnt, vars, localFlux ) ;
		
		//Loop over the edges on the element and add a conribution to the flux
		for( i=0 ; i < 3 ; i++ )
			flux[ el->edges[i]->id ] += localFlux[i] ;
	}
	
	//Take an average for non-boundary edges
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		ind = grid->mapU2G[i] ;
		if( grid->edges[ind].parentEls[0] != grid->edges[ind].parentEls[1] )
			flux[i] /= 2.0 ;
	}
}

/** Solves the system of equations for the flux on the given element. This gives
 *	an approximation to the flux on an edge for the current element only, not
 *	globally
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] el	The element on which the local problem is to be solved
 *	\param [in] elInd	The index of the element on the grid
 *	\param [in] vars	The variables associated with an approximation for a
 *						mixed finite element formulation of the Richards
 *						equation
 *	\param [out] localFlux	The approximation to the flux coefficients on the
 *							edges of the current element
 */
void solve_local_flux_system( const REData * data,
	const Element * el, unsigned int elInd,
	const REApproxVars * vars, double * localFlux )
{
	unsigned int i ; //Loop counter
	double edgeMagSquare[3] ; //The square of the magnitude of the edges on an
							  //element
	double press ; //The value of the pressure on an element
	double hCond ; //The value of the hydraulic conductivity on the current
				   //element
	double fact ; //A common factor to scale the entries in the local matrix by
	double localMat[9] ; //The local matrix to invert to get the flux
						 //coefficient
	int pivot[3] ; //Stores the pivoting information in the LAPACK solve
	int mWidth = 3 ; //The width of the matrix to invert. This is required by
					 //LAPACk
	
	int nrhs = 1 ; //The number of right hand sides that will be passed to the
				   //LAPACK linear solver
	double e12, e13, e23 ; //The inner products of the edges on the current
						   //element
	const Node *points[3] ; //The points on the element
	int ok ; //Required for the LAPACK linear solver
				   
	//Store the value of the pressure on the current element
	press = vars->press[elInd] ;
	//Store the value of the hydraulic conductivity on the current element
	hCond = calc_hydr_cond( data, el->soilType,
		calc_theta( data, el->soilType, press ) ) ;
	
	//Store the magnitude of the square of the edges on the current element
	for( i=0 ; i < 3 ; i++ )
		edgeMagSquare[i] = pow( el->edges[i]->mag, 2 ) ;
	
	//Get the nodes on the element	
	get_el_nodes( el, points ) ;
	//Store the value of the inner products of the edges
	e12 = (points[1]->x - points[0]->x) * (points[2]->x - points[1]->x) +
		(points[1]->z - points[0]->z) * (points[2]->z - points[1]->z) ;
	e13 = (points[1]->x - points[0]->x) * (points[0]->x - points[2]->x) +
		(points[1]->z - points[0]->z) * (points[0]->z - points[2]->z) ;
	e23 = (points[2]->x - points[1]->x) * (points[0]->x - points[2]->x) +
		(points[2]->z - points[1]->z) * (points[0]->z - points[2]->z) ;
		
	//We now construct the local matrix. This is ordered by columns, but seeing
	//as the matrix is symmetric the row ordering would give the same
	//Set the diagonal entries
	localMat[0] = edgeMagSquare[0] - 3 * e23 ;
	localMat[4] = edgeMagSquare[1] - 3 * e13 ;
	localMat[8] = edgeMagSquare[2] - 3 * e12 ;
	
	//Set the off diagonal entries
	localMat[1] = localMat[3] = edgeMagSquare[0] - edgeMagSquare[2] + e13 ;
	localMat[2] = localMat[6] = edgeMagSquare[2] - edgeMagSquare[1] + e23 ;
	localMat[5] = localMat[7] = edgeMagSquare[2] - edgeMagSquare[0] + e13 ;
	
	//Scale the matrix appropriately
	fact = 24 * el->area ;
	for( i=0 ; i < 9 ; i++ )
		localMat[i] /= fact ;
		
	//Set up the right hand side to solve for
	for( i=0 ; i < 3 ; i++ )
		localFlux[i] = hCond * ( press - vars->lM[el->edges[i]->id] -
			get_z_op( el, i ) ) ;
			
	//Solve the linear system of equations
	dgesv_( &mWidth, &nrhs, localMat, &mWidth, pivot, localFlux, &mWidth,
		&ok ) ;
}

/** Recovers the pressure on each element using the Lagrange multipliers on the
 *	edges of the given grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for soils used in the current
 *						problem
 *	\param [in] grid	The grid on which to recover the pressure
 *	\param [in,out] vars	The Lagrange multipliers are used to calculate the
 *							pressure on each element
 */
void recover_pressure( const Params * params,
	const REData * data, const Grid * grid,
	REApproxVars * vars )
{
	unsigned int elCnt, i, newtCnt ; //Loop counters
//	unsigned int elCnt, i ; //Loop counters
	double rhs ; //The right hand side of the equation to solve on each element.
				 //This is equal to the sum of the Lagrange multipliers on
				 //the edges
	double resid, origResid ;
	const Element *el ; //An element on the grid
	unsigned int maxNewtIts = 5 ; //The number of Newton iterations to perform
								  //to get an approximate solution
	double pressFunc, pressFuncDeriv ; //The value of the function and the
				//derivative of the function to be inverted
	
	//Loop over the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		//Calculate the right hand side for the local equation, which is the
		//sum of the Lagrange multipliers on the edges of the current element
		rhs = 0 ;
		for( i=0 ; i < 3 ; i++ )
			rhs += vars->lM[ el->edges[i]->id ] ;
			
		//Perform the Newton iterations. It is assumed that an initial
		//approximation for the pressure on the element has been previously set
		
		el_press_func_and_deriv( params, data, vars->press[elCnt],
			vars->prevTheta[elCnt], el, &pressFunc, &pressFuncDeriv ) ;
		origResid = resid = rhs - pressFunc ;
		newtCnt = 0 ;
		
		while( fabs( resid / origResid ) > 1e-10 && newtCnt < maxNewtIts ){
			vars->press[elCnt] += resid / pressFuncDeriv ;
			el_press_func_and_deriv( params, data, vars->press[elCnt],
				vars->prevTheta[elCnt], el, &pressFunc, &pressFuncDeriv ) ;
			resid = rhs - pressFunc ;
			newtCnt++ ;
			if( resid < 1e-10 ) break ;
		}
		vars->press[elCnt] += resid / pressFuncDeriv ;
		
//		for( i=0 ; i < maxNewtIts ; i++ ){
//			el_press_func_and_deriv( params, data, vars->press[elCnt],
//				vars->prevTheta[elCnt], el, &pressFunc, &pressFuncDeriv ) ;
//			vars->press[elCnt] += (rhs - pressFunc) / pressFuncDeriv ;
//		}
	}
}

/** Sets the Dirichlet boundary information for the Lagrange multipliers in the
 *	mixed finite element approximation of the Richards equation
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the function is defined
 *	\param [out] lM		The Lagrange multipliers for which to set the boundary
 *						data
 */
void set_lagrange_mult_bnd_data( const Params *params, const Grid *grid,
	double *lM )
{
	unsigned int i ;
	if( params->testCase == 1 || params->testCase == 2 ||
		params->testCase == 3 ){
		//For test case one we use homogeneous Dirichlet boundary conditions
		for( i=grid->numUnknowns ; i < grid->numEdges ; i++ )
			lM[i] = 0.0 ;
	}
	else if( params->testCase == 4 && params->currTime >= 5 ){
		for( i=grid->numUnknowns ; i < grid->numEdges ; i++ )
			lM[i] = params->dirConst ;
	}
	else if( params->testCase == 4 ){
		for( i=grid->numUnknowns ; i < grid->numEdges ; i++ )
			lM[i] = ( 1 - params->currTime / 5.0 ) * params->dirConst ;
	}
	else{
		printf( "Unknown test case %u\n", params->testCase ) ;
		exit( 0 ) ;
	}
}

/** Calculates the residual of the Richards equation on the given grid. It is
 *	assumed that the pressure has been solved for on each element before this
 *	function has been called, so that it is not necessary to compute this
 *	information again
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to calculate the residual
 *	\param [in] data	Soil chracteristics of the soils used in the current
 *						problem
 *	\param [in,out] vars	Variables associated with the approximation to the
 *							mixed finite element formulation of the Richards
 *							equation. The member variable REApproxVars::resid
 *							is set in this method
 */
void calc_richards_resid( const Params * params,
	const Grid * grid, const REData * data,
	REApproxVars * vars )
{
	unsigned int elCnt, i, j ; //Loop counters
	const Element *el ; //An element on the grid
	const Edge *edge ; //An edge on the grid
	double hCond ; //Stores the hydraulic conductivity on the current element
	double val ; //Stores an interim value in the calculation of the 
	unsigned int ind ; //Index into the local stiffness matrix
	
	//Set the residual to be the right hand side initially
	copy_dvec( grid->numUnknowns, vars->rhs, vars->resid ) ;
	
	//Loop over the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		//Calculate the hydraulic conductivity on the current element
		hCond = calc_hydr_cond( data, el->soilType, 
			calc_theta( data, el->soilType, vars->press[elCnt] ) ) ;
			
		//Loop over the edges on the element
		for( i=0 ; i < 3 ; i++ ){
			//Get a handle to the current edge
			edge = el->edges[i] ;
			//Check that we are not on the boundary
			if( edge->type == EDGE_KNOWN ) continue ;
			val = 0.0 ;
			//Set the index into the local stiffness matrix
			ind = 3*i ;
			//Loop through the edges on the element
			for( j=0 ; j < 3 ; j++ ){
				val += el->invStiffMat[ind++] * ( vars->press[elCnt] -
					vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
			}
			//Update the value of the residual at the current point
			vars->resid[ edge->id ] -= hCond * val ;
		}
	}
}

/** Calculates the Jacobian matrix for the Richards equation. It is assumed that
 *	the pressure has been recovered on the elements for the current
 *	approximation such that this does not need to be recalculated
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the Jacobian should be calculated
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [in] vars	Variables associated with the approximation of the
 *						mixed finite element formulation of the Richards
 *						equation
 *	\param [out] jacobian	The Jacobian matrix for the current approximation
 */
void calc_richards_jacobian( const Params * params,
	const Grid * grid, const REData * data,
	const REApproxVars * vars, SparseMatrix * jacobian )
{
	unsigned int elCnt, i, j ; //Loop counters
	
	unsigned int matInd ; //An index into the Jacobian matrix
	
	const Element *el ; //An element on the grid
	const Edge *rowEdge, *colEdge ; //Edges on the grid
	
	double hCond, hCondD ; //The hydraulic conductivity and its derivative
	double volWet, volWetD ; //The volumetric wetness and its derivative
	double pressFuncDeriv ; //The inverse of the function relating the pressure
							//to the Lagrange multipliers
	double tmpVal ; //Stores an intermediate value in the calculation of the 
					//entry in the Jacobian matrix
	
	//Set the entries in the Jacobian to zero
	set_matrix_entries_to_zero( jacobian ) ;
	
	//Loop through the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		
		//Calculate the hydraulic conductivity, the volumetric wetness and their
		//derivatives
		calc_hydr_cond_vol_wet_deriv( data, el->soilType, vars->press[elCnt],
			&hCond, &hCondD, &volWet, &volWetD ) ;
			
		//Calculate the derivative of the pressure function
		pressFuncDeriv = el_press_func_deriv_from_data( params, data, hCond,
			hCondD, volWet, volWetD, vars->prevTheta[elCnt], el ) ;
			
		//Loop over the edges on the current element
		for( i=0 ; i < 3 ; i++ ){
			//Get a handle to the current edge
			rowEdge = el->edges[i] ;
			//Check to see if we are at a Dirichlet boundary
			if( rowEdge->type == EDGE_KNOWN ) continue ;
			//Calculate an intermediate value to use in the calculations
			tmpVal = 0.0 ;
			matInd = 3*i ;
			//Loop through the edges on the current element
			for( j=0 ; j < 3 ; j++ ){
				tmpVal += el->invStiffMat[matInd +j] * ( vars->press[elCnt] -
					vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
			}
			tmpVal =  ( hCondD * tmpVal / hCond + el->invMatRowSum ) /
				pressFuncDeriv ;
			//Loop through the edges on the current element
			for( j=0 ; j < 3 ; j++ ){
				//Get a handle to the current edge
				colEdge = el->edges[j] ;
				//Check to see if we are at a Dirichlet boundary
				if( colEdge->type == EDGE_KNOWN ) continue ;
				//Get the index in the Jacobian matrix
				matInd = get_matrix_ind( jacobian, rowEdge->id, colEdge->id ) ;
				//Update the value in the Jacobian matrix
				jacobian->globalEntry[matInd] -= hCond *
					( tmpVal - el->invStiffMat[3*i+j] ) ;
			}
		}
	}
}

/** Calculates the symmetric part of the Jacobian matrix for the Richards
 *	equation. It is assumed that the pressure has been recovered on the elements
 *	for the current	approximation such that this does not need to be
 *	recalculated
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the Jacobian should be calculated
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [in] vars	Variables associated with the approximation of the
 *						mixed finite element formulation of the Richards
 *						equation
 *	\param [out] jacobian	The symmetric part of the Jacobian matrix for the
 *							current approximation
 */
void calc_richards_jacobian_symm_part( const Params *params,
	const Grid *grid, const REData *data,
	const REApproxVars *vars, SparseMatrix *jacobian )
{
	unsigned int elCnt, i, j ; //Loop counters
	
	unsigned int matInd ; //An index into the Jacobian matrix
	
	const Element *el ; //An element on the grid
	const Edge *rowEdge, *colEdge ; //Edges on the grid
	
	double hCond, hCondD ; //The hydraulic conductivity and its derivative
	double volWet, volWetD ; //The volumetric wetness and its derivative
	double pressFuncDeriv ; //The inverse of the function relating the pressure
							//to the Lagrange multipliers
	double tmpVal ; //Stores an intermediate value in the calculation of the 
					//entry in the Jacobian matrix
	
	//Set the entries in the Jacobian to zero
	set_matrix_entries_to_zero( jacobian ) ;
	
	//Loop through the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		
		//Calculate the hydraulic conductivity, the volumetric wetness and their
		//derivatives
		calc_hydr_cond_vol_wet_deriv( data, el->soilType, vars->press[elCnt],
			&hCond, &hCondD, &volWet, &volWetD ) ;
			
		//Calculate the derivative of the pressure function
		pressFuncDeriv = el_press_func_deriv_from_data( params, data, hCond,
			hCondD, volWet, volWetD, vars->prevTheta[elCnt], el ) ;
			
		//Loop over the edges on the current element
		for( i=0 ; i < 3 ; i++ ){
			//Get a handle to the current edge
			rowEdge = el->edges[i] ;
			//Check to see if we are at a Dirichlet boundary
			if( rowEdge->type == EDGE_KNOWN ) continue ;
			//Calculate an intermediate value to use in the calculations
			tmpVal = 0.0 ;
/*			matInd = 3*i ;*/
/*			//Loop through the edges on the current element*/
/*			for( j=0 ; j < 3 ; j++ ){*/
/*				tmpVal += el->invStiffMat[matInd +j] * ( vars->press[elCnt] -*/
/*					vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;*/
/*			}*/
			tmpVal = el->invMatRowSum / pressFuncDeriv ;
			//Loop through the edges on the current element
			for( j=0 ; j < 3 ; j++ ){
				//Get a handle to the current edge
				colEdge = el->edges[j] ;
				//Check to see if we are at a Dirichlet boundary
				if( colEdge->type == EDGE_KNOWN ) continue ;
				//Get the index in the Jacobian matrix
				matInd = get_matrix_ind( jacobian, rowEdge->id, colEdge->id ) ;
				//Update the value in the Jacobian matrix
				jacobian->globalEntry[matInd] -= hCond *
					( tmpVal - el->invStiffMat[3*i+j] ) ;
			}
		}
	}
}

/** Calculates the Jacobian matrix using numerical differentation. This is much
 *	slower than using the explicit formula, but is useful in checking that the
 *	derivative is calculated correctly
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the Jacobian is to be calculated
 *	\param [in] data	Soil characteristics for soils used in the current
 *						problem
 *	\param [in] vars	Variables associated with the approximation to the mixed
 *						formulation of the Richards equation
 *	\param [out] jacobian	The Jacobian matrix
 */
void calc_richards_numeric_jacobian( const Params * params,
	const Grid * grid, const REData * data,
	REApproxVars * vars, SparseMatrix * jacobian )
{
	unsigned int elCnt, i, j, k ; //Loop counters
	const Edge *rowEdge, *colEdge ; //Edges on the grid
	const Element *el ; //An element on the grid
	double hCond ; //The hydraulic conductivity
	double pPress ; //The perturbed pressure
	double pHCond ; //The perturbed hydraulic conductivity
	double residVal, pResidVal ; //The contribution to the residual using the
				//original and perturbed approximations
	double val ; //Stores an intermediate value in the calculation
	unsigned int ind ; //An index into a matrix
	double rhs ; //The right hand side of the equation to solve for the pressure
	unsigned int numNewtIts = 3 ;
	double pressFunc, pressFuncDeriv ; //Used to calculate the pressure on the
			//current element
	
	//Set the Jacobian matrix to zero
	set_matrix_entries_to_zero( jacobian ) ;
	
	//Loop over the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		//Calculate the hydraulic conductivity on the current element
		hCond = calc_hydr_cond( data, el->soilType, 
			calc_theta( data, el->soilType, vars->press[elCnt] ) ) ;
			
		//Loop over the edges on the element
		for( i=0 ; i < 3 ; i++ ){
			//Get a handle to the current edge
			rowEdge = el->edges[i] ;
			//Check that we are not on the boundary
			if( rowEdge->type == EDGE_KNOWN ) continue ;
			val = 0.0 ;
			//Set the index into the local stiffness matrix
			ind = 3*i ;
			//Loop through the edges on the element
			for( j=0 ; j < 3 ; j++ ){
				val += el->invStiffMat[ind++] * ( vars->press[elCnt] -
					vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
			}
			//Update the value of the residual at the current point
			residVal = hCond * val ;
			
			//Loop over the edges of the element
			for( k=0 ; k < 3 ; k++ ){
				colEdge = el->edges[k] ;
				//Check that we are not at a boundary
				if( colEdge->type == EDGE_KNOWN ) continue ;
				//Perturb the value of the Lagrange multiplier for the current
				//edge
				vars->lM[ colEdge->id ] += M_EPS ;
				
				//Calculate the value of the pressure on the current element
				//Calculate the right hand side for the local equation, which is
				//the sum of the Lagrange multipliers on the edges of the
				//current element
				rhs = 0 ;
				for( j=0 ; j < 3 ; j++ )
					rhs += vars->lM[ el->edges[j]->id ] ;
			
				//Perform three Newton iterations. It is assumed that an initial
				//approximation for the pressure on the element has been
				//previously set
				pPress = vars->press[elCnt] ;
				for( j=0 ; j < numNewtIts ; j++ ){
					el_press_func_and_deriv( params, data, pPress,
						vars->prevTheta[elCnt], el, &pressFunc,
						&pressFuncDeriv ) ;
					pPress += (rhs - pressFunc) / pressFuncDeriv ;
				}
				
				//Now that I have the perturbed value of the pressure calculate
				//the perturbed value of the hydraulic conductivity
				pHCond = calc_hydr_cond( data, el->soilType,
					calc_theta( data, el->soilType, pPress ) ) ;
					
				//Calcualte the perturbed contribution to the residual
				val = 0.0 ;
				//Set the index into the local stiffness matrix
				ind = 3*i ;
				//Loop through the edges on the element
				for( j=0 ; j < 3 ; j++ ){
					val += el->invStiffMat[ind++] * ( pPress -
						vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
				}
				
				//Calculate the perturbed residual value
				pResidVal = pHCond * val ;
				
				//Get the index into the Jacobian matrix
				ind = get_matrix_ind( jacobian, rowEdge->id, colEdge->id ) ;
				
				//Update the value in the current position of the Jacobian
				//matrix
				jacobian->globalEntry[ ind ] -= (pResidVal - residVal) / M_EPS ;
				
				//Reset the value of the Lagrange multiplier
				vars->lM[ colEdge->id ] -= M_EPS ;
			}
		}
	}
}

/** Calculates the diagonals of the Jacobian matrix for the Richards equation.
 *	It is assumed that the pressure has been recovered on the elements for the
 *	current approximation such that this does not need to be recalculated
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the Jacobian should be calculated
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [in] vars	Variables associated with the approximation of the
 *						mixed finite element formulation of the Richards
 *						equation
 *	\param [out] jacDiags	The Jacobian matrix for the current approximation
 */
void calc_richards_jacobian_diags( const Params *params, const Grid *grid,
	const REData *data, const REApproxVars *vars, double *jacDiags )
{
	unsigned int elCnt, i, j ; //Loop counters
	
	unsigned int matInd ; //An index into the Jacobian matrix
	
	const Element *el ; //An element on the grid
	const Edge *rowEdge ; //An edge on the grid
	
	double hCond, hCondD ; //The hydraulic conductivity and its derivative
	double volWet, volWetD ; //The volumetric wetness and its derivative
	double pressFuncDeriv ; //The inverse of the function relating the pressure
							//to the Lagrange multipliers
	double tmpVal ; //Stores an intermediate value in the calculation of the 
					//entry in the Jacobian matrix
	
	//Set the diagonals of the Jacobian matrix to zero
	set_dvec_to_zero( grid->numUnknowns, jacDiags ) ;
	
	//Loop through the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		
		//Calculate the hydraulic conductivity, the volumetric wetness and their
		//derivatives
		calc_hydr_cond_vol_wet_deriv( data, el->soilType, vars->press[elCnt],
			&hCond, &hCondD, &volWet, &volWetD ) ;
			
		//Calculate the derivative of the pressure function
		pressFuncDeriv = el_press_func_deriv_from_data( params, data, hCond,
			hCondD, volWet, volWetD, vars->prevTheta[elCnt], el ) ;
			
		//Loop over the edges on the current element
		for( i=0 ; i < 3 ; i++ ){
			//Get a handle to the current edge
			rowEdge = el->edges[i] ;
			//Check to see if we are at a Dirichlet boundary
			if( rowEdge->type == EDGE_KNOWN ) continue ;
			//Calculate an intermediate value to use in the calculations
			tmpVal = 0.0 ;
			matInd = 3*i ;
			//Loop through the edges on the current element
			for( j=0 ; j < 3 ; j++ ){
				tmpVal += el->invStiffMat[matInd +j] * ( vars->press[elCnt] -
					vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
			}
			tmpVal =  ( hCondD * tmpVal / hCond + el->invMatRowSum ) /
				pressFuncDeriv ;
			jacDiags[rowEdge->id] -= hCond * (tmpVal - el->invStiffMat[4*i]) ;
		}
	}
}

/** Calculates the residual and the Jacobian for the Richards equation at the
 *	same time to save on computational expense
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to calculate the residual and the
 *						Jacobian
 *	\param [in]	data	Soil characteristics for the soils used in the current
 *						approximation
 *	\param [in,out] vars	Variables associated with the approximation of the
 *							mixed finite element formulation of the Richards
 *							equation. The member variable REApproxVars::resid
 *							is updated in this method
 *	\param [out] jacobian	The Jacobian matrix.			
 */
void calc_richards_resid_and_jacobian( const Params *params, const Grid *grid,
	const REData *data, REApproxVars *vars, SparseMatrix *jacobian )
{
	unsigned int elCnt, i, j ; //Loop counters
	
	unsigned int matInd ; //An index into the Jacobian matrix
	
	const Element *el ; //An element on the grid
	const Edge *rowEdge, *colEdge ; //Edges on the grid
	
	double hCond, hCondD ; //The hydraulic conductivity and its derivative
	double volWet, volWetD ; //The volumetric wetness and its derivative
	double pressFuncDeriv ; //The inverse of the function relating the pressure
							//to the Lagrange multipliers
	double tmpVal ; //Stores an intermediate value in the calculation of the 
					//entry in the Jacobian matrix
	
	//Set the entries in the Jacobian to zero
	set_matrix_entries_to_zero( jacobian ) ;
	//Set the residual to be the right hand side
	copy_dvec( grid->numUnknowns, vars->rhs, vars->resid ) ;
	
	//Loop through the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		
		//Calculate the hydraulic conductivity, the volumetric wetness and their
		//derivatives
		calc_hydr_cond_vol_wet_deriv( data, el->soilType, vars->press[elCnt],
			&hCond, &hCondD, &volWet, &volWetD ) ;
			
		//Calculate the derivative of the pressure function
		pressFuncDeriv = el_press_func_deriv_from_data( params, data, hCond,
			hCondD, volWet, volWetD, vars->prevTheta[elCnt], el ) ;
			
		//Loop over the edges on the current element
		for( i=0 ; i < 3 ; i++ ){
			//Get a handle to the current edge
			rowEdge = el->edges[i] ;
			//Check to see if we are at a Dirichlet boundary
			if( rowEdge->type == EDGE_KNOWN ) continue ;
			//Calculate an intermediate value to use in the calculations
			tmpVal = 0.0 ;
			matInd = 3*i ;
			//Loop through the edges on the current element
			for( j=0 ; j < 3 ; j++ ){
				tmpVal += el->invStiffMat[matInd+j] * ( vars->press[elCnt] -
					vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
			}
			vars->resid[ rowEdge->id ] -= hCond * tmpVal ;
			tmpVal =  ( hCondD * tmpVal / hCond + el->invMatRowSum ) /
				pressFuncDeriv ;
			//Loop through the edges on the current element
			for( j=0 ; j < 3 ; j++ ){
				//Get a handle to the current edge
				colEdge = el->edges[j] ;
				//Check to see if we are at a Dirichlet boundary
				if( colEdge->type == EDGE_KNOWN ) continue ;
				//Update the value in the Jacobian matrix
				jacobian->globalEntry[ get_matrix_ind( jacobian, rowEdge->id,
					colEdge->id ) ] -= hCond *
					( tmpVal - el->invStiffMat[matInd+j] ) ;
			}
		}
	}
}

/** Calculates the residual and the diagonals of the Jacobian matrix for the
 *	Richards equation at the same time to save on computational expense
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to calculate the residual and the
 *						Jacobian
 *	\param [in]	data	Soil characteristics for the soils used in the current
 *						approximation
 *	\param [in,out] vars	Variables associated with the approximation of the
 *							mixed finite element formulation of the Richards
 *							equation. The member variable REApproxVars::resid
 *							is updated in this method
 *	\param [out] jacDiags	The diagonals of the Jacobian matrix
 */
void calc_richards_resid_and_jacobian_diags( const Params *params,
	const Grid *grid, const REData *data, REApproxVars *vars,
	double *jacDiags )
{
	unsigned int elCnt, i, j ; //Loop counters
	
	unsigned int matInd ; //An index into the Jacobian matrix
	
	const Element *el ; //An element on the grid
	const Edge *rowEdge ; //An edge on the grid
	
	double hCond, hCondD ; //The hydraulic conductivity and its derivative
	double volWet, volWetD ; //The volumetric wetness and its derivative
	double pressFuncDeriv ; //The inverse of the function relating the pressure
							//to the Lagrange multipliers
	double tmpVal ; //Stores an intermediate value in the calculation of the 
					//entry in the Jacobian matrix
	
	//Set the entries in the diagonal of the Jacobian matrix to zero initially
	set_dvec_to_zero( grid->numUnknowns, jacDiags ) ;
	//Set the residual to be the right hand side
	copy_dvec( grid->numUnknowns, vars->rhs, vars->resid ) ;
	
	//Loop through the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid->els[elCnt]) ;
		
		//Calculate the hydraulic conductivity, the volumetric wetness and their
		//derivatives
		calc_hydr_cond_vol_wet_deriv( data, el->soilType, vars->press[elCnt],
			&hCond, &hCondD, &volWet, &volWetD ) ;
			
		//Calculate the derivative of the pressure function
		pressFuncDeriv = el_press_func_deriv_from_data( params, data, hCond,
			hCondD, volWet, volWetD, vars->prevTheta[elCnt], el ) ;
			
		//Loop over the edges on the current element
		for( i=0 ; i < 3 ; i++ ){
			//Get a handle to the current edge
			rowEdge = el->edges[i] ;
			//Check to see if we are at a Dirichlet boundary
			if( rowEdge->type == EDGE_KNOWN ) continue ;
			//Calculate an intermediate value to use in the calculations
			tmpVal = 0.0 ;
			matInd = 3*i ;
			//Loop through the edges on the current element
			for( j=0 ; j < 3 ; j++ ){
				tmpVal += el->invStiffMat[matInd+j] * ( vars->press[elCnt] -
					vars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
			}
			vars->resid[ rowEdge->id ] -= hCond * tmpVal ;
			tmpVal =  ( hCondD * tmpVal / hCond + el->invMatRowSum ) /
				pressFuncDeriv ;
			//Update the value in the diagonal of the Jacobian matrix
			jacDiags[ rowEdge->id ] -= hCond * ( tmpVal -
				el->invStiffMat[4*i] ) ;
		}
	}
}

/** Sets an initial approximation on a grid. It is assumed that the values of
 *	the pressure are constant on all of the domain, including the Dirichlet
 *	boundaries. This then models the case that the Dirichlet boundary conditions
 *	are instantaneously imposed on a steady system
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [in] grid	The grid on which to set up the approximation
 *	\param [out] vars	Variables associated with the approximation of the mixed
 *						formulation of the Richards equation. The Lagrange
 *						multipliers as well as the pressure is populated in this
 *						method
 */
void set_richards_initial_data( const Params *params, const REData *data,
	const Grid *grid, REApproxVars *vars )
{
	unsigned int i ; //Loop counter
	
	//Check that the appropriate memory has been assigned
	assert( vars->lM != NULL ) ;
	
	if( params->approxFilePrefix[0] != '\0' ){
		char fName[50] ;
		//Read in the values from the appropriate files. It is assumed that the
		//Lagrange multipliers are saved in the file with the suffix 'LM.txt'
		//and that the pressure is saved in the file with suffix 'Press.txt'
		sprintf( fName, "%sLM.txt", params->approxFilePrefix ) ;
		read_dvec_from_file( fName, grid->numEdges, vars->lM ) ;
		sprintf( fName, "%sPress.txt", params->approxFilePrefix ) ;
		read_dvec_from_file( fName, grid->numElements, vars->press ) ;
		sprintf( fName, "%sPrevTheta.txt", params->approxFilePrefix ) ;
		read_dvec_from_file( fName, grid->numElements, vars->prevTheta ) ;
		
		//Recover the pressure
		recover_pressure( params, data, grid, vars ) ;
		
		return ;
	}
	
	//Set the data on the Dirichlet boundary
	set_lagrange_mult_bnd_data( params, grid, vars->lM ) ;
	
	set_richards_init_lagrange_mult( params, data, grid, vars ) ;
	
	set_richards_init_pressure( params, data, grid, vars ) ;
	
	//Set the value of the volumetric wetness
	for( i=0 ; i < grid->numElements ; i++ ){
		vars->prevTheta[i] = calc_theta( data, grid->els[i].soilType,
			vars->press[i] ) ;
	}
	
	//Make sure that the current value of the pressure matches the Lagrange
	//multipliers
//	recover_pressure( params, data, grid, vars ) ;
}

/** Sets the initial approximation of the Lagrange multipliers for the Richards
 *	equation, depending on the parameters passed in
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [in] grid	The grid on which the approximation is to be initialised
 *	\param [out] vars	Variables associated with the approximation of the mixed
 *						finite element formulation of the Richards equation. The
 *						output is the variable REApproxVars::lM
 */
void set_richards_init_lagrange_mult( const Params *params, const REData *data,
	const Grid *grid, REApproxVars *vars )
{
	//For the time being we are interested only in the case of hydrostatic
	//equilibrium, so set the initial approximation to this
	if( params->testCase == 4 ){
		set_lagrange_mult_hydrostatic_eq( grid, vars, params->dirConst ) ;
		return ;
	}
	
	//Set the Lagrange multipliers to zero. At the moment this fits in with the
	//boundary data, but should not be kept like this
	//set_dvec_to_zero( grid->numEdges, vars->lM ) ;
	
	set_lagrange_mult_const( grid, vars, params->dirConst ) ;
}

/** Sets the values of the Lagrange multipliers to be in hydrostatic
 *	equilibrium. This assumes that the grid is rectangular and that the top
 *	boundary has z-coordinate zero. The value \p offset is added to all of the
 *	values
 *	\param [in] grid	The grid on which to set the Lagrange multipliers
 *	\param [out] vars	Variables associated with the mixed finite element
 *						approximation of the Richards equation. The output of
 *						the function is REApproxVars::lM.
 */
void set_lagrange_mult_hydrostatic_eq( const Grid *grid,
	REApproxVars *vars, double offset )
{
	unsigned int i ; //Loop counter
	const Edge *edge ; //An edge on the grid
	double midPoint[2] ; //The mid-point of an edge
	
	//Loop over the edges of the grid
	for( i=0 ; i < grid->numEdges ; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(grid->edges[i]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		
		//Set the value in the approximation to be the negative of the
		//z-coordinate of the data
		vars->lM[edge->id] = offset-midPoint[1] ;
	}
}

/** Sets a constant value for the Lagrange multipliers on the unknown edges of a
 *	grid
 *	\param [in] grid	The grid on which to set the Lagrange multipliers
 *	\param [out] vars	Variables associated with the mixed finite element
 *						approximation of the Richards equation. The desired
 *						output from the method is REApproxVars::lM
 *	\param [in] constVal	The constant value to set at the unknown edges of
 *							the grid
 */
void set_lagrange_mult_const( const Grid *grid, REApproxVars *vars,
	double constVal )
{
	unsigned int i ; //Loop counter
	
	//Set the Lagrange multipliers to take the given constant value on every
	//edge
	for( i=0; i < grid->numUnknowns ; i++ )
		vars->lM[i] = constVal ;
}

/** Sets the initial pressure profile for the Richards equation using the
 *	Lagrange multipliers. It is assumed that the Lagrange multipliers have
 *	already been initialised
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [in] grid	The grid on which the pressure is to be initialised
 *	\param [out] vars	Variables associated with the approximation of the mixed
 *						finite element formulation of the Richards equation. The
 *						output is the variable REApproxVars::press
 */
void set_richards_init_pressure( const Params *params, const REData *data,
	const Grid *grid, REApproxVars *vars )
{
	unsigned int elCnt, i ; //Loop counters
	double val ; //The value of the pressure to assign
	const Element *el ; //An element on the grid
	
	//Loop over the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a handle to the current element on the grid
		el = &(grid->els[elCnt]) ;
		val = 0.0 ;
		//Loop over the edges of the element and add a contribution to the
		//value to assign for the pressure
		for( i=0 ; i < 3 ; i++ )
			val += vars->lM[ el->edges[i]->id ] ;
			
		//Take the average of the values assigned
		val /= 3.0 ;
		//Set the value of the pressure
		vars->press[elCnt] = val ;
	}
}

/** Sets the value of the volumetric wetness as at the end of a time-step
 *	\param [in] grid	The grid on which to discretise
 *	\param [in,out] vars	Variables associated with the approximation of the
 *							mixed finite element formulation of the Richards
 *							equation. This method sets the variable
 *							REApproxVars::prevTheta under the assumption that
 *							the pressure profile to use is stored in
 *							REApproxVars::press
 */
void set_richards_prev_theta( const Grid *grid, const REData *data,
	REApproxVars *vars )
{
	unsigned int i ; //Loop counter
	
	for( i=0 ; i < grid->numElements ; i++ )
		vars->prevTheta[i] = calc_theta( data, grid->els[i].soilType,
			vars->press[i] ) ;
}

/** Updates the approximation to the Richards equation using the correction
 *	calculated in the linear Newton system
 *	\param [in] grid	The grid on which the approximations are defined
 *	\param [in] linVars	Variables associated with the linear Newton system. The
 *						correction is calculated in here
 *	\param [out] reVars	Variables associated with the mixed finite element
 *						approximation of the Richards equation. The variable
 *						REApproxVars::approx is the desired output from this
 *						method
 */
void update_re_approx_from_newt_correct( const Params *params, const Grid *grid,
	const LinApproxVars *linVars, REApproxVars *reVars )
{
	unsigned int i ; //Loop counter
	
	//Add the contribution from the linear approximation to the values of the
	//Lagrange multipliers
	for( i=0 ; i < grid->numUnknowns ; i++ )
		reVars->lM[i] -= params->newtWeight * linVars->approx[i] ;
}

/** Updates the approximation to the Richards equation using the correction
 *	calculated in an FAS iteration
 *	\param [in] grid	The grid on which the functions exist
 *	\param [in] correct	The correction term
 *	\param [out] lM	The Lagrange multipliers to correct
 */
void update_re_approx_from_fas_correct( const Grid *grid, const double *correct,
	double *lM )
{
	unsigned int i ; //Loop counter
	for( i=0 ; i < grid->numUnknowns ; i++ )
		lM[i] += correct[i] ;
}

/** Initialises the hierarchy of variables required to approximate the Richards
 *	equation on each grid in a hierarchy of grids. This does not set up the 
 *	values just sets the memory required on the heap
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] gridHierarchy	The hierarchy of grids on which the Richards
 *								equation is to be approximated
 *	\return The variables required to solve the mixed finite element formulation
 *			of the Richards equation on each grid in the grid hierarchy
 */
REApproxVars** setup_re_vars_hierarchy( const Params *params,
	Grid** gridHierarchy )
{
	unsigned int i ; //Loop counter
	REApproxVars **reHierarchy ; //The hierarchy of variables associated with
			//the approximation of the mixed finite element formulation of the
			//Richards equation
			
	//Set the appropriate memory for the outer pointer
	reHierarchy = (REApproxVars**)calloc( params->fGridLevel+1,
		sizeof( REApproxVars* ) ) ;
		
	//For each non-assigned grid set the variables to NULL
	for( i=0 ; i < params->cGridLevel ; i++ ) reHierarchy[i] = NULL ;
		
	//Loop through the assigned grids
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		reHierarchy[i] = (REApproxVars*)calloc( 1, sizeof( REApproxVars ) ) ;
		//Initialise the structure to NULL
		set_re_vars_to_null( reHierarchy[i] ) ;
		//Set the appropriate memory for the approximation on the current grid
		setup_re_vars_on_grid( params, gridHierarchy[i], reHierarchy[i] ) ;
	}
	
	return reHierarchy ;
}

/** Frees the memory assigned on the heap for the variables required to solve
 *	the mixed finite element formulation of the Richards equation on a hierarchy
 *	of grids
 *	\param [in] params	Parametes defining how the algorithm is to be executed
 *	\param [in] reHierarchy	The hierarchy of variables required to solve the
 *							mixed finite element formulation of the Richards
 *							equation
 *	\return The NULL pointer, which should be assigned to the hierarchy passed
 *			in to avoid trying to free the structure again
 */
REApproxVars** free_re_vars_hierarchy( const Params *params,
	REApproxVars **reHierarchy )
{
	unsigned int i ; //Loop counter
	
	//If there is nothing to free simply return the NULL pointer
	if( reHierarchy == NULL ) return NULL ;
	
	//Loop through the structures in the hierarchy that have had a value
	//assigned to them
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		//Free the memory on the heap for the current structure
		if( reHierarchy[i] != NULL ){
			free_re_vars( reHierarchy[i] ) ;
			//Free the pointer to the structure
			free( reHierarchy[i] ) ;
			reHierarchy[i] = NULL ;
		}
	}
	
	//Free the list of structures
	free( reHierarchy ) ;
	reHierarchy = NULL ;
	return reHierarchy ;
}

/** Sets up the perturbed right hand side for an FAS iteration for the Richards
 *	equation. The steps to take are to restrict the approximation, the previous
 *	volumetric wetness, the current pressure and the current residual, and then
 *	to apply the nonlinear operator on the coarse grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [in] fInterp	Interpolation operators on the fine grid. The
 *						restriction operator is required here
 *	\param [in] fGrid	The fine grid
 *	\param [in] cGrid	The coarse grid
 *	\param [in] fVars	Variables required for the solution of the Richards
 *						equation on the fine grid
 *	\param [out] cVars	Variables required for the solution of the Richards
 *						equation on the coarse grid
 */
void calc_richards_fas_coarse_rhs( Params *params, const REData *data,
	const Interpolation *fInterp, const Grid *fGrid, const Grid *cGrid,
	const REApproxVars *fVars, REApproxVars *cVars )
{
	unsigned int elCnt, i, j ; //Loop counters
	unsigned int ind ; //An index into the inverse of the stiffness matrix on an
					   //element
	const Element *el ; //An element on the coarse grid
	const Edge *edge ; //An edge on the coarse grid
	double hCond ; //The hydraulic conductivity on an element of the grid
	double val ; //An intermediate value used in the application of the
				 //nonlinear operator on the coarse grid
	
	//Restrict the fine grid approximation to the coarse grid
	restrict_approx( fInterp, fVars->lM, cVars->lM ) ;
	
	//Restrict the pressure to the coarse grid
	restrict_const_el_func( fGrid, cGrid, fVars->press, cVars->press ) ;
	
	//Restrict the value of the previous volumetric wetness
	restrict_const_el_func( fGrid, cGrid, fVars->prevTheta,
		cVars->prevTheta ) ;
	
	//Recover the actual value of the pressure
	recover_pressure( params, data, cGrid, cVars ) ;
	
	//Restrict the residual to the coarse grid right hand side
	restrict_fas_resid( fInterp, fVars->resid, cVars->rhs ) ;
	
	//Apply the operator on the coarse grid and update the value of the right
	//hand side
	//Loop over the elements on the grid
	for( elCnt = 0 ; elCnt < cGrid->numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(cGrid->els[elCnt]) ;
		//Calculate the hydraulic conductivity on the current element
		hCond = calc_hydr_cond( data, el->soilType, 
			calc_theta( data, el->soilType, cVars->press[elCnt] ) ) ;
			
		//Loop over the edges on the element
		for( i=0 ; i < 3 ; i++ ){
			//Get a handle to the current edge
			edge = el->edges[i] ;
			//Check that we are not on the boundary
			if( edge->type == EDGE_KNOWN ) continue ;
			val = 0.0 ;
			//Set the index into the local stiffness matrix
			ind = 3*i ;
			//Loop through the edges on the element
			for( j=0 ; j < 3 ; j++ ){
				val += el->invStiffMat[ind++] * ( cVars->press[elCnt] -
					cVars->lM[ el->edges[j]->id ] - el->zfacts[j] ) ;
			}
			//Update the value of the residual at the current point
			cVars->rhs[ edge->id ] += hCond * val ;
		}
	}
}
















