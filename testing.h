#ifndef _TESTING_H
#define _TESTING_H

#include "definitions.h"

//The entry point for testing modules
void test( Params *params, REData *data ) ;

#ifndef NDEBUG
//tests that a sparse matrix is set up on a grid with the correct structure of
//non-zero entries
void test_sparse_matrix_grid_struct( Params *params ) ;

//Tests that the z-coordinate opposite an edge on an element is obtained
//correctly
void test_get_z_op( Params *params ) ;

//Tests that the edges are set up appropriately on a grid so that they are
//ordered on the grid in terms of geometric position, but in the unknowns in
//terms of whether they are known or unknown. This prints out information
//about every edge on the grid, so it should be done only for a small grid
void test_edge_set_up( Params *params ) ;

//Test that when the grid is set up the correct elements are assigned to each
//edge
void test_edge_parent_els( Params *params ) ;

//Test that the edges that are pointed to by a fine grid edge correspond to the
//parent edges on the coarse grid
void test_edge_parent_edges( Params *params ) ;

//Test that the restriction operator is set up properly given a fine and coarse
//grid
void test_prolong_mat_set_up( Params *params ) ;

//Tests that the restriction and prolongation operators between neighbouring
//grids is set up properly
void test_restrict_prolong_set_up( Params *params ) ;

//Tests that the Galerkin coarse grid operator is calculated correctly
void test_galerkin_coarse_grid_calc( Params *params ) ;

//Tests that the Galerkin coarse grid operator is calcualted correctly in the
//case that the entries not in the sparsity pattern are dropped
void test_galerkin_coarse_grid_calc_drop_entries( Params *params ) ;

//Tests that the sparse matrix-vector multiplication is performed correctly
void test_sparse_mat_vec_mult( Params *params ) ;

//Tests that the calculation of the linear residual is performed correctly
void test_calc_lin_resid( Params *params ) ;

//Tests that the soil parameters are read appropriately from file
void test_set_up_soil_data( REData *data ) ;

//Tests that the calculation of the derivative of the hydraulic conductivity is
//performed correctly
void test_hydr_cond_deriv_calc( Params *params, REData *data ) ;

//Tests that the calculation of the volumetric wetness and its derivative is
//performed correctly
void test_theta_deriv_calc( Params *params, REData *data ) ;

//Checks that when the hydraulic conductivity and the volumetric wetness are
//calculated simultaneously that they are calculated correctly
void test_calc_hydr_cond_vol_wet( Params *params, REData *data ) ;

//Tests that when the hydraulic conductivity, the volumetric wetness and their
//derivatives are calculated simultaneously that this gives the same as when
//they are calculated individually
void test_calc_hydr_cond_vol_wet_deriv( Params *params, REData *data ) ;

//Tests that the derivative of the local function (defined in Bause and Knabner)
//on the pressure on an element is calculated properly
void test_el_press_func_deriv( Params *params, REData *data ) ;

//Tests that the derivative of the local function (defined in Bause and Knabner)
//on the pressure on an element, is calculated properly when the values are
//calculated simultaneously
void test_el_press_func_and_deriv( Params *params, REData *data ) ;

//Tests that a piecewise constant function on a grid is output appropriately to
//a vtk format
void test_write_element_func_to_file( Params *params ) ;

//Tests that a piecewise constant function defined on the edges can be written
//to file properly
void test_write_edge_func_to_file( Params *params ) ;

//Tests that a function of pointwise fluxes over the edges of a grid can be
//written to file
void test_write_edge_normal_func_to_file( Params *params ) ;

//Tests that the pressure is solved for appropriately on the grid
void test_recover_pressure( Params *params, REData *data ) ;

//Test that the pressure that is recovered from a steady state solution does not
//change
void test_recover_pressure_steady_state( Params *params, REData *data ) ;

//Tests that a grid hierarchy is read in successfully
void test_read_grid_hierarchy( Params *params, REData *data ) ;

//Tests that the restiction of a function from a fine to a coarse grid works as
//expected
void test_prolong_restrict( Params *params ) ;

//Tests that the inverse of the local stiffness matrix is calculated correctly
void test_calc_loc_stiff_mat_inv( Params *params ) ;

//Tests that the inverses of the local stiffness matrices are set up when a grid
//is read in from file
void test_loc_mat_inv_grid_setup( Params *params ) ;

//Writes a 3x3 matrix to console
void write_3x3_mat_to_console( const double *mat ) ;

//Tests that the local function defining the relationship between the pressure
//and the Lagrange multipliers is calculated properly if values are calculated
//before calling the method
void test_calc_el_press_func_deriv_with_data( Params *params, REData *data ) ;

//Tests that the index into the global matrix is returned given the row and 
//column number
void test_get_mat_ind( Params *params, REData *data ) ;

//Tests that the residual and the Jacobian matrix are calculated correctly for 
//the Richards equation
void test_calc_richards_resid_jac( Params *params, REData *data ) ;

//Writes a sparse matrix out to file in full matrix form for inspection. This is
//useful for debugging purposes
void write_sparse_matrix_to_file( const char *fName, const SparseMatrix *mat ) ;

//Tests that the hierarchy of linear variables can be set up on a hierarchy of
//grids
void test_setup_lin_vars_hierarchy( Params *params, REData *data ) ;

//Tests that the hierarchy of interpolation operators is set up properly on a
//hierarchy of grids
void test_setup_interp_hierarchy( Params *params, REData *data ) ;

//Useful for writing out debugging information regarding a hierarchy of 
//interpolation operators
void write_interp_hierarchy_info_to_console( const Params *params,
	Grid **gridHierarchy, Interpolation **interpHierarchy ) ;
	
//Useful for writing out debugging information regarding a hierarchy of
//variables required to perform a linear solve
void write_lin_approx_hierarchy_info_to_console( const Params *params,
	Grid **gridHierarchy, LinApproxVars **linHierarchy ) ;
	
//useful for writing debugging information regarding a hierarchy of grids
void write_grid_hierarchy_info_to_console( const Params *params,
	Grid **gridHierarchy ) ;
	
//Tests that the appropriate memory is set for the variables required to perform
//a Newton-Multigrid iteration
void test_setup_newton_ctxt( Params *params, REData *data ) ;

//Tests that the residual and the Jacobian matrix are set up properly when they
//are populated simultaneously
void test_calc_richards_resid_and_jac( Params *params, REData *data ) ;

//Tests that a Newton context is properly initialised
void test_set_newton_ctxt( Params *params, REData *data ) ;

//Writes out the first 'numRows' rows of a sparse matrix. This is useful for
//debugging purposes
void write_sparse_matrix_rows_to_console( const SparseMatrix *mat,
	unsigned int numRows ) ;
	
//Tests that the initial data is appropriately set for the Richards equation
void test_set_richards_initial_data( Params *params, REData *data ) ;

//Tests that a Newton method with linear smoothing applied will converge for a
//Richards equation type problem
void test_newton_smooth( Params *params, REData *data ) ;

//Tests that smoothing converges in each Newton iteration
void test_newton_smooth_to_converge( Params *params, REData *data ) ;

//Tests that multigrid can be used as part of a Newton iteration to solve a
//nonlinear system of equations at a single time
void test_single_newton_mg( Params *params, REData *data ) ;

//Tests that the flux is recovered appropriately from the data passed in
void test_recover_flux( Params *params, REData *data ) ;

//Tests that a Newton iteration with smoothing as the linear solver works as
//expected
void test_solve_richards_with_smooths( Params *params, REData *data ) ;

//Tests that a factor required in the calculation of the residual and the
//Jacobian matrix is set up properly
void test_setup_zfact( Params *params, REData *data ) ;

//Tests that when the problem is linear that the Jacobian matrix is the same as
//the operator matrix
void test_linear_jac_set_up( Params *params, REData *data ) ;

//Tests that a bounding box of an element is correctly returned
void test_get_el_bnd_box( Params *params, REData *data ) ;

//Tests that the soil types are allocated correctly. I am working under the
//assumption that the soil geometry is fully resolved by all grids
void test_set_soil_type( Params *params, REData *data ) ;

//Tests that the hierarchy of variables required to solve a Richards equation is
//set up properly
void test_setup_re_vars_hierarchy( Params *params, REData *data ) ;

//Tests that the final rows of a sparse matrix structure can be deleted
void test_delete_sparse_matrix_rows( Params *params, REData *data ) ;

//Tests that the prolongation of a function from a coarse to a fine grid and the
//restriction from a fine to a coarse grid works when using the operators for
//an FAS iteration
void test_fas_prolong_restrict( Params *params ) ;

//Tests that the calculation of the diagonals of the Jacobian matrix is
//performed as expected
void test_calc_jac_diags( Params *params, REData *data ) ;

//Tests that the residual and the Jacobian diagonals calculated together are
//as expected
void test_calc_richards_resid_and_jac_diags( Params *params, REData *data ) ;

//Tests that the nonlinear smoother converges
void test_nonlinear_smooth( Params *params, REData *data ) ;

//Tests that the nonlinear smoother converges
void test_nonlinear_smooth_to_converge( Params *params, REData *data ) ;

//Tests that a piecewise constant function can be restricted from a fine to a
//coarse grid
void test_restrict_pc_func( Params *params, REData *data ) ;

//Tests that the perturbed right hand side for the FAS iteration is calculated
//correctly on a coarse grid
void test_fas_coarse_rhs_calc( Params *params, REData *data ) ;

//Tests that the FAS multigrid iteration is working as expected
void test_fas_multigrid_iteration( Params *params, REData *data ) ;

//Tests that the modified Gram-Schmidt process produces orthogonal vectors
void test_modified_gram_schmidt( Params *params, REData *data ) ;

//Tests that an upper Hessenberg matrix can be transformed into an upper
//triangular matrix
void test_hess_to_upper( Params *params, REData *data ) ;

//Tests that the back-substitution works
void test_back_substitute( Params *params, REData *data ) ;

//Tests that GMRES as a linear iteration will converge
void test_gmres_solve( Params *params, REData *data ) ;

//Tests that a preconditioned GMRES iteration is performed correctly
void test_prec_gmres_solve( Params *params, REData *data ) ;

#endif
#endif






