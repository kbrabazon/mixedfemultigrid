#include "linApprox.h"
#include "sparseMatrix.h"
#include "interpolation.h"
#include "gridHandling.h"

/** Sets the pointers in the LinApproxVars structure to NULL
 *	\param [in,out] linVars	The LinApproxVars structure for which to set the
 *							pointers to NULL
 */
void set_lin_approx_vars_to_null( LinApproxVars *linVars )
{
	linVars->approx = NULL ;
	linVars->rhs = NULL ;
	linVars->resid = NULL ;
	set_sparse_mat_to_null( &(linVars->op) ) ;
}

/** Frees the memory assigned on the heap for the LinApproxVars structure passed
 *	in
 *	\param [in,out] linVars	The LinApproxVars structure for which to free the
 *							memory
 */
void free_lin_approx_vars( LinApproxVars *linVars )
{
	if( linVars->approx != NULL ){
		free( linVars->approx ) ;
		linVars->approx = NULL ;
	}
	if( linVars->rhs != NULL ){
		free( linVars->rhs ) ;
		linVars->rhs = NULL ;
	}
	if( linVars->resid != NULL ){
		free( linVars->resid ) ;
		linVars->resid = NULL ;
	}
	free_sparse_mat( &(linVars->op) ) ;
}

/** Initialises the approximation variables for a linear system of equations on
 *	the grid passed in. This involves setting the appropriate memory for the
 *	member variables
 *	\param [in] grid	The grid on which to initialise the LinApproxVars
 *						structure
 *	\param [out] linVars	The LinApproxVars structure to initialise
 */
void init_lin_approx_vars_on_grid( const Grid *grid, LinApproxVars *linVars )
{
	//Set the memory for the approximation, the residual and the right hand side
	//We also store Dirichlet boundary data in the approximation
	linVars->approx = (double*)calloc( grid->numUnknowns, sizeof( double ) ) ;
	linVars->rhs = (double*)calloc( grid->numUnknowns, sizeof( double ) ) ;
	linVars->resid = (double*)calloc( grid->numUnknowns, sizeof( double ) ) ;
	//Set the sparse matrix structure for the operator
	set_sparse_op_structure( grid, &(linVars->op) ) ;
}

/** Sets the sparse structure for the operator in the linear approximation
 *	variable that is passed in
 *	\param [in] grid	The grid on which the linear operator is to operate
 *  \param [out] linVars	The sparse structure in the linear representation of
 *							the operator is set in this method this method
 */
void set_sparse_op_structure( const Grid *grid, SparseMatrix *mat )
{
	unsigned int elCnt, i, j ; //Loop counters
	int unknownEdges ; //Stores the number of unknown edges on an element
	unsigned int *numNeighbours ; //Stores the number of neighbours of an edge
				//on the grid. This information is used to populate the column
				//and row information
	unsigned int rowInd ; //The index into the global array for the start of a
				//row in the matrix
	unsigned int edgeInd ; //The index of an edge in the unknown array
	
	const Element *el ; //An element on the grid
	
	//Check that that matrix being passed in is NULL, as we assign the memory
	//for this in this method
	assert( mat->rowStartInds == NULL ) ;
	assert( mat->colNums == NULL ) ;
	assert( mat->globalEntry == NULL ) ;
	
	//Set the dimensions of the matrix
	mat->numRows = grid->numUnknowns ;
	mat->numCols = grid->numUnknowns ;
	
	//Assign the appropriate amount of memory for the vector of row start
	//indices
	mat->rowStartInds = (unsigned int*)calloc( mat->numRows + 1,
		sizeof( unsigned int ) ) ;
	numNeighbours = (unsigned int*)calloc( mat->numRows,
		sizeof(unsigned int) ) ;
	
	//Loop over the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a placeholder to the current element
		el = &(grid->els[elCnt]) ;
		//Loop over the edges on the element
		unknownEdges = -1 ;
		for( i = 0 ; i < 3 ; i++ ){
			if( el->edges[i]->type == EDGE_UNKNOWN ) unknownEdges++ ;
		}
		//Loop over the edges on the element again
		for( i=0 ; i < 3 ; i++ ){
			if( el->edges[i]->type == EDGE_UNKNOWN )
				numNeighbours[ el->edges[i]->id ] += unknownEdges ;
		}
	}
	
	//Now we have the number of entries in each row of the matrix
	//Set the row start indices for each of the rows in the matrix
	mat->numNonZeros = 0 ;
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		//Set the index of the row to start on to be the current total number of
		//non-zero entries in the matrix
		mat->rowStartInds[ i ] = mat->numNonZeros ;
		//Increment the number of non-zero entries in the matrix by the number
		//of non-zero entries for the current row
		mat->numNonZeros += numNeighbours[i]+1 ;
	}
	//Set the final row start index
	mat->rowStartInds[ grid->numUnknowns ] = mat->numNonZeros ;
	
	//Set the memory for the matrix of non-zero values
	mat->globalEntry = (double*)calloc( mat->numNonZeros, sizeof( double ) ) ;
	mat->colNums = (unsigned int*)calloc( mat->numNonZeros,
		sizeof( unsigned int ) ) ;
		
	//Set the column number of the first column to be the diagonal entry
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		mat->colNums[ mat->rowStartInds[i] ] = i ;
		numNeighbours[i] = 1 ;
	}
		
	//Loop over the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a placeholder to the current element
		el = &(grid->els[elCnt]) ;
		//Loop over the edges on the element
		for( i=0 ; i < 3 ; i++ ){
			if( el->edges[i]->type == EDGE_UNKNOWN ){
				edgeInd = el->edges[i]->id ;
				rowInd = mat->rowStartInds[ edgeInd ] ;
				//Loop over the remaining edges
				for( j=1 ; j < 3 ; j++ ){
					if( el->edges[(i+j)%3]->type == EDGE_UNKNOWN ){
						mat->colNums[rowInd + numNeighbours[edgeInd]++] =
							el->edges[(i+j)%3]->id ;
					}
				}
			}
		}
	}
	
	//Clean up
	free( numNeighbours ) ;
}

/** Calculates the coarse grid operator as the product \f$RAP\f$ where \f$R\f$
 *	is the restriction operator, \f$A\f$ is the linear operator on the fine grid
 *	and \f$P\f$ is the prolongation operator from the coarse to the fine grid.
 *	It is assumed that the memory has already been set for the linear operators
 *	\param [in] fOp	The linear operator on the fine grid
 *	\param [in] r	The restriction operator from the fine to coarse grid
 *	\param [in] p	The prolongation operator from the coarse to the fine grid.
 *					This is simply the transpose of the restriction operator
 *	\param [out] cOp	The linear operator on the coarse grid. This is
 *						populated in this method
 */
void calculate_coarse_grid_op( const SparseMatrix *fOp, const SparseMatrix *r,
	const SparseMatrix *p, SparseMatrix *cOp )
{
	unsigned int i, j, k, l, m ; //Loop counters
	unsigned int nonZeroCols[30] ; //Stores the indices of columns that are
								   //non-zero for a particular row
	unsigned int numCols ; //Counts the number of non-zero columns for a
						   //particular row
	unsigned int rowLength ; //The length of a row in the coarse operator
	unsigned int cInd ; //Index into the coarse operator global array
	unsigned int rCol ; //The current column in the restriction matrix
	unsigned int fOpCol ; //The current column in the fine grid operator matrix
	double *rowVals ; //The values for a row in the coarse grid operator
	bool *flag ; //Sets a flag to show if a column has been encountered or not
								 
	//Set the otherCols to be zero initially
	for( i=0 ; i < 30 ; i++ )
		nonZeroCols[i] = 0 ;
		
	//Reset the values in the coarse operator to be zero
	for( i=0 ; i < cOp->numNonZeros ; i++ )
		cOp->globalEntry[i] = 0 ;
		
	//Set the memory for the rowVals. This should be the size of the number of
	//rows in the restriction matrix (i.e. the number of unknowns on the coarse
	//grid
	rowVals = (double*)calloc( r->numRows, sizeof( double ) ) ;
	flag = (bool*)calloc( r->numRows, sizeof( bool ) ) ;
	
	//The matrix multiplication is performed from left to right in the case of
	//sparse matrices
	//Loop through the rows of the restriction matrix
	for( i=0 ; i < r->numRows ; i++ ){
		//Set the number of other columns to zero intially
		numCols = 0 ;
		//Add the columns in the sparsity pattern for the coarse operator to the
		//list of non-zero columns
		for( j=cOp->rowStartInds[i] ; j < cOp->rowStartInds[i+1] ; j++ ){
			flag[cOp->colNums[j]] = true ;
			nonZeroCols[numCols++] = cOp->colNums[j] ;
		}
		//Loop through the non-zero columns in the current row
		for( k=r->rowStartInds[i] ; k < r->rowStartInds[i+1] ; k++ ){
			//Store the column number in the restriction matrix
			rCol = r->colNums[k] ;
			//Loop through the columns in row 'restrictCol' in the fine operator
			for( l=fOp->rowStartInds[rCol] ;
				l < fOp->rowStartInds[rCol+1] ; l++ ){
				//Store the column number in the fine grid operator
				fOpCol = fOp->colNums[l] ;
				//Loop through the row 'fOpCol' in the prolongation matrix
				for( m=p->rowStartInds[fOpCol] ;
					m < p->rowStartInds[fOpCol+1] ; m++ ){
					//Store the column number in the prolongation matrix
					j = p->colNums[m] ;
					//Add a contribution to column j in the current row
					rowVals[j] += r->globalEntry[k] * fOp->globalEntry[l] *
						p->globalEntry[m] ;
					if( !flag[j] ){
						flag[j] = true ;
						nonZeroCols[numCols++] = j ;
					}
				}
			}
		}
		//Add the contribution calculated to the coarse operator. We note that
		//the sparsity pattern will not allow all non-zero columns to be added.
		//Any entry outside the sparsity pattern gets added to the diagonal
		//entry
		//First add the entries that are in the sparsity pattern
		cInd = cOp->rowStartInds[i] ;
		rowLength = cOp->rowStartInds[i+1] - cInd ;
		for( j=0 ; j < rowLength ; j++, cInd++ ){
			cOp->globalEntry[cInd] += rowVals[nonZeroCols[j]] ;
			rowVals[nonZeroCols[j]] = 0 ;
			flag[ nonZeroCols[j] ] = false ;
		}
		cInd = cOp->rowStartInds[i] ;
		for( j=rowLength ; j < numCols ; j++ ){
			cOp->globalEntry[cInd] += rowVals[nonZeroCols[j]] ;
			rowVals[ nonZeroCols[j] ] = 0 ;
			flag[ nonZeroCols[j] ] = false ; 
		}
	}
	
	//Free the memory assigned on the heap
	free( rowVals ) ;
	free( flag ) ;
}

/** Calculates the Galerkin coarse grid operator, where the entries outside of
 *	the desired sparsity pattern are dropped, rather than added to the diagonal
 *	of the matrix
 *	\param [in] fOp	The linear operator on the fine grid
 *	\param [in] r	The restriction operator from the fine to coarse grid
 *	\param [in] p	The prolongation operator from the coarse to the fine grid.
 *					This is simply the (scaled) transpose of the restriction
 *					operator
 *	\param [out] cOp	The linear operator on the coarse grid. This is
 *						populated in this method
 */
void calculate_coarse_grid_op_drop_entries( const SparseMatrix *fOp,
	const SparseMatrix *r, const SparseMatrix *p, SparseMatrix *cOp )
{
	unsigned int i, j, k, l, m ; //Loop counters
	unsigned int nonZeroCols[30] ; //Stores the indices of columns that are
								   //non-zero for a particular row
	unsigned int numCols ; //Counts the number of non-zero columns for a
						   //particular row
	unsigned int rowLength ; //The length of a row in the coarse operator
	unsigned int cInd ; //Index into the coarse operator global array
	unsigned int rCol ; //The current column in the restriction matrix
	unsigned int fOpCol ; //The current column in the fine grid operator matrix
	double *rowVals ; //The values for a row in the coarse grid operator
	bool *flag ; //Sets a flag to show if a column has been encountered or not
								 
	//Set the otherCols to be zero initially
	for( i=0 ; i < 30 ; i++ )
		nonZeroCols[i] = 0 ;
		
	//Reset the values in the coarse operator to be zero
	for( i=0 ; i < cOp->numNonZeros ; i++ )
		cOp->globalEntry[i] = 0 ;
		
	//Set the memory for the rowVals. This should be the size of the number of
	//rows in the restriction matrix (i.e. the number of unknowns on the coarse
	//grid
	rowVals = (double*)calloc( r->numRows, sizeof( double ) ) ;
	flag = (bool*)calloc( r->numRows, sizeof( bool ) ) ;
	
	//The matrix multiplication is performed from left to right in the case of
	//sparse matrices
	//Loop through the rows of the restriction matrix
	for( i=0 ; i < r->numRows ; i++ ){
		//Set the number of other columns to zero intially
		numCols = 0 ;
		//Add the columns in the sparsity pattern for the coarse operator to the
		//list of non-zero columns
		for( j=cOp->rowStartInds[i] ; j < cOp->rowStartInds[i+1] ; j++ ){
			flag[cOp->colNums[j]] = true ;
			nonZeroCols[numCols++] = cOp->colNums[j] ;
		}
		//Loop through the non-zero columns in the current row
		for( k=r->rowStartInds[i] ; k < r->rowStartInds[i+1] ; k++ ){
			//Store the column number in the restriction matrix
			rCol = r->colNums[k] ;
			//Loop through the columns in row 'restrictCol' in the fine operator
			for( l=fOp->rowStartInds[rCol] ;
				l < fOp->rowStartInds[rCol+1] ; l++ ){
				//Store the column number in the fine grid operator
				fOpCol = fOp->colNums[l] ;
				//Loop through the row 'fOpCol' in the prolongation matrix
				for( m=p->rowStartInds[fOpCol] ;
					m < p->rowStartInds[fOpCol+1] ; m++ ){
					//Store the column number in the prolongation matrix
					j = p->colNums[m] ;
					//Add a contribution to column j in the current row
					rowVals[j] += r->globalEntry[k] * fOp->globalEntry[l] *
						p->globalEntry[m] ;
					if( !flag[j] ){
						flag[j] = true ;
						nonZeroCols[numCols++] = j ;
					}
				}
			}
		}
		//Add the contribution calculated to the coarse operator. We note that
		//the sparsity pattern will not allow all non-zero columns to be added.
		//Any entry outside the sparsity pattern gets added to the diagonal
		//entry
		//First add the entries that are in the sparsity pattern
		cInd = cOp->rowStartInds[i] ;
		rowLength = cOp->rowStartInds[i+1] - cInd ;
		for( j=0 ; j < rowLength ; j++, cInd++ ){
			cOp->globalEntry[cInd] += rowVals[nonZeroCols[j]] ;
			rowVals[nonZeroCols[j]] = 0 ;
			flag[ nonZeroCols[j] ] = false ;
		}

//		cInd = cOp->rowStartInds[i] ;
		for( j=rowLength ; j < numCols ; j++ ){
//			cOp->globalEntry[cInd] += rowVals[nonZeroCols[j]] ;
			rowVals[ nonZeroCols[j] ] = 0 ;
			flag[ nonZeroCols[j] ] = false ; 
		}
	}
	
	//Free the memory assigned on the heap
	free( rowVals ) ;
	free( flag ) ;
}

/** Calculates the linear residual
 *	\param [in,out] vars 	Variables associated with the approximation on the
 *							current grid. The residual is calculated as the
 *							right hand side LinApproxVars::rhs minus the
 *							matrix-vector product of LinApproxVars::op and
 *							the approximation LinApproxVars::approx
 */
void calculate_lin_resid( LinApproxVars *vars )
{
	unsigned int i ;
	//Perform the sparse matrix-vector product of the operator and the
	//approximation
	sparse_matrix_vec_mult( &(vars->op), vars->approx, vars->resid ) ;
	
	//Update the residual in place
	for( i=0 ; i < vars->op.numRows ; i++ )
		vars->resid[i] = vars->rhs[i] - vars->resid[i] ;
}

/** Performs linear smoothing. Depending on the values in params this decides
 *	what type of smoothing should be performed
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numSmooths	The number of smoothing iterations to perform
 *	\param [in,out] vars	Variables associated with the approximation on the
 *							current grid. The variable LinApproxVars::approx is
 *							the desired output of this method
 */
void lin_smooth( const Params * params, unsigned int numSmooths,
	LinApproxVars * vars )
{
	//Perform a switch on the type of smoothing operator given
	switch( params->smoother ){
		case( SMOOTH_JAC ):
			lin_jac( (Params*)params, numSmooths, vars ) ;
			break ;
		case( SMOOTH_GS ):
			lin_gs( params, numSmooths, vars ) ;
			break ;
		case( SMOOTH_SYM_GS ):
			lin_sym_gs( params, numSmooths, vars ) ;
			break ;
		default:
			printf( "Unknown smoothing iteration given\n" ) ;
	}
}

/** Performs a linear Gauss-Seidel iteration for the number of iterations
 *	specified
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numSmooths	The number of smoothing iterations to perform
 *	\param [in, out] vars	Variables required for the approximation of a linear
 *							system of equations. The variable
 *							LinApproxVars::approx is the desired output of this
 *							method
 */
void lin_gs( const Params * params, unsigned int numSmooths,
	LinApproxVars *vars )
{
	unsigned int i, j, smoothCnt ; //Loop counter
	const SparseMatrix *mat ; //Placeholder for the linear operator
	double val ; //Stores an intermediate value in the application of the
				 //smoothing operator
	
	//Set a placeholder for the linear operator
	mat = &(vars->op) ;
	
	//Perform the specified number of smooths
	for( smoothCnt = 0 ; smoothCnt < numSmooths ; smoothCnt++ ){
		//Loop through the rows in the linear operator
		for( i=0 ; i < mat->numRows ; i++ ){
			val = 0 ;
			//Loop through the columns in the current row and calculate the
			//contribution to the residual
			for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ )
				val += mat->globalEntry[j] * vars->approx[ mat->colNums[j] ] ;
			val = vars->approx[i] + (vars->rhs[i] - val) /
				mat->globalEntry[ mat->rowStartInds[i] ] ;
			//Update the value in the current row of the approximation
			vars->approx[i] = (1-params->smoothWeight) * vars->approx[i] +
				params->smoothWeight * val ;
		}
	}
}

/** Performs a linear symmetric Gauss-Seidel iteration for the number of
 *	iterations specified
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numSmooths	The number of smoothing iterations to perform
 *	\param [in, out] vars	Variables required for the approximation of a linear
 *							system of equations. The variable
 *							LinApproxVars::approx is the desired output of this
 *							method
 */
void lin_sym_gs( const Params * params, unsigned int numSmooths,
	LinApproxVars * vars )
{
	unsigned int i, j, smoothCnt ; //Loop counters
	unsigned int ind ; //The index of a row in the matrix
	const SparseMatrix *mat ; //Placeholder for the linear operator
	double val ; //Stores an intermediate value in the application of the
				 //smoothing operator
				 
	//Set a placeholder for the linear operator
	mat = &(vars->op) ;
	
	//Perform the specified number of smooths
	for( smoothCnt = 0 ; smoothCnt < numSmooths ; smoothCnt++ ){
		//Loop through the rows in the linear operator
		for( i=0 ; i < mat->numRows ; i++ ){
			val = 0 ;
			//Loop through the columns in the current row and calculate the 
			//contribution to the residual
			for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ )
				val += mat->globalEntry[j] * vars->approx[ mat->colNums[j] ] ;
			val = vars->approx[i] + (vars->rhs[i] - val) /
				mat->globalEntry[ mat->rowStartInds[i] ] ;
			//Update the value in the current row of the approximation
			vars->approx[i] = (1-params->smoothWeight) * vars->approx[i] +
				params->smoothWeight * val ;
		}
		//Loop through the rows in reverse order
		for( i=0 ; i < mat->numRows ; i++ ){
			val = 0 ;
			ind = mat->numRows - 1 - i ;
			//Loop through the columns in the current row and calculate the
			//contribution to the residual
			for( j = mat->rowStartInds[ ind ] ; j < mat->rowStartInds[ ind+1 ] ;
				j++ )
				val += mat->globalEntry[j] * vars->approx[ mat->colNums[j] ] ;
			val = vars->approx[ind] + (vars->rhs[ind] - val) /
				mat->globalEntry[ mat->rowStartInds[ind] ] ;
			//Update the value in the current row of the approximation
			vars->approx[ind] = (1-params->smoothWeight) * vars->approx[ind] +
				params->smoothWeight * val ;
		}
	}
}

/** Performs a linear Jacobi iteration the specified number of times
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numSmooths	The number of smoothing iterations to perform
 *	\param [out] vars	Variables required for the solution of a linear system
 *						of equations. The variable LinApproxVars::approx is the
 *						desired output from this function
 */
void lin_jac( Params *params, unsigned int numSmooths,
	LinApproxVars * vars )
{
	unsigned int i, j, smoothCnt ; //Loop counters
	double *newApprox ; //Pointer for the new values calculated using the Jacobi
						//iteration
	const SparseMatrix *mat ; //The matrix representing the linear operator
	double val ; //Stores an intermediate value in the application of the
				 //smoothing operator
		
	//Make sure that the memory has been assigned to store the new approximation				
	assert( params->sparePtr != NULL ) ;

	newApprox = params->sparePtr ;
	mat = &(vars->op) ;
	//Perform the specified number of smooths
	for( smoothCnt = 0 ; smoothCnt < numSmooths ; smoothCnt++ ){
		//Loop through the rows of the matrix
		for( i=0 ; i < mat->numRows ; i++ ){
			val = 0 ;
			//Loop through the non-zero entries in the row and calculate the
			//contribution to the residual
			for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ )
				val += mat->globalEntry[j] * vars->approx[ mat->colNums[j] ] ;
			val = vars->approx[i] + (vars->rhs[i] - val) /
				mat->globalEntry[ mat->rowStartInds[ i ] ] ;
			//Calculate the new value in the approximation
			newApprox[i] = (1-params->smoothWeight) * vars->approx[i] +
				params->smoothWeight * val ;
		}
		//Copy the new approximation into the previous one
		copy_dvec( mat->numRows, newApprox, vars->approx ) ;
	}
}

/** Sets up the structures required to solve a linear problem on each grid in
 *	the grid hierarchy
 *	\param [in] params	Parameters defining how the algorithm is to be exectued
 *	\param [in] gridHierarchy	The hierarchy of grids on which a linear problem
 *								needs to be solved
 *	\return	Pointer to a list of LinApproxVars structures which are set up to
 *			solve a linear problem on each of the grids in the hierarchy
 */
LinApproxVars** set_lin_approx_hierarchy( const Params * params,
	Grid **gridHierarchy )
{
	unsigned int i ; //Loop counter
	LinApproxVars **hierarchy ; //The hierarchy of LinApproxVars to return
	
	//Make sure that the grids have been initialised
	assert( gridHierarchy != NULL ) ;
	
	//Set the memory for the hierarchy
	hierarchy = (LinApproxVars**)calloc( params->fGridLevel+1,
		sizeof( LinApproxVars* ) ) ;
		
	//For any non-assigned grid set the corresponding pointer to NULL
	for( i=0 ; i < params->cGridLevel ; i++ )
		hierarchy[i] = NULL ;
		
	//For each grid set up the structure of the linear approximation variables
	//on that grid
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		//Assign the appropriate memory and initialise the structure to NULL.
		//This will make errors more clear if any part of the assignment is not
		//done properly
		hierarchy[i] = (LinApproxVars*)calloc( 1, sizeof( LinApproxVars ) ) ;
		set_lin_approx_vars_to_null( hierarchy[i] ) ;
		//Initialise the memory for the approximation variables using the grid
		init_lin_approx_vars_on_grid( gridHierarchy[i], hierarchy[i] ) ;
	}
	
	return hierarchy ;
}

/** Frees the memory assigned on the heap for the hierarchy of variables
 *	required for the solution of linear problems on a hierarchy of grids
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] hierarchy	The hierarchy to be freed
 *	\return	NULL pointer, as all the memory will have been unassigned
 */
LinApproxVars** free_lin_approx_hierarchy( const Params *params,
	LinApproxVars **hierarchy )
{
	unsigned int i ; //Loop counter
	
	//If there is no memory to unassign return immediately
	if( hierarchy == NULL ) return NULL ;
	
	//Free the memory for each item in the hierarchy
	for( i = params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		if( hierarchy[i] != NULL ){
			free_lin_approx_vars( hierarchy[i] ) ;
			free( hierarchy[i] ) ;
			hierarchy[i] = NULL ;
		}
	}
	
	//Now free the list of pointers
	free( hierarchy ) ;
	hierarchy = NULL ;
	return NULL ;
}


/** Performs linear smoothing until some convergence criterion is met
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] vars	Variables required for the solution of a linear
 *							problem. The desired output of this method is the
 *							variable LinApproxVars::approx
 */
void lin_smooth_to_converge( const Params * params,
	LinApproxVars * vars )
{
	unsigned int numSmooths = 10 ; //The number of smooths to perform before
							  	   //re-checking the residual
	unsigned int smoothCnt = 0 ; //Counts the number of smoothing iterations
								 //performed
	double residNorm ; //The discrete L2 norm of the residual
	double origResidNorm ; //The original L2 norm of the residual
	
	//Calculate the residual and the residual norm
	calculate_lin_resid( vars ) ;
	origResidNorm = residNorm = discrete_l2_norm( vars->op.numRows,
		vars->resid ) ;
	
	//Perform smoothing until some convergence criterion is met
	while( residNorm > LIN_TOL && smoothCnt < MAX_LIN_SMOOTHS ){
//	while( residNorm / origResidNorm > 1e-7 && smoothCnt < MAX_LIN_SMOOTHS ){
		//Perform a number of smoothing iterations
		lin_smooth( params, numSmooths, vars ) ;
		
		//Calculate the residual and the residual norm
		calculate_lin_resid( vars ) ;
		residNorm = discrete_l2_norm( vars->op.numRows, vars->resid ) ;
		
		//Increase the count of the number of smoothing iterations performed
		smoothCnt += numSmooths ;
	}
}

/** Performs a linear multigrid iteration. The exact execution depends on the
 *	parameters passed in
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] gridHierarchy	The hierarchy of grids on which multigrid is to
 *								be performed
 *	\param [in,out] linHierarchy	Variables used to solve the linear
 *									approximation on each grid level
 *	\param [in]	interpHierarchy	Interpolation operators used to move between
 *								grids
 *	\param [in] currLevel	The current level at which to perform multigrid
 */
void linear_multigrid( const Params *params, Grid **gridHierarchy,
	LinApproxVars **linHierarchy, Interpolation **interpHierarchy,
	unsigned int currLevel )
{
	unsigned int mgCnt ; //Loop counter
	const Grid *fGrid, *cGrid ; //The fine and coarse grids
	const Interpolation *fInterp, *cInterp ; //The fine and coarse interpolation
				//operators
	double *correction ; //The correction term
	LinApproxVars *fVars, *cVars ; //Variables required for the approximation of
				//a linear system on the fine and coarse grid
				
	assert( params->sparePtr != NULL ) ;
				
	//Set a placeholder for the variables required for a linear system on the
	//current grid
	fVars = linHierarchy[ currLevel ] ;
	
	if( currLevel == params->cGridLevel ){
		//Perform smoothing until convergence
		lin_smooth_to_converge( params, fVars ) ;
		return ;
	}
	
	//Set a placeholder for the current grid, the coarse grid and the
	//interpolation operators
	fGrid = gridHierarchy[ currLevel ] ;
	cGrid = gridHierarchy[ currLevel-1 ] ;
	fInterp = interpHierarchy[ currLevel ] ;
	cInterp = interpHierarchy[ currLevel-1 ] ;
	cVars = linHierarchy[ currLevel-1 ] ;
	
	//Perform pre-smoothing on the current grid
	lin_smooth( params, params->preSmooth, fVars ) ;
	
	//Calculate the residual on the current grid
	calculate_lin_resid( fVars ) ;
	
	//Restrict the residual from the fine grid to the coarse grid right hand
	//side
	restrict_resid( fInterp, fVars->resid, cVars->rhs ) ;
	
//	if( currLevel == params->fGridLevel ){
//		print_indent( 2, "Before CGC: %.18lf\n",
//			discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ) ;
//	}
	
	//Set the initial approximation on the coarse grid to zero
	set_dvec_to_zero( cGrid->numUnknowns, cVars->approx ) ;
	
	//Perform multigrid on the next level down
	for( mgCnt = 0 ; mgCnt < params->numMGCycles ; mgCnt++ )
		linear_multigrid( params, gridHierarchy, linHierarchy, interpHierarchy,
			currLevel-1 ) ;
		
	//Prolongate the correction to the current grid
	correction = params->sparePtr ;
	prolong_approx( cInterp, cVars->approx, correction ) ;
	
	//Update the approximation on the current grid
	correct_lin_approx( fGrid->numUnknowns, correction, fVars->approx ) ;
	
//	if( currLevel == params->fGridLevel ){
//		calculate_lin_resid( fVars ) ;
//		print_indent( 2, "After CGC: %.18lf\n",
//			discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ) ;
//	}
	
	//Perform post smoothing
	lin_smooth( params, params->postSmooth, fVars ) ;
}

/** Corrects a function by adding an elementwise contribution from a correction
 *	term
 *	\param [in] length	The length of the approximation to correct
 *	\param [in] correction	The correction term
 *	\param [out] approx	The approximation to correct
 */
void correct_lin_approx( unsigned int length, const double * correction,
	double *approx )
{
	unsigned int i ;
	//Update the approximation
	for( i=0 ; i < length ; i++ )
		approx[i] += correction[i] ;
}

/** Sets the pointers in a least squares context to NULL
 *	\param [out] ctxt	The context for which to set the pointers to NULL
 */
void set_least_squares_ctxt_to_null( LeastSqCtxt *ctxt )
{
	ctxt->hessMat = NULL ;
	ctxt->upTMat = NULL ;
	ctxt->rhs = NULL ;
	//Set the dimension of the problem to zero, as we have nothing to solve at
	//the moment
	ctxt->dim = 0 ;
}

/** Frees the memory assigned on the heap for the least squares context
 *	\param [out] ctxt	The context for which to free the memory
 */
void free_least_squares_ctxt( LeastSqCtxt *ctxt )
{
	unsigned int i ; //Loop counter
	for( i=0 ; i <= MAX_INNER_ITS ; i++ ){
		if( ctxt->hessMat[i] != NULL ){
			free( ctxt->hessMat[i] ) ;
			ctxt->hessMat[i] = NULL ;
		}
	}
	if( ctxt->hessMat != NULL ){
		free( ctxt->hessMat ) ;
		ctxt->hessMat = NULL ;
	}
	
	for( i=0 ; i <= MAX_INNER_ITS ; i++ ){
		if( ctxt->upTMat[i] != NULL ){
			free( ctxt->upTMat[i] ) ;
			ctxt->upTMat[i] = NULL ;
		}
	}
	if( ctxt->upTMat != NULL ){
		free( ctxt->upTMat ) ;
		ctxt->upTMat = NULL ;
	}
	
	if( ctxt->rhs != NULL ){
		free( ctxt->rhs ) ;
		ctxt->rhs = NULL ;
	}
}

/** Assigns the memory on the heap for the least squares context
 *	\param [out] ctxt	The context for which to assign the memory
 */
void init_least_squares_ctxt( LeastSqCtxt *ctxt )
{
	unsigned int i ; //Loop counter
	
	//Check that the memory has not already been assigned
	assert( ctxt->hessMat == NULL ) ;
	
	//Assign the memory on the heap
	ctxt->hessMat = (double**)calloc( MAX_INNER_ITS + 1, sizeof( double* ) ) ;
	for( i=0 ; i <= MAX_INNER_ITS ; i++ ){
		ctxt->hessMat[i] = (double*)calloc( MAX_INNER_ITS, sizeof( double ) ) ;
	}
	ctxt->upTMat = (double**)calloc( MAX_INNER_ITS+1, sizeof( double* ) ) ;
	for( i=0 ; i <= MAX_INNER_ITS ; i++ ){
		ctxt->upTMat[i] = (double*)calloc( MAX_INNER_ITS, sizeof( double ) ) ;
	}
	ctxt->rhs = (double*)calloc( MAX_INNER_ITS + 1, sizeof( double ) ) ;
}

/** Sets the pointers in the GMRES context to NULL
 *	\param [out] ctxt	The structure for which to set the pointers to NULL
 */
void set_gmres_ctxt_to_null( GMRESCtxt *ctxt )
{
	ctxt->basis = NULL ;
	ctxt->size=0 ;
	set_lin_approx_vars_to_null( &(ctxt->vars) ) ;
	set_least_squares_ctxt_to_null( &(ctxt->leastSq) ) ;
	ctxt->residNorm = -1.0 ; 
}

/** Frees the memory assigned on the heap for the GMRES context
 *	\param [out] ctxt	The context for which to free the memory
 */
void free_gmres_ctxt( GMRESCtxt *ctxt )
{
	unsigned int i ; //Loop counter
	for( i=0; i < MAX_INNER_ITS ; i++ ){
		if( ctxt->basis[i] != NULL ){
			free( ctxt->basis[i] ) ;
			ctxt->basis[i] = NULL ;
		}
	}
	if( ctxt->basis != NULL ){
		free( ctxt->basis ) ;
		ctxt->basis = NULL ;
	}
	free_lin_approx_vars( &(ctxt->vars) ) ;
	free_least_squares_ctxt( &(ctxt->leastSq) ) ;
}

/** Sets the appropriate memory necessary to perform a GMRES solve on the given
 *	grid
 *	\param [in] grid	The grid on which to perform a GMRES iteration
 *	\param [out] ctxt	Variables required to perform a GMRES iteration on the
 *						given grid
 */
void setup_gmres_ctxt_on_grid( const Grid *grid, GMRESCtxt *ctxt )
{
	unsigned int i ; //Loop counter
	
	//Set the memory required for the basis
	ctxt->basis = (double**)calloc( MAX_INNER_ITS, sizeof( double* ) ) ;
	for( i=0 ; i < MAX_INNER_ITS ; i++ )
		ctxt->basis[i] = (double*)calloc( grid->numUnknowns,
			sizeof( double ) ) ;
		
	//Set the memory for the variables required for a linear solve on the given
	//grid
	init_lin_approx_vars_on_grid( grid, &(ctxt->vars) ) ;
	
	//Set the memory for the least squares context
	init_least_squares_ctxt( &(ctxt->leastSq) ) ;
}

/** Performs modified Gram-Schmidt to make the vector passed in orthogonal to
 *	the first \p basisSize vectors in the basis
 *	\param [in] basisSize	The number of basis vectors to make the vector
 *							orthogonal to
 *	\param [in] vecLength	The length of the vectors in the basis
 *	\param [out] basis		The vectors in the basis to orthogonalise
 */
void modified_gram_schmidt( unsigned int basisSize, unsigned int vecLength,
	double **basis )
{
	unsigned int i, j, k ; //Loop counters
	double *basisVec ; //A basis vector
	double dotProd ; //The dot product of two vectors
	
	//Normalise the first vector in the basis
	basisVec = basis[0] ;
	normalise_vec( vecLength, basisVec ) ;
	
	//Perform orthogonalisation using modified Gram-Schmidt
	for( i=1 ; i < basisSize ; i++ ){
		//Get the current basis vector
		basisVec = basis[i] ;
		
		//Loop through the other basis vectors and make the current vector
		//orthogonal to them
		for( j=0 ; j < i ; j++ ){
			dotProd = discrete_l2_inner_prod( vecLength, basisVec, basis[j] ) ;
			for( k=0 ; k < vecLength ; k++ )
				basisVec[k] -= dotProd * basis[j][k] ;
		}
		
		//Normalise the vector
		normalise_vec( vecLength, basisVec ) ;
	}
}

/** Performs a GMRES iteration the specified number of times
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to perform GMRES
 *	\param [out] vars	Variables required for a GMRES iteration on the current
 *						grid
 *	\param [in] numIts	The number of GMRES iterations to perform
 */
void gmres_iteration( Params *params, const Grid *grid,	GMRESCtxt *ctxt,
	unsigned int numIts )
{
	unsigned int itCnt, i, j ; //Loop counters
	double *currBasisVec ; //Placeholder for the current basis vector
	double *basisVec ; //Placeholder for a basis vector
	double **lsMat ; //Least squares matrix
	double lsEntry ; //Entry in the least squares matrix
	double val ; //Intermediate value used in the updating of the approximation
	LinApproxVars *vars ; //Placeholder for the variables required to solve a
				//linear system of equations
	
	//Set the number of approximations to zero
	ctxt->size = 0 ;
	vars = &(ctxt->vars) ;
	
	//Calculate the linear residual
	calculate_lin_resid( vars ) ;
	
	//Calculate the norm of the linear residual
	ctxt->residNorm = discrete_l2_norm( grid->numUnknowns, vars->resid ) ;
	
	//Set the first basis vector
	basisVec = ctxt->basis[0] ;
	copy_scaled_dvec( grid->numUnknowns, 1.0 / ctxt->residNorm, vars->resid,
		basisVec ) ;
	//Increment the size of the GMRES system to solve
	ctxt->size++ ;
		
	//Set a placeholder for the matrix used in the least squares problem
	lsMat = ctxt->leastSq.hessMat ;
	
	//Perform the specified number of GMRES iterations
	for( itCnt=1 ; itCnt <= numIts ; itCnt++ ){
		currBasisVec = ctxt->basis[itCnt] ;
		//Set the value of the basis vector to be the most recently calculated
		//basis vector multiplied by the linear operator
		basisVec = ctxt->basis[itCnt-1] ;
		sparse_matrix_vec_mult( &(vars->op), basisVec, currBasisVec ) ;
		
		//Perform orthogonalisation using modified Gram-Schmidt
		for( i=0 ; i < itCnt ; i++ ){
			basisVec = ctxt->basis[i] ;
			//Calculate the inner product between the vectors
			lsEntry = discrete_l2_inner_prod( grid->numUnknowns, currBasisVec,
				basisVec ) ;
			//Set the appropriate entry in the matrix to be used for the least
			//squares problem
			lsMat[i][itCnt-1] = lsEntry ;
			//Take the value calculated from the current approximation
			for( j=0 ; j < grid->numUnknowns ; j++ )
				currBasisVec[j] -= lsEntry * basisVec[j] ;
		}
		
		//Calculate the entry in the matrix to be used for the least squares
		//problem
		lsEntry = discrete_l2_norm( grid->numUnknowns, currBasisVec ) ;
		lsMat[itCnt][itCnt-1] = lsEntry ;
		//Normalise the basis vector
		scale_vec( 1.0 / lsEntry, grid->numUnknowns, currBasisVec ) ;
		ctxt->size++ ;
	}
	//Solve the least squares problem
	ctxt->leastSq.dim = ctxt->size ;
	set_dvec_to_zero( ctxt->leastSq.dim, ctxt->leastSq.rhs ) ;
	ctxt->leastSq.rhs[0] = ctxt->residNorm ;
	solve_least_squares( &(ctxt->leastSq) ) ;
	
	//The residual is the final value in the right hand side vector
	ctxt->residNorm = fabs( ctxt->leastSq.rhs[numIts] ) ;
	
	//Update the approximation
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		val = 0.0 ;
		for( j=0 ; j < numIts ; j++ ){
			vars->approx[i] += ctxt->leastSq.rhs[j] * ctxt->basis[j][i] ;
		}
	}
}

/** Transforms an upper Hessenberg matrix into an upper triangular matrix to be
 *	used as part of a linear least squares problem
 *	\param [in,out] ctxt	Variables required to perform a least squares solve.
 *							The desired output is the member variable
 *							LeastSqCtxt::upTMat
 */
void hess_to_upper( LeastSqCtxt *ctxt )
{
	unsigned int i, j, rowCnt ; //Loop counters
	unsigned int colStart ; //The first non-zero column in a row
	
	double **hessMat ; //The upper Hessenberg matrix
	double **upTMat ; //The upper triangular matrix
	
	double *newRow[2] ; //The new values of a row in the transformation from
						//an upper Hessenberg to an upper triangular matrix
	double rhsUpdate[2] ; //The new values to put into the right hand side
						  //vector in the transformation from an upper
						  //Hessenberg to an upper triangular matrix
						  
	double s, c ; //Values used in a plane rotation to elimnate the sub-diagonal
				  //in a matrix
	double normFact ; //Normalisation factor required to make sure that the
					  //values s and c are members in a rotation matrix (i.e.
					  //that the multiplication by the plane rotation matrix
					  //does not change the norm of the matrix)
	
	hessMat = ctxt->hessMat ;
	upTMat = ctxt->upTMat ;
	
	//Copy the values from the Hessenberg martix into the upper triangular
	//matrix
	//Loop through the rows in the Hessenberg matrix
	for( i=0 ; i < ctxt->dim ; i++ ){
		colStart = (i==0) ? 0 : i-1 ;
		//Loop through the columns in the Hessenberg matrix
		for( j=colStart ; j < ctxt->dim-1 ; j++ )
			upTMat[i][j] = hessMat[i][j] ;
	}
	
	//Set the required memory to store the transformed rows in the matrix
	for( rowCnt=0 ; rowCnt < 2 ; rowCnt++ )
		newRow[rowCnt] = (double*)calloc( ctxt->dim-1, sizeof( double ) ) ;
		
	//Loop through the rows in the Hessenberg matrix
	for( i=0 ; i < ctxt->dim-1 ; i++ ){
		//Calculate the values by which to multiply the rows in the Hessenberg
		//matrix
		s = upTMat[i+1][i] ;
		c = upTMat[i][i] ;
		normFact = sqrt( pow(c,2) + pow(s,2) ) ;
		c /= normFact ;
		s /= normFact ;
		
		//Apply the multiplication of the matrix with the block [c, s; -c, s]
		//on the i^th diagonal to the Hessenberg matrix. We first set the value
		//in the matrix to zero at row i+1, column i
		for( j=i ; j < ctxt->dim-1 ; j++ ){
			newRow[0][j] = upTMat[i][j] * c + upTMat[i+1][j] * s ;
			newRow[1][j] = -upTMat[i][j] * s + upTMat[i+1][j] * c ;
		}
		rhsUpdate[0] = ctxt->rhs[i]*c ;
		rhsUpdate[1] = -ctxt->rhs[i]*s ;
		
		//Update the rows in the matrix
		for( rowCnt=0 ; rowCnt < 2 ; rowCnt++ ){
			ctxt->rhs[i+rowCnt] = rhsUpdate[rowCnt] ;
			//Loop through the columns in the row
			for( j=i ; j < ctxt->dim-1 ; j++ )
				upTMat[i+rowCnt][j] = newRow[rowCnt][j] ;
		}
	}
	
	//Free the memory assigned on the heap
	for( rowCnt=0 ; rowCnt < 2 ; rowCnt++ )
		free( newRow[rowCnt] ) ;
}

/** Performs back substitution of an upper triangular system of equations as
 *	part of a linear least squares solve
 *	\params [in,out] ctxt	Variables required to perform a linear least squares
 *							solve
 */
void back_substitute( LeastSqCtxt *ctxt )
{
	unsigned int i, j ; //Loop counters
	unsigned int numRows ; //The number of rows in the upper triangular matrix
	double val ; //Stores an intermediate value in the back substitution
	
	numRows = ctxt->dim-1 ;
	
	//Loop through the rows in reverse order
	for( i=numRows ; i > 0 ; i-- ){
		val = ctxt->rhs[i-1] ;
		for( j=i ; j < numRows ; j++ )
			val -= ctxt->upTMat[i-1][j] * ctxt->rhs[j] ;
		ctxt->rhs[i-1] = val / ctxt->upTMat[i-1][i-1] ;
	}
}

/** Solves the least squares problem defined by the variables stored in the
 *	structure passed in
 *	\param [in,out] ctxt	Structure containing information required to solve
 *							a least squares problem. The result is stored in the
 *							variable LeastSqCtxt::rhs
 */
void solve_least_squares( LeastSqCtxt *ctxt )
{
	//Transform the upper Hessenberg matrix to an upper triangular matrix
	hess_to_upper( ctxt ) ;
	
	//Perform back substitution
	back_substitute( ctxt ) ;
}

/** Sets the pointers in a preconditioned GMRES context to NULL
 *	\param [out] ctxt	The for which to set the pointers to NULL
 */
void set_prec_gmres_ctxt_to_null( PrecGMRESCtxt *ctxt )
{
	ctxt->size = 0 ;
	ctxt->gridHierarchy = NULL ;
	ctxt->linHierarchy = NULL ;
	ctxt->interpHierarchy = NULL ;
	ctxt->basis = NULL ;
	ctxt->precBasis = NULL ;
	set_least_squares_ctxt_to_null( &(ctxt->leastSq) ) ;
}

/** Frees the memory assigned on the heap for the preconditioned GMRES context
 *	used as part of a Newton iteration
 *	\param [out] ctxt	The structure for which to free the memory on the heap
 */
void free_prec_gmres_ctxt( const Params *params, PrecGMRESCtxt *ctxt )
{
	unsigned int i ; //Loop counter
	
	if( ctxt->gridHierarchy != NULL )
		ctxt->gridHierarchy = free_grid_hierarchy( params,
			ctxt->gridHierarchy ) ;

	if( ctxt->linHierarchy != NULL )
		ctxt->linHierarchy = free_lin_approx_hierarchy( params,
			ctxt->linHierarchy ) ;

	if( ctxt->interpHierarchy != NULL )
		ctxt->interpHierarchy = free_interp_hierarchy( params,
			ctxt->interpHierarchy ) ;
	
	//Free the least squares context
	free_least_squares_ctxt( &(ctxt->leastSq) ) ;
	
	//Free the vectors in the preconditioned basis, unless this has already been
	//done
	if( ctxt->precBasis != NULL ){
		for( i=0 ; i < MAX_INNER_ITS ; i++ ){
			if( ctxt->precBasis[i] != NULL ){
				free( ctxt->precBasis[i] ) ;
				ctxt->precBasis[i] = NULL ;
			}
		}
		free( ctxt->precBasis ) ;
		ctxt->precBasis = NULL ;
	}
	
	//Free the vectors in the basis, unless this has already been done
	if( ctxt->basis != NULL ){
		for( i=0 ; i < MAX_INNER_ITS ; i++ ){
			if( ctxt->basis[i] != NULL ){
				free( ctxt->basis[i] ) ;
				ctxt->basis[i] = NULL ;
			}
		}
		free( ctxt->basis ) ;
		ctxt->basis = NULL ;
	}
	
	if( ctxt->precMatrix != NULL ){
		free( ctxt->precMatrix ) ;
		ctxt->precMatrix = NULL ;
	}
}

/** Sets the memory required to perform a multigrid preconditioned GMRES
 *	iteration
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 *	\param [out] ctxt	Variables required to perform a multigrid preconditioned
 *						GMRES iteration
 */
void setup_mg_prec_gmres_ctxt( const Params *params, const REData *data,
	PrecGMRESCtxt *ctxt )
{
	unsigned int i ; //Loop counter
	const Grid *fGrid ; //The finest grid in the hierarchy of grids to use
	
	//Check that the memory in the current structure has not already been set
	assert( ctxt->basis == NULL ) ;
	
	ctxt->precMatrix = NULL ;
	
	//Set the memory required for the basis and the preconditioned basis
	ctxt->basis = (double**)calloc( MAX_INNER_ITS+1, sizeof( double* ) ) ;
	ctxt->precBasis = (double**)calloc( MAX_INNER_ITS+1,
		sizeof( double * ) ) ;
		
	//Set the memory required for the least squares solve
	init_least_squares_ctxt( &(ctxt->leastSq) ) ;
	
	//Initialise the grid hierarchy
	ctxt->gridHierarchy = read_grid_hierarchy( params, data ) ;
	//Set the linear approximation variables on each grid in the hierarchy
	ctxt->linHierarchy = set_lin_approx_hierarchy( params,
		ctxt->gridHierarchy ) ;
	//Set the interpolation operators between the grids
	ctxt->interpHierarchy = setup_interp_hierarchy( params,
		ctxt->gridHierarchy ) ;
	
	fGrid = ctxt->gridHierarchy[params->fGridLevel] ;
	for( i=0 ; i < MAX_INNER_ITS ; i++ ){
		ctxt->basis[i] = (double*)calloc( fGrid->numUnknowns,
			sizeof( double ) ) ;
		ctxt->precBasis[i] = (double*)calloc( fGrid->numUnknowns,
			sizeof( double ) ) ;
	}
	
	if( params->precSymm ){
		ctxt->precMatrix = (double*)calloc(
			ctxt->linHierarchy[params->fGridLevel]->op.numNonZeros,
			sizeof( double ) ) ;
	}
}

/** Performs a multigrid preconditioned GMRES iteration the specified number of
 *	times
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] ctxt	Variables required to perform a multigrid
 *							preconditioned GMRES iteration. The desired output
 *							from the method is the linear approximation on the
 *							finest grid level
 *	\param [in] numIts	The number of preconditioned GMRES iterations to perform
 */
void preconditioned_gmres_iteration( Params *params, PrecGMRESCtxt *ctxt,
	unsigned int numIts )
{
	unsigned int itCnt, i, j ; //Loop counters
	double *currBasisVec ; //Placeholder for the current basis vector
	double *basisVec ; //Placeholder for a basis vector
	double **lsMat ; //Least squares matrix
	double lsEntry ; //Entry in the least squares matrix
	double val ; //Intermediate value used in the updating of the approximation
	LinApproxVars *vars ; //Placeholder for the variables required to solve a
				//linear system of equations
	const Grid *grid ; //The fine grid on which the GMRES iteration is to be
				//performed
	double *tmpRHS ; //Placeholder for the right hand side vector on the
				//finest grid
	double *approx ; //The approximation to the solution of the linear
				//system of equations
	double *swapPtr = NULL ; //Used to swap pointers
	
	//Set the number of approximations to zero
	ctxt->size = 0 ;
	vars = ctxt->linHierarchy[params->fGridLevel] ;
	grid = ctxt->gridHierarchy[params->fGridLevel] ;
	
	//Calculate the linear residual
	calculate_lin_resid( vars ) ;
	
	//Calculate the norm of the linear residual
	ctxt->residNorm = discrete_l2_norm( grid->numUnknowns, vars->resid ) ;
	
	//Set the first basis vector
	basisVec = ctxt->basis[0] ;
	copy_scaled_dvec( grid->numUnknowns, 1.0 / ctxt->residNorm, vars->resid,
		basisVec ) ;
	//Increment the size of the GMRES system to solve
	ctxt->size++ ;
		
	//Set a placeholder for the matrix used in the least squares problem
	lsMat = ctxt->leastSq.hessMat ;
	
	//Set placeholders for the right hand side and approximation to the linear
	//system of equations
	tmpRHS = vars->rhs ;
	approx = vars->approx ;
	
	//Perform the specified number of GMRES iterations
	for( itCnt=1 ; itCnt <= numIts ; itCnt++ ){
		currBasisVec = ctxt->basis[itCnt] ;
		
		//Set the value of the basis vector to be the most recently calculated
		//basis vector multiplied by the linear operator
		basisVec = ctxt->basis[itCnt-1] ;
		
		//Set the right hand side for the linear system of equations to be the
		//previous basis vector temporarily
		vars->rhs = basisVec ;
		//Set the approximation for the linear system of equations to be the
		//current preconditioned basis vector
		vars->approx = ctxt->precBasis[itCnt-1] ;
		
		//Perform a multigrid iteration
		//Set the initial approximation to zero
		set_dvec_to_zero( grid->numUnknowns, vars->approx ) ;
		
		if( params->precSymm ){
			swapPtr = vars->op.globalEntry ;
			vars->op.globalEntry = ctxt->precMatrix ;
		}
		
		linear_multigrid( params, ctxt->gridHierarchy, ctxt->linHierarchy,
			ctxt->interpHierarchy, params->fGridLevel ) ;
			
		if( params->precSymm )
			vars->op.globalEntry = swapPtr ;
		
		//Apply the operator to the preconditioned vector
		sparse_matrix_vec_mult( &(vars->op), vars->approx, currBasisVec ) ;
		
		//Perform orthogonalisation using modified Gram-Schmidt
		for( i=0 ; i < itCnt ; i++ ){
			basisVec = ctxt->basis[i] ;
			//Calculate the inner product between the vectors
			lsEntry = discrete_l2_inner_prod( grid->numUnknowns, currBasisVec,
				basisVec ) ;
			//Set the appropriate entry in the matrix to be used for the least
			//squares problem
			lsMat[i][itCnt-1] = lsEntry ;
			//Take the value calculated from the current approximation
			for( j=0 ; j < grid->numUnknowns ; j++ )
				currBasisVec[j] -= lsEntry * basisVec[j] ;
		}
		
		//Calculate the entry in the matrix to be used for the least squares
		//problem
		lsEntry = discrete_l2_norm( grid->numUnknowns, currBasisVec ) ;
		lsMat[itCnt][itCnt-1] = lsEntry ;
		//Normalise the basis vector
		scale_vec( 1.0 / lsEntry, grid->numUnknowns, currBasisVec ) ;
		ctxt->size++ ;
	}
	//Solve the least squares problem
	ctxt->leastSq.dim = ctxt->size ;
	set_dvec_to_zero( ctxt->leastSq.dim, ctxt->leastSq.rhs ) ;
	ctxt->leastSq.rhs[0] = ctxt->residNorm ;
	solve_least_squares( &(ctxt->leastSq) ) ;
	
	//The residual is the final value in the right hand side vector
	ctxt->residNorm = fabs( ctxt->leastSq.rhs[numIts] ) ;
	
	//Reset the right hand side of the linear system of equations
	vars->rhs = tmpRHS ;
	vars->approx = approx ;
	
	//Update the approximation
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		val = 0.0 ;
		for( j=0 ; j < numIts ; j++ ){
			vars->approx[i] += ctxt->leastSq.rhs[j] * ctxt->precBasis[j][i] ;
		}
	}
}

/** Sets the pointers in the given context to NULL
 *	\param [out] ctxt	The context for which to set the pointers to NULL
 */
void set_lin_mg_ctxt_to_null( LinMGCtxt *ctxt )
{
	ctxt->gridHierarchy = NULL ;
	ctxt->linHierarchy = NULL ;
	ctxt->interpHierarchy = NULL ;
}

/** Frees the memory assigned on the heap for the linear multigrid context
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] ctxt	The context for which to free the memory assigned on the
 *						heap
 */
void free_lin_mg_ctxt( const Params *params, LinMGCtxt *ctxt )
{
	if( ctxt->gridHierarchy != NULL ){
		ctxt->gridHierarchy = free_grid_hierarchy( params,
			ctxt->gridHierarchy ) ;
	}
	if( ctxt->linHierarchy != NULL ){
		ctxt->linHierarchy = free_lin_approx_hierarchy( params,
			ctxt->linHierarchy ) ;
	}
	if( ctxt->interpHierarchy != NULL ){
		ctxt->interpHierarchy = free_interp_hierarchy( params,
			ctxt->interpHierarchy ) ;
	}
}

/** Sets the required memory for the linear multigrid context to be used as a
 *	solver as part of a Newton iteration
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] ctxt	The context for which the memory is to be set
 */
void setup_lin_mg_ctxt( const Params *params, const REData *data,
	LinMGCtxt *ctxt )
{
	//Check that memory has not yet been set
	assert( ctxt->gridHierarchy == NULL ) ;
	
	//Initialise the grid hierarchy
	ctxt->gridHierarchy = read_grid_hierarchy( params, data ) ;
	
	//Set the linear approximation variables on each grid in the hierarchy
	ctxt->linHierarchy = set_lin_approx_hierarchy( params,
		ctxt->gridHierarchy ) ;
	
	//Set the interpolation operators between the grids
	ctxt->interpHierarchy = setup_interp_hierarchy( params,
		ctxt->gridHierarchy ) ;
}





















