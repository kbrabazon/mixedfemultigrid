#ifndef _INTERPOLATION_H
#define _INTERPOLATION_H

/** \file interpolation.h
 *	\brief Constructs, maintains and destroys sparse matrices to perform
	restriction and prolongation between grids
 */
 
#include "definitions.h"

//Sets the pointers in the Interpolation structure to NULL
void set_interp_ops_to_null( Interpolation *interp ) ;
//Frees the memory allocated on the heap for an Interpolation structure
void free_interp_ops( Interpolation *interp ) ;

//Initialises the prolongation operator given a grid. We assume that the grid is
//the fine grid, and we wish to go to the next coarsest grid
void set_prolong_mat_on_grid( const Grid *fGrid, const Grid *cGrid,
	Interpolation *interp ) ;
	
//Initialises the prolongation operator where rows for the boundary nodes are
//included
void set_prolong_mat_on_full_grid( const Grid *fGrid, const Grid *cGrid,
	Interpolation *interp ) ;
	
//Sets the interpolation matrices on the given fine and coarse grids
void set_restrict_prolong_mat_on_grid( const Grid *fGrid, const Grid *cGrid,
	Interpolation *fInterp, Interpolation *cInterp ) ;

//Sets the interpolation matrices on the given fine and coarse grids for an FAS
//type iteration
void set_fas_restrict_prolong_mat_on_grid( const Grid *fGrid, const Grid *cGrid,
	Interpolation *fInterp, Interpolation *cInterp ) ;

//Sets the scaling factor for the rows in the restriction matrix. These have the
//effect of normalising the entries in a row of the restriction matrix, such
//that they add up to one	
void set_restrict_scale_facts( const SparseMatrix *rMat, double* scaleFacts ) ;

//Scales the rows in the restriction matrix using corresponding factors in the
//vector of scaling factors passed in
void scale_restrict_mat( const double *scaleFacts, SparseMatrix *rMat ) ;
	
//Restricts an approximation of pointwise values from a fine to a coarse grid
void restrict_approx( const Interpolation *fInterp,
	const double *fApprox, double *cApprox ) ;
	
//Restricts a residual, which is an integral over edges, from a fine to a coarse
//grid
void restrict_resid( const Interpolation *fInterp,
	const double *fResid, double *cResid ) ;
	
//Restricts a residual in the case of an FAS iteration. In this iteration we
//require the restricted residual to be weighted to be the correct right hand
//side for the nonlinear problem
void restrict_fas_resid( const Interpolation *fInterp, const double *fResid,
	double *cResid ) ;
	
//Scales each entry in the vector approx by the corresponding entry in the vector
//scaleFacts
void scale_restrict_approx_by_facts( unsigned int length,
	const double *scaleFacts, double *resid ) ;

//Prolongates an approximation of pointwise values from a coarse to a fine grid
void prolong_approx( const Interpolation *cInterp,
	const double *cApprox, double *fApprox ) ;
	
//Sets up a hierarchy of interpolation operators between the grids in a
//hierarchy of grids
Interpolation** setup_interp_hierarchy( const Params *params,
	Grid **gridHierarchy ) ;
	
//Sets up a hierarchy of interpolation operators between the grids in a
//hierarchy of grids. This sets up the operators to take into account Dirichlet
//boundary information
Interpolation** setup_fas_interp_hierarchy( const Params *params,
	Grid **gridHierarchy ) ;
	
//Frees the memory assigned on the heap for the hierarchy of interpolation
//operators passed in
Interpolation** free_interp_hierarchy( const Params *params,
	Interpolation **hierarchy ) ;

#endif
