#include "definitions.h"
#include "string.h"

/** Sets the console output style to bold on a Linux system. As yet undefined on
 *	any other system
 */
void make_output_bold()
{
#ifdef __linux
#if	WRITE_TO_FILE != 1
	printf( "\033[1m" ) ;
	fflush( stdout ) ;
#endif
#endif
}

/** Resets the console output style to the default on a Linux system. As yet
 *	undefined on any other system
 */
void reset_output_style()
{
#ifdef __linux
#if WRITE_TO_FILE != 1
	printf( "\033[0m" ) ;
	fflush( stdout ) ;
#endif
#endif
}

/** Sets the colour of the console output on a Linux system. As yet undefined
 *	on any other system
 *	\param [in] colName	The name of the colour to use. This may be one of "red",
 *						"green", "blue", "magenta" or "cyan". The case must
 *						match
 */
void set_output_colour( const char * colName )
{
#ifdef __linux
#if WRITE_TO_FILE != 1
	if( strcmp( colName, "red" ) == 0 )
		printf( "\033[31m" ) ;
	else if( strcmp( colName, "green" ) == 0 )
		printf( "\033[32m" ) ;
	else if( strcmp( colName, "blue" ) == 0 )
		printf( "\033[34m" ) ;
	else if( strcmp( colName, "magenta" ) == 0 )
		printf( "\033[35m" ) ;
	else if( strcmp( colName, "cyan" ) == 0 )
		printf( "\033[36m" ) ;
	fflush( stdout ) ;
#endif
#endif
}

/** Copies the values from the source vector to the destination vector. It is
 *	not assumed that the vectors do not alias, so that overlapping vectors
 *	may be passed in
 *	\param [in] length	The length of the vectors to copy
 *	\param [in] source	The source vector to copy values from
 *	\param [out] dest	The destination vector to copy values to
 */
void copy_dvec( unsigned int length, const double *source, double *dest )
{
	unsigned int i ; //Loop counter
	
	for( i=0 ; i < length ; i++ )
		dest[i] = source[i] ;
}

/** Scales and then copies the values from the source vector into the
 *	destination vector
 *	\param [in] length	The length of the vectors to copy
 *	\param [in] fact	The factor by which to scale the source vector
 *	\param [in] source	The source vector to copy values from
 *	\param [out] dest	The destination vector to copy values to
 */
void copy_scaled_dvec( unsigned int length, double fact, const double *source,
	double *dest )
{
	unsigned int i ; //Loop counter
	
	for( i=0 ; i < length ; i++ )
		dest[i] = fact * source[i] ;
}

/** Sets the entries in a double vector to zero
 *	\param [in] length	The length of the vector
 *	\param [out] vec	The vector to set to zero
 */
void set_dvec_to_zero( unsigned int length, double *vec )
{
	unsigned int i ; //Loop counter
	
	for( i=0 ; i < length ; i++ )
		vec[i] = 0.0 ;
}

/** Writes output to console indented by a specified amount. This is used to
 *	make output more readable
 *	\param [in] indentLvl	The level of indentation to use
 *	\param [in] string	The string to write to console
 */
void print_indent( unsigned int indentLvl, const char *fms, ... )
{
	unsigned int i ;
	char indentString[] = "  " ;
	va_list argList ;
	
	//Indent the string the given number of times
	for( i=0 ; i < indentLvl ; i++ ){
		printf( "%s", indentString ) ;
	}
	
	//Make the argument list point to the first argument after the format string
	va_start( argList, fms ) ;
	
	//Now call printf with the argument list
	vprintf( fms, argList ) ;
}

/** Sets the string representation of the linear iteration type used in a Newton
 *	iteration
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] str	The string representation of the linear iteration used
 *						as the inner iteration in a Newton iteration
 */
void lin_it_str_rep( const Params *params, char *str )
{
	switch( params->linIt ){
		case( IT_LIN_MG ):
			sprintf( str, "Linear Multigrid" ) ;
			break ;
		case( IT_GMRES ):
			sprintf( str, "GMRES" ) ;
			break ;
		case( IT_PGMRES ):
			sprintf( str, "Multigrid Preconditioned GMRES" ) ;
			break ;
		case( IT_PCG ):
			sprintf( str, "Multigrid Preconditioned CG" ) ;
			break ;
		default:
			sprintf( str, "Unknown linear iteration" ) ;
	}
}







