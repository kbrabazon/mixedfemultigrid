#ifndef _NEWTON_ITERATION_H
#define _NEWTON_ITERATION_H

#include "definitions.h"

//Sets the pointers required for a Newton iteration to NULL
void set_newton_ctxt_to_null( NewtonCtxt *ctxt ) ;

//Frees the memory assigned on the heap for the given context
void free_newton_ctxt( const Params *params, NewtonCtxt *ctxt ) ;

//Sets the memory for the variables in the context storing information necessary
//to perform a Newton-Multigrid iteration
void setup_newton_ctxt( const Params *params, const REData *data,
	NewtonCtxt *ctxt ) ;
	
//Sets the variables that will not change on all grids in a hierarchy for a
//Newton-Multigrid iteration
void set_newton_mg_iteration_vars( const Params *params, const REData *data,
	NewtonCtxt *ctxt, Grid **gridHierarchy, LinApproxVars **linHierarchy,
	Interpolation **interpHierarchy ) ;
	
//Sets the variables required to perform a linear iteration as part of a Newton
//iteration. The functionality depends on the linear iteration being performed
void set_newton_lin_vars( const Params *params, NewtonCtxt *ctxt ) ;
	
//Sets the coase grid operators in the hierarchy using the Galerkin formulation
void set_galerkin_coarse_operators( const Params *params,
	LinApproxVars **linHierarchy, Interpolation **interpHierarchy ) ;
	
//Sets the variables for an initial approximation to the linear Newton systems
//on the given grid
void init_newt_approx_on_grid( const REApproxVars *reVars, const Grid *grid,
	LinApproxVars *linVars ) ;
	
//Performs the linear inner iteration as part of a Newton iteration
void perform_newton_inner_it( Params *params, NewtonCtxt *ctxt ) ;
	
//Performs a Newton iteration for the current time-step of the Richards
//equation
void richards_newton_step( Params *params, const REData *data,
	NewtonCtxt *ctxt ) ;
	
//Returns the grid on which to perform a Newton iteration. This requires
//different functionality depending on the type of linear solve used
Grid* get_newton_grid( const Params *params, const NewtonCtxt *ctxt ) ;

//Returns the variables required to perform a linear solve for a Newton
//iteration
LinApproxVars* get_newton_lin_vars( const Params *params,
	const NewtonCtxt *ctxt ) ;

//Returns the hierarchy of variables required to perform a linear solve on each
//grid in a hierarchy of grids
LinApproxVars** get_newton_lin_hierarchy( const Params *params,
	const NewtonCtxt *ctxt ) ;
	
//Returns the hierarchy of grids used in a Newton iteration, if any
Grid** get_newton_grid_hierarchy( const Params *params,
	const NewtonCtxt *ctxt ) ;
	
//Returns the interpolation operators between grids in a hierarchy used in a
//Newton iteration, if any
Interpolation **get_newton_interp_hierarchy( const Params *params,
	const NewtonCtxt *ctxt ) ;
	
//Performs an adaptive update of the Newton step length using Armijo's rule
//see [Deuflhard 2004, pg. 121]
void update_newton_armijo( Params *params, const REData *data,
	REApproxVars *reVars, NewtonCtxt *ctxt ) ;

#endif
