#include "richardsTimeStepping.h"
#include "newtonIteration.h"
#include "richardsFunctions.h"
#include "params.h"
#include "gridHandling.h"
#include "sparseMatrix.h"
#include "linApprox.h"
#include "fasIteration.h"
#include <time.h>

/** Performs a time dependent solve of the Richards equation
 *	\param [in] params	Parameters defining how the solve should be performed
 *	\param [in] data	Soil characteristics of soils used in the current
 *						problem
 */
void solve_richards_eq( Params *params, REData *data )
{
	if( params->algorithm == ALG_NEWTON ){
		if( params->writeToFile )
			newton_solve_richards_eq_write_info( params, data ) ;
		else
			newton_solve_richards_eq( params, data ) ;
	}
	else{
		if( params->writeToFile )
			fas_multigrid_solve_write_info( params, data ) ;
		else
			fas_multigrid_solve( params, data ) ;
	}
}

/** Performs a time dependent solve of the Richards equation where the nonlinear
 *	solve is performed using a Newton iteration. In this method information,
 *	including the writing of data to file, is performed. This is useful for
 *	debugging, but will not be the quickest to execute
 *	\param [in] params	Parameters defining how the solve should be performed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						approximation
 */
void newton_solve_richards_eq_write_info( Params *params, REData *data )
{
	unsigned int tCnt ; //Loop counter
	NewtonCtxt ctxt ; //Stores data structures necessary to perform a solve
					  //using a Newton iteration
	double *prevPress ; //The pressure at the previous time step
	double *prevLM ; //The Lagrange multipliers at the previous time step
	double *fluxMags ;
	char outFName[30] ;
					  
	clock_t algStart, solveStart, end ; //Rough timing estimates
				
	//Start the timer to also measure the set-up as well as the solve cost
	algStart = clock() ;
				
	//Set the pointers in the data structures to NULL initially
	set_newton_ctxt_to_null( &ctxt ) ;
	
	//Initialise all of the memory required to perform a Newton-Multigrid
	//iteration
	setup_newton_ctxt( params, data, &ctxt ) ;
	
	//Set the memory for an array that may be used when needed in the code. This
	//removes the need for repeated allocation and deallocation of memory
	set_param_spare_memory( ctxt.grid->numEdges, params ) ;
	
	//Set the memory for the magnitudes of the fluxes across the edges
	fluxMags = (double*)calloc( ctxt.grid->numEdges, sizeof( double ) ) ;
	
	//Initialise the approximation on the grid
	set_richards_initial_data( params, data, ctxt.grid, &(ctxt.reVars) ) ;
	
	//Write out the initial functions to file
	sprintf( outFName, "/tmp/lM%u_0.vtk", params->fGridLevel ) ;
	write_edge_func_paraview_format( outFName, ctxt.grid, ctxt.reVars.lM ) ;
	sprintf( outFName, "/tmp/press%u_0.vtk", params->fGridLevel ) ;
	write_element_func_paraview_format( outFName, ctxt.grid,
		ctxt.reVars.press ) ;
	sprintf( outFName, "/tmp/volWet%u_0.vtk", params->fGridLevel ) ;
	write_element_func_paraview_format( outFName, ctxt.grid,
		ctxt.reVars.prevTheta ) ;
	//Recover the flux
	sprintf( outFName, "/tmp/flux%u_0.vtk", params->fGridLevel ) ;
	recover_flux( params, data, ctxt.grid, &(ctxt.reVars), fluxMags ) ;
	write_edge_flux_func_paraview_format( outFName, ctxt.grid, fluxMags ) ;
	
	//Set the memory for the previous approximation to the pressure
	prevPress = (double*)malloc( ctxt.grid->numElements * sizeof( double ) ) ;
	prevLM = (double*)malloc( ctxt.grid->numUnknowns * sizeof( double ) ) ;
	
	//Start the time to measure only the solve
	solveStart = clock() ;
	
	//Now perform the time-stepping
	params->endTime = params->startTime + params->timeStep * params->numTSteps ;
	params->currTime = params->startTime ;
	tCnt = 0 ;
//	for( tCnt = 0 ; tCnt < params->numTSteps ; tCnt++ ){
	while( params->currTime < params->endTime ){
		//Set the current time
//		params->currTime = params->startTime + tCnt * params->timeStep ;
		printf( "(%u) Current time: %.5lf (%.8e)\t", tCnt, params->currTime,
			params->timeStep ) ;
		fflush( stdout ) ;
		
		//Copy the current pressure into the previous pressure
		copy_dvec( ctxt.grid->numElements, ctxt.reVars.press, prevPress ) ;
		copy_dvec( ctxt.grid->numUnknowns, ctxt.reVars.lM, prevLM ) ;
		
		//Perform a Newton solve for the current system
		richards_newton_step( params, data, &ctxt ) ;
		
		//Write out the Lagrange multipliers and the pressure to file
		sprintf( outFName, "/tmp/lM%u_%u.vtk", params->fGridLevel, tCnt+1 ) ;
		write_edge_func_paraview_format( outFName, ctxt.grid, ctxt.reVars.lM ) ;
		sprintf( outFName, "/tmp/press%u_%u.vtk", params->fGridLevel, tCnt+1 ) ;
		write_element_func_paraview_format( outFName, ctxt.grid,
			ctxt.reVars.press ) ;
		sprintf( outFName, "/tmp/volWet%u_%u.vtk", params->fGridLevel,
			tCnt+1 ) ;
		write_element_func_paraview_format( outFName, ctxt.grid,
			ctxt.reVars.prevTheta ) ;
		//Recover the flux
		sprintf( outFName, "/tmp/flux%u_%u.vtk", params->fGridLevel, tCnt+1 ) ;
		recover_flux( params, data, ctxt.grid, &(ctxt.reVars), fluxMags ) ;
		write_edge_flux_func_paraview_format( outFName, ctxt.grid, fluxMags ) ;
		
		//Check that the iteration converged in the desired number of iterations
		if( ctxt.currResidNorm / ctxt.origResidNorm > NEWT_TOL ||
			isnan( ctxt.currResidNorm ) || isinf( ctxt.currResidNorm ) ){
			//Do something in her
			if( !params->adaptTStep || params->timeStep == params->minTStep ){
				end = clock() ;
				
				printf( "Unable to converge in specified number of"
					" iterations\n" ) ;
				
				//Print out timing data
				printf( "Total time (with set-up): %.7lf (%.7lf)\n",
					((float)(end - solveStart))/CLOCKS_PER_SEC,
					((float)(end - algStart))/CLOCKS_PER_SEC ) ;
					
				//Write the pressure and the Lagrange multipliers to file
				write_dvec_to_file( "/tmp/finalLM.txt", ctxt.grid->numEdges,
					ctxt.reVars.lM ) ;
				write_dvec_to_file( "/tmp/finalPress.txt",
					ctxt.grid->numElements, ctxt.reVars.press ) ;
				write_dvec_to_file( "/tmp/finalPrevTheta.txt",
					ctxt.grid->numElements, ctxt.reVars.prevTheta ) ;
				write_element_func_paraview_format( "/tmp/finalPress.vtk",
					ctxt.grid, prevPress ) ;
					
				//Free the memory assigned on the heap
				free( prevPress ) ;
				free( prevLM ) ;
				free_newton_ctxt( params, &ctxt ) ;
				return ;
			}
			
			printf( "Repeating time step with reduced time step size\n" ) ;
			params->timeStep *= SCALE_DOWN_FACT ;
			if( params->timeStep < params->minTStep )
				params->timeStep = params->minTStep ;
			
			copy_dvec( ctxt.grid->numUnknowns, prevLM, ctxt.reVars.lM ) ;
			copy_dvec( ctxt.grid->numElements, prevPress, ctxt.reVars.press ) ;
			recover_pressure( params, data, ctxt.grid, &(ctxt.reVars) ) ;
			
			continue ;
		}
		
		params->currTime += params->timeStep ;
		tCnt++ ;
		
		//See if the time step should be updated
		if( params->adaptTStep ){
			update_time_step( ctxt.numIts, params ) ;
		}
		
		//Update the volumetric wetness to be valid for the next time-step
//		if( tCnt != params->numTSteps-1 ){
		if( params->currTime < params->endTime ){
			set_richards_prev_theta( ctxt.grid, data, &(ctxt.reVars) ) ;
			recover_pressure( params, data, ctxt.grid, &(ctxt.reVars) ) ;
		}
	}
	
	//Stop the timer
	end = clock() ;
	
	//Write the pressure and the Lagrange multipliers to file
	write_dvec_to_file( "/tmp/finalLM.txt", ctxt.grid->numEdges,
		ctxt.reVars.lM ) ;
	write_dvec_to_file( "/tmp/finalPress.txt", ctxt.grid->numElements,
		ctxt.reVars.press ) ;
	write_dvec_to_file( "/tmp/finalPrevTheta.txt", ctxt.grid->numElements,
		ctxt.reVars.prevTheta ) ;
	write_element_func_paraview_format( "/tmp/finalPress.vtk", ctxt.grid,
		ctxt.reVars.press ) ;
	
	printf( "Final time: %.5lf\t", params->currTime ) ;
	printf( "Residual norm: %.18lf\n", discrete_l2_norm( ctxt.grid->numUnknowns,
		ctxt.reVars.resid ) ) ;
		
	//Print out timing data
	printf( "Total time (with set-up): %.7lf (%.7lf)\n",
		((float)(end - solveStart))/CLOCKS_PER_SEC,
		((float)(end - algStart))/CLOCKS_PER_SEC ) ;
	
	//Free the memory assigned on the heap
	free( prevPress ) ;
	free( prevLM ) ;
	free_newton_ctxt( params, &ctxt ) ;
}

/** Performs a time dependent solve of the Richards equation where the nonlinear
 *	solve is performed using a Newton iteration. In this method information,
 *	including the writing of data to file, is performed. This is useful for
 *	debugging, where the output of the function is desired
 *	\param [in] params	Parameters defining how the solve should be performed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						approximation
 */
void newton_solve_richards_eq( Params *params, REData *data )
{
	unsigned int tCnt ; //Loop counter
	NewtonCtxt ctxt ; //Stores data structures necessary to perform a solve
					  //using a Newton iteration
	double *prevPress ; //The pressure at the previous time step
	double *prevLM ; //The Lagrange multipliers at the previous time step
					  
	clock_t algStart, solveStart, end ; //Rough timing estimates
				
	//Start the timer to also measure the set-up as well as the solve cost
	algStart = clock() ;
				
	//Set the pointers in the data structures to NULL initially
	set_newton_ctxt_to_null( &ctxt ) ;
	
	//Initialise all of the memory required to perform a Newton-Multigrid
	//iteration
	setup_newton_ctxt( params, data, &ctxt ) ;
	
	//Set the memory for an array that may be used when needed in the code. This
	//removes the need for repeated allocation and deallocation of memory
	set_param_spare_memory( ctxt.grid->numEdges, params ) ;
	
	//Initialise the approximation on the grid
	set_richards_initial_data( params, data, ctxt.grid, &(ctxt.reVars) ) ;
	
	//Set the memory for the previous approximation to the pressure
	prevPress = (double*)malloc( ctxt.grid->numElements * sizeof( double ) ) ;
	prevLM = (double*)malloc( ctxt.grid->numUnknowns * sizeof( double ) ) ;
	
	//Start the time to measure only the solve
	solveStart = clock() ;
	
	//Now perform the time-stepping
	params->endTime = params->startTime + params->timeStep * params->numTSteps ;
	params->currTime = params->startTime ;
	tCnt = 0 ;
//	for( tCnt = 0 ; tCnt < params->numTSteps ; tCnt++ ){
	while( params->currTime < params->endTime ){
		//Set the current time
//		params->currTime = params->startTime + tCnt * params->timeStep ;
		printf( "(%u) Current time: %.5lf (%.8e)\t", tCnt, params->currTime,
			params->timeStep ) ;
		fflush( stdout ) ;
		
		//Copy the current pressure into the previous pressure
		copy_dvec( ctxt.grid->numElements, ctxt.reVars.press, prevPress ) ;
		copy_dvec( ctxt.grid->numUnknowns, ctxt.reVars.lM, prevLM ) ;
		
		//Perform a Newton solve for the current system
		richards_newton_step( params, data, &ctxt ) ;
		
		//Check that the iteration converged in the desired number of iterations
		if( ( ctxt.currResidNorm / ctxt.origResidNorm > NEWT_TOL ||
			isnan( ctxt.currResidNorm ) || isinf( ctxt.currResidNorm ) ) &&
			ctxt.currResidNorm > 1e-9 ){
			
			printf( "Residual norm: %.18lf\n", ctxt.currResidNorm ) ;
			//Do something in her
			if( !params->adaptTStep || params->timeStep == params->minTStep ){
				end = clock() ;
				
				printf( "Unable to converge in specified number of"
					" iterations\n" ) ;
				
				//Print out timing data
				printf( "Total time (with set-up): %.7lf (%.7lf)\n",
					((float)(end - solveStart))/CLOCKS_PER_SEC,
					((float)(end - algStart))/CLOCKS_PER_SEC ) ;
					
				//Write the pressure and the Lagrange multipliers to file
				write_dvec_to_file( "/tmp/finalLM.txt", ctxt.grid->numEdges,
					ctxt.reVars.lM ) ;
				write_dvec_to_file( "/tmp/finalPress.txt",
					ctxt.grid->numElements, ctxt.reVars.press ) ;
				write_dvec_to_file( "/tmp/finalPrevTheta.txt",
					ctxt.grid->numElements, ctxt.reVars.prevTheta ) ;
				write_element_func_paraview_format( "/tmp/finalPress.vtk",
					ctxt.grid, prevPress ) ;
					
				//Free the memory assigned on the heap
				free( prevPress ) ;
				free( prevLM ) ;
				free_newton_ctxt( params, &ctxt ) ;
				return ;
			}
			
			printf( "Repeating time step with reduced time step size\n" ) ;
			params->timeStep *= SCALE_DOWN_FACT ;
			if( params->timeStep < params->minTStep )
				params->timeStep = params->minTStep ;
			
			copy_dvec( ctxt.grid->numUnknowns, prevLM, ctxt.reVars.lM ) ;
			copy_dvec( ctxt.grid->numElements, prevPress, ctxt.reVars.press ) ;
			recover_pressure( params, data, ctxt.grid, &(ctxt.reVars) ) ;
			
			continue ;
		}
		
		params->currTime += params->timeStep ;
		tCnt++ ;
		
		//See if the time step should be updated
		if( params->adaptTStep ){
			update_time_step( ctxt.numIts, params ) ;
		}
		
		//Update the volumetric wetness to be valid for the next time-step
//		if( tCnt != params->numTSteps-1 ){
		if( params->currTime < params->endTime ){
			set_richards_prev_theta( ctxt.grid, data, &(ctxt.reVars) ) ;
			recover_pressure( params, data, ctxt.grid, &(ctxt.reVars) ) ;
		}
	}
	
	//Stop the timer
	end = clock() ;
	
	//Write the pressure and the Lagrange multipliers to file
	write_dvec_to_file( "/tmp/finalLM.txt", ctxt.grid->numEdges,
		ctxt.reVars.lM ) ;
	write_dvec_to_file( "/tmp/finalPress.txt", ctxt.grid->numElements,
		ctxt.reVars.press ) ;
	write_dvec_to_file( "/tmp/finalPrevTheta.txt", ctxt.grid->numElements,
		ctxt.reVars.prevTheta ) ;
	write_element_func_paraview_format( "/tmp/finalPress.vtk", ctxt.grid,
		ctxt.reVars.press ) ;
	
	printf( "Final time: %.5lf\t", params->currTime ) ;
	printf( "Residual norm: %.18lf\n", discrete_l2_norm( ctxt.grid->numUnknowns,
		ctxt.reVars.resid ) ) ;
		
	//Print out timing data
	printf( "Total time (with set-up): %.7lf (%.7lf)\n",
		((float)(end - solveStart))/CLOCKS_PER_SEC,
		((float)(end - algStart))/CLOCKS_PER_SEC ) ;
	
	//Free the memory assigned on the heap
	free( prevPress ) ;
	free( prevLM ) ;
	free_newton_ctxt( params, &ctxt ) ;
}

/** Performs Newton iterations where the linear systems are solved using
 *	smoothing only. This is used to test that the Newton iteration is being
 *	performed correctly
 *	\param [in] params	Parameters defining how the solve should be performed
 *	\param [in] data	Soil characteristics of the soils used in the current
 *						problem
 */
void newton_smooth_solve_richards_eq( Params *params, REData *data )
{
	unsigned int tCnt, newtCnt, smoothCnt ; //Loop counters
	Grid grid ; //The grid on which to solve the Richards equation
	REApproxVars reVars ; //Variables required for the mixed finite element
				//approximation of the Richards equation
	LinApproxVars linVars ; //Variables required to solve a linear system of
				//equations
	double reResid, origREResid ; //The current residual norm and original
				//residual norm of the Richards equation
	double linResid, origLinResid ; //The current residual norm and original
				//residual norm of the linear Newton system
	char outFName[20] ; //Filename to write output to
	double *fluxMags ; //Stores the magnitude of the fluxes over element
					   //boundaries
	
	//Set the pointers in the data structures to NULL initially
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &reVars ) ;
	set_lin_approx_vars_to_null( &linVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	//Set up the soils on the grid
	set_soil_type( params, data, &grid ) ;
	//Set some memory that can be accessed as and when required
	set_param_spare_memory( grid.numEdges, params ) ;
	
	//Set the memory for the fluxes
	fluxMags = (double*)calloc( grid.numEdges, sizeof( double ) ) ;
	
	//Set the memory for the variables required for the Richards equation on the
	//current grid
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	
	//Set the memory for the variables required to solve a linear system on the
	//current grid
	init_lin_approx_vars_on_grid( &grid, &linVars ) ;
	
	//Initialise the approximation on the current grid
	set_richards_initial_data( params, data, &grid, &reVars ) ;
	
	//Write the pressure and the Lagrange multipliers to file
	sprintf( outFName, "/tmp/smoothLM0.vtk" ) ;
	write_edge_func_paraview_format( outFName, &grid, reVars.lM ) ;
	sprintf( outFName, "/tmp/smoothPress0.vtk" ) ;
	write_element_func_paraview_format( outFName, &grid, reVars.press ) ;
	//Recover the flux
	sprintf( outFName, "/tmp/smoothFlux0.vtk" ) ;
	recover_flux( params, data, &grid, &reVars, fluxMags ) ;
	write_edge_flux_func_paraview_format( outFName, &grid, fluxMags ) ;
	
	//Perfor the time stepping
	for( tCnt = 0 ; tCnt < params->numTSteps ; tCnt ++ ){
		//Calculate the residual and the Jacobian matrix
		calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
			&(linVars.op) ) ;
		
		//Calculate the norm of the approximation in the Richards equation
		reResid = discrete_l2_norm( grid.numUnknowns, reVars.resid ) ;
		origREResid = reResid ;
		
		printf( "Time-step %u: Residual Norm: %.18lf\n", tCnt,
			origREResid ) ;
			
		newtCnt = 0 ;
		//Perform Newton iterations
		while( reResid/origREResid > NEWT_TOL && newtCnt < params->maxIts ){
			//Set the initial approximation to the linear system
			init_newt_approx_on_grid( &reVars, &grid, &linVars ) ;
			
			//Calculate the original linear residual
			calculate_lin_resid( &linVars ) ;
			origLinResid = discrete_l2_norm( grid.numUnknowns, linVars.resid ) ;
			linResid = origLinResid ;
			
			smoothCnt = 0 ;
			//print_indent( 2, "Smooth %u: Residual Norm: %.18lf\n", smoothCnt,
			//	linResid ) ;
			
			//Perform the linear solve
			//while( linResid/origLinResid > 1e-5 && linResid > SOLVED_TOL ){
			while( linResid/origLinResid > 1e-7 && linResid > 1e-15 ){
				//Perform 10 smooths then recalculate the residual
				lin_smooth( params, 10, &linVars ) ;
				
				//Calculate the linear residual
				calculate_lin_resid( &linVars ) ;
				linResid = discrete_l2_norm( grid.numUnknowns, linVars.resid ) ;
				smoothCnt += 10 ;
				//print_indent( 2, "Smooth %u: Residual Norm: %.18lf\n",
				//	smoothCnt, linResid ) ;
				//if( linResid / origLinResid > 1e3 ){
				//	printf( "Divergence!\n" ) ;
				//	exit( 0 ) ;
				//}
			}
			
			//Print the correction to file
			sprintf( outFName, "/tmp/smoothCorrect%u.txt", newtCnt+1 ) ;
			write_dvec_to_file( outFName, grid.numUnknowns, linVars.approx ) ;
			sprintf( outFName, "/tmp/smoothCorrect%u.vtk", newtCnt+1 ) ;
			write_interior_edge_func_paraview_format( outFName, &grid,
				linVars.approx ) ;
			
			//Correct the Newton approximation
			update_re_approx_from_newt_correct( params, &grid, &linVars,
				&reVars ) ;
				
			//Recover the pressure
			recover_pressure( params, data, &grid, &reVars ) ;
			
			//Calculate the residual and the Jacobian
			calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
				&(linVars.op) ) ;
				
			//Calculate the updated residual norm
			reResid = discrete_l2_norm( grid.numUnknowns, reVars.resid ) ;
			
			print_indent( 1, "Newton iteration %u: Residual Norm: %.18lf\n",
				newtCnt+1, reResid ) ;
			
			//Increment the counter for the Newton iterations
			newtCnt++ ;
			
			if( reResid <= SOLVED_TOL ) break ;
		}
		
		//Write out the Lagrange multipliers and the pressure to file
		sprintf( outFName, "/tmp/smoothLM%u.vtk", tCnt+1 ) ;
		write_edge_func_paraview_format( outFName, &grid, reVars.lM ) ;
		sprintf( outFName, "/tmp/smoothPress%u.vtk", tCnt+1 ) ;
		write_element_func_paraview_format( outFName, &grid, reVars.press ) ;
		//Recover the flux
		sprintf( outFName, "/tmp/smoothFlux%u.vtk", tCnt+1 ) ;
		recover_flux( params, data, &grid, &reVars, fluxMags ) ;
		write_edge_flux_func_paraview_format( outFName, &grid, fluxMags ) ;
		
		//Calculate the data for the next time-step
		if( tCnt != params->numTSteps-1 )
			set_richards_prev_theta( &grid, data, &reVars ) ;
	}
	
	printf( "Final time: %.5lf\t", params->startTime +
		params->numTSteps * params->timeStep ) ;
	printf( "Residual norm: %.18lf\n", discrete_l2_norm( grid.numUnknowns,
		reVars.resid ) ) ;
		
	//Write out the pressure and the Lagrange multipliers
	sprintf( outFName, "/tmp/smoothLM%u.vtk", tCnt+1 ) ;
	write_edge_func_paraview_format( outFName, &grid, reVars.lM ) ;
	sprintf( outFName, "/tmp/smoothPress%u.vtk", tCnt+1 ) ;
	write_element_func_paraview_format( outFName, &grid, reVars.press ) ;
	
	//Write the pressure and the Lagrange multipliers to file
	write_dvec_to_file( "/tmp/finalLM.txt", grid.numEdges, reVars.lM ) ;
	write_dvec_to_file( "/tmp/finalPress.txt", grid.numElements,
		reVars.press ) ;
	write_dvec_to_file( "/tmp/finalPrevTheta.txt", grid.numElements,
		reVars.prevTheta ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &reVars ) ;
	free_lin_approx_vars( &linVars ) ;
	free( fluxMags ) ;
}

/** Performs a time-dependent solve of the Richards equation using an FAS
 *	iteration
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 */
void fas_multigrid_solve_write_info( Params *params, const REData *data )
{
	unsigned int tCnt, itCnt ; //Loop counters
	FASCtxt ctxt ; //Stores information necessary to perform an FAS iteration
				   //for the Richards equation
	Grid *grid ; //The finest grid on which to solve
	REApproxVars *vars ; //Variables required to solve the Richards equation on
						 //the finest grid level
	double resid, origResid ; //The norm in the current and original residual
				//during a time-step
	char outFName[20] ; //Name of the file to write output to
	double *fluxMags ; //Magnitudes of the flux
	
	double *prevLM ; //Stores the previous value of the Lagrange multipliers
	double *prevPress ; //Stores the previous value of the pressure
	
	clock_t algStart, solveStart, end ; //Rough timing estimates
	
	//Start a timer to include the set-up time
	algStart = clock() ;
				   
	//Set the pointers in the data structures to NULL initially
	set_fas_ctxt_to_null( &ctxt ) ;
	
	//Set up the memory required to perform the iteration
	set_up_fas_ctxt( params, data, &ctxt ) ;
	
	//Set the memory for the spare pointer in the parameters structure
	set_param_spare_memory( ctxt.gridHierarchy[params->fGridLevel]->numEdges,
		params ) ;
	
	//Set a placeholder for the finest grid and the approximations on that grid
	grid = ctxt.gridHierarchy[params->fGridLevel] ;
	vars = ctxt.reHierarchy[params->fGridLevel] ;
	
	//Set the memory for the fluxes
	fluxMags = (double*)calloc( grid->numEdges, sizeof( double ) ) ;
	prevLM = (double*)malloc( grid->numUnknowns * sizeof( double ) ) ;
	prevPress = (double*)malloc( grid->numElements * sizeof( double ) ) ;
	
	//Set up the initial approximation on the fine grid
	set_richards_initial_data( params, data, grid, vars ) ;
	
	//Start the timer after the set-up of the data
	solveStart = clock() ;
	params->endTime = params->startTime + params->timeStep * params->numTSteps ;
	params->currTime = params->startTime ;
	tCnt = 0 ;
//	for( tCnt=0 ; tCnt < params->numTSteps ; tCnt++ ){
	while( params->currTime < params->endTime ){
		//Set the current time
//		params->currTime = params->startTime + tCnt * params->timeStep ;
		//Recover the pressure
		recover_pressure( params, data, grid, vars ) ;
		
		//Copy the current pressure into the previous pressure
		copy_dvec( grid->numElements, vars->press, prevPress ) ;
		copy_dvec( grid->numUnknowns, vars->lM, prevLM ) ;
		
		//Calculate the residual in approximation
		calc_richards_resid_and_jacobian_diags( params, grid, data, vars,
			ctxt.jacDiags ) ;
			
		//Write the pressure and the Lagrange multipliers to file
		sprintf( outFName, "/tmp/lM%u_%u.vtk", params->fGridLevel, tCnt ) ;
		write_edge_func_paraview_format( outFName, grid, vars->lM ) ;
		sprintf( outFName, "/tmp/press%u_%u.vtk", params->fGridLevel, tCnt ) ;
		write_element_func_paraview_format( outFName, grid, vars->press ) ;
		//Recover the flux
		sprintf( outFName, "/tmp/flux%u_%u.vtk", params->fGridLevel, tCnt ) ;
		recover_flux( params, data, grid, vars, fluxMags ) ;
		write_edge_flux_func_paraview_format( outFName, grid, fluxMags ) ;
		
		//Calculate the norm of the residual
		origResid = resid = discrete_l2_norm( grid->numUnknowns, vars->resid ) ;
		
		printf( "(%u) Current Time: %.5lf (%.8e)\tResidual Norm: %.18lf", tCnt,
			params->currTime, params->timeStep, origResid ) ;
		fflush( stdout ) ;
		
		itCnt = 0 ;
		while( resid / origResid > FAS_TOL && itCnt < params->maxIts ){
			//Perform an FAS iteration
			fas_multigrid_iteration( params, data, params->fGridLevel, &ctxt ) ;
			
			//Calculate the residual norm
			resid = discrete_l2_norm( grid->numUnknowns, vars->resid ) ;
//			print_indent( 1, "FAS iteration: %u\tResidual Norm: %.18lf\n",
//				itCnt, resid ) ;
			itCnt++ ;
		}
		
		printf( "\t%u iterations performed\n", itCnt ) ;
		
		//Check that the iteration converged in the desired number of iterations
		if( resid / origResid > FAS_TOL || isnan( resid ) || isinf( resid ) ){
			//Do something in her
			if( !params->adaptTStep || params->timeStep == params->minTStep ){
				end = clock() ;
				
				printf( "Unable to converge in specified number of"
					" iterations\n" ) ;
				
				//Print out timing data
				printf( "Total time (with set-up): %.7lf (%.7lf)\n",
					((float)(end - solveStart))/CLOCKS_PER_SEC,
					((float)(end - algStart))/CLOCKS_PER_SEC ) ;
					
				//Write the pressure and the Lagrange multipliers to file
				write_dvec_to_file( "/tmp/finalLM.txt", grid->numEdges,
					vars->lM ) ;
				write_dvec_to_file( "/tmp/finalPress.txt",
					grid->numElements, vars->press ) ;
				write_dvec_to_file( "/tmp/finalPrevTheta.txt",
					grid->numElements, vars->prevTheta ) ;
				write_element_func_paraview_format( "/tmp/finalPress.vtk",
					grid, prevPress ) ;
					
				//Free the memory assigned on the heap
				free( prevPress ) ;
				free( prevLM ) ;
				free_fas_ctxt( params, &ctxt ) ;
				return ;
			}
			
			printf( "Repeating time step with reduced time step size\n" ) ;
			params->timeStep *= SCALE_DOWN_FACT ;
			if( params->timeStep < params->minTStep )
				params->timeStep = params->minTStep ;
			
			copy_dvec( grid->numUnknowns, prevLM, vars->lM ) ;
			copy_dvec( grid->numElements, prevPress, vars->press ) ;
			recover_pressure( params, data, grid, vars ) ;
			
			continue ;
		}
		
		params->currTime += params->timeStep ;
		tCnt++ ;
		
		//Check if the time step should be updated
		if( params->adaptTStep ){
			update_time_step( itCnt, params ) ;
		}
		
		//Set the value ready for the next time-step
//		if( tCnt != params->numTSteps-1 )
		if( params->currTime < params->endTime )
			set_richards_prev_theta( grid, data, vars ) ;
	}
	
	//Stop the timer
	end = clock() ;
	
	printf( "Final time: %.5lf\t", params->startTime +
		params->numTSteps * params->timeStep ) ;
	printf( "Residual norm: %.18lf\n", discrete_l2_norm( grid->numUnknowns,
		vars->resid ) ) ;
	
	printf( "Total time (with set-up): %.7lf (%.7lf)\n",
		((float)(end - solveStart))/CLOCKS_PER_SEC,
		((float)(end - algStart))/CLOCKS_PER_SEC ) ;
	
	//Write the pressure and the Lagrange multipliers to file
	sprintf( outFName, "/tmp/lM%u_%u.vtk", params->fGridLevel, tCnt ) ;
	sprintf( outFName, "/tmp/press%u_%u.vtk", params->fGridLevel, tCnt ) ;
	//Recover the flux
	sprintf( outFName, "/tmp/flux%u_%u.vtk", params->fGridLevel, tCnt ) ;
	recover_flux( params, data, grid, vars, fluxMags ) ;
	write_edge_flux_func_paraview_format( outFName, grid, fluxMags ) ;
	
	//Write the pressure and the Lagrange multipliers to file
	write_dvec_to_file( "/tmp/finalLM.txt", grid->numEdges, vars->lM ) ;
	write_dvec_to_file( "/tmp/finalPress.txt", grid->numElements,
		vars->press ) ;
	write_dvec_to_file( "/tmp/finalPrevTheta.txt", grid->numElements,
		vars->prevTheta ) ;
	
	//Free the memory assigned on the heap
	free_fas_ctxt( params, &ctxt ) ;
	free( fluxMags ) ;
	free( prevLM ) ;
	free( prevPress ) ;
}

/** Performs a time-dependent solve of the Richards equation using an FAS
 *	iteration
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 */
void fas_multigrid_solve( Params *params, const REData *data )
{
	unsigned int tCnt, itCnt ; //Loop counters
	FASCtxt ctxt ; //Stores information necessary to perform an FAS iteration
				   //for the Richards equation
	Grid *grid ; //The finest grid on which to solve
	REApproxVars *vars ; //Variables required to solve the Richards equation on
						 //the finest grid level
	double resid, origResid ; //The norm in the current and original residual
				//during a time-step
				
	double *prevLM ; //Stores the previous value of the Lagrange multipliers
	double *prevPress ; //Stores the previous value of the pressure
				
	clock_t algStart, solveStart, end ; //Rough timing estimates
	
	//Start a timer to include the set-up time
	algStart = clock() ;
				   
	//Set the pointers in the data structures to NULL initially
	set_fas_ctxt_to_null( &ctxt ) ;
	
	//Set up the memory required to perform the iteration
	set_up_fas_ctxt( params, data, &ctxt ) ;
	
	//Set the memory for the spare pointer in the parameters structure
	set_param_spare_memory( ctxt.gridHierarchy[params->fGridLevel]->numEdges,
		params ) ;
	
	//Set a placeholder for the finest grid and the approximations on that grid
	grid = ctxt.gridHierarchy[params->fGridLevel] ;
	vars = ctxt.reHierarchy[params->fGridLevel] ;
	
	//Set up the initial approximation on the fine grid
	set_richards_initial_data( params, data, grid, vars ) ;
	
	prevLM = (double*)malloc( grid->numUnknowns * sizeof( double ) ) ;
	prevPress = (double*)malloc( grid->numElements * sizeof( double ) ) ;
	
	//Start the timer after the set-up of the data
	solveStart = clock() ;
	params->endTime = params->startTime + params->timeStep * params->numTSteps ;
	params->currTime = params->startTime ;
	tCnt = 0 ;
//	for( tCnt=0 ; tCnt < params->numTSteps ; tCnt++ ){
	while( params->currTime < params->endTime ){
		//Set the current time
//		params->currTime = params->startTime + tCnt * params->timeStep ;
		//Recover the pressure
		recover_pressure( params, data, grid, vars ) ;
		
		//Copy the current pressure into the previous pressure
		copy_dvec( grid->numElements, vars->press, prevPress ) ;
		copy_dvec( grid->numUnknowns, vars->lM, prevLM ) ;
		
		//Calculate the residual in approximation
		calc_richards_resid_and_jacobian_diags( params, grid, data, vars,
			ctxt.jacDiags ) ;
		
		//Calculate the norm of the residual
		origResid = resid = discrete_l2_norm( grid->numUnknowns, vars->resid ) ;
		
		printf( "(%u) Current Time: %.5lf (%.8e)\tResidual Norm: %.18lf",
			tCnt, params->currTime, params->timeStep, origResid ) ;
		fflush( stdout ) ;
		
		itCnt = 0 ;
//		printf( "\n" ) ;
		while( resid / origResid > FAS_TOL && itCnt < params->maxIts ){
//		while( itCnt < params->maxIts ){
//			printf( "\33[2K\rFAS iteration: %d", itCnt ) ;
//			fflush( stdout ) ;
			//Perform an FAS iteration
			fas_multigrid_iteration( params, data, params->fGridLevel, &ctxt ) ;
			
			//Calculate the residual norm
			resid = discrete_l2_norm( grid->numUnknowns, vars->resid ) ;
//			print_indent( 1, "FAS iteration: %u\tResidual Norm: %.18lf\n",
//				itCnt, resid ) ;
			itCnt++ ;
		}
		
		printf( "\t%u iterations performed\n", itCnt ) ;
		
		//Check that the iteration converged in the desired number of iterations
		if( resid / origResid > FAS_TOL || isnan( resid ) || isinf( resid ) ){
			if( !params->adaptTStep || params->timeStep == params->minTStep ){
				end = clock() ;
				
				printf( "Unable to converge in specified number of"
					" iterations\n" ) ;
				
				//Print out timing data
				printf( "Total time (with set-up): %.7lf (%.7lf)\n",
					((float)(end - solveStart))/CLOCKS_PER_SEC,
					((float)(end - algStart))/CLOCKS_PER_SEC ) ;
					
				//Write the pressure and the Lagrange multipliers to file
				write_dvec_to_file( "/tmp/finalLM.txt", grid->numEdges,
					vars->lM ) ;
				write_dvec_to_file( "/tmp/finalPress.txt",
					grid->numElements, vars->press ) ;
				write_dvec_to_file( "/tmp/finalPrevTheta.txt",
					grid->numElements, vars->prevTheta ) ;
				write_element_func_paraview_format( "/tmp/finalPress.vtk",
					grid, prevPress ) ;
					
				//Free the memory assigned on the heap
				free( prevPress ) ;
				free( prevLM ) ;
				free_fas_ctxt( params, &ctxt ) ;
				return ;
			}
			
			printf( "Repeating time step with reduced time step size\n" ) ;
			params->timeStep *= SCALE_DOWN_FACT ;
			if( params->timeStep < params->minTStep )
				params->timeStep = params->minTStep ;
			
			copy_dvec( grid->numUnknowns, prevLM, vars->lM ) ;
			copy_dvec( grid->numElements, prevPress, vars->press ) ;
			recover_pressure( params, data, grid, vars ) ;
			
			continue ;
		}
		
		params->currTime += params->timeStep ;
		tCnt++ ;
		
		//Check if the time step should be updated
		if( params->adaptTStep ){
			update_time_step( itCnt, params ) ;
		}
		
		if( params->currTime < params->endTime )
			set_richards_prev_theta( grid, data, vars ) ;
	}
	
	//Stop the timer
	end = clock() ;
	
	printf( "Final time: %.5lf\t", params->startTime +
		params->numTSteps * params->timeStep ) ;
	printf( "Residual norm: %.18lf\n", discrete_l2_norm( grid->numUnknowns,
		vars->resid ) ) ;
		
	printf( "Total time (with set-up): %.7lf (%.7lf)\n",
		((float)(end - solveStart))/CLOCKS_PER_SEC,
		((float)(end - algStart))/CLOCKS_PER_SEC ) ;
	
	//Free the memory assigned on the heap
	free_fas_ctxt( params, &ctxt ) ;
	free( prevPress ) ;
	free( prevLM ) ;
}

/** Updates the time step when using an adaptive time stepping strategy
 *	\param [in] numIts	The number of nonlinear iterations performed at the
 *						current time step
 *	\param [in,out] params	Parameters defining how the algorithm is to be
 *							executed
 */
void update_time_step( unsigned int numIts, Params *params )
{
	unsigned int numGoodIts, numBadIts ;
	
	if( params->algorithm == ALG_FAS ){
		numGoodIts = 4 ;
		numBadIts = 10 ;
	}
	else{
		numGoodIts = 5 ;
		numBadIts = 10 ;
	}
	
	if( numIts <= numGoodIts ){
		params->timeStep *= SCALE_UP_FACT ;
	}
	else if( numIts > numBadIts ){
		params->timeStep *= SCALE_DOWN_FACT ;
	}
	
	if( params->timeStep < params->minTStep ){
		params->timeStep = params->minTStep ;
	}
	else if( params->timeStep > params->maxTStep ){
		params->timeStep = params->maxTStep ;
	}
	
	if( params->currTime + params->timeStep > params->endTime )
		params->timeStep = params->endTime - params->currTime ;
}












