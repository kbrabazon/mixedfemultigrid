#include "gridHandling.h"
#include "richardsFunctions.h"
#include <stdio.h>

//Define the variables oEven and oOdd to be the orientation of the edges on
//even (odd) numbered elements, respectively
const enum EDGE_ORIENTATION oEven[] = {O_BACKWARD, O_FORWARD, O_BACKWARD} ;
const enum EDGE_ORIENTATION oOdd[] = {O_FORWARD, O_FORWARD, O_BACKWARD} ;

/** Sets all pointers in the grid structure to NULL pointers. This should be
 *	performed only for the initialisation of the structure
 *	\param [out] grid	The grid structure for which to set the pointers to NULL
 */
void set_grid_to_null( Grid *grid )
{
	grid->nodes = NULL ;
	grid->edges = NULL ;
	grid->els = NULL ;
	grid->mapU2G = NULL ;
}

/** Frees the memory taken by the grid structure
 *	\param [in,out] grid	The grid structure for which to free the memory
 */
void free_grid( Grid *grid )
{
	//We check that the pointers are not NULL to make this function safe to call
	//multiple times
	if( grid->nodes != NULL ){
		free( grid->nodes ) ;
		grid->nodes = NULL ;
	}
	if( grid->els[0].invStiffMat != NULL ){
		free( (double*)grid->els[0].invStiffMat ) ;
		grid->els[0].invStiffMat = NULL ;
	}
	if( grid->els[1].invStiffMat != NULL ){
		free( (double*)grid->els[1].invStiffMat ) ;
		grid->els[0].invStiffMat = NULL ;
	}
	if( grid->edges != NULL ){
		free( grid->edges ) ;
		grid->edges = NULL ;
	}
	if( grid->els != NULL ){
		free( grid->els ) ;
		grid->els = NULL ;
	}
	if( grid->mapU2G != NULL ){
		free( grid->mapU2G ) ;
		grid->mapU2G = NULL ;
	}
}

/** Reads a grid from the file specified. This simply populates the variables
 *	that are structures on the grid. The restriction and prolongation operators
 *	are not populated here, and must be done so subsequently.
 *	\param [in] fname	The filename to read the grid from
 *	\param [out] grid	The grid variable to be populated
 */
void read_grid_from_file( const char *fname, Grid *grid )
{
	int i, j ; //Loop counters
	
	FILE *gridFile ; //The file to read the grid information from
	int bnds[4] ; //Temporarily stores the boundary types read in from
						   //file
	unsigned int nType ; //Temporarily stores the type of node read in from file
	unsigned int nInds[2] ; //Indices pointing to edge end nodes
	unsigned int edgeInds[3] ; //Indices pointing to element edges
	unsigned int dirBndInd ; //Index used in setting up the mapping between
				//known and unknown edges in an approximation and on the grid
	double *invStiffMatEven ; //The inverse of the stiffness matrix on even
							  //numbered elements
	double *invStiffMatOdd ; //The inverse of the stiffness matrix on odd
							 //numbered elements
	double invRowSum ; //The sum of a row in the inverse of the local stiffness
					   //matrix on an element
				
	unsigned int *numEls ; //Counts the number of elements added to an edge
	
	Node *node ; //Pointer to a node on the grid
	Edge *edge ; //Pointer to an edge on the grid
	Element *el ; //Pointer to an element on the grid
	
	//Open the file for reading
	gridFile = fopen( fname, "r" ) ;
	
	//Check that the file has been opened correctly
	if( gridFile == NULL ){
		printf( "Error opening file: %s\n", fname ) ;
		printf( "In function read_grid_from_file in gridHandling.c\n" ) ;
		exit(0) ;
	}
	
	//Read the first line from the file, which gives the number of nodes, edges
	//and elements, in that order
	fscanf( gridFile, "%u %u %u", &(grid->numNodes), &(grid->numEdges),
		&(grid->numElements ) ) ;
	
	//Read in the number of rows and columns of nodes on the grid
	fscanf( gridFile, "%u %u", &(grid->numRows), &(grid->numCols) ) ;
	
	//Read in the boundary types
	fscanf( gridFile, "%d %d %d %d", &(bnds[0]), &(bnds[1]), &(bnds[2]),
		&(bnds[3]) ) ;
	for( i=0 ; i < 4 ; i++ )
		grid->bndType[i] = (enum BND_TYPE)(-bnds[i]) ;
		
	//Set the memory required for the nodes, edges and elements on the grid
	grid->nodes = (Node*)malloc( grid->numNodes * sizeof( Node ) ) ;
	grid->edges = (Edge*)malloc( grid->numEdges * sizeof( Edge ) ) ;
	grid->els = (Element*)malloc( grid->numElements * sizeof( Element ) ) ;
	grid->mapU2G = (unsigned int*)malloc( grid->numEdges *
		sizeof( unsigned int ) ) ;
	
	//Store the information for the nodes
	for( i=0 ; i < grid->numNodes ; i++ ){
		//Get a pointer to the node on the grid
		node = &(grid->nodes[i]) ;
		//Set the id of the node
		node->id = i ;
		//Read the information from the file about the type of node, and the
		//coordinate
		fscanf( gridFile, "%u %lf %lf", &(nType), &(node->x), &(node->z) ) ;
		//Cast the type of node into the correct form
		node->type = (enum NODE_TYPE)nType ;
	}
	
	//Set the number of unknowns on the grid to be zero initially
	grid->numUnknowns = 0 ;
	dirBndInd = grid->numEdges - 1 ;
	
	//Store the information for the edges
	for( i=0 ; i < grid->numEdges ; i++ ){
		//Get a pointer to the current edge on the grid
		edge = &(grid->edges[i]) ;
		//Read in the end points of the edge
		fscanf( gridFile, "%u %u", &(nInds[0]), &(nInds[1]) ) ;
		//Set the end points for the current edge
		edge->endPoints[0] = &(grid->nodes[nInds[0]]) ;
		edge->endPoints[1] = &(grid->nodes[nInds[1]]) ;
		edge->mag = calc_edge_mag( edge ) ;
		
		//Check if the edge is entirely on the Dirichlet boundary
		//I don't know what counts as an edge that is entirely on the Dirichlet
		//boundary. At the moment I assume that it is an edge with both end
		//points on the Dirichlet boundary, as this is enough to define a linear
		//function on the element, so that the value will not be an unknown
		if( edge->endPoints[0]->type == NODE_DIR_BND &&
			edge->endPoints[1]->type == NODE_DIR_BND &&
			(edge->endPoints[0]->x == edge->endPoints[1]->x
			|| edge->endPoints[0]->z == edge->endPoints[1]->z) ){
//		if( edge->endPoints[0]->type == NODE_DIR_BND &&
//			edge->endPoints[1]->type == NODE_DIR_BND ){
			//If the edge is defined entirely by the Dirichlet boundary add this
			//the list of knowns at the back of the list of unknowns
			edge->type = EDGE_KNOWN ;
			edge->id = dirBndInd ;
			grid->mapU2G[dirBndInd--] = i ;
		}
		else{
			//Put the unknown edge at the end of the list of unknowns
			edge->type = EDGE_UNKNOWN ;
			edge->id = grid->numUnknowns ;
			grid->mapU2G[grid->numUnknowns++] = i ;
		}
		
		edge->parentEdges[0] = edge->parentEdges[1] = NULL ;
	}
	
	//Set the memory for the count of the number of elements added to an edge
	numEls = (unsigned int*)calloc( grid->numEdges, sizeof( unsigned int ) ) ;
	
	//Store the information for the elements
	for( i=0 ; i < grid->numElements ; i++ ){
		//Get a pointer to the current element on the grid
		el = &(grid->els[i]) ;
		//Read in the edges on the grid
		fscanf( gridFile, "%u %u %u", &(edgeInds[0]), &(edgeInds[1]),
			&(edgeInds[2]) ) ;
		for( j=0 ; j < 3 ; j++ ){
			//Add the edges to the current element, and add the current element
			//to the appropriate edge
			edge = &(grid->edges[edgeInds[j]]) ;
			el->edges[j] = edge ;
			edge->parentEls[ numEls[edgeInds[j]]++ ] = el ;
		}
		
		if( i%2 == 0 )
			el->edgeOrient = &(oEven[0]) ;
		else
			el->edgeOrient = &(oOdd[0]) ;
			
		//For every edge on the element calculate a constant factor to be used
		//in the calculation of the residual and Jacobian matrix
		for( j=0 ; j < 3 ; j++ )
			el->zfacts[j] = calc_zfact( el, j ) ;
			
		//Calculate the area
		el->area = calc_el_area( el ) ;
	}
	
	//Set the inverse matrix of the local stiffness matrix on each element. As
	//we are using a regular grid I am cheating and using the fact that all of
	//the inverse matrices will be the same
	invStiffMatEven = (double*)calloc( 9, sizeof( double ) ) ;
	invStiffMatOdd = (double*)calloc( 9, sizeof( double ) ) ;
	calc_loc_mat_inv( &(grid->els[0]), invStiffMatEven ) ;
	calc_loc_mat_inv( &(grid->els[1]), invStiffMatOdd ) ;
	invRowSum = el_inv_mat_row_sum( &(grid->els[0]) ) ;
	//Set the inverse matrix for the even numbered elements
	for( i=0 ; i < grid->numElements ; i+=2 ){
		grid->els[i].invStiffMat = invStiffMatEven ;
		grid->els[i].invMatRowSum = invRowSum ;
	}
	//Set the inverse matrix for the odd numbered elements
	for( i=1 ; i < grid->numElements ; i+=2 ){
		grid->els[i].invStiffMat = invStiffMatOdd ;
		grid->els[i].invMatRowSum = invRowSum ;
	}
	
	//Loop through the edges and check that the number of elements it is
	//incident to is 2
	for( i=0 ; i < grid->numEdges ; i++ ){
		if( numEls[i] != 2 ) grid->edges[i].parentEls[1] =
			grid->edges[i].parentEls[0] ;
	}
	
	//Close the file
	fclose( gridFile ) ;
	
	//Free all memory on the heap
	free( numEls ) ;
}

/** Calculates the magnitude of an edge passed in by taking the Euclidean norm
 *	of the vector of the end points
 *	\param [in] edge	The edge for which to calculate the magnitude
 *	\return The \p double value of the magnitude of the edge
 */
double calc_edge_mag( const Edge *edge )
{
	return sqrt( pow(edge->endPoints[0]->x - edge->endPoints[1]->x, 2) +
		pow( edge->endPoints[0]->z - edge->endPoints[1]->z, 2 ) ) ;
}

/** We take advantage of the order in which the elements are stored and the
 *  order in which the nodes are stored in the elements to get an easy formula
 *  for the area of the triangle that holds for all triangles on our regular
 *	grid
 * \param [in] el	The element on the grid for which to calculate the area
 * \return The area of the element
 */
double calc_el_area( const Element *el )
{
	const Edge *e1, *e2 ;
	//Set placeholders to the edges on the element
	e1 = el->edges[1] ;
	e2 = el->edges[2] ;
	
	//Calculate the area as a product of rotations of the edges
	return ((e2->endPoints[1]->x - e2->endPoints[0]->x) *
		(e1->endPoints[1]->z - e1->endPoints[0]->z) -
		(e2->endPoints[1]->z - e2->endPoints[0]->z) *
		(e1->endPoints[1]->x - e1->endPoints[0]->x)) / 2 ;
}

/** Returns the z-coordinate of the node opposite the edge with index \p eInd on
 *	the current element
 *	\param [in] el	The element on which to find the opposite z-coordinate
 *	\param [in] eInd	The index of the edge on the element
 *	\return The z-coordinate of the node opposite the edge with index \p eInd
 */
double get_z_op( const Element *el, const unsigned int eInd )
{
	unsigned int nextInd ; //The index of the next edge on the element in an
						   //anti-clockwise direction
	nextInd = (eInd+1)%3 ; //Get the index of the next edge on the grid
	
	//Get the z-coordinate of the end point of the next edge
	return el->edges[nextInd]->endPoints[ (int)el->edgeOrient[ nextInd ] ]->z ;
}

/** Sets the parents of the fine grid edges on the coarse grid. It is assumed
 *	that the grids are regular and that regular refinement has been used to
 *	create them. No checks are made that this is the case
 *	\param [in] cGrid	The coarse grid
 *	\param [out] fGrid	The fine grid for which to assign coarse grid elements
 */
void set_edge_parents( const Grid *cGrid, Grid *fGrid )
{
	unsigned int rowCnt, colCnt, i ; //Loop counters
	unsigned int fInd, cInd ; //Index into the fine (coarse) edges, respectively
	unsigned int diff ;
	Edge *fEdge ; //An edge on the fine (coarse) grid,
				//respectively
	
	//Make sure that we are using grid levels that are directly above / below
	//each other
	assert( 2*cGrid->numCols-1 == fGrid->numCols ) ;

	fInd = cInd = 0 ;
	//Loop through all of the coarse grid rows
	for( rowCnt = 0 ; rowCnt < cGrid->numRows-1 ; rowCnt++ ){
		//First we sort out the horizontal line at the bottom of a row. Here all
		//fine grid edges are on coarse grid edges
		for( colCnt = 0 ; colCnt < cGrid->numCols-1 ; colCnt++ ){
			//Get a handle to the fine grid edge
			for( i=0 ; i < 2 ; i++ ){
				fEdge = &(fGrid->edges[fInd++]) ;
				fEdge->parentEdges[0] = fEdge->parentEdges[1] =
					&(cGrid->edges[cInd]) ;
			}
			//Increment the index into the coarse grid edges
			cInd++ ;
		}
		
		//We alternate now between two edges that are entirely on a coarse
		//grid edge, and edges that are shared between coarse grid edges
		diff = cGrid->numCols ;
		for( colCnt = 0 ; colCnt < cGrid->numCols-1 ; colCnt++ ){
			//Set two edges that are contained in coarse grid edges
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = fEdge->parentEdges[1] =
				&(cGrid->edges[cInd++]) ;
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = fEdge->parentEdges[1] =
				&(cGrid->edges[cInd]) ;
			//Set two edges that are not contained in coarse grid edges
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = &(cGrid->edges[cInd]) ;
			fEdge->parentEdges[1] = &(cGrid->edges[cInd++ - diff++]) ;
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = &(cGrid->edges[cInd]) ;
			fEdge->parentEdges[1] = &(cGrid->edges[cInd - diff]) ;
		}
		//Set the final edge on this fine row
		fEdge = &(fGrid->edges[fInd++]) ;
		fEdge->parentEdges[0] = fEdge->parentEdges[1] =
			&(cGrid->edges[cInd]) ;
			
		//Now set the edge parents on the row of horizontal edges which are only
		//on the fine grid
		cInd -= fGrid->numCols-1 ;
		for( colCnt = 0 ; colCnt < fGrid->numCols-1 ; colCnt++ ){
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = &(cGrid->edges[cInd++]) ;
			fEdge->parentEdges[1] = &(cGrid->edges[cInd]) ;
		}
		//Increment the coarse grid index
		cInd++ ;
		diff = fGrid->numCols ;
		for( colCnt = 0 ; colCnt < cGrid->numCols-1 ; colCnt++ ){
			//The first edge is on the boundary, so it completely on a coarse
			//grid edge
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = fEdge->parentEdges[1] =
				&(cGrid->edges[cInd-diff]) ;
				
			//Now assign two edges not on the coarse grid
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = &(cGrid->edges[cInd]) ;
			fEdge->parentEdges[1] = &(cGrid->edges[cInd-diff--]) ;
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = &(cGrid->edges[cInd]) ;
			fEdge->parentEdges[1] = &(cGrid->edges[cInd-diff]) ;
			
			//Assign an edge that is entirely on the coarse grid
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = fEdge->parentEdges[1] =
				&(cGrid->edges[cInd++ - diff]) ;
		}
		
		//Assign the last edge in the row
		fEdge = &(fGrid->edges[fInd++]) ;
		fEdge->parentEdges[0] = fEdge->parentEdges[1] =
			&(cGrid->edges[cInd-diff--]) ;
		cInd -= diff ;
	}
	//Assign the edges on the top row
	for( colCnt = 0 ; colCnt < cGrid->numCols-1 ; colCnt++ ){
		//Get a handle to the fine grid edge
		for( i=0 ; i < 2 ; i++ ){
			fEdge = &(fGrid->edges[fInd++]) ;
			fEdge->parentEdges[0] = fEdge->parentEdges[1] =
				&(cGrid->edges[cInd]) ;
		}
		//Increment the index into the coarse grid edges
		cInd++ ;
	}
	
	//Make sure that the correct number of edges have been assigned
	assert( fInd == fGrid->numEdges ) ;
}

/** Returns the local index of the edge on the given element. It is assumed that
 *	the edge is on the element, and no checks are performed that it is not
 *	\param [in] edge	The edge to find
 *	\param [in] el	The element on which to look for the edge
 *	\return The local index on the given element of the given edge
 */
unsigned int get_edge_el_index( const Edge * edge,
	const Element * el )
{
	unsigned int i ;
	
	//Loop until we have found the appropriate edge. We assume that the equality
	//can be determined by inspecting the location in memory of the edge and
	//element.
	for( i = 0 ; i < 3 ; i++ )
		if( el->edges[i] == edge ) return i ;
		
	//Make sure that the edge is on the element passed in
	assert( i != 2 ) ;
	
	//Return an invalid value so that problems will arise if this is used
	return 3 ;
}

/** This returns the normal as the rotation of the edge anti-clockwise by
 *	90 degrees, disregarding the orientation of the edge. This means that the
 *	normal will point in the same direction, irrespective of the element the
 *	edge is contained in
 *	\param [in] edge	The edge for which to calculate the normal
 *	\param [out] normal	The unit normal vector of the edge
 */
void get_edge_normal( const Edge * edge, Vec2D *normal )
{
	double edgeVec[2] ;
	get_edge_vec_rep( edge, edgeVec ) ;
	normal->x = edgeVec[1] / edge->mag ;
	normal->z = -edgeVec[0] / edge->mag ;
}

/** Checks whether two different node structures are equal. They are considered
 *	equal if their coordinates are equal
 *	\param [in] node1	First node to compare
 *	\param [in] node2	Second node to compare
 *	\return Boolean value indicating whether the nodes are considered equal or
 *			not
 */
bool nodes_eq( const Node * node1, const Node * node2 )
{
	return node1->x == node2->x && node1->z == node2->z ;
}

/** Gets the coordinates of a bounding box of the given element. The coordinates
 *	are given in the form [left, bottom, right, top]
 *	\param [in] el	The element for which to get the bounding box
 *	\param [out] bnds	Coordinates giving a bounding box of the element. This
 *						is stored in the form [left, bottom, right, top]
 */
void get_el_bnd_box( const Element *el, BndBox *bnd )
{
	unsigned int i ;
	double minX, maxX ;
	double minZ, maxZ ;
	const Node *nodes[3] ; //The nodes on the element
	
	//Get the nodes on the element
	get_el_nodes( el, nodes ) ;
	
	//Set the minimum and maximum coordinates to be the first node on the
	//element
	minX = maxX = nodes[0]->x ;
	minZ = maxZ = nodes[0]->z ;
	
	//Change the maximum / minimum values as required
	for( i=1 ; i < 3 ; i++ ){
		if( maxX < nodes[i]->x ) maxX = nodes[i]->x ;
		else if( minX > nodes[i]->x ) minX = nodes[i]->x ;
		
		if( maxZ < nodes[i]->z ) maxZ = nodes[i]->z ;
		else if( minZ > nodes[i]->z ) minZ = nodes[i]->z ;
	}
	
	bnd->left = minX ;
	bnd->bottom = minZ ;
	bnd->right = maxX ;
	bnd->top = maxZ ;
}

/** Sets the soils up on the grid. Each element is assigned a single soil type
 *	at the moment. This can cause problems if a grid does not resolve the
 *	geometry of the soils
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics used for the soils given on the
 *						current grid
 *	\param [in,out] grid	The grid on which to set the soil types. The
 *							soil type is stored in each element on the grid
 */
void set_soil_type( const Params *params, const REData *data, Grid *grid )
{
	unsigned int i ; //Loop counter
	
	//Check that some soil data is stored
	assert( data->numSoils != 0 ) ;
	
	if( params->testCase == 2 || params->testCase == 4 ){
		Element *el ; //An element on the grid
		BndBox elBnd ; //A bounding box of an element
		if( data->numSoils != 2 ){
			printf( "Expected two soil types for test case %u\n",
				params->testCase ) ;
			exit( 0 ) ;
		}
		//I am working with the assumption here that the grid resolves all of
		//the soil data. Therefore all I need to check is the location of the
		//boundaries of the elements
		for( i=0 ; i < grid->numElements ; i++ ){
			//Get a handle to the current element
			el = &(grid->els[i]) ;
			//Get the bounding box of the element
			get_el_bnd_box( el, &elBnd ) ;
			if( (elBnd.right <= 75.0 && elBnd.bottom >= -25.0) ||
				(elBnd.left >= 25.0 && elBnd.top <= -50.0 &&
				elBnd.bottom >= -75.0) )
				el->soilType = 1 ;
			else
				el->soilType = 0 ;
		}
		//Return from the function to avoid executing the default code
		return ;
	}
	else if( params->testCase == 3 ){
		Element *el ; //An element on the grid
		BndBox elBnd ; //A bounding box of an element
		
		if( data->numSoils != 2 ){
			printf( "Expected two soil types for test case %u\n",
				params->testCase ) ;
			exit( 0 ) ;
		}
		//Assume that the grid resolves the soil geometry exactly
		for( i=0 ; i < grid->numElements ; i++ ){
			//Get a handle to the current element
			el = &(grid->els[i]) ;
			//Get the bounding box of the element
			get_el_bnd_box( el, &elBnd ) ;
			if( elBnd.right <= 62.5 && elBnd.left >= 12.5 &&
				elBnd.top <= -12.5 && elBnd.bottom >= -62.5 )
				el->soilType = 1 ;
			else
				el->soilType = 0 ;
		}
		//Return from the function to avoid executing the default code
		return ;
	}
	
	if( data->numSoils > 1 ){
		printf( "More than one soil type passed in. Expected a single soil " ) ;
		printf( "type only for test case %u\n", params->testCase ) ;
		exit( 0 ) ;
	}
	
	//Loop through the grid and set all the soil types to zero. This is the
	//default behaviour
	for( i=0 ; i < grid->numElements ; i++ )
		grid->els[i].soilType = 0 ;
}

/** Prints a grid to the file specified by the filename with the header that is
 *	given
 *	\param [in] fname	The filename to write the grid to
 *	\param [in] header	A useful header to write to the start of the file
 *	\param [in] grid	The grid to write out to file
 */
void write_grid_paraview_format( const char * fname,
	const char * header, const Grid * grid )
{
	unsigned int i ; //Loop counter
	FILE *fout ; //The file to write to
	const Element *e ; //An element on the grid
	const Node *node ; //A node on the grid
	const Node *elNodes[3] ; //The nodes on the element
	
	//Open the file for writing
	fout = fopen( fname, "w" ) ;
	
	//Print the head for the vtk format
	fprintf( fout, "# vtk DataFile Version 4.2\n" ) ;
	fprintf( fout, "%s\n", header ) ;
	fprintf( fout, "ASCII\n" ) ;
	//Start the definition of the dataset
	fprintf( fout, "DATASET UNSTRUCTURED_GRID\n" ) ;
	fprintf( fout, "POINTS %u double\n", grid->numNodes ) ;
	for( i=0 ; i < grid->numNodes; i++ ){
		node = &(grid->nodes[i]) ;
		fprintf( fout, "%.18lf %.18lf 0.0\n", node->x, node->z ) ;
	}
	fprintf( fout, "\n" ) ;
	//Print the information for the triangles on the mesh
	fprintf( fout, "CELLS %u %u\n", grid->numElements, grid->numElements * 4 ) ;
	for( i=0 ; i < grid->numElements ; i++ ){
		//Get a placeholder to the current element
		e = &(grid->els[i]) ;
		//Get the nodes on the element
		get_el_nodes( e, elNodes ) ;
		//Print out the indices of the nodes on the current element
		fprintf( fout, "3 %u %u %u\n", elNodes[0]->id, elNodes[1]->id,
			elNodes[2]->id ) ;
	}
	
	fprintf( fout, "\n" ) ;
	//Print the type of polygon we are dealing with for each of the elements
	fprintf( fout, "CELL_TYPES %d\n", grid->numElements ) ;
	for( i=0 ; i < grid->numElements ; i++ )
		fprintf( fout, "5\n" ) ;
	
	//Close the file
	fclose( fout ) ;
}

/** Writes the mid-points of the edges to a file in vtk format
 *	\param [in] fname	The name of the file to write output to
 *	\param [in] header 	A header to write at the top of a file
 *	\param [in] grid	The grid to be written to file
 */
void write_grid_edge_mids_paraview_format( const char * fname,
	const char * header, const Grid * grid )
{
	unsigned int edCnt ; //Loop counter
	FILE *fout ; //The file to write output to
	const Edge *edge ; //An edge on the grid
	double midPoint[2] ; //The mid point of an edge
	
	//Open the file for writing
	fout = fopen( fname, "w" ) ;
	
	//Print the head for the vtk format
	fprintf( fout, "# vtk DataFile Version 4.2\n" ) ;
	fprintf( fout, "%s\n", header ) ;
	fprintf( fout, "ASCII\n" ) ;
	fprintf( fout, "DATASET POLYDATA\n" ) ;
	//Print the edge midpoints to file
	fprintf( fout, "POINTS %u double\n", grid->numEdges ) ;
	for( edCnt = 0 ; edCnt < grid->numEdges ; edCnt++ ){
		//Get a handle to the current edge
		edge = &(grid->edges[edCnt]) ;
		//Print the mid point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		fprintf( fout, "%.18lf %.18lf 0.0\n", midPoint[0], midPoint[1] ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Calculates the mid-point of an edge
 *	\param [in] edge	The edge to get the mid-point of
 *	\param [out] midPoint	The mid-point of the edge passed in
 */
void get_edge_mid_point( const Edge * edge, double * midPoint )
{
	midPoint[0] = ( edge->endPoints[0]->x + edge->endPoints[1]->x ) / 2.0 ;
	midPoint[1] = ( edge->endPoints[0]->z + edge->endPoints[1]->z ) / 2.0 ;
}

/** Writes a grid in such a format as to allow discontinuous data on elements
 *	to be represented. This means that each element writes out its own nodes
 *	to file
 *	\param [in] fname	The name of the file to write output to
 *	\param [in] header	A header to write at the top of a file
 *	\param [in] grid	The grid to be written to file
 */
void write_grid_for_discont_func_paraview_format( const char * fname,
	const char * header, const Grid * grid )
{
	unsigned int i, elCnt ; //Loop counter
	FILE *fout ; //The file to write to
	const Element *e ; //An element on the grid
	const Node *elNodes[3] ; //The nodes on the element
	
	//Open the file for writing
	fout = fopen( fname, "w" ) ;
	
	//Print the head for the vtk format
	fprintf( fout, "# vtk DataFile Version 4.2\n" ) ;
	fprintf( fout, "%s\n", header ) ;
	fprintf( fout, "ASCII\n" ) ;
	fprintf( fout, "DATASET UNSTRUCTURED_GRID\n" ) ;
	fprintf( fout, "POINTS %u double\n", grid->numElements * 3 ) ;
	
	//Loop over the elements
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get the current element
		e = &(grid->els[elCnt]) ;
		//Get the nodes on the element
		get_el_nodes( e, elNodes ) ;
		//Print each of the nodes to file
		for( i=0 ; i < 3 ; i++ )
			fprintf( fout, "%.18lf %.18lf 0.0\n", elNodes[i]->x,
				elNodes[i]->z ) ;
	}
	
	//Print the elements out to file
	fprintf( fout, "\n" ) ;
	fprintf( fout, "CELLS %u %u\n", grid->numElements, grid->numElements * 4 ) ;
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		i = elCnt * 3 ;
		fprintf( fout, "3 %u %u %u\n", i, i+1, i+2 ) ;
	}
	
	fprintf( fout, "\n" ) ;
	//Print the type of polygon we are dealing with for each of the elements
	fprintf( fout, "CELL_TYPES %d\n", grid->numElements ) ;
	for( i=0 ; i < grid->numElements ; i++ )
		fprintf( fout, "5\n" ) ;
	
	//Close the file
	fclose( fout ) ;
}

/** Writes the edges of a grid to file, so that the values can be defined on
 *	them for the Lagrange multipliers
 *	\param [in] fname	The name of the file to write output to
 *	\param [in] header	A header to write at the top of a file
 *	\param [in] grid	The grid from which the edges are to be written
 */
void write_grid_edge_struct_paraview_format( const char * fname,
	const char * header, const Grid * grid )
{
	unsigned int i, edCnt ; //Loop counters
	unsigned int ind ; //The index of a node in the list of nodes
	const Edge *edge ; //An edge on the grid
	FILE *fout ; //The file to write output to
	
	//Open the file for writing
	fout = fopen( fname, "w" ) ;
	
	//Print the head for the vtk format
	fprintf( fout, "# vtk DataFile Version 4.2\n" ) ;
	fprintf( fout, "%s\n", header ) ;
	fprintf( fout, "ASCII\n" ) ;
	fprintf( fout, "DATASET POLYDATA\n" ) ;
	//Print the points to file
	fprintf( fout, "POINTS %u double\n", grid->numEdges * 2 ) ;
	for( edCnt = 0 ; edCnt < grid->numEdges ; edCnt++ ){
		//Get a handle to the current edge
		edge = &(grid->edges[edCnt] ) ;
		for( i=0 ; i < 2 ; i++ )
			fprintf( fout, "%.18lf %.18lf 0.0\n", edge->endPoints[i]->x,
				edge->endPoints[i]->z ) ;
	}
	
	//Now write out the edges
	fprintf( fout, "\n" ) ;
	fprintf( fout, "LINES %u %u\n", grid->numEdges, grid->numEdges * 3 ) ;
	for( edCnt = 0 ; edCnt < grid->numEdges ; edCnt++ ){
		ind = edCnt * 2 ;
		fprintf( fout, "2 %u %u\n", ind, ind+1 ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Writes the edges of a grid which do not lie on the Dirichlet boundary to
 *	file
 *	\param [in] fname	The name of the file to write output to
 *	\param [in] header	A header to write at the top of the file
 *	\param [in] grid	The grid from which the edges are to be taken
 */
void write_grid_interior_edge_struct_paraview_format( const char *fname,
	const char *header, const Grid *grid )
{
	unsigned int i, edCnt ; //Loop counters
	unsigned int ind ; //The index of a node in the list of nodes
	const Edge *edge ; //An edge on the grid
	FILE *fout ; //The file to write the output to
	
	//Open the file for writing
	fout = fopen( fname, "w" ) ;
	
	if( fout == NULL ){
		printf( "Unable to open file %s for writing in function ", fname ) ;
		printf( "write_grid_interior_edge_struct_paraview_format\n" ) ;
		exit( 0 ) ;
	}
	
	//Print the head for the vtk format
	fprintf( fout, "# vtk DataFile Version 4.2\n" ) ;
	fprintf( fout, "%s\n", header ) ;
	fprintf( fout, "ASCII\n" ) ;
	fprintf( fout, "DATASET POLYDATA\n" ) ;
	//Print the points to file
	fprintf( fout, "POINTS %u double\n", grid->numUnknowns * 2 ) ;
	for( edCnt = 0 ; edCnt < grid->numUnknowns ; edCnt++ ){
		//Get a handle to the current edge
		edge = &(grid->edges[ grid->mapU2G[edCnt] ]) ;
		for( i=0 ; i < 2 ; i++ )
			fprintf( fout, "%.18lf %.18lf 0.0\n", edge->endPoints[i]->x,
				edge->endPoints[i]->z ) ;
	}
	
	//Now write out the edges
	fprintf( fout, "\n" ) ;
	fprintf( fout, "LINES %u %u\n", grid->numUnknowns, grid->numUnknowns *3 ) ;
	for( edCnt = 0 ; edCnt < grid->numUnknowns ; edCnt++ ){
		ind = edCnt * 2 ;
		fprintf( fout, "2 %u %u\n", ind, ind+1 ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Returns the list of nodes on the element, starting with the starting point
 *	of the first edge, going round in an anti-clockwise order
 *	\param [in] el	The element for which to get the nodes
 *	\param [out] nodes	The nodes on the element to return pointers to
 */
void get_el_nodes( const Element * el, const Node **nodes )
{
	unsigned int i ; //Loop counter
	
	//Loop through and set the point at the start of the edge (taking into
	//account the orientation of the edge on the element
	for( i=0 ; i < 3 ; i++ )
		nodes[i] = el->edges[i]->endPoints[!(bool)el->edgeOrient[i]] ;
}

/** Calculates the centre of gravity of a triangular element, which is simply
 *	an average of the nodal coordinates
 *	\param [in] el	The element for which to calculate the centre of gravity
 *	\param [out] cog	The coordinates of the centre of gravity of the
 *						element
 */
void el_center_of_gravity( const Element * el, double * cog )
{
	unsigned int i ; //Loop counter
	const Node *elNodes[3] ;
	
	//Get the nodes on the grid
	get_el_nodes( el, elNodes ) ;
	
	//Set the average x- and z-values
	cog[0] = cog[1] = 0.0 ;
	for( i=0 ; i < 3 ; i++ ){
		cog[0] += elNodes[i]->x ;
		cog[1] += elNodes[i]->z ;
	}
	cog[0] /= 3.0 ;
	cog[1] /= 3.0 ;
}

/** Writes a function which is piecewise constant on elements of a grid to file
 *	\param [in] fname	The name of the file to write to
 *	\param [in] grid	The grid on which the function is defined
 *	\param [in] elFunc	The piecewise constant function defined on the elements
 */
void write_element_func_paraview_format( const char * fname,
	const Grid * grid, const double * elFunc )
{
	unsigned int elCnt, i ; //Loop counters
	FILE *fout ; //The file to write to
	
	char header[256] ;
	sprintf( header, "Piecwise constant cell data" ) ;
	
	//Print the grid at the start of the file
	write_grid_for_discont_func_paraview_format( fname, header, grid ) ;
	
	//Open the file to append to it
	fout = fopen( fname, "a" ) ;
	
	fprintf( fout, "\n" ) ;
	fprintf( fout, "POINT_DATA %u\n", grid->numElements * 3 ) ;
	fprintf( fout, "SCALARS el_scalars double 1\n" ) ;
	fprintf( fout, "LOOKUP_TABLE default\n" ) ;
	
	//Write the values of the function to file
	for( elCnt=0 ; elCnt < grid->numElements ; elCnt++ ){
		for( i=0 ; i < 3 ; i++ )
			fprintf( fout, "%.18lf\n", elFunc[elCnt] ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Writes a function which is piecewise constant on the edges of a grid to file
 *	\param [in] fname	The name of the file to write to
 *	\param [in] grid	The grid on which the function is defined
 *	\param [in] edFunc	The piecewise constant function on the edges of the grid
 */
void write_edge_func_paraview_format( const char * fname,
	const Grid * grid, const double * edFunc )
{
	unsigned int i, elCnt ; //Loop counter
	FILE *fout ; //The file to write output to

	char header[256] ;
	sprintf( header, "Piecewise constant edge data" ) ;
	
	//Write the edges at the start of the file
	write_grid_edge_struct_paraview_format( fname, header, grid ) ;
	
	//Open the file for appending
	fout = fopen( fname, "a" ) ;
	
	fprintf( fout, "\n" ) ;
	fprintf( fout, "POINT_DATA %u\n", grid->numEdges * 2 ) ;
	fprintf( fout, "SCALARS ed_scalars double 1\n" ) ;
	fprintf( fout, "LOOKUP_TABLE default\n" ) ;
	
	//Write the values of the function to file
	for( elCnt=0 ; elCnt < grid->numEdges ; elCnt++ ){
		for( i=0 ; i < 2 ; i++ )
			fprintf( fout, "%.18lf\n", edFunc[grid->edges[elCnt].id] ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Writes a function which is piecewise constant on the interior edges of a
 *	grid to file. The function values are not specified on the Dirichlet
 *	boundary of the domain. The values on the edges on the Dirichlet boundary
 *	are given as zero. This will be correct when the function being written is
 *	the residual, as the residual is exact at Dirichlet boundaries
 *	\param [in] fname	The name of the file to write to
 *	\param [in] grid	The grid on which the function is defined
 *	\param [in] edFunc	The piecewise constant function on the non-Dirichlet
 *						boundary edges
 */
void write_interior_edge_func_paraview_format( const char *fname,
	const Grid *grid, const double *edFunc )
{
	unsigned int i, edCnt ; //Loop counters
	FILE *fout ; //The file to write output to
	
	char header[256] ;
	sprintf( header, "Piecewise constant residual data" ) ;
	
	//Write the edges at the start of the file
	write_grid_interior_edge_struct_paraview_format( fname, header, grid ) ;
	
	//Open the file for appending
	fout = fopen( fname, "a" ) ;
	
	fprintf( fout, "\n" ) ;
	fprintf( fout, "POINT_DATA %u\n", grid->numUnknowns * 2 ) ;
	fprintf( fout, "SCALARS ed_scalars double 1\n" ) ;
	fprintf( fout, "LOOKUP_TABLE default\n" ) ;
	
	//Write the values of the function to file
	for( edCnt = 0 ; edCnt < grid->numUnknowns ; edCnt++ ){
		for( i=0 ; i < 2 ; i++ )
			fprintf( fout, "%.18lf\n", edFunc[edCnt] ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Writes a function of vector values of a flux over the edges of  a grid to
 *	file in a format that can be read by paraview
 *	\param [in] fname	The name of the file to write to
 *	\param [in] grid	The grid on which the function is defined
 *	\param [in] edNormFunc	Function of normal components to an edge on a grid
 */
void write_edge_vec_func_paraview_format( const char * fname,
	const Grid * grid, const Vec2D * edNormFunc )
{
	unsigned int edCnt ; //Loop counter
	unsigned int ind ; //Index into the vector function to be written
	FILE *fout ; //File to write output ot
	
	char header[256] ;
	sprintf( header, "Flux data over grid edges" ) ;
	
	//Write the edge mid-points to file
	write_grid_edge_mids_paraview_format( fname, header, grid ) ;
	
	//Open the file for appending
	fout = fopen( fname, "a" ) ;
	
	fprintf( fout, "\n" ) ;
	fprintf( fout, "POINT_DATA %u\n", grid->numEdges ) ;
	fprintf( fout, "VECTORS flux_data double\n" ) ;
	
	//Write the values of the flux to file
	for( edCnt=0 ; edCnt < grid->numEdges ; edCnt++ ){
		ind = grid->edges[edCnt].id ;
		fprintf( fout, "%.18lf %.18lf 0\n", edNormFunc[ ind ].x,
			edNormFunc[ind].z ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Writes the normal components of the fluxes over the edges of a grid to file
 *	in a paraview format. The vector passed in is the magnitude of the normal
 *	component of a flux over an edge
 *	\param [in] fname	The name of the file to write to
 *	\param [in] grid	The grid on which the magnitude of the normal component
 *						of the flux is given
 *	\param [in] fluxMag The magnitude of the normal component of the flux over
 *						the edges of the grid
 */
void write_edge_flux_func_paraview_format( const char * fname,
	const Grid *grid, const double *fluxMag )
{
	unsigned int i ; //Loop counter
	Vec2D *normalFlux ; //Will store the normal fluxes over the edges
	const Edge *edge ; //An edge on the grid
	
	//Set the appropriate memory for the normal flux
	normalFlux = (Vec2D*)calloc( grid->numEdges, sizeof( Vec2D ) ) ;
	
	//Loop over the edges of the grid
	for( i=0 ; i < grid->numEdges ; i++ ){
		//Get a placeholder to the current edge
		edge = &(grid->edges[i]) ;
		//Get the normal on the edge
		get_edge_normal( edge, &(normalFlux[edge->id]) ) ;
		//Multiply the normal components by the appropriate magnitude
		normalFlux[edge->id].x *= fluxMag[edge->id] ;
		normalFlux[edge->id].z *= fluxMag[edge->id] ;
	}
	
	//Write the calculated function out to file
	write_edge_vec_func_paraview_format( fname, grid, normalFlux ) ;
	
	//Free the memory assigned on the heap
	free( normalFlux ) ;
}

/** Gets the vector representation of the edge. This does not take into account
 *	the orientation of the edge on an element
 *	\param [in] edge	The edge for which to get the vector representation
 *	\param [out] edgeVec	The vector representation of the edge
 */
void get_edge_vec_rep( const Edge * edge, double * edgeVec )
{
	edgeVec[0] = edge->endPoints[1]->x - edge->endPoints[0]->x ;
	edgeVec[1] = edge->endPoints[1]->z - edge->endPoints[0]->z ;
}

/** Gets the normal component of a flux over the edge
 *	\param [in] edge	The edge over which to compute the normal component
 *	\param [in] flux	The flux function, not necessarily normal to the edge
 *	\param [out] normFlux	The normal flux over the edge
 */
void get_vec_edge_normal_comp( const Edge * edge, const Vec2D *flux,
	Vec2D *normFlux )
{
	Vec2D normVec ; //Vector representation of the normal to the edge
	double scaleFact ; //The scaling factor used to make the edge perpendicular
	
	//Get the vector representation of the edge
	get_edge_normal( edge, &normVec ) ;
	
	scaleFact = (normVec.x * flux->x + normVec.z * flux->z) ;
	normFlux->x = scaleFact * normVec.x ;
	normFlux->z = scaleFact * normVec.z ;
}

/** Indicates whether the given element is incident to a Dirichlet boundary or
 *	not
 *	\param [in] el	The element to check whether it is incident to a Dirichlet
 *					boundary or not
 *	\return Boolean value indicating whether the element is incident to a
 *			Dirichlet boundary
 */
bool is_dir_bnd_el( const Element * el )
{
	unsigned int i ; //Loop counter
	
	//Loop over the edges of the element and check whether they lie on the
	//Dirichlet boundary or not
	for( i=0 ; i < 3 ; i++ )
		if( el->edges[i]->type == EDGE_KNOWN ) return true ;
		
	//If none of the edges were on the Dirichlet boundary return false
	return false ;
}

/** Reads a hierarchy of grids from file and saves them in the array of pointers
 *	given by \p gridHierarchy. It is assumed that no memory has been assigned
 *	for the hierarchy of grids
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\return	The hierarchy of grids to be populated
 */
Grid** read_grid_hierarchy( const Params * params,
	const REData * data )
{
	unsigned int i ; //Loop counter
	Grid **gridHierarchy ; //The hierarchy of grids to use
	
	//Set the memory for the hierarchy of grids
	gridHierarchy = (Grid**)calloc( params->fGridLevel+1, sizeof( Grid* ) ) ;
	
	//For each of the grids to use initialise a pointer and read in the grid
	//from file
	for( i = params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		gridHierarchy[i] = (Grid*)calloc( 1, sizeof( Grid ) ) ;
		//Set the grids to NULL initially
		set_grid_to_null( gridHierarchy[i] ) ;
		//Read the grid in from file
		read_grid_from_file( gridFiles[i], gridHierarchy[i] ) ;
		set_soil_type( params, data, gridHierarchy[i] ) ;
	}
	
	//Set the parents of the edges in the hierarchy
	for( i=params->fGridLevel ; i > params->cGridLevel ; i-- ){
		set_edge_parents( gridHierarchy[i-1], gridHierarchy[i] ) ;
	}
	
	return gridHierarchy ;
}

/** Frees the memory assigned on the heap for the hiearchy of grids passed in
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] gridHierarchy	The grid hierarchy to free the memory for
 */
Grid** free_grid_hierarchy( const Params *params, Grid **gridHierarchy )
{
	unsigned int i ; //Loop counter
	
	//Loop through the grids and free them one by one
	for( i = params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		//Free the grid
		free_grid( gridHierarchy[i] ) ;
		//Free the pointer to the grid
		free( gridHierarchy[i] ) ;
		//Set the pointer to the grid to be NULL
		gridHierarchy[i] = NULL ;
	}
		
	//Free the memory assigned for the array of pointers
	free( gridHierarchy ) ;
	return NULL ;
}

/** Writes information about the grid out to console. This is useful for 
 *	debugging purposes
 *	\param [in] heading	A head to print at the start of the information
 *	\param [in] grid	The grid for which to write the information to console
 */
void write_grid_info_to_console( const char * heading,
	const Grid * grid )
{
	unsigned int i ; //Loop counter
	printf( "%s\n", heading ) ;
	printf( "  Number of rows: %u\n", grid->numRows ) ;
	printf( "  Number of columns: %u\n", grid->numCols ) ;
	printf( "  Number of nodes: %u\n", grid->numNodes ) ;
	printf( "  Number of edges: %u\n", grid->numEdges ) ;
	printf( "  Number of Unknowns: %u\n", grid->numUnknowns ) ;
	printf( "  Boundary Type: [" ) ;
	for( i=0 ; i < 4 ; i++ )
		printf( " %d", (int)grid->bndType[i] ) ;
	printf( " ]\n" ) ;
}

/** Writes information regarding the element passed in to console
 *	\param [in] heading	A head to print at the start of the information
 *	\param [in] el	The element for which to write information to console
 */
void write_el_info_to_console( const char * heading,
	const Element * el )
{
	unsigned int i ; //Loop counter
	const Node *nodes[3] ; //The nodes on the element
	
	//Get the nodes on the element
	get_el_nodes( el, nodes ) ;
	
	//Print the heading
	printf( "%s\n", heading ) ;
	
	//Print the nodes on the grid
	printf( "  Nodes:\n" ) ;
	for( i=0 ; i < 3 ; i++ )
		printf( "    Node %u: [ %.10lf, %.10lf ]\n", i+1, nodes[i]->x,
			nodes[i]->z ) ;
	printf( "  Area: %.18lf\n", el->area ) ;
	
}

/** Writes a vector to file, with an entry per line of the file
 *	\param [in] fname	The name of the file to write to
 *	\param [in] length	The length of the vector to write
 *	\param [in] vec	The vector, the entries of which are to be written to file
 */
void write_dvec_to_file( const char *fname, unsigned int length,
	const double *dvec )
{
	unsigned int i ; //Loop counter
	FILE *fout ; //The file to write output to
	
	//Open the file for writing
	fout = fopen( fname, "w" ) ;
	
	if( fout == NULL ){
		printf( "Unable to open file %s for writing\n", fname ) ;
		exit( 0 ) ;
	}
	
	//Loop through the entries of the vector, and write each on a line of the
	//file
	for( i=0 ; i < length ; i++ )
		fprintf( fout, "%.18lf\n", dvec[i] ) ;
	
	//Close the file
	fclose( fout ) ;
}

/** Reads data from file and saves them in thegiven vector. It is assumed that
 *	both the file and the vector have at least \p length entries in them, and
 *	that all of the data in the file are in a double format
 *	\param [in] fname	The file to read from
 *	\param [in] length	The number of entries to read
 *	\param [out] dvec	The vector to populate with values from the file
 */
void read_dvec_from_file( const char *fname, unsigned int length, double *dvec )
{
	unsigned int i ; //Loop counter
	FILE *fin ; //The file to read from
	
	//Open the file for reading
	fin = fopen( fname, "r" ) ;
	
	if( fin == NULL ){
		printf( "Error opening file %s for reading\n", fname ) ;
		exit( 0 ) ;
	}
	
	//Read the data in
	for( i=0 ; i < length ; i++ )
		fscanf( fin, "%lf", dvec + i ) ;
	
	//Close the file
	fclose( fin ) ;
}

/** Returns a factor required in the calculation of the residual and the
 *	Jacobian of the mixed finite element formulation of the Richards equation
 *	\param [in] edge	The edge on the element for which to calculate the
 *						factor
 *	\param [in] el	The element on which to calculate the factor
 *	\return The appropriate factor to use in the calculation of the residual
 *			and the Jacobian
 */
double calc_zfact( const Element *el, unsigned int edgeInd )
{
	return ((el->edges[edgeInd]->endPoints[0]->z +
		el->edges[edgeInd]->endPoints[1]->z) / 2  -
		get_z_op( el, edgeInd )) / 3 ;
}

/** Writes information about the nodes of the element to console. This is useful
 *	for debugging purposes
 *	\param [in] el	The element for which to write information to the console
 */
void write_el_nodes_to_console( const Element *el )
{
	unsigned int i ; //Loop counter
	const Node *nodes[3] ; //The nodes on the element 
	
	//Get the current nodes on the grid
	get_el_nodes( el, nodes ) ;
	//For each node write some information out to the console
	for( i=0 ; i < 3 ; i++ )
		print_indent( 1, "Node %u: %.10lf %.10lf\n", i, nodes[i]->x,
			nodes[i]->z ) ;
}

/** Performs the restriction of a function that is piecewise constant on a grid
 *	\param [in] fGrid	The fine grid from which to restrict
 *	\param [in] cGrid	The coarse grid to restrict onto
 *	\param [in] fFunc	The function to restrict
 *	\param [out] cFunc	The function to restrict onto
 */
void restrict_const_el_func( const Grid *fGrid, const Grid *cGrid,
	const double *fFunc, double *cFunc )
{
	unsigned int rowCnt ; //Counter for the number of rows on the coarse grid
	unsigned int colCnt ; //Counter for the number of columns on the coarse grid
	unsigned int fElsPerRow ; //The number of elements per row on the fine grid
	unsigned int cElsPerRow ; //The number of elements per row on the coarse
							  //grid
	unsigned int cEl ; //Index of an element on the coarse grid
	unsigned int fEl ; //Index of an element on the fine grid
					
	//Set the number of elements that there are in a row on the fine grid
	fElsPerRow = 2 * (fGrid->numCols - 1) ;
	cElsPerRow = 2 * (cGrid->numCols - 1) ;
	
	//Loop through all of the rows on the grid
	for( rowCnt = 0 ; rowCnt < cGrid->numRows-1 ; rowCnt ++ ){
		//Set the values for the elements with hypotenuse on the lower side
		//in the current coarse grid row
		fEl = 2 * rowCnt * fElsPerRow ;
		cEl = rowCnt * cElsPerRow ;
		for( colCnt = 0 ; colCnt < cGrid->numCols-1 ; colCnt++, cEl+=2, fEl+=4 )
			cFunc[cEl] = (fFunc[fEl] + fFunc[fEl + fElsPerRow] +
				fFunc[fEl + fElsPerRow + 1] + fFunc[fEl + fElsPerRow + 2])
				/ 4.0 ;
			
		//Set the values for the elements with hypotenuse on the upper side
		//in the current coarse grid row
		fEl = 2 * rowCnt * fElsPerRow + 1 ;
		cEl = rowCnt * cElsPerRow + 1 ;
		for( colCnt = 0 ; colCnt < cGrid->numCols-1 ; colCnt++, cEl+=2, fEl+=4 )
			cFunc[cEl] = (fFunc[fEl] + fFunc[fEl+1] + fFunc[fEl + 2] +
				fFunc[fEl + 2 + fElsPerRow]) / 4.0 ;
	}
}















