#include "newtonIteration.h"
#include "richardsFunctions.h"
#include "gridHandling.h"
#include "linApprox.h"
#include "interpolation.h"
#include "sparseMatrix.h"

/** Sets the pointers required for a Newton iteration to NULL
 *	\param [in] ctxt	Context storing the information required to perform a
 *						Newton iteration
 */
void set_newton_ctxt_to_null( NewtonCtxt *ctxt )
{
	ctxt->linItCtxt = NULL ;
	set_re_vars_to_null( &(ctxt->reVars) ) ;
}

/**	Frees the memory assigned on the heap for the given context
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] ctxt	Context for which to free the memory
 */
void free_newton_ctxt( const Params *params, NewtonCtxt *ctxt )
{
	//Free the variables required to solve the mixed finite element formulation
	//of the Richards equation
	free_re_vars( &(ctxt->reVars) ) ;
	if( ctxt->linItCtxt != NULL ){
		//Perform the correct free depending on which linear iteration was used
		if( params->linIt == IT_LIN_MG ){
			free_lin_mg_ctxt( params, (LinMGCtxt*)ctxt->linItCtxt ) ;
			ctxt->grid = NULL ;
			ctxt->linVars = NULL ;
		}
		else if( params->linIt == IT_PGMRES ){
			free_prec_gmres_ctxt( params, (PrecGMRESCtxt*)ctxt->linItCtxt ) ;
			ctxt->grid = NULL ;
			ctxt->linVars = NULL ;
		}
		
		//Free the memory held by the pointer to the linear iteration context
		free( ctxt->linItCtxt ) ;
		ctxt->linItCtxt = NULL ;
	}
	
	if( params->linIt == IT_GMRES ){
		//If we were performing GMRES then there is only a single grid, which is
		//stored as part of the Newton context rather than the context for
		//GMRES
		if( ctxt->grid != NULL ){
			free_grid( ctxt->grid ) ;
			ctxt->grid = NULL ;
		}
		if( ctxt->linVars != NULL ){
			free_lin_approx_vars( ctxt->linVars ) ;
			ctxt->linVars = NULL ;
		}
	}
}

/** Sets the memory for the variables in the context storing information
 *	necessary to perform a Newton-Multigrid iteration
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] ctxt	The context for which to set the memory
 */
void setup_newton_ctxt( const Params *params, const REData *data,
	NewtonCtxt *ctxt )
{		
	//Set up the information required for the linear solver
	if( params->linIt == IT_LIN_MG ){
		//If we are performing a linear multigrid iteration for the inner
		//iteration set the appropriate memory
		
		//Set the memory for the context
		ctxt->linItCtxt = (void*)calloc( 1, sizeof( LinMGCtxt ) ) ;
		//Now set the memory for the linear multigrid context
		setup_lin_mg_ctxt( params, data, (LinMGCtxt*)ctxt->linItCtxt ) ;
		//Set the finest grid for the Newton iteration
		ctxt->grid = get_newton_grid( params, ctxt ) ;
		ctxt->linVars = get_newton_lin_vars( params, ctxt ) ;
	}
	else if( params->linIt == IT_PGMRES ){
		//We are performing a multigrid preconditioned GMRES iteration
		
		//Set the memory for the context
		ctxt->linItCtxt = (void*)calloc( 1, sizeof( PrecGMRESCtxt ) ) ;
		setup_mg_prec_gmres_ctxt( params, data,
			(PrecGMRESCtxt*)ctxt->linItCtxt ) ;
		//Set the finest grid for the Newton iteration
		ctxt->grid = get_newton_grid( params, ctxt ) ;
		ctxt->linVars = get_newton_lin_vars( params, ctxt ) ;
	}
	else{
		printf( "Set up of the Newton context not yet implemented for " ) ;
		printf( "the given linear iteration\n" ) ;
		exit( 0 ) ;
	}
		
	//Setup the variables associated with the mixed finite element formulation
	//of the Richards equation on the finest grid
	set_re_vars_to_null( &(ctxt->reVars) ) ;
	setup_re_vars_on_grid( params, ctxt->grid, &(ctxt->reVars) ) ;
}

/** Returns the grid on which to perform a Newton iteration. This requires
 *	different functionality depending on the type of linear solve used
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] ctxt	Variables required to perform a Newton iteration
 *	\return Pointer to the grid on which to perform a Newton iteration
 */
Grid* get_newton_grid( const Params *params, const NewtonCtxt *ctxt )
{
	if( params->linIt == IT_LIN_MG ){
		LinMGCtxt *linMGCtxt ;
		linMGCtxt = (LinMGCtxt*)ctxt->linItCtxt ;
		
		return linMGCtxt->gridHierarchy[params->fGridLevel] ;
	}
	else if( params->linIt == IT_PGMRES ){
		PrecGMRESCtxt *gmresCtxt ;
		gmresCtxt = (PrecGMRESCtxt*)ctxt->linItCtxt ;
		
		return gmresCtxt->gridHierarchy[params->fGridLevel] ;
	}
	else{
		printf( "Functionality to retrieve grid for Newton iteration not " ) ;
		printf( "yet defined for the given linear iteration\n" ) ;
		exit( 0 ) ;
	}
	return NULL ;
}

/** Returns the variables required to perform a linear solve for a Newton
 *	iteration
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] ctxt	The Newton context for which to retrieve the variables
 *						required for a linear solve
 *	\return	Variables required to perform a linear solve for a Newton iteration
 */
LinApproxVars* get_newton_lin_vars( const Params *params,
	const NewtonCtxt *ctxt )
{
	if( params->linIt == IT_LIN_MG ){
		LinMGCtxt *linMGCtxt ;
		linMGCtxt = (LinMGCtxt*)ctxt->linItCtxt ;
		
		return linMGCtxt->linHierarchy[params->fGridLevel] ;
	}
	else if( params->linIt == IT_PGMRES ){
		PrecGMRESCtxt *gmresCtxt ;
		gmresCtxt = (PrecGMRESCtxt*)ctxt->linItCtxt ;
		
		return gmresCtxt->linHierarchy[params->fGridLevel] ;
	}
	else{
		printf( "Functionality to retrieve linear approximation variables " ) ;
		printf( "not defined for the given linear iteration\n" ) ;
		exit( 0 ) ;
	}
	return NULL ;
}

/** Returns the hierarchy of variables required to perform a linear solve on
 *	each grid in a hierarchy of grids
 *	\param [in] ctxt	The context for which to return the hierarchy of
 *						variables required to perform a linear solve on each
 *						grid in a hierarchy
 */
LinApproxVars** get_newton_lin_hierarchy( const Params *params,
	const NewtonCtxt *ctxt )
{
	if( params->linIt == IT_LIN_MG ){
		LinMGCtxt *mgCtxt ;
		mgCtxt = (LinMGCtxt*)ctxt->linItCtxt ;
		
		return mgCtxt->linHierarchy ;
	}
	else if( params->linIt == IT_PGMRES ){
		PrecGMRESCtxt *gmresCtxt ;
		gmresCtxt = (PrecGMRESCtxt*)ctxt->linItCtxt ;
		
		return gmresCtxt->linHierarchy ;
	}
	else{
		printf( "Undefined behaviour to get the hierarchy of linear " ) ;
		printf( "variables for the Newton iteration for the specified " ) ;
		printf( "inner iteration\n" ) ;
		exit( 0 ) ;
	}
	return NULL ;
}

/** Returns the hierarchy of grids used in a Newton iteration, if any
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] ctxt	The Newton context for which to return the hierarchy of
 *						grids
 *	\return The hierarchy of grids (if any) used in the Newton iteration
 *			described by the context passed in
 */
Grid** get_newton_grid_hierarchy( const Params *params,
	const NewtonCtxt *ctxt )
{
	if( params->linIt == IT_LIN_MG ){
		LinMGCtxt *mgCtxt ;
		mgCtxt = (LinMGCtxt*)ctxt->linItCtxt ;
		
		return mgCtxt->gridHierarchy ;
	}
	else if( params->linIt == IT_PGMRES ){
		PrecGMRESCtxt *gmresCtxt ;
		gmresCtxt = (PrecGMRESCtxt*)ctxt->linItCtxt ;
		
		return gmresCtxt->gridHierarchy ;
	}
	else{
		printf( "Undefined behaviour to get the hierarchy of grids for the " ) ;
		printf( "Newtion iteration for the specified inner iteration\n" ) ;
		exit( 0 ) ;
	}
	return NULL ;
}

/** Returns the interpolation operators between grids in a hierarchy used in a
 *	Newton iteration, if any
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] ctxt	The Newton context for which to return the interpolation
 *						operators
 *	\return The interpolation operators (if any) used in the Newton iteration
 *			described by the context passed in
 */
Interpolation** get_newton_interp_hierarchy( const Params *params,
	const NewtonCtxt *ctxt )
{
	if( params->linIt == IT_LIN_MG ){
		LinMGCtxt *mgCtxt ;
		mgCtxt = (LinMGCtxt*)ctxt->linItCtxt ;
		
		return mgCtxt->interpHierarchy ;
	}
	else if( params->linIt == IT_PGMRES ){
		PrecGMRESCtxt *gmresCtxt ;
		gmresCtxt = (PrecGMRESCtxt*)ctxt->linItCtxt ;
		
		return gmresCtxt->interpHierarchy ;
	}
	else{
		printf( "Undefined behavirou to get the interpolation operators " ) ;
		printf( "for the Newton iteration for the specified inner " ) ;
		printf( "iteration\n" ) ;
		exit( 0 ) ;
	}
	return NULL ;
}

/** Sets variables requried for the execution of a Newton iteration. This
 *	the setup of the residual and right hand side on the fine grid, and the
 *	setup of the Jacobian (or approximate Jacobian) matrices on all grid levels
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for soils to be used in the current
 *						problem
 *	\param [out] ctxt	Context storing information necessary for the execution
 *						of a Newton-Multigrid iteration
 *	\param [in] gridHierarchy	The hierarchy of grids to use in the multigrid
 *								iteration
 *	\param [in,out] linHierarchy	Variables required to perform a linear solve
 *									on each grid in the hierarchy
 *	\param [in] interpHierarchy	Interpolation operators betweeen the grids in
 *								the hierarchy
 */
void set_newton_mg_iteration_vars( const Params *params, const REData *data,
	NewtonCtxt *ctxt, Grid **gridHierarchy, LinApproxVars **linHierarchy,
	Interpolation **interpHierarchy )
{
	//We assume that the residual has not been calculated. The first thing
	//to calculate is the residual and Jacobian matrix on the finest grid level
	calc_richards_resid_and_jacobian( params,
		gridHierarchy[ params->fGridLevel ], data,
		&(ctxt->reVars), &(linHierarchy[ params->fGridLevel ]->op) ) ;
	
	//Make sure that the interpolation operators have been set
	assert( interpHierarchy[ params->fGridLevel ]->r.numNonZeros != 0 ) ;
	
	//Calculate the Galerkin coarse grid operators
	set_galerkin_coarse_operators( params, linHierarchy, interpHierarchy ) ;
}

/** Sets the coarse grid operators in the hierarchy using the Galerkin
 *	formulation
 *	\param [in,out] linHierarchy	The hierarchy of contexts used to store
 *									information necessary to solve a linear
 *									problem. The variable LinApproxVars::op is
 *									set on each coarse grid level
 *	\param [in] interpHierarchy	Hierarhcy of interpolation operators to use
 */
void set_galerkin_coarse_operators( const Params *params,
	LinApproxVars **linHierarchy, Interpolation **interpHierarchy )
{
	unsigned int i ; //Loop counter
	
	//It is assumed that the memory has already been set for all of the
	//interpolation operators and for the matrices on all of the grid levels
	//Make sure that the memory has been assigned for the interpolation
	//operators
	assert( interpHierarchy[ params->fGridLevel ]->r.numNonZeros != 0 ) ;
	assert( linHierarchy[ params->fGridLevel-1]->op.numNonZeros != 0 ) ;
	
	//Loop through the grids from the finer to coarser and calculate the coarse
	//grid operators using the fine grid operator
	for( i=params->fGridLevel ; i > params->cGridLevel ; i-- ){
		assert( linHierarchy[i]->op.numNonZeros != 0 ) ;
		assert( interpHierarchy[i]->r.numNonZeros != 0 ) ;
		assert( interpHierarchy[i-1]->p.numNonZeros != 0 ) ;
		assert( linHierarchy[i-1]->op.numNonZeros != 0 ) ;
		
		//Set the coarse grid operator to add the entries in the Galerkin coarse
		//grid operator not in the sparsity pattern to the diagonal of the row
		calculate_coarse_grid_op( &(linHierarchy[i]->op),
			&(interpHierarchy[i]->r), &(interpHierarchy[i-1]->p),
			&(linHierarchy[i-1]->op) ) ;
	}
}

/** Sets the variables for an initial approximation to the linear Newton system
 *	on the given grid. This involves setting a zero initial approximation, and
 *	setting the right hand side of the linear system to be the residual of the
 *	nonlinear system
 *	\param [in] reVars	Variables associated with the approximation of the mixed
 *						finite element formulation of the Richards equation
 *	\param [in] grid	The grid on which the Newton system is to be set up
 *	\param [out] linVars	The variables required to solve the linear Newton
 *							system
 */
void init_newt_approx_on_grid( const REApproxVars *reVars, const Grid *grid,
	LinApproxVars *linVars )
{	
	//Set the linear approximation to zero
	set_dvec_to_zero( grid->numUnknowns, linVars->approx ) ;
	
	//Copy the nonlinear residual into the linear right hand side
	copy_dvec( grid->numUnknowns, reVars->resid, linVars->rhs ) ;
}

/** Sets the variables required to perform a linear iteration as part of a
 *	Newton iteration. The functionality depends on the linear iteration being
 *	performed
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for the soils used in the current
 *						problem
 *	\param [out] ctxt	The Newton context for which the variables should be set
 *						up in order to perform the inner iteration
 */
void set_newton_lin_vars( const Params *params, NewtonCtxt *ctxt )
{
	if( params->linIt == IT_LIN_MG ){
		//Get a handle to the multigrid context
		LinMGCtxt *mgCtxt ;
		mgCtxt = (LinMGCtxt*)ctxt->linItCtxt ;
		
		//We need to set the coarse grid operators here. We assume that the
		//Jacobian stored on the fine grid is up to date
		set_galerkin_coarse_operators( params, mgCtxt->linHierarchy,
			mgCtxt->interpHierarchy ) ;
	}
	else if( params->linIt == IT_PGMRES ){
		//Get a handle to the preconditioned GMRES context
		PrecGMRESCtxt *gmresCtxt ;
		gmresCtxt = (PrecGMRESCtxt*)ctxt->linItCtxt ;
		
		if( params->precSymm ){
			double *swapPtr = ctxt->linVars->op.globalEntry ;
			ctxt->linVars->op.globalEntry = gmresCtxt->precMatrix ;
			
			//We need to set the coarse grid operators here. We assume that the
			//Jacobian stored on the fine grid is up to date
			set_galerkin_coarse_operators( params, gmresCtxt->linHierarchy,
				gmresCtxt->interpHierarchy ) ;
			
			ctxt->linVars->op.globalEntry = swapPtr ;
		}
		else{
			//We need to set the coarse grid operators here. We assume that the
			//Jacobian stored on the fine grid is up to date
			set_galerkin_coarse_operators( params, gmresCtxt->linHierarchy,
				gmresCtxt->interpHierarchy ) ;
		}
	}
	else{
		printf( "Functionality to set the variables at the start of a " ) ;
		printf( "Newton iteration not defined for the given linear " ) ;
		printf( "iteration\n" ) ;
		exit( 0 ) ;
	}
}

/** Performs the linear inner iteration as part of a Newton iteration
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] ctxt	The Newton context for which a linear inner solve is to
 *						be performed
 */
void perform_newton_inner_it( Params *params, NewtonCtxt *ctxt )
{
	unsigned int itCnt ; //Loop counter
	
	if( params->linIt == IT_LIN_MG ){
//		double origResid, resid, oldResid ;
//		LinApproxVars *fVars ;
//		Grid *fGrid ;
		//Get a handle to the multigrid context
		LinMGCtxt *mgCtxt ;
		mgCtxt = (LinMGCtxt*)ctxt->linItCtxt ;
		
//		fVars = mgCtxt->linHierarchy[params->fGridLevel] ;
//		fGrid = mgCtxt->gridHierarchy[params->fGridLevel] ;
//		calculate_lin_resid( fVars ) ;
//		origResid = resid = discrete_l2_norm(fGrid->numUnknowns, fVars->resid) ;
		
		//Now perform the specified number of multigrid iterations
//		printf( "\n" ) ;
		for( itCnt=0 ; itCnt < params->numLinIts ; itCnt++ ){
//			oldResid = resid ;
			linear_multigrid( params, mgCtxt->gridHierarchy,
				mgCtxt->linHierarchy, mgCtxt->interpHierarchy,
				params->fGridLevel ) ;
				
//			calculate_lin_resid( fVars ) ;
//			resid = discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ;
//			print_indent(1, "Resid: %.18lf\tRatio: %.18lf\n", resid,
//				resid / oldResid ) ;
		}
	}
	else if( params->linIt == IT_PGMRES ){
		//Get a handle to the preconditioned GMRES context
		PrecGMRESCtxt *gmresCtxt ;
		gmresCtxt = (PrecGMRESCtxt*)ctxt->linItCtxt ;
		
		//Perform the specified number of preconditioned GMRES iterations
		preconditioned_gmres_iteration( params, gmresCtxt, params->numLinIts ) ;
	}
	else{
		printf( "The specified linear solve has not been set up to be used " ) ;
		printf( "as part of a Newton iteration\n" ) ;
		exit( 0 ) ;
	}
}

/** Performs a Newton iteration for the current time-step of the Richards
 *	equation
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for soils to be used in the current
 *						problem
 * 	\param [in,out] ctxt	Stores all the information required to perform a
 *							Newton-Multigrid solve of a nonlinear system of
 *							equations
 */
void richards_newton_step( Params *params, const REData *data,
	NewtonCtxt *ctxt )
{
	REApproxVars *reVars ; //Variables required to solve the mixed finite
				//element formulation of the Richards equation
	double *swapPtr = NULL ;
	PrecGMRESCtxt *gmresCtxt = NULL ;
	char outFName[20] ; //Used for debugging
	
	//We assume that the previous value of the volumetric wetness has been set
	//and that the pressure is also up to date
	reVars = &(ctxt->reVars) ;
	
	ctxt->numIts = 0 ;
	
	//First step is to calculate the nonlinear residual and the Jacobian matrix
	calc_richards_resid_and_jacobian( params, ctxt->grid, data, reVars,
		&(ctxt->linVars->op) ) ;
		
	//Calculate the residual norm
	ctxt->origResidNorm = discrete_l2_norm( ctxt->grid->numUnknowns,
		reVars->resid ) ;
	ctxt->currResidNorm = ctxt->origResidNorm ;
	
	if( params->linIt == IT_PGMRES && params->precSymm ){
		//Calculate the symmetric part of the Jacobian matrix
		gmresCtxt = (PrecGMRESCtxt*)ctxt->linItCtxt ;
		swapPtr = ctxt->linVars->op.globalEntry ;
		ctxt->linVars->op.globalEntry = gmresCtxt->precMatrix ;
		
		calc_richards_jacobian_symm_part( params, ctxt->grid, data,
			reVars, &(ctxt->linVars->op) ) ;
			
		ctxt->linVars->op.globalEntry = swapPtr ;
	}
	
	//Write the residual norm to console
	printf( "Residual norm: %.18lf", ctxt->currResidNorm ) ;
	
	//Perform a Newton iteration until some convergence criterion is met
	//while( residNorm / origResidNorm > NEWT_TOL && newtCnt < params->maxIts ){
//	printf( "\n" ) ;
	do{
//		printf( "\33[2K\rNewton iteration: %d", ctxt->numIts ) ;
//		fflush( stdout ) ;
		//Set the operators necessary for the linear iteration
		set_newton_lin_vars( params, ctxt ) ;
			
		//Set up the initial approximation for the Newton iteration
		init_newt_approx_on_grid( reVars, ctxt->grid, ctxt->linVars ) ;
		
		//Perform the linear iterations
		perform_newton_inner_it( params, ctxt ) ;
				
//		sprintf( outFName, "/tmp/correct%u.txt", newtCnt+1 ) ;
//		write_dvec_to_file( outFName, ctxt->grid->numUnknowns,
//			ctxt->linVars->approx ) ;
//		sprintf( outFName, "/tmp/correct%u.vtk", ctxt->numIts+1 ) ;
//		write_interior_edge_func_paraview_format( outFName, ctxt->grid,
//			ctxt->linVars->approx ) ;

		if( params->globalNewton ){
			update_newton_armijo( params, data, reVars, ctxt ) ;
		}
		else{
			//Update the value of the approximation
			update_re_approx_from_newt_correct( params, ctxt->grid,
				ctxt->linVars, reVars ) ;
				
			//Update the value of the pressure
			recover_pressure( params, data, ctxt->grid, reVars ) ;
		
			//Calculate the residual and the Jacobian matrix
			calc_richards_resid_and_jacobian( params, ctxt->grid, data, reVars,
				&(ctxt->linVars->op) ) ;
			
			//Update the residual norm
			ctxt->currResidNorm = discrete_l2_norm( ctxt->grid->numUnknowns,
				reVars->resid ) ;
		}
		
		//Increment the counter of the Newton iteration
		ctxt->numIts++ ;
		
		if( params->linIt == IT_PGMRES && params->precSymm ){
			//Calculate the symmetric part of the Jacobian matrix
			ctxt->linVars->op.globalEntry = gmresCtxt->precMatrix ;
			
			calc_richards_jacobian_symm_part( params, ctxt->grid, data,
				reVars, &(ctxt->linVars->op) ) ;
				
			ctxt->linVars->op.globalEntry = swapPtr ;
		}
		
//		print_indent( 1, "Newton Iteration: %u\t", ctxt->numIts ) ;
//		print_indent( 0, "Residual Norm: %.18lf\n", ctxt->currResidNorm ) ;
		
//		if( residNorm <= SOLVED_TOL ) break ;
//	}while( ctxt->numIts < params->maxIts ) ;
	}while( ctxt->currResidNorm / ctxt->origResidNorm > NEWT_TOL &&
		ctxt->numIts < params->maxIts && ctxt->currResidNorm > SOLVED_TOL ) ;
		
	printf( "\t%u iterations performed\n", ctxt->numIts ) ;
}

/** Performs an adaptive update of the Newton step length using Armijo's rule
 *	see [Deuflhard 2004, pg. 121]
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] data	Soil characteristics for soils to be used in the current
 *						problem
 *	\param [in,out] reVars	Variables associated with the approximation of the
 *							mixed finite element formulation of the Richards
 *							equation
 *	\param [in,out] ctxt	
 *	\param [in] correction	The correction term calculated by solving a linear
 *							system of equations
 *	\param [in] oldResidNorm	The residual norm of the approximation at the
 *								previous Newton iterate
 *	\param [in,out] aVars	Variables associated with the approximation on the
 *							current grid. The approximation is updated in this
 *							method
 *	\return	The residual L2 norm of the updated approximation
 */
void update_newton_armijo( Params *params, const REData *data,
	REApproxVars *reVars, NewtonCtxt *ctxt )
{
	int i ; //Loop counter
	double oldResidNorm = ctxt->currResidNorm ;
	double newResidNorm = ctxt->currResidNorm ;
	double *testApprox = params->sparePtr ;
	double *swapPtr = reVars->lM ;

	//Square the value of the old residual norm, as it is this that we are
	//interested in
	oldResidNorm *= oldResidNorm ;
	
	//Set the correction term to one initially
	params->newtWeight = 1.0 ;
	
	do{
		//Calcualte the updated value
		for( i=0 ; i < ctxt->grid->numUnknowns ; i++ ){
			testApprox[i] = reVars->lM[i] -
				params->newtWeight * ctxt->linVars->approx[i] ;
		}
		
		reVars->lM = testApprox ;
		
		//Calculate the new residual
		recover_pressure( params, data, ctxt->grid, reVars ) ;
		calc_richards_resid( params, ctxt->grid, data, reVars ) ;
		
		reVars->lM = swapPtr ;
		
		//Update the residual norm
		newResidNorm = discrete_l2_inner_prod( ctxt->grid->numUnknowns,
			reVars->resid, reVars->resid ) ;
		
		//Halve the Newton damping factor
		params->newtWeight /= 2 ;
	}while( (newResidNorm > (1 - params->newtWeight / 2.0) * oldResidNorm) &&
		params->newtWeight >= MIN_NEWT_WEIGHT ) ;
		
//	if( params->newtWeight < MIN_NEWT_WEIGHT )
//		printf( "\n\tWarning - minimum Newton factor not good enough" ) ;
		
	params->newtWeight *= 2 ;

	//Set the current approximation to be the updated approximation
	copy_dvec( ctxt->grid->numUnknowns, testApprox, reVars->lM ) ;
	
	ctxt->currResidNorm = sqrt( newResidNorm ) ;
}
	



















