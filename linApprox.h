#ifndef _LIN_APPROX_H
#define _LIN_APPROX_H
/** \file linApprox.h
 *	\brief Functions handling specifically the construction, maintenance and
 	destruction of LinApproxVars structures
 */
 
#include "definitions.h"

//Initialise all the varibles to NULL
void set_lin_approx_vars_to_null( LinApproxVars *linVars ) ;

//Free the memory assigned on the heap
void free_lin_approx_vars( LinApproxVars *linVars ) ;

//Sets the appropriate memory for the LinApproxVars structure on the given grid
void init_lin_approx_vars_on_grid( const Grid *grid, LinApproxVars *linVars ) ;

//Set the sparse structure for the matrix representation of the linear operator
void set_sparse_op_structure( const Grid *grid, SparseMatrix *mat ) ;

//Calculates the Galerkin coarse grid operator where entries not in the desired
//sparsity pattern are added to the diagonal of the matrix
void calculate_coarse_grid_op( const SparseMatrix *fOp, const SparseMatrix *r,
	const SparseMatrix *p, SparseMatrix *cOp ) ;
	
//Calculates the Galerkin coarse grid operator, dropping entries from the coarse
//grid matrix that are not in the desired sparsity pattern
void calculate_coarse_grid_op_drop_entries( const SparseMatrix *fOp,
	const SparseMatrix *r, const SparseMatrix *p, SparseMatrix *cOp ) ;
	
//Calculates the residual in an approximation for a linear operator
void calculate_lin_resid( LinApproxVars *vars ) ;

//Performs some linear smoothing iteration which is defined in 'params'
void lin_smooth( const Params *params, unsigned int numSmooths,
	LinApproxVars *vars ) ;

//Performs a linear Gauss-Seidel iteration for the specified number of
//iterations
void lin_gs( const Params *params, unsigned int numSmooths,
	LinApproxVars *vars ) ;
	
//Performs a symmetric Gauss-Seidel iteration for the specified number of
//iterations
void lin_sym_gs( const Params *params, unsigned int numSmooths,
	LinApproxVars *vars ) ;
	
//Performs a linear Jacobi iteration for the specified number of iterations
void lin_jac( Params *params, unsigned int numSmooths, LinApproxVars *vars ) ;
	
//Sets up the structures required to solve a linear problem on each grid in the
//grid hierarchy
LinApproxVars** set_lin_approx_hierarchy( const Params *params,
	Grid **gridHierarchy ) ;

//Frees the memory assigned on the heap for the hieararchy of variables required
//for the solution of linear problmes on a hierarchy of grids	
LinApproxVars** free_lin_approx_hierarchy( const Params *params,
	LinApproxVars **hierarchy ) ;
	
//Performs linear smoothing until some convergence criterion is met
void lin_smooth_to_converge( const Params *params, LinApproxVars *vars ) ;
	
//Corrects a function by adding an elementwise contribution from a correction
//term
void correct_lin_approx( unsigned int length, const double *correction,
	double *approx ) ;
	
//Performs a linear multigrid iteration
void linear_multigrid( const Params *params, Grid **gridHierarchy,
	LinApproxVars **linHierarchy, Interpolation **interpHierarchy,
	unsigned int currLevel ) ;
	
//Sets the pointers in a least squares context to NULL
void set_least_squares_ctxt_to_null( LeastSqCtxt *ctxt ) ;

//Frees the memory assigned on the heap for the least squares context
void free_least_squares_ctxt( LeastSqCtxt *ctxt ) ;

//Assigns the memory on the heap for the least squares context
void init_least_squares_ctxt( LeastSqCtxt *ctxt ) ;

//Sets the pointers in the GMRES context to NULL
void set_gmres_ctxt_to_null( GMRESCtxt *ctxt ) ;

//Frees the memory assigned on the heap for the GMRES context
void free_gmres_ctxt( GMRESCtxt *ctxt ) ;

//Sets the memory necessary to perform a GMRES solve on the given grid
void setup_gmres_ctxt_on_grid( const Grid *grid, GMRESCtxt *ctxt ) ;

//Performs modified Gram-Schmidt to make the vector passed in orthogonal to the
//basis passed in
void modified_gram_schmidt( unsigned int basisSize, unsigned int vecLength,
	double **basis ) ;
	
//Transforms an upper Hessenberg matrix into an upper triangular matrix to be
//used as part of a linear least squares problem
void hess_to_upper( LeastSqCtxt *ctxt ) ;

//Performs back substitution af an upper triangular system of equations as part
//of a linear least squares solve
void back_substitute( LeastSqCtxt *ctxt ) ;

//Solves the least squares problem defined by the variables stored in the
//structure passed in
void solve_least_squares( LeastSqCtxt *ctxt ) ;

//Performs a GMRES iteration the specified number of times
void gmres_iteration( Params *params, const Grid *grid, GMRESCtxt *ctxt,
	unsigned int numIts ) ;
	
//Sets the pointers in a preconditioned GMRES context to NULL
void set_prec_gmres_ctxt_to_null( PrecGMRESCtxt *ctxt ) ;

//Frees the memory assigned on the heap for the preconditioned GMRES context
//used as part of Newton iteration
void free_prec_gmres_ctxt( const Params *params, PrecGMRESCtxt *ctxt ) ;
	
//Sets the memory required to perform a multigrid preconditioned GMRES iteration
void setup_mg_prec_gmres_ctxt( const Params *params, const REData *data,
	PrecGMRESCtxt *ctxt ) ;
	
//Performs a multigrid preconditioned GMRES iteration the specified number of
//times
void preconditioned_gmres_iteration( Params *params, PrecGMRESCtxt *ctxt,
	unsigned int numIts ) ;
	
//Sets the pointers in the given context to NULL
void set_lin_mg_ctxt_to_null( LinMGCtxt *ctxt ) ;

//Frees the memory assigned on the heap for the linear multigrid context
void free_lin_mg_ctxt( const Params *params, LinMGCtxt *ctxt ) ;

void setup_lin_mg_ctxt( const Params *params, const REData *data,
	LinMGCtxt *ctxt ) ;

#endif









