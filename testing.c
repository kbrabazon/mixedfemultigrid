#include "testing.h"

#ifdef NDEBUG
//If we are not performing debugging we do not include the testing functions, as
//these take up a lot of room in memory
void test( Params *params, REData *data )
{
	printf( "Debugging is turned off, so testing functions are disabled." ) ;
	printf( " Do not define pre-compile flag NDEBUG to have access to" ) ;
	printf( " testing functions\n" ) ;
}
#else
#include "linApprox.h"
#include "gridHandling.h"
#include "interpolation.h"
#include "sparseMatrix.h"
#include "richardsFunctions.h"
#include "newtonIteration.h"
#include "params.h"
#include "richardsTimeStepping.h"
#include "fasIteration.h"
#include <stdlib.h>
#include <time.h>

//Performs testing. This is the entry point for the testing, and any functions
//to be tested should be put in here
void test( Params *params, REData *data )
{
	make_output_bold() ;
	printf( "Note that any files read in for testing should be placed in " ) ;
	printf( "the folder ../testing. Files in this folder should not be " ) ;
	printf( "renamed or deleted\n\n" ) ;
	reset_output_style() ;

	//Test that the sparse matrix structure is properly set up on a grid
	//test_sparse_matrix_grid_struct( params ) ;
	
	//Test that the z-coordinate opposite an edge on an element is obtained
	//test_get_z_op( params ) ;
	
	//Test that the edges are set up appropriately on the grid (i.e. that the
	//unique identifier points into the list of unknown values
	//test_edge_set_up( params ) ;
	
	//Test that when the grid is set up that the correct elements are assigned
	//to each edge
	//test_edge_parent_els( params ) ;
	
	//Test that when two grids are set up that the correct coarse grid edges
	//are assigned to fine grid edges
	//test_edge_parent_edges( params ) ;
	
	//Test that the restriction operator is set up properly given a fine and
	//coarse grid
	//test_prolong_mat_set_up( params ) ;
	
	//Test the the restriction and prolongation operators between neighbouring
	//grids is set up properly
	//test_restrict_prolong_set_up( params ) ;
	
	//Tests that the Galerkin coarse grid operator is set up properly
	//test_galerkin_coarse_grid_calc( params ) ;
	
	//Tests that the Galerkin coarse grid operator is set up properly in the
	//case that the entries not in the sparsity pattern are dropped from the
	//matrix
	//test_galerkin_coarse_grid_calc_drop_entries( params ) ;
	
	//Tests that a sparse matrix vector multiply is performed correctly
	//test_sparse_mat_vec_mult( params ) ;
	
	//Tests that the linear residual is calculated correctly
	//test_calc_lin_resid( params ) ;
	
	//Tests that the soil parameters are read appropriately from file
	//test_set_up_soil_data( data ) ;
	
	//Tests that the calculation of the hydraulic conductivity and its
	//derivative are performed correctly
	//test_hydr_cond_deriv_calc( params, data ) ;
	
	//Tests that the calculation of the volumetric wetness and its derivative
	//are performed correctly
	//test_theta_deriv_calc( params, data ) ;
	
	//Tests that when the hydraulic conductivity and the volumetric wetness are
	//calculated simultaneously that they are calculated correctly
	//test_calc_hydr_cond_vol_wet( params, data ) ;
	
	//Tests that when the hydraulic conductivity, the volumetric wetness and
	//their derivatives are calculated simultaneously that this gives the same
	//as when they are calculated individually
	//test_calc_hydr_cond_vol_wet_deriv( params, data ) ;
	
	//Tests that the derivative of the local functino (defined in Bause and
	//Knabner) on the pressure on an element is calculated properly
	//test_el_press_func_deriv( params, data ) ;
	
	//Tests that the derivative of the local function (defined in Bause and
	//Knabner) on the pressure on an element, is calculated properly when the
	//values are calculated simultaneously
	//test_el_press_func_and_deriv( params, data ) ;

	//Tests that a piecewise constant function on a grid is output appropriately
	//to a vtk format
	//test_write_element_func_to_file( params ) ;
	
	//Tests that a piecewise constant function defined on the edges can be
	//written to file properly
	//test_write_edge_func_to_file( params ) ;
	
	//Tests that a function of pointwise fluxes over the edges of a grid can be
	//written to file
	//test_write_edge_normal_func_to_file( params ) ;
	
	//Tests that the piecewise constant pressure function can be solved for
	//using the lagrange multipliers on an element
	//test_recover_pressure( params, data ) ;
	
	//Tests that the pressure that is recovered from a steady state solution
	//does not change
	//test_recover_pressure_steady_state( params, data ) ;
	
	//Tests that a grid hierarchy is read in successfully
	//test_read_grid_hierarchy( params, data ) ;
	
	//Tests that the restriction of a function from a fine to a coarse grid
	//works as expected
	//test_prolong_restrict( params ) ;
	
	//Tests that the inverse of the local stiffness matrix is calculated
	//correctly
	//test_calc_loc_stiff_mat_inv( params ) ;
	
	//Tests that the inverses of the local stiffness matrices are set up when a
	//grid is read in from file
	//test_loc_mat_inv_grid_setup( params ) ;
	
	//Tests that the local function defining the relationship between the
	//pressure and the Lagrange multipliers is calculated properly if values are
	//calculated before calling the method
	//test_calc_el_press_func_deriv_with_data( params, data ) ;
	
	//Tests that the index into the global matrix is returned given the row and
	//column number
	//test_get_mat_ind( params, data ) ;
	
	//Tests that the residual and the Jacobian matrix are calculated correctly
	//for the Richards equation
	//test_calc_richards_resid_jac( params, data ) ;
	
	//Tests that when the problem is linear that the Jacobian matrix is the same
	//as the operator
	//test_linear_jac_set_up( params, data ) ;
	
	//Tests that the hierarchy of linear variables can be set up on a hierarchy
	//of grids
	//test_setup_lin_vars_hierarchy( params, data ) ;
	
	//Tests that the hierarchy of interpolation operators is set up properly on
	//a hierarchy of grids
	//test_setup_interp_hierarchy( params, data ) ;
	
	//Tests that the appropriate memory is set for the variables required to
	//perform a Newton-Multigrid iteration
	//test_setup_newton_ctxt( params, data ) ;
	
	//Tests that thee residual and the Jacobian matrix are set up properly when
	//they are populated simultaneously
	//test_calc_richards_resid_and_jac( params, data ) ;
	
	//Tests that a Newton context is properly initialised
	//test_set_newton_ctxt( params, data ) ;
	
	//Tests that the initial data is appropriately set for the Richards equation
	//test_set_richards_initial_data( params, data ) ;
	
	//Tests that a Newton method with linear smoothing applied will converge for
	//a Richards equation type problem
	//test_newton_smooth( params, data ) ;
	
	//Tests that smoothing converges in each Newton iteration
	//test_newton_smooth_to_converge( params, data ) ;
	
	//Tests that multigrid can be used as part of a Newton iteration to solve a
	//nonlinear system of equations
	//test_single_newton_mg( params, data ) ;
	
	//Tests that the flux is recovered appropriately from the data passed in
	//test_recover_flux( params, data ) ;
	
	//Tests that a Newton iteration with smoothing as the linear solver works as
	//expected
	//test_solve_richards_with_smooths( params, data ) ;
	
	//Tests that a factor required in the calculation of the residual and the
	//Jacobian matrix is set up properly
	//test_setup_zfact( params, data ) ;
	
	//Tests that a bounding box of an element is correctly returned
	//test_get_el_bnd_box( params, data ) ;
	
	//Tests that the soil types are allocated correctly
	//test_set_soil_type( params, data ) ;
	
	//Tests that the hierarchy of variables required to solve a Richards
	//equation is set up properly
	//test_setup_re_vars_hierarchy( params, data ) ;
	
	//Tests that rows can be deleted from a sparse matrix
	//test_delete_sparse_matrix_rows( params, data ) ;
	
	//Tests that the prolongation of a function from a coarse to a fine grid and
	//the restriction from a fine to a coarse grid works when using the
	//operators for an FAS iteration
	//test_fas_prolong_restrict( params ) ;
	
	//Tests that the calculation of the diagonals of the Jacobian matrix is
	//performed as expected
	//test_calc_jac_diags( params, data ) ;
	
	//Tests that the residual and the Jacobian diagonals calculated together are
	//as expected
	//test_calc_richards_resid_and_jac_diags( params, data ) ;
	
	//Tests that the nonlinear smoother converges
	//test_nonlinear_smooth( params, data ) ;
	
	//Tests that the nonlinear smoother converges
	//test_nonlinear_smooth_to_converge( params, data ) ;
	
	//Tests that a piecewise constant function can be restricted from a fine to
	//a coarse grid
	//test_restrict_pc_func( params, data ) ;
	
	//Tests that the perturbed right hand side for the FAS iteration is
	//calculated correctly on a coarse grid
	//test_fas_coarse_rhs_calc( params, data ) ;
	
	//Tests that the FAS multigrid iteration is working as expected
	//test_fas_multigrid_iteration( params, data ) ;
	
	//Tests that the modified Gram-Schmidt is performed correctly
	//test_modified_gram_schmidt( params, data ) ;
	
	//Tests that an upper Hessenberg matrix can be transformed into an upper
	//triangular matrix
	//test_hess_to_upper( params, data ) ;
	
	//Tests that the back-substitution works
	//test_back_substitute( params, data ) ;
	
	//Tests that GMRES as a linear iteration will converge
	//test_gmres_solve( params, data ) ;
	
	//Tests that a precondtioned GMRES iteration performs correctly
	test_prec_gmres_solve( params, data ) ;
}

//Tests that a sparse matrix is set up on a grid with the correct structure of
//non-zero entries
void test_sparse_matrix_grid_struct( Params *params )
{
	int i, j ; //Loop counters 
	const Element *el ; //An element on the grid
	Grid grid ; //The grid on which to set up the sparse matrix
	LinApproxVars linVars ; //This is where the sparse matrix is stored
	
	//Set the grid to NULL
	set_grid_to_null( &grid ) ;
	//Set the linear approximation variables to NULL
	set_lin_approx_vars_to_null( &linVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( "/tmp/test.out", &grid ) ;
	
	//Set up the sparse matrix structure
	init_lin_approx_vars_on_grid( &grid, &linVars ) ;
	
	//Now print out the sparse matrix as it is
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		printf( "Row %u: ", i ) ;
		for( j = linVars.op.rowStartInds[i] ;
			j < linVars.op.rowStartInds[i+1] ; j++ ){
			printf( "%u ", linVars.op.colNums[j] ) ;
		}
		printf( "\n" ) ;
	}
	
	printf( "\nElement 9:\n" ) ;
	el = &(grid.els[9]) ;
	for( i=0 ; i < 3 ; i++ )
		printf( "Edge %d magnitude: %.18lf\n", i, el->edges[i]->mag ) ;
	
	printf( "\nElement 10\n" ) ;
	el = &(grid.els[10]) ;	
	for( i=0 ; i < 3 ; i++ )
		printf( "Edge %d magnitude: %.18lf\n", i, el->edges[i]->mag ) ;
	
	//Free the grid
	free_grid( &grid ) ;
	free_lin_approx_vars( &linVars ) ;
}

//Tests that z coordinate opposite an edge on an element is obtained correctly
void test_get_z_op( Params *params )
{
	unsigned int i ; //Loop counter
	double z ; //z-coordinate of a point on the grid
	Grid grid ;
	const Element *el ; //An element on a grid
	
	//Initialise the grid to NULL
	set_grid_to_null( &grid ) ;
	
	//Read the grid from a file. This is hard coded for the moment, but should
	//be changed
	read_grid_from_file( "/tmp/test.out", &grid ) ;
	
	//Get a particular element on the grid, and for each edge print out the
	//z-coordinate opposite
	el = &(grid.els[11]) ;
	printf( "Element 11:\n" ) ;
	for( i=0 ; i<3 ; i++ ){
		z = get_z_op( el, i ) ;
		printf( "z-coordinate opposite edge %u: %.10lf\n", i, z ) ;
	}
	
	el = &(grid.els[12]) ;
	printf( "\nElement 12:\n" ) ;
	for( i=0 ; i < 3 ; i++ ){
		z = get_z_op( el, i ) ;
		printf( "z-coordinate opposite edge %u: %.10lf\n", i, z ) ;
	}
	
	//Free the memory allocated on the heap for the grid structure
	free_grid( &grid ) ;
}

//Tests that the edges are set up appropriately on a grid so that they are
//ordered on the grid in terms of geometric position, but in the unknowns in
//terms of whether they are known or unknown. This prints out information
//about every edge on a grid, so should only be done for a small grid
void test_edge_set_up( Params *params )
{
	unsigned int eCnt ; //Loop counter
	const Edge *edge ; //An edge on the grid
	Grid grid ;
	
	//Initialise the grid to NULL
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file. The file path is hard caded, but this should
	//be changed
	read_grid_from_file( "/tmp/test.out", &grid ) ;
	
	//Loop through all of the edges
	for( eCnt = 0 ; eCnt < grid.numEdges ; eCnt++ ){
		//Get a handle to the edge
		edge = &(grid.edges[eCnt]) ;
		printf( "Edge index: %u\tUnknown index: %u\n", eCnt, edge->id ) ;
	}
	
	//Free the memory assigned on the heap to the grid structure
	free_grid( &grid ) ;
}

//Test that when the grid is set up the correct elements are assigned to each
//edge
void test_edge_parent_els( Params *params )
{
	unsigned int eCnt, i ; //Loop counters
	const Edge *edge ; //An edge on the grid
	const Element *const*el ; //An element on the grid
	Grid grid ; //A grid to read in
	
	//Initialise the grid to NULL
	set_grid_to_null( &grid ) ;
	
	//Read in the grid from file. At the moment this file is hard coded in, but
	//this should be changed
	read_grid_from_file( "/tmp/test.out", &grid ) ;
	
	//For a series of edges on the grid print out the elements to which the edge
	//belongs
	for( eCnt = 22 ; eCnt < 27 ; eCnt++ ){
		edge = &(grid.edges[eCnt]) ;
		printf( "Edge %u:\n\t", eCnt ) ;
		el = edge->parentEls ;
		for( i=0 ; i < 2 ; i++, el++ ){
			printf( "Element %u Edges: %u, %u, %u\n", i, el[0]->edges[0]->id,
				el[0]->edges[1]->id, el[0]->edges[2]->id ) ;
			if( i!=1 ) printf( "\t" ) ;
		}
		if( eCnt != 28 ) printf( "\n" ) ;
	}
	
	//Free the memory allocated on the heap for the grid structure
	free_grid( &grid ) ;
}

//Test that the edges that are pointed to by a fine grid edge correspond to the
//parent edges on the coarse grid
void test_edge_parent_edges( Params *params )
{
	unsigned int i ; //Loop counter
	Grid fGrid, cGrid ; //A fine and a coarse grid
	const Edge *fEdge ; //An edge on the fine grid
	
	//Initialise both grids to NULL
	set_grid_to_null( &fGrid ) ;
	set_grid_to_null( &cGrid ) ;
	
	//Read the fine grid in from file
	read_grid_from_file( "../testing/mesh4.out", &fGrid ) ;
	//Read the coarse grid in from file. The file path is hard coded, but this
	//should be changed
	read_grid_from_file( "../testing/mesh2.out", &cGrid ) ;
	
	//Now assign the edges from the fine grid to edges on the coarse grid
	set_edge_parents( &cGrid, &fGrid ) ;
	
	//For each fine grid edge print out the corresponding coarse grid edges
	//on which it is contained
	for( i=0 ; i < fGrid.numEdges ; i++ ){
		fEdge = &(fGrid.edges[i]) ;
		printf( "Edge %u:\tCoarse Edges: %u, %u\n", i,
			fEdge->parentEdges[0]->id, fEdge->parentEdges[1]->id ) ;
	}
	
	//Free the memory allocated on the heap for the Grid structures
	free_grid( &fGrid ) ;
	free_grid( &cGrid ) ;
}

//Test that the prolongation operator is set up properly given a fine and coarse
//grid
void test_prolong_mat_set_up( Params *params )
{
	unsigned int i, j ; //Loop counters
	Grid fGrid, cGrid ; //A fine (coarse) grid
	Interpolation interp ; //The interpolation matrices on the fine grid
	
	//Initialise the grids to NULL
	set_grid_to_null( &fGrid ) ;
	set_grid_to_null( &cGrid ) ;
	
	//Read the fine grid in from file
	read_grid_from_file( "../testing/mesh4.out", &fGrid ) ;
	//Read the coarse grid in from file. The file path is hard coded, but this
	//should be changed
	read_grid_from_file( "../testing/mesh2.out", &cGrid ) ;
	
	//Now assign the edges from the fine grid to edges on the coarse grid
	set_edge_parents( &cGrid, &fGrid ) ;
	
	//Initialise the interpolation operators to NULL
	set_interp_ops_to_null( &interp ) ;
	
	//Now set up the structure of the restriction operator from the fine grid
	//structure
	set_prolong_mat_on_grid( &fGrid, &cGrid, &interp ) ;
	
	//Now we should have an appropriate structure that we can query for output
	printf( "Number of non-zero entries in restriction operator: %u\n",
		interp.p.numNonZeros ) ;
	printf( "Number of unknowns on fine grid: %u\n", fGrid.numUnknowns ) ;
	
	//Write out the column numbers for each row in the matrix
	for( i=0 ; i < fGrid.numUnknowns ; i++ ){
		printf( "Row %u:", i ) ;
		//Loop through the columns in the restriction matrix
		for( j=interp.p.rowStartInds[i] ; j < interp.p.rowStartInds[i+1] ; j++ )
			printf( " %u", interp.p.colNums[j] ) ;
		printf( "," ) ;
		for( j=interp.p.rowStartInds[i] ; j < interp.p.rowStartInds[i+1] ; j++ )
			printf( " %.2f", interp.p.globalEntry[j] ) ;
		printf( "\n" ) ;
	}
	
	//Free the memory assigned to the interpolation operators on the heap
	free_interp_ops( &interp ) ;
	//Free the memory assigned to the grids on the heap
	free_grid( &fGrid ) ;
	free_grid( &cGrid ) ;
}

//Tests that the restriction and prolongation operators between neighbouring
//grids is set up properly
void test_restrict_prolong_set_up( Params *params )
{
	unsigned int i, j ; //Loop counters
	Grid fGrid, cGrid ; //Fine and coarse grids, respectively
	Interpolation fInterp, cInterp ; //Interpolation operators on the fine and
				//coarse grids
	FILE *fOut ;
	
	//Set the grids to NULL initially
	set_grid_to_null( &fGrid ) ;
	set_grid_to_null( &cGrid ) ;
	
	//Set the interpolation operators to NULL
	set_interp_ops_to_null( &fInterp ) ;
	set_interp_ops_to_null( &cInterp ) ;
	
	//Read in the fine grid from file
	read_grid_from_file( "../testing/mesh4.out", &fGrid ) ;
	//read in the coarse grid from file. The file name is hard coded for the
	//moment, but this should be changed
	read_grid_from_file( "../testing/mesh2.out", &cGrid ) ;
	
	//Now assign the edges from the fine grid to edges on the coarse grid
	set_edge_parents( &cGrid, &fGrid ) ;
	
	//Set up the restriction and prolongation operators on the fine and coarse
	//grid, respectively
	set_restrict_prolong_mat_on_grid( &fGrid, &cGrid, &fInterp, &cInterp ) ;
	
	//Write the output to a file
	//Open a file to write the restriction operator to
	fOut = fopen( "/tmp/testRestrict.txt", "w" ) ;
	
	//Loop over the rows in the restriction operator
	for( i=0 ; i < fInterp.r.numRows ; i++ ){
		fprintf( fOut, "Row %u:", i ) ;
		//Loop over the columns in the current row
		for( j= fInterp.r.rowStartInds[i] ;
			j < fInterp.r.rowStartInds[i+1] ; j++ ){
			fprintf( fOut, " %u", fInterp.r.colNums[j] ) ;
		}
		fprintf( fOut, "," ) ;
		for( j=fInterp.r.rowStartInds[i] ;
			j < fInterp.r.rowStartInds[i+1] ; j++ ){
			fprintf( fOut, " %.2lf", fInterp.r.globalEntry[j] ) ;
		}
		fprintf( fOut, "\n" ) ;
	}
	
	//Close the file
	fclose( fOut ) ;
	
	//Open a file to write the prolongation operator to
	fOut = fopen( "/tmp/testProlong.txt", "w" ) ;
	
	//Loop over the rows in the matrix
	for( i=0 ; i < cInterp.p.numRows ; i++ ){
		fprintf( fOut, "Row %u:", i ) ;
		for( j=cInterp.p.rowStartInds[i] ; j < cInterp.p.rowStartInds[i+1] ;
			j++ ){
			fprintf( fOut, " %u", cInterp.p.colNums[j] ) ;
		}
		fprintf( fOut, "," ) ;
		for( j=cInterp.p.rowStartInds[i] ; j < cInterp.p.rowStartInds[i+1] ;
			j++ ){
			fprintf( fOut, " %.2lf", cInterp.p.globalEntry[j] ) ;
		}
		fprintf( fOut, "\n" ) ;
	}
	
	//Close the file
	fclose( fOut ) ;
	
	//Free the memory allocated on the heap for the interpolation structures
	free_interp_ops( &fInterp ) ;
	free_interp_ops( &cInterp ) ;
	
	//Free the memory allocated on the heap for the grid structures
	free_grid( &fGrid ) ;
	free_grid( &cGrid ) ;
}


//Tests that the Galerkin coarse grid operator is set up properly. This is an
//artificial example, the results of which can be verified using MATLAB
void test_galerkin_coarse_grid_calc( Params *params )
{
	unsigned int i, j ; //Loop counters
	unsigned int ind ; //Index into the global array
	unsigned int colNum ; //The column number of an entry in a matrix
	SparseMatrix fOp, cOp ; //A fine and coarse grid operator
	SparseMatrix r, p ; //A restriction and prolongation matrix
	
	//Set the matrices to NULL initially
	set_sparse_mat_to_null( &fOp ) ;
	set_sparse_mat_to_null( &cOp ) ;
	set_sparse_mat_to_null( &r ) ;
	set_sparse_mat_to_null( &p ) ;
	
	//Explicitly construct the fine grid operator
	fOp.numRows = 16 ;
	fOp.numCols = 16 ;
	fOp.numNonZeros = 3*(fOp.numRows-2) + 4 ;
	fOp.globalEntry = (double*)calloc( fOp.numNonZeros, sizeof( double ) ) ;
	fOp.colNums = (unsigned int*)malloc( fOp.numNonZeros *
		sizeof( unsigned int ) ) ;
	fOp.rowStartInds = (unsigned int*)malloc( (fOp.numRows+1) *
		sizeof( unsigned int ) ) ;
		
	//Set the row start indices
	fOp.rowStartInds[0] = 0 ;
	fOp.rowStartInds[1] = 2 ;
	for( i=2 ; i < fOp.numRows ; i++ )
		fOp.rowStartInds[i] = fOp.rowStartInds[i-1] + 3 ;
	fOp.rowStartInds[fOp.numRows] = fOp.numNonZeros ;
	
	//For the first and last rows set the columns and the values
	fOp.globalEntry[0] = 2 ;
	fOp.colNums[0] = 0 ;
	fOp.globalEntry[1] = -1 ;
	fOp.colNums[1] = 1 ;
	ind = fOp.rowStartInds[fOp.numRows-1] ;
	fOp.globalEntry[ind] = 2 ;
	fOp.colNums[ind] = fOp.numRows-1 ;
	fOp.globalEntry[ind+1] = -1 ;
	fOp.colNums[ind+1] = fOp.numRows-2 ;
	
	//Set the column numbers and entries in the remaining rows
	for( i=1 ; i < fOp.numRows-1 ; i++ ){
		ind = fOp.rowStartInds[i] ;
		fOp.globalEntry[ind] = 2 ;
		fOp.colNums[ind++] = i ;
		fOp.globalEntry[ind] = -1 ;
		fOp.colNums[ind++] = i-1 ;
		fOp.globalEntry[ind] = -1 ;
		fOp.colNums[ind] = i+1 ;
	}
	
	//Print out the values in this matrix to make sure that they are what is
	//expected
//	write_sparse_matrix_to_console( &fOp ) ;
	
	//set up the sparsity pattern for the coarse grid operator
	cOp.numRows = 8 ;
	cOp.numCols = 8 ;
	cOp.numNonZeros =  3*(cOp.numRows-2) + 4;
	cOp.globalEntry = (double*)calloc( cOp.numNonZeros, sizeof( double ) ) ;
	cOp.colNums = (unsigned int*)malloc( cOp.numNonZeros *
		sizeof( unsigned int ) ) ;
	cOp.rowStartInds = (unsigned int*)malloc( (cOp.numRows+1) *
		sizeof( unsigned int ) ) ;
		
	//Set the row start indices
	cOp.rowStartInds[0] = 0 ;
	cOp.rowStartInds[1] = 2 ;
	for( i=2 ; i < cOp.numRows ; i++ )
		cOp.rowStartInds[i] = cOp.rowStartInds[i-1] + 3 ;
	cOp.rowStartInds[cOp.numRows] = cOp.numNonZeros ;
		
	//For the first and last rows set the columns and the values
	cOp.colNums[0] = 0 ;
	cOp.colNums[1] = 1 ;
	ind = cOp.rowStartInds[cOp.numRows-1] ;
	cOp.colNums[ind] = cOp.numRows-1 ;
	cOp.colNums[ind+1] = cOp.numRows-2 ;
	
	//Set the column numbers and entries in the remaining rows
	for( i=1 ; i < cOp.numRows-1 ; i++ ){
		ind = cOp.rowStartInds[i] ;
		cOp.colNums[ind++] = i ;
		cOp.colNums[ind++] = i-1 ;
		cOp.colNums[ind] = i+1 ;
	}
	
	//Print out the values in this matrix to make sure that they are what is
	//expected
//	write_sparse_matrix_to_console( &cOp ) ;
	
	//Now set the values for the restriction operator
	r.numRows = cOp.numRows ;
	r.numCols = fOp.numCols ;
	r.numNonZeros = 4*(r.numRows-1) + 3 ;
	//Set the memory for the restriction operator
	r.globalEntry = (double*)calloc( r.numNonZeros, sizeof( double ) ) ;
	r.colNums = (unsigned int*)malloc( r.numNonZeros *
		sizeof( unsigned int ) ) ;
	r.rowStartInds = (unsigned int*)malloc( (r.numRows+1) *
		sizeof( unsigned int ) ) ;
	//Set the row start indices
	r.rowStartInds[0] = 0 ;
	for( i=1 ; i < r.numRows ; i++ )
		r.rowStartInds[i] = r.rowStartInds[i-1] + 4 ;
	r.rowStartInds[r.numRows] = r.numNonZeros ;
	//set the column number and the entry
	for( i=0 ; i < r.numRows/2 ; i++ ){
		ind = r.rowStartInds[i] ;
		colNum=2*(i+1)-1 ;
		r.globalEntry[ind] = 1 ;
		r.colNums[ind++] = colNum-1 ;
		r.globalEntry[ind] = 2 ;
		r.colNums[ind++] = colNum ;
		r.globalEntry[ind] = 3 ;
		r.colNums[ind++] = colNum+1 ;
		r.globalEntry[ind] = -1 ;
		r.colNums[ind] = colNum+r.numRows ;
	}
	for( i=r.numRows/2 ; i < r.numRows-1 ; i++ ){
		ind = r.rowStartInds[i] ;
		colNum = 2*(i+1)-1 ;
		r.globalEntry[ind] = 1 ; 
		r.colNums[ind++] = colNum-1 ;
		r.globalEntry[ind] = 2 ;
		r.colNums[ind++] = colNum ;
		r.globalEntry[ind] = 3 ;
		r.colNums[ind++] = colNum+1 ;
		r.globalEntry[ind] = -1 ;
		r.colNums[ind] = colNum - r.numRows ;
	}
	ind = r.rowStartInds[r.numRows-1] ;
	colNum = 2*(r.numRows)-1 ;
	r.globalEntry[ind] = 1 ;
	r.colNums[ind++] = colNum-1 ;
	r.globalEntry[ind] = 2 ;
	r.colNums[ind++] = colNum ;
	r.globalEntry[ind] = -1 ;
	r.colNums[ind] = colNum - r.numRows ;
	
	//Write out the matrix to check that it contains what is expected
//	write_sparse_matrix_to_console( &r ) ;
	
	//Transpose the matrix
	sparse_matrix_transpose( &r, &p, true ) ;
	
	//Write the matrix out to check that it contains what is expected
//	write_sparse_matrix_to_console( &p ) ;
	
	//Now that I have everything that I want I will 'multiply' the results,
	//whilst keeping the same sparsity pattern as already contained in cOp
	calculate_coarse_grid_op( &fOp, &r, &p, &cOp ) ;
	
	//Print out the values in the coarse grid operator to see if they are as
	//expected
	for( i=0 ; i < cOp.numRows ; i++ ){
		printf( "Row %u:", i ) ;
		for( j=cOp.rowStartInds[i] ; j < cOp.rowStartInds[i+1] ; j++ )
			printf( " %u", cOp.colNums[j] ) ;
		printf( "," ) ;
		for( j=cOp.rowStartInds[i] ; j < cOp.rowStartInds[i+1] ; j++ )
			printf( " %.2lf", cOp.globalEntry[j] ) ;
		printf( "\n" ) ;
	}
	
	//Free the memory assigned on the heap for the matrices
	free_sparse_mat( &fOp ) ;
	free_sparse_mat( &cOp ) ;
	free_sparse_mat( &r ) ;
	free_sparse_mat( &p ) ;
}

//Tests that the Galerkin coarse grid operator is set up properly when the
//entries not in the sparsity pattern are dropped. This is an artificial
//example, the results of which can be verified using MATLAB
void test_galerkin_coarse_grid_calc_drop_entries( Params *params )
{
	unsigned int i, j ; //Loop counters
	unsigned int ind ; //Index into the global array
	unsigned int colNum ; //The column number of an entry in a matrix
	SparseMatrix fOp, cOp ; //A fine and coarse grid operator
	SparseMatrix r, p ; //A restriction and prolongation matrix
	
	//Set the matrices to NULL initially
	set_sparse_mat_to_null( &fOp ) ;
	set_sparse_mat_to_null( &cOp ) ;
	set_sparse_mat_to_null( &r ) ;
	set_sparse_mat_to_null( &p ) ;
	
	//Explicitly construct the fine grid operator
	fOp.numRows = 16 ;
	fOp.numCols = 16 ;
	fOp.numNonZeros = 3*(fOp.numRows-2) + 4 ;
	fOp.globalEntry = (double*)calloc( fOp.numNonZeros, sizeof( double ) ) ;
	fOp.colNums = (unsigned int*)malloc( fOp.numNonZeros *
		sizeof( unsigned int ) ) ;
	fOp.rowStartInds = (unsigned int*)malloc( (fOp.numRows+1) *
		sizeof( unsigned int ) ) ;
		
	//Set the row start indices
	fOp.rowStartInds[0] = 0 ;
	fOp.rowStartInds[1] = 2 ;
	for( i=2 ; i < fOp.numRows ; i++ )
		fOp.rowStartInds[i] = fOp.rowStartInds[i-1] + 3 ;
	fOp.rowStartInds[fOp.numRows] = fOp.numNonZeros ;
	
	//For the first and last rows set the columns and the values
	fOp.globalEntry[0] = 2 ;
	fOp.colNums[0] = 0 ;
	fOp.globalEntry[1] = -1 ;
	fOp.colNums[1] = 1 ;
	ind = fOp.rowStartInds[fOp.numRows-1] ;
	fOp.globalEntry[ind] = 2 ;
	fOp.colNums[ind] = fOp.numRows-1 ;
	fOp.globalEntry[ind+1] = -1 ;
	fOp.colNums[ind+1] = fOp.numRows-2 ;
	
	//Set the column numbers and entries in the remaining rows
	for( i=1 ; i < fOp.numRows-1 ; i++ ){
		ind = fOp.rowStartInds[i] ;
		fOp.globalEntry[ind] = 2 ;
		fOp.colNums[ind++] = i ;
		fOp.globalEntry[ind] = -1 ;
		fOp.colNums[ind++] = i-1 ;
		fOp.globalEntry[ind] = -1 ;
		fOp.colNums[ind] = i+1 ;
	}
	
	//Print out the values in this matrix to make sure that they are what is
	//expected
//	write_sparse_matrix_to_console( &fOp ) ;
	
	//set up the sparsity pattern for the coarse grid operator
	cOp.numRows = 8 ;
	cOp.numCols = 8 ;
	cOp.numNonZeros =  3*(cOp.numRows-2) + 4;
	cOp.globalEntry = (double*)calloc( cOp.numNonZeros, sizeof( double ) ) ;
	cOp.colNums = (unsigned int*)malloc( cOp.numNonZeros *
		sizeof( unsigned int ) ) ;
	cOp.rowStartInds = (unsigned int*)malloc( (cOp.numRows+1) *
		sizeof( unsigned int ) ) ;
		
	//Set the row start indices
	cOp.rowStartInds[0] = 0 ;
	cOp.rowStartInds[1] = 2 ;
	for( i=2 ; i < cOp.numRows ; i++ )
		cOp.rowStartInds[i] = cOp.rowStartInds[i-1] + 3 ;
	cOp.rowStartInds[cOp.numRows] = cOp.numNonZeros ;
		
	//For the first and last rows set the columns and the values
	cOp.colNums[0] = 0 ;
	cOp.colNums[1] = 1 ;
	ind = cOp.rowStartInds[cOp.numRows-1] ;
	cOp.colNums[ind] = cOp.numRows-1 ;
	cOp.colNums[ind+1] = cOp.numRows-2 ;
	
	//Set the column numbers and entries in the remaining rows
	for( i=1 ; i < cOp.numRows-1 ; i++ ){
		ind = cOp.rowStartInds[i] ;
		cOp.colNums[ind++] = i ;
		cOp.colNums[ind++] = i-1 ;
		cOp.colNums[ind] = i+1 ;
	}
	
	//Print out the values in this matrix to make sure that they are what is
	//expected
//	write_sparse_matrix_to_console( &cOp ) ;
	
	//Now set the values for the restriction operator
	r.numRows = cOp.numRows ;
	r.numCols = fOp.numCols ;
	r.numNonZeros = 4*(r.numRows-1) + 3 ;
	//Set the memory for the restriction operator
	r.globalEntry = (double*)calloc( r.numNonZeros, sizeof( double ) ) ;
	r.colNums = (unsigned int*)malloc( r.numNonZeros *
		sizeof( unsigned int ) ) ;
	r.rowStartInds = (unsigned int*)malloc( (r.numRows+1) *
		sizeof( unsigned int ) ) ;
	//Set the row start indices
	r.rowStartInds[0] = 0 ;
	for( i=1 ; i < r.numRows ; i++ )
		r.rowStartInds[i] = r.rowStartInds[i-1] + 4 ;
	r.rowStartInds[r.numRows] = r.numNonZeros ;
	//set the column number and the entry
	for( i=0 ; i < r.numRows/2 ; i++ ){
		ind = r.rowStartInds[i] ;
		colNum=2*(i+1)-1 ;
		r.globalEntry[ind] = 1 ;
		r.colNums[ind++] = colNum-1 ;
		r.globalEntry[ind] = 2 ;
		r.colNums[ind++] = colNum ;
		r.globalEntry[ind] = 3 ;
		r.colNums[ind++] = colNum+1 ;
		r.globalEntry[ind] = -1 ;
		r.colNums[ind] = colNum+r.numRows ;
	}
	for( i=r.numRows/2 ; i < r.numRows-1 ; i++ ){
		ind = r.rowStartInds[i] ;
		colNum = 2*(i+1)-1 ;
		r.globalEntry[ind] = 1 ; 
		r.colNums[ind++] = colNum-1 ;
		r.globalEntry[ind] = 2 ;
		r.colNums[ind++] = colNum ;
		r.globalEntry[ind] = 3 ;
		r.colNums[ind++] = colNum+1 ;
		r.globalEntry[ind] = -1 ;
		r.colNums[ind] = colNum - r.numRows ;
	}
	ind = r.rowStartInds[r.numRows-1] ;
	colNum = 2*(r.numRows)-1 ;
	r.globalEntry[ind] = 1 ;
	r.colNums[ind++] = colNum-1 ;
	r.globalEntry[ind] = 2 ;
	r.colNums[ind++] = colNum ;
	r.globalEntry[ind] = -1 ;
	r.colNums[ind] = colNum - r.numRows ;
	
	//Write out the matrix to check that it contains what is expected
//	write_sparse_matrix_to_console( &r ) ;
	
	//Transpose the matrix
	sparse_matrix_transpose( &r, &p, true ) ;
	
	//Write the matrix out to check that it contains what is expected
//	write_sparse_matrix_to_console( &p ) ;
	
	//Now that I have everything that I want I will 'multiply' the results,
	//whilst keeping the same sparsity pattern as already contained in cOp
	calculate_coarse_grid_op_drop_entries( &fOp, &r, &p, &cOp ) ;
	
	//Print out the values in the coarse grid operator to see if they are as
	//expected
	for( i=0 ; i < cOp.numRows ; i++ ){
		printf( "Row %u:", i ) ;
		for( j=cOp.rowStartInds[i] ; j < cOp.rowStartInds[i+1] ; j++ )
			printf( " %u", cOp.colNums[j] ) ;
		printf( "," ) ;
		for( j=cOp.rowStartInds[i] ; j < cOp.rowStartInds[i+1] ; j++ )
			printf( " %.2lf", cOp.globalEntry[j] ) ;
		printf( "\n" ) ;
	}
	
	//Free the memory assigned on the heap for the matrices
	free_sparse_mat( &fOp ) ;
	free_sparse_mat( &cOp ) ;
	free_sparse_mat( &r ) ;
	free_sparse_mat( &p ) ;
}

//Tests that the sparse matrix-vector multiplication is performed correctly
void test_sparse_mat_vec_mult( Params *params )
{
	unsigned int i ; //Loop counters
	SparseMatrix mat ; //The sparse matrix
	double *vec ; //The vector of values to multiply the matrix with
	double *res ; //The result of the sparse matrix-vector multiply
	unsigned int ind ; //Index into the global array of the matrix
	double val ; //The value to put into the global array of the matrix
	unsigned int vecLength ; //The size of the vector to use
	
	vecLength = 8 ;
	
	//Set the matrix to NULL initially
	set_sparse_mat_to_null( &mat ) ;
	
	//Assign the appropriate memory
	vec = (double*)calloc( vecLength, sizeof( double ) ) ;
	res = (double*)calloc( vecLength, sizeof( double ) ) ;
	mat.numRows = vecLength ;
	mat.numCols = vecLength ;
	mat.rowStartInds = (unsigned int*)malloc( (mat.numRows+1) *
		sizeof( unsigned int ) ) ;
	mat.numNonZeros = 3*(mat.numRows-2) + 4 ;
	mat.globalEntry = (double*)calloc( mat.numNonZeros, sizeof( double ) ) ;
	mat.colNums = (unsigned int*)malloc( mat.numNonZeros *
		sizeof( unsigned int ) ) ;
		
	//Set up the row start indices
	mat.rowStartInds[0] = 0 ;
	mat.rowStartInds[1] = 2 ;
	for( i=2 ; i < mat.numRows ; i++ )
		mat.rowStartInds[i] = mat.rowStartInds[i-1] + 3 ;
	mat.rowStartInds[mat.numRows] = mat.rowStartInds[mat.numRows-1] + 2 ;
	
	//Set the column numbers and the entries in the matrix
	mat.globalEntry[0] = 1 ;
	mat.colNums[0] = 0 ;
	mat.globalEntry[1] = 2 ;
	mat.colNums[1] = 1 ;
	val = 4 ;
	for( i=1 ; i < mat.numRows-1 ; i++ ){
		ind = mat.rowStartInds[i] ;
		mat.globalEntry[ind] = val ;
		mat.colNums[ind++] = i ;
		mat.globalEntry[ind] = val-1 ;
		mat.colNums[ind++] = i-1 ;
		mat.globalEntry[ind] = val+1 ;
		mat.colNums[ind] = i+1 ;
		val+=3 ;
	}
	ind = mat.rowStartInds[mat.numRows-1] ;
	mat.globalEntry[ind] = val ;
	mat.colNums[ind++] = mat.numRows-1 ;
	mat.globalEntry[ind] = val-1 ;
	mat.colNums[ind] = mat.numRows-2 ;
	
	//Print out the matrix to make sure that it is what is expected
	write_sparse_matrix_to_console( &mat ) ;
	
	//Populate the vector
	for( i=0 ; i < mat.numRows ; i++ )
		vec[i] = (double)(i+1) ;
		
	//Perform the sparse matrix-vector multiply
	sparse_matrix_vec_mult( &mat, vec, res ) ;
	
	//Print out the result to check that it is as expected
	printf( "Result:" ) ;
	for( i=0 ; i < mat.numRows ; i++ )
		printf( " %.3lf", res[i] ) ;
	printf( "\n" ) ;
		
	//Free the memory assigned on the heap
	free_sparse_mat( &mat ) ;
	free( vec ) ;
	free( res ) ;
}

// Tests that the linear residual is calculated properly
void test_calc_lin_resid( Params *params )
{
	unsigned int i ; //Loop counter
	LinApproxVars vars ; //Variables associated with the linear approximation
	SparseMatrix *mat ; //Placeholder for the linear operator
	double val ; //A value used to populate the linear operator
	unsigned int ind ; //An index into the linear operator
	unsigned int size ; //The size of the system to use for testing purposes
						//this is going to be small
	
	size = 8 ;
	
	//Set the LinApproxVars structure to NULL initially
	set_lin_approx_vars_to_null( &vars ) ;
	
	//Set the memory for the approximation variables
	vars.approx = (double*)calloc( size, sizeof( double ) ) ;
	vars.rhs = (double*)calloc( size, sizeof( double ) ) ;
	vars.resid = (double*)calloc( size, sizeof( double ) ) ;
	
	mat = &(vars.op) ;
	mat->numRows = size ;
	mat->numCols = size ;
	mat->rowStartInds = (unsigned int*)malloc( (mat->numRows+1) *
		sizeof( unsigned int ) ) ;
	mat->numNonZeros = 3*(mat->numRows-2) + 4 ;
	mat->globalEntry = (double*)calloc( mat->numNonZeros, sizeof( double ) ) ;
	mat->colNums = (unsigned int*)malloc( mat->numNonZeros *
		sizeof( unsigned int ) ) ;
		
	//Set up the row start indices
	mat->rowStartInds[0] = 0 ;
	mat->rowStartInds[1] = 2 ;
	for( i=2 ; i < mat->numRows ; i++ )
		mat->rowStartInds[i] = mat->rowStartInds[i-1] + 3 ;
	mat->rowStartInds[mat->numRows] = mat->rowStartInds[mat->numRows-1] + 2 ;
	
	//Set the column numbers and the entries in the matrix
	mat->globalEntry[0] = 1 ;
	mat->colNums[0] = 0 ;
	mat->globalEntry[1] = 2 ;
	mat->colNums[1] = 1 ;
	val = 4 ;
	for( i=1 ; i < mat->numRows-1 ; i++ ){
		ind = mat->rowStartInds[i] ;
		mat->globalEntry[ind] = val ;
		mat->colNums[ind++] = i ;
		mat->globalEntry[ind] = val-1 ;
		mat->colNums[ind++] = i-1 ;
		mat->globalEntry[ind] = val+1 ;
		mat->colNums[ind] = i+1 ;
		val+=3 ;
	}
	ind = mat->rowStartInds[mat->numRows-1] ;
	mat->globalEntry[ind] = val ;
	mat->colNums[ind++] = mat->numRows-1 ;
	mat->globalEntry[ind] = val-1 ;
	mat->colNums[ind] = mat->numRows-2 ;
	
	//Print out the matrix to make sure that it is what is expected
	write_sparse_matrix_to_console( mat ) ;
	
	//Set the right hand and the approximation vector
	for( i=0 ; i < mat->numRows ; i++ ){
		vars.approx[i] = (double)(i+1) ;
		vars.rhs[i] = (double)(2*(i+1)) ;
	}
	
	//Now calculate the linear residual
	calculate_lin_resid( &vars ) ;
	
	//Print out the residual to make sure that it has been appropriately set
	printf( "Residual:" ) ;
	for( i=0 ; i < mat->numRows ; i++ )
		printf( " %.3lf", vars.resid[i] ) ;
	printf( "\n" ) ;
	
	
	//Free the memory assigned on the heap
	free_lin_approx_vars( &vars ) ;
}

//Tests that the soil parameters are read appropriately from file
void test_set_up_soil_data( REData *data )
{
	//All that I need to do is to write out the contents of the data structure
	//to console
	write_re_data_to_console( data ) ;
}

//Tests that the hydraulic conductivity and the derivative are calculated
//correctly
void test_hydr_cond_deriv_calc( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	unsigned int numPoints = 10000 ; //The number of points at which to
				//approximate the derivative of the hydraulic conductivity
	double stepSize ;
	double minP = -20.0 ;
	
	FILE *fout ; //File to write output to
	
	double *pHead ; //The pressure head
	double *hCond ; //The hydraulic conductivity
	double pHCond ; //The perturbed hydraulic conductivity
	double *gradHCond ; //The gradient of the hydraulic conductivity wrt the
						//pressure
	double *theta ; //The volumetric wetness
						
	//Set the appropriate memory
	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
	hCond = (double*)calloc( numPoints, sizeof( double ) ) ;
	gradHCond = (double*)calloc( numPoints, sizeof( double ) ) ;
	theta = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//Calculate the step size
	stepSize = -minP / (numPoints-1) ;
	//set up the pressure
	for( i=0 ; i < numPoints ; i++ )
		pHead[i] = minP + i * stepSize ;
		
	//Print the data associated with the Richards equation to console
	write_re_data_to_console( data ) ;
	
	//Set the volumetric wetness
	for( i=0 ; i < numPoints ; i++ )
		theta[i] = calc_theta( data, 0, pHead[i] ) ;
	
	//Set the hydraulic conductivity
	for( i=0 ; i < numPoints ; i++ )
		hCond[i] = calc_hydr_cond( data, 0, theta[i] ) ;
		
	//Set the derivative of the hydraulic conductivity
	for( i=0 ; i < numPoints ; i++ )
		gradHCond[i] = calc_hydr_cond_deriv( data, 0, pHead[i] ) ;
		
	//Open the file for writing
	fout = fopen( "/tmp/testHCondD.txt", "w" ) ;
	
	//Write out the pressure and the hydraulic conductivity and the derivative
	//of the hydraulic conductivity
	for( i=0 ; i < numPoints ; i++ )
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], hCond[i],
			gradHCond[i] ) ;
	
	//Close the file
	fclose( fout ) ;
	
	//Check that the calculated gradient of the hydraulic conductivity is close
	//to that using the numerical derivative
	//Calculate the perturbed values of theta
	for( i=0 ; i < numPoints ; i++ )
		theta[i] = calc_theta( data, 0, pHead[i] + M_EPS ) ;
		
	for( i=0 ; i < numPoints ; i++ ){
		pHCond = calc_hydr_cond( data, 0, theta[i] ) ;
		gradHCond[i] = (pHCond - hCond[i])/ M_EPS ;
	}
	
	//Write the output to file
	fout = fopen( "/tmp/numericHCondD.txt", "w" ) ;
	
	//Write the pressure, the hydraulic conductivity and the derivative of the
	//hydraulic conductivity to file
	for( i=0 ; i < numPoints ; i++ )
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], hCond[i],
			gradHCond[i] ) ;
	
	//Close the file
	fclose( fout ) ;
	
	//Free the memory assigned on the heap
	free( pHead ) ;
	free( hCond ) ;
	free( gradHCond ) ;
	free( theta ) ;
}

//Tests that the calculation of the volumetric wetness and its derivative is
//performed correctly
void test_theta_deriv_calc( Params *params, REData *data )
{
 	unsigned int i ; //Loop counter
 	unsigned int numPoints = 10000 ; //The number of points 
 	double stepSize ;
 	double minP = -10 ;
 	
 	FILE *fout ; //File to write output to
 	
 	double *pHead ; //The pressure given as hydraulic head
 	double *theta ; //The volumetric wetness
 	double *thetaD ; //The derivative of the volumetric wetness wrt the pressure
 	double pTheta ; //The value of the volumetric wetness for a perturbed
 					//pressure
 	
 	//Assing the appropriate memory
 	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
 	theta = (double*)calloc( numPoints, sizeof( double ) ) ;
 	thetaD = (double*)calloc( numPoints, sizeof( double ) ) ;
 	
 	stepSize = -minP / (numPoints -1) ;
 	
 	//Set up the pressure
 	for( i=0 ; i < numPoints ; i++ )
 		pHead[i] = minP + i * stepSize ;
 		
 	//Print to console the data stored regarding the soil types
 	write_re_data_to_console( data ) ;
 	
 	//Calculate the volumetric wetness
 	for( i=0 ; i < numPoints ; i++ )
 		theta[i] = calc_theta( data, 0, pHead[i] ) ;
 	
 	//Calculate the gradient of the volumetric wetness
 	for( i=0 ; i < numPoints ; i++ )
 		thetaD[i] = calc_theta_deriv( data, 0, pHead[i] ) ;
 		
 	//Print out the volumetric wetness and the gradient of the volumetric
 	//wetness
 	fout = fopen( "/tmp/testThetaD.txt", "w" ) ;
 	
 	for( i=0 ; i < numPoints ; i++ )
	 	fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], theta[i],
	 		thetaD[i] ) ;
 	
 	//Close the file
 	fclose( fout ) ;
 	
 	//Calculate the numeric derivative of the volumetric wetness
 	for( i=0 ; i < numPoints ; i++ ){
 		pTheta = calc_theta( data, 0, pHead[i] + M_EPS ) ;
 		thetaD[i] = (pTheta - theta[i]) / M_EPS ;
 	}
 	
 	//Print out the volumetric wetness and the gradient of the volumetric
 	//wetness
 	fout = fopen( "/tmp/numThetaD.txt", "w" ) ;
 	
 	for( i=0 ; i < numPoints ; i++ )
 		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], theta[i],
 			thetaD[i] ) ;
 	
 	//Close the file
 	fclose( fout ) ;
 	
 	//Free the memory assigned on the heap
 	free( pHead ) ;
 	free( theta ) ;
 	free( thetaD ) ;
}

//Checks that when the hydraulic conductivity and the volumetric wetness are
//calculated simultaneously that they are calculated correctly
void test_calc_hydr_cond_vol_wet( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	unsigned int numPoints = 10000 ;
	double stepSize ;
	double minP = -10 ;
	
	double *pHead ; //The pressure given as hydraulic head
	double *theta ; //The volumetric wetness
	double *hCond ; //The hydraulic conductivity
	double *thetaNew ;
	double *hCondNew ;
	
	//Set the appropriate memory
	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
	theta = (double*)calloc( numPoints, sizeof( double ) ) ;
	hCond = (double*)calloc( numPoints, sizeof( double ) ) ;
	thetaNew = (double*)calloc( numPoints, sizeof( double ) ) ;
	hCondNew = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//Calculate the step size
	stepSize = -minP / (numPoints-1) ;
	
	//Set the values of the pressure
	for( i=0 ; i < numPoints ; i++ )
		pHead[i] = minP + i * stepSize ;
		
	//Set the values for the volumetric wetness and the hydraulic conductivity
	//separately
	for( i=0 ; i< numPoints ; i++ ){
		theta[i] = calc_theta( data, 0, pHead[i] ) ;
		hCond[i] = calc_hydr_cond( data, 0, theta[i] ) ;
	}
	
	//Now calculate the values for the volumetric wetness and the hydraulic
	//conductivity simultaneously
	for( i=0 ; i < numPoints ; i++ )
		calc_hydr_cond_vol_wet( data, 0, pHead[i], hCondNew + i,
			thetaNew + i ) ;
			
	//Find the maximum difference in the volumetric wetness and the hydraulic
	//conductivity
	printf( "Maximum difference in hydraulic conductivities: %.18lf\n",
		dvec_diff_infty_norm( hCond, hCondNew, numPoints ) ) ;
	printf( "Maximum difference in volumetric wetnesses: %.18lf\n",
		dvec_diff_infty_norm( theta, thetaNew, numPoints ) ) ;
	
	//Free the memory assigned on the heap
	free( pHead ) ;
	free( theta ) ;
	free( hCond ) ;
	free( thetaNew ) ;
	free( hCondNew ) ;
}

//Tests that when the hydraulic conductivity, the volumetric wetness and their
//derivatives are calculated simultaneously that this gives the same as when
//they are calculated individually
void test_calc_hydr_cond_vol_wet_deriv( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	unsigned int numPoints = 10000 ;
	double stepSize ;
	double minP = -10 ;
	
	double *pHead ; //The pressure given as hydraulic head
	double *theta ; //The volumetric wetness
	double *hCond ; //The hydraulc conductivity
	double *thetaD ; //The derivative of the volumetric wetness
	double *hCondD ; //The derivative of the hydraulic conductivity
	double *thetaNew ;
	double *hCondNew ;
	double *thetaDNew ;
	double *hCondDNew ;
	
	//Assign the appropriate memory
	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
	theta = (double*)calloc( numPoints, sizeof( double ) ) ;
	hCond = (double*)calloc( numPoints, sizeof( double ) ) ;
	thetaD = (double*)calloc( numPoints, sizeof( double ) ) ;
	hCondD = (double*)calloc( numPoints, sizeof( double ) ) ;
	thetaNew = (double*)calloc( numPoints, sizeof( double ) ) ;
	hCondNew = (double*)calloc( numPoints, sizeof( double ) ) ;
	thetaDNew = (double*)calloc( numPoints, sizeof( double ) ) ;
	hCondDNew = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//Calculate the step size
	stepSize = -minP / (numPoints-1) ;
	
	//Set the pressure head, the hydraulic conductivity, the volumetric wetness
	//and the derivative functions individually
	for( i=0 ; i < numPoints ; i++ ){
		pHead[i] = minP + i * stepSize ;
		theta[i] = calc_theta( data, 0, pHead[i] ) ;
		thetaD[i] = calc_theta_deriv( data, 0, pHead[i] ) ;
		hCond[i] = calc_hydr_cond( data, 0, theta[i] ) ;
		hCondD[i] = calc_hydr_cond_deriv( data, 0, pHead[i] ) ;
	}
	
	//Now set the variables all at the same time
	for( i=0 ; i < numPoints ; i++ )
		calc_hydr_cond_vol_wet_deriv( data, 0, pHead[i], hCondNew + i,
			hCondDNew + i, thetaNew + i, thetaDNew + i ) ;
			
	//Print out the maximum difference in the values
	printf( "Maximum difference in the volumetric wetness: %.18lf\n",
		dvec_diff_infty_norm( theta, thetaNew, numPoints ) ) ;
	printf( "Maximum difference in the derivative of the volumetric " ) ;
	printf( "wetness: %.18lf\n",
		dvec_diff_infty_norm( thetaD, thetaDNew, numPoints ) ) ;
	printf( "Maximum difference in the hydraulic conductivity: %.18lf\n",
		dvec_diff_infty_norm( hCond, hCondNew, numPoints ) ) ;
	printf( "Mamximum difference in the derivative of the hydraulic " ) ;
	printf( "conductivity: %.18lf\n",
		dvec_diff_infty_norm( hCondD, hCondDNew, numPoints ) ) ;
	
	//free the memory assigned on the heap
	free( pHead ) ;
	free( theta ) ;
	free( hCond ) ;
	free( thetaD ) ;
	free( hCondD ) ;
	free( thetaNew ) ;
	free( hCondNew ) ;
	free( thetaDNew ) ;
	free( hCondDNew ) ;
}

//Tests that the derivative of the local function (defined in Bause and Knabner)
//on the pressure on an element, is calculated properly
void test_el_press_func_deriv( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	unsigned int numPoints = 100000 ;
	double stepSize ;
	double minP = -100 ;
	double prevTheta ;
	
	double *pHead ; //Pressure given as hydraulic head
	double *elFunc ; //The local function
	double *elFuncD ; //The derivative of the local function
	double *elFuncDNum ; //The numeric derivative of the local function
	double pFunc ; //The function evaluation at a perturbed value
	
	Grid grid ;
	const Element *el ; //An element on the grid
	
	FILE *fout ; //File to write output to
	
	//Initialise the grid to NULL
	set_grid_to_null( &grid ) ;
	
	//Read in a grid from file. This is hard coded for now, but this should be
	//changed
	read_grid_from_file( gridFiles[0], &grid ) ;
	set_soil_type( params, data, &grid ) ;
	
	el = &(grid.els[0]) ;
	
	//Set the appropriate memory
	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
	elFunc = (double*)calloc( numPoints, sizeof( double ) ) ;
	elFuncD = (double*)calloc( numPoints, sizeof( double ) ) ;
	elFuncDNum = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//Calculate the step size
	stepSize = -minP / (numPoints-1) ;
	prevTheta = calc_theta( data, el->soilType,
		minP + stepSize * (numPoints/2) ) ;
	
	//Set up the pressure
	for( i=0 ; i < numPoints ; i++ )
		pHead[i] = minP + i * stepSize ;
		
	//Calculate the function and the function derivative
	for( i=0 ; i < numPoints ; i++ ){
		elFunc[i] = el_press_func( params, data, pHead[i], prevTheta, el ) ;
		elFuncD[i] = el_press_func_deriv( params, data, pHead[i], prevTheta,
			el ) ;
	}
	
	//Write the pressure, the local function and the derivative of the local
	//function to file
	fout = fopen( "/tmp/testLocFuncD.txt", "w" ) ;
	
	for( i=0 ; i < numPoints ; i++ )
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], elFunc[i],
			elFuncD[i] ) ;
	
	//Close the file
	fclose( fout ) ;
	
	//Calculate the numerical derivative of the local function
	for( i=0 ; i < numPoints ; i++ ){
		pFunc = el_press_func( params, data, pHead[i] + M_EPS, prevTheta, el ) ;
		elFuncDNum[i] = (pFunc - elFunc[i]) / M_EPS ;
	}
	
	//Write the pressure, the local function and the derivative of the local
	//function to file
	fout = fopen( "/tmp/numLocFuncD.txt", "w" ) ;
	
	for( i=0 ; i < numPoints ; i++ )
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], elFunc[i],
			elFuncDNum[i] ) ;
			
	//Write the maximum difference in numerical and exact derivative terms
	printf( "Maximum difference in exact and numeric derivative: %.18lf\n",
		dvec_diff_infty_norm( elFuncD, elFuncDNum, numPoints ) ) ;
			
	//Close the file
	fclose( fout ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free( pHead ) ;
	free( elFunc ) ;
	free( elFuncD ) ;
	free( elFuncDNum ) ;
}

//Tests that the derivative of the local function (defined in Bause and Knabner)
//on the pressure on an element, is calculated properly when the values are
//calculated simultaneously
void test_el_press_func_and_deriv( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	unsigned int numPoints = 100000 ;
	double stepSize ;
	double minP = -100 ;
	double prevTheta ;
	
	double *pHead ; //Pressure given as hydraulic head
	double *elFunc ; //The local function
	double *elFuncD ; //The derivative of the local function
	double *elFuncDNum ; //The numeric derivative of the local function
	double pFunc ; //The function evaluation at a perturbed value
	
	Grid grid ;
	const Element *el ; //An element on the grid
	
	FILE *fout ; //File to write output to
	
	//Initialise the grid to NULL
	set_grid_to_null( &grid ) ;
	
	//Read in a grid from file. This is hard coded for now, but this should be
	//changed
	read_grid_from_file( "../testing/mesh4.out", &grid ) ;
	set_soil_type( params, data, &grid ) ;
	
	el = &(grid.els[0]) ;
	
	//Set the appropriate memory
	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
	elFunc = (double*)calloc( numPoints, sizeof( double ) ) ;
	elFuncD = (double*)calloc( numPoints, sizeof( double ) ) ;
	elFuncDNum = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//Calculate the step size
	stepSize = -minP / (numPoints-1) ;
	prevTheta = calc_theta( data, el->soilType,
		minP + stepSize * (numPoints/2) ) ;
		
	printf( "Theta: %.18lf\n", prevTheta ) ;
	
	//Set up the pressure
	for( i=0 ; i < numPoints ; i++ )
		pHead[i] = minP + i * stepSize ;
		
	//Calculate the function and the function derivative
	for( i=0 ; i < numPoints ; i++ )
		el_press_func_and_deriv( params, data, pHead[i], prevTheta, el,
			elFunc + i, elFuncD + i ) ;
	
	//Write the pressure, the local function and the derivative of the local
	//function to file
	fout = fopen( "/tmp/testLocFuncD.txt", "w" ) ;
	
	for( i=0 ; i < numPoints ; i++ )
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], elFunc[i],
			elFuncD[i] ) ;
	
	//Close the file
	fclose( fout ) ;
	
	//Calculate the numerical derivative of the local function
	for( i=0 ; i < numPoints ; i++ ){
		pFunc = el_press_func( params, data, pHead[i] + M_EPS, prevTheta, el ) ;
		elFuncDNum[i] = (pFunc - elFunc[i]) / M_EPS ;
	}
	
	//Write the pressure, the local function and the derivative of the local
	//function to file
	fout = fopen( "/tmp/numLocFuncD.txt", "w" ) ;
	
	for( i=0 ; i < numPoints ; i++ )
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], elFunc[i],
			elFuncDNum[i] ) ;
			
	printf( "Maximum difference in exact and numeric derivative: %.18lf\n",
		dvec_diff_infty_norm( elFuncD, elFuncDNum, numPoints ) ) ;
			
	//Close the file
	fclose( fout ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free( pHead ) ;
	free( elFunc ) ;
	free( elFuncD ) ;
	free( elFuncDNum ) ;
}

//Tests that a piecewise constant function on a grid is output appropriately to
//a vtk format
void test_write_element_func_to_file( Params *params )
{
	unsigned int i ; //Loop counter
	Grid grid ; //A grid on which to output a function
	char fname[50] ; //The filename to write the output to
	double *elFunc ; //A function to write out to file
	double cog[2] ; //The centre of gravity of an element
	
	//Set the grid to NULL initially
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file. The path is hard coded, but this should change
	read_grid_from_file( "/tmp/test.out", &grid ) ;
	
	//Set the appropriate memory
	elFunc = (double*)calloc( grid.numElements, sizeof( double ) ) ;
	
	//Set the function value
	for( i=0 ; i < grid.numElements ; i++ ){
		//Get the element centre of gravity
		el_center_of_gravity( &(grid.els[i]), cog ) ;
		elFunc[i] = sin( PI * cog[0] ) * sin( PI * cog[1] ) ;
	}

	//Set the filename to write out to	
	sprintf( fname, "/tmp/pwConstFunc.vtk" ) ;
	
	//Write the function out to the file specified
	write_element_func_paraview_format( fname, &grid, elFunc ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free( elFunc ) ;
}

//Tests that a piecewise constant function defined on the edges can be written
//to file properly
void test_write_edge_func_to_file( Params *params )
{
	unsigned int i ; //Loop counter
	Grid grid ; //A grid on which to define a function
	char fname[50] ; //The filename to write output to
	double *edFunc ; //A function to write out to file
	double midPoint[2] ; //The mid point of an edge on the grid
	
	//Set the grid to NULL initially
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file. The path is hard coded, but this should be
	//changed
	read_grid_from_file( "/tmp/test.out", &grid ) ;
	
	//Set the appropriate memory
	edFunc = (double*)calloc( grid.numEdges, sizeof( double ) ) ;
	
	//Set the function value
	for( i=0 ; i < grid.numEdges ; i++ ){
		//Get the edge mid-point
		get_edge_mid_point( &(grid.edges[i]), midPoint ) ;
		edFunc[ grid.edges[i].id ] = sin( PI * midPoint[0] ) *
			sin( PI * midPoint[1] ) ;
	}
	
	//Set the filename to write out to
	sprintf( fname, "/tmp/pwConstEdFunc.vtk" ) ;
	
	//Write the function to the file specified
	write_edge_func_paraview_format( fname, &grid, edFunc ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free( edFunc ) ;
}

//Tests that a function of pointwise fluxes over the edges of a grid can be
//written to file
void test_write_edge_normal_func_to_file( Params *params )
{
	unsigned int edCnt ; //Loop counter
	Grid grid ; //A grid on which to define the functions
	const Edge *edge ; //An edge on the grid
	double midPoint[2] ; //The mid-point of an edge
	char fname[50] ; //Name of the file to write output to
	Vec2D *fluxes ; //The function of fluxes across element edges
	Vec2D *normFluxes ; //The normal component of the fluxes across element
						//edges
							  
	//Set the grid to NULL intially
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file. The file name is hard coded at the moment, but
	//this should be changed
	read_grid_from_file( "/tmp/test.out", &grid ) ;
	
	//Set the appropriate memory
	fluxes = (Vec2D*)calloc( grid.numEdges, sizeof( Vec2D ) ) ;
	normFluxes = (Vec2D*)calloc( grid.numEdges, sizeof( Vec2D ) ) ;
		
	//Compute the fluxes and write them out to file
	for( edCnt = 0 ; edCnt < grid.numEdges ; edCnt++ ){
		//Get a handle to the current edge
		edge = &(grid.edges[edCnt]) ;
		//Get the mid-point and vector representation of the edge
		get_edge_mid_point( edge, midPoint ) ;
		
		//Set the flux
		fluxes[edge->id].x = PI * cos( PI * midPoint[0] ) *
			sin( PI * midPoint[1] ) ;
		fluxes[edge->id].z = PI * sin( PI * midPoint[0] ) *
			cos( PI * midPoint[1] ) ;
		//Calculate the normal component of the flux
		get_vec_edge_normal_comp( edge, &(fluxes[edge->id]),
			&(normFluxes[ edge->id ]) ) ;
	}
	
	//Set the filename to write to
	sprintf( fname, "/tmp/testFluxFunc.vtk" ) ;
	
	//Write the fluxes to file
	write_edge_vec_func_paraview_format( fname, &grid, fluxes ) ;
	
	sprintf( fname, "/tmp/testPerpFluxFunc.vtk" ) ;
	
	//Write the normal component of the fluxes to file
	write_edge_vec_func_paraview_format( fname, &grid, normFluxes ) ;
	
	//Free the memory on the heap
	free_grid( &grid ) ;
	free( fluxes ) ;
	free( normFluxes ) ;
}

//Test that the pressure can be recovered given the Lagrange multipliers
void test_recover_pressure( Params *params, REData *data )
{
	unsigned int i, elCnt ; //Loop counters
	Grid grid ; //A grid on which to discretise
	const Element *el ; //An element on the current grid
	REApproxVars vars ; //Variables associated with the approximation to the
						//mixed finite element approximation of the Richards
						//equation
	double constPress = 0.0 ; //The constant value of the pressure to assign
							    //on the grid
	double *lMSum ; //The sum of the Lagrange multipliers on an element
	double *lMSumCalc ; //The sum of  the Lagrange multipliers on an element
						//after the calculation of the pressure
	
	//Set the grid to NULL initially
	set_grid_to_null( &grid ) ;
	//Set the approximation variables to NULL initially
	set_re_vars_to_null( &vars ) ;
	
	//Read the grid in from file. The file name is hard coded for the time being
	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	//read_grid_from_file( "../testing/mesh8.out", &grid ) ;
	set_soil_type( params, data, &grid ) ;
	
	//Set the necessary memory for the variables associated with the
	//approximation
	setup_re_vars_on_grid( params, &grid, &vars ) ;
	
	//Figure out how to set up the problem so that I can solve for a new
	//pressure
	//Set the pressure to be constant on the elements
	for( i=0 ; i < grid.numElements ; i++ ){
		vars.press[i] = constPress ;
		vars.prevTheta[i] = calc_theta( data, grid.els[i].soilType,
			vars.press[i] ) ;
	}
		
	//Set the value of the Lagrange multipliers to be a half of the pressure so
	//that we actually have something to solve
//	constPress /= 2.0 ;
	for( i=0 ; i < grid.numUnknowns ; i++ )
		vars.lM[i] = constPress ;
		
	//Using the definitions of the pressure that I have got, and using the 
	//prevPress as an initial guess we try and solve for the current pressure
	recover_pressure( params, data, &grid, &vars ) ;
	
	//After solving we want to print out the current pressure
	write_element_func_paraview_format( "/tmp/newPress.vtk", &grid,
		vars.press ) ;
		
	//Write out the volumetric wetness from the previous timestep
	write_element_func_paraview_format( "/tmp/oldTheta.vtk", &grid,
		vars.prevTheta ) ;
		
	//Also write out the Lagrange multipliers
	write_edge_func_paraview_format( "/tmp/lM.vtk", &grid, vars.lM ) ;
	
	//Assign the appropriate memory
	lMSum = (double*)calloc( grid.numElements, sizeof( double ) ) ;
	lMSumCalc = (double*)calloc( grid.numElements, sizeof( double ) ) ;
	
	//Set the values of the sum of the Lagrange multipliers
	for( elCnt = 0 ; elCnt < grid.numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid.els[ elCnt ]) ;
		//Loop through the edges on the element
		for( i=0 ; i < 3 ; i++ )
			lMSum[ elCnt ] += vars.lM[ el->edges[i]->id ] ;
	}
	
	//Calculate the values of the new function
	for( elCnt = 0 ; elCnt < grid.numElements ; elCnt++ )
		lMSumCalc[ elCnt ] = el_press_func( params, data, vars.press[elCnt],
			vars.prevTheta[elCnt], &(grid.els[elCnt]) ) ;
			
	//Write out the functions to file
	write_element_func_paraview_format( "/tmp/lMSum.vtk", &grid, lMSum ) ;
	write_element_func_paraview_format( "/tmp/lMSumRecalc.vtk", &grid,
		lMSumCalc ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &vars ) ;
	free( lMSum ) ;
	free( lMSumCalc ) ;
}

//Test that the pressure that is recovered from a steady state solution does not
//change
void test_recover_pressure_steady_state( Params *params, REData *data )
{
	unsigned int i ; //Loop counters
	Grid grid ; //A grid on which to discretise
	REApproxVars vars ; //Variables associated with the approximation to the
						//mixed finite element approximation of the Richards
						//equation
	double *origPress ; //The original values in the pressure before performing
						//recovery of the pressure
	
	//Set the grid to NULL initially
	set_grid_to_null( &grid ) ;
	//Set the approximation variables to NULL initially
	set_re_vars_to_null( &vars ) ;
	
	//Read the grid in from file. The file name is hard coded for the time being
	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	//read_grid_from_file( "../testing/mesh8.out", &grid ) ;
	set_soil_type( params, data, &grid ) ;
	
	//Set the necessary memory for the variables associated with the
	//approximation
	setup_re_vars_on_grid( params, &grid, &vars ) ;
	
	//Set an initial value for the Lagrange multipliers
	set_lagrange_mult_bnd_data( params, &grid, vars.lM ) ;
	set_lagrange_mult_hydrostatic_eq( &grid, &vars, 0.0 ) ;
	
	//Initialise the pressure
	set_richards_init_pressure( params, data, &grid, &vars ) ;
	
	//Set the volumetric wetnes using the current pressure
	for( i=0 ; i < grid.numElements ; i++ ){
		vars.prevTheta[i] = calc_theta( data, grid.els[i].soilType,
			vars.press[i] ) ;
	}
	
	//Set the appropriate memory to store the original pressure approximation
	origPress = (double*)calloc( grid.numElements, sizeof( double ) ) ;
	//Copy the current pressure into the original pressure
	copy_dvec( grid.numUnknowns, vars.press, origPress ) ;
	//Write the pressure out to file
	write_element_func_paraview_format( "/tmp/origPress.vtk", &grid,
		origPress ) ;
		
	//Perform the recovery of the pressure
	recover_pressure( params, data, &grid, &vars ) ;
	
	//After solving we want to print out the current pressure
	write_element_func_paraview_format( "/tmp/newPress.vtk", &grid,
		vars.press ) ;
		
	//Calculate the maximum difference in the pressure values
	printf( "Maximum difference in pressures: %.18e\n",
		dvec_diff_infty_norm( origPress, vars.press, grid.numElements ) ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &vars ) ;
	free( origPress ) ;
}

//Tests that a grid hierarchy is read in successfully
void test_read_grid_hierarchy( Params *params, REData *data )
{
	Grid **gridHierarchy = NULL ;
	
	//Read in the grid hierarchy from file
	gridHierarchy = read_grid_hierarchy( params, data ) ;
	
	assert( gridHierarchy != NULL ) ;
	
	//Now print out some information regarding the grid hierarchy
	write_grid_hierarchy_info_to_console( params, gridHierarchy ) ;
	
	//Free the memory assigned on the heap
	gridHierarchy = free_grid_hierarchy( params, gridHierarchy ) ;
	assert( gridHierarchy == NULL ) ;
}

//useful for writing debugging information regarding a hierarchy of grids
void write_grid_hierarchy_info_to_console( const Params *params,
	Grid **gridHierarchy )
{
	unsigned int i, j ; //Loop counters
	const Grid *grid ; //A grid in the hierarchy
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		//Get the current grid
		grid = gridHierarchy[i] ;
		printf( "Grid level %u\n", i ) ;
		printf( "  Number of rows: %u\n", grid->numRows ) ;
		printf( "  Number of columns: %u\n", grid->numCols ) ;
		printf( "  Number of nodes: %u\n", grid->numNodes ) ;
		printf( "  Number of elements: %u\n", grid->numElements ) ;
		printf( "  Number of edges: %u\n", grid->numEdges ) ;
		printf( "  Number of unknowns: %u\n", grid->numUnknowns ) ;
		printf( "  Boundary type: [" ) ;
		for( j=0 ; j < 4 ; j++ )
			printf( " %d", (int)grid->bndType[j] ) ;
		printf( " ]\n" ) ;
	}
}

//Tests that the prolongation of a function from a coarse to a fine grid and the
//restriction from a fine to a coarse grid works
void test_prolong_restrict( Params *params )
{
	unsigned int i ; //Loop counter
	Interpolation fInterp ; //Matrices representing the interpolation operators
							//on a fine grid
	Interpolation cInterp ; //Matrices representing the interpolation operators
							//on a coarse grid
	double *fFunc ; //A function to restrict from the fine to the coarse grid
	double *cFunc ; //The function that is restricted from the fine grid
	Grid fGrid ; //A fine grid
	Grid cGrid ; //A coarse grid
	
	const Edge *edge ; //An edge on the grid
	double midPoint[2] ; //The coordinate at the mid-point of an edge
	
	//Set the grids to NULL initially
	set_grid_to_null( &fGrid ) ;
	set_grid_to_null( &cGrid ) ;
	//Set the interpolation operators to NULL initially
	set_interp_ops_to_null( &fInterp ) ;
	set_interp_ops_to_null( &cInterp ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &fGrid ) ;
	read_grid_from_file( gridFiles[ params->fGridLevel -1 ], &cGrid ) ;
	//Set the coarse grid edges that are the parents of the fine grid edges
	set_edge_parents( &cGrid, &fGrid ) ;
	
	set_output_colour( "red" ) ;
	write_grid_info_to_console( "Fine grid:", &fGrid ) ;
	printf( "\n" ) ;
	set_output_colour( "blue" ) ;
	write_grid_info_to_console( "Coarse grid:", &cGrid ) ;
	reset_output_style() ;
	
	//Set up the restriction operator on the grid
	set_restrict_prolong_mat_on_grid( &fGrid, &cGrid, &fInterp, &cInterp ) ;
	
	printf( "Number of rows in the restriction matrix: %u\n",
		fInterp.r.numRows ) ;
	printf( "Number of rows in the prolongation matrix: %u\n",
		cInterp.p.numRows ) ;
	
	//Now that I have got a restriction operator I need a function to restrict
	//Set the appropriate memory for the functions
	fFunc = (double*)calloc( fGrid.numEdges, sizeof( double ) ) ;
	cFunc = (double*)calloc( cGrid.numEdges, sizeof( double ) ) ;
	
	//Loop through the unknown edges on the fine grid
	for( i=0 ; i < fGrid.numEdges ; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(fGrid.edges[ fGrid.mapU2G[ i ] ]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		//Calculate the value of the function at the mid-point of the edge
		fFunc[i] = sin( PI * midPoint[0] / 200.0 ) *
			sin( PI * midPoint[1] / 200.0 ) ;
	}
	printf( "Fine grid function created\n" ) ;
	
	//Now that the function is set up we restrict it to the coarse grid
	restrict_approx( &fInterp, fFunc, cFunc ) ;
	printf( "Fine grid function restricted to coarse grid\n" ) ;
	
	//We write out the fine and coarse functions to file to compare them
	write_interior_edge_func_paraview_format( "/tmp/testRFine.vtk", &fGrid,
		fFunc ) ;
	printf( "Fine grid function written to file /tmp/testRFine.vtk\n" ) ;
	write_interior_edge_func_paraview_format( "/tmp/testRCoarse.vtk", &cGrid,
		cFunc ) ;
	printf( "Restricted coarse grid function written to file " ) ;
	printf( "/tmp/testRCoarse.vtk\n" ) ;
	
	//Now perform the prolongation the other way around
	//Loop through the unknown edges on the coarse grid
	for( i=0 ; i < cGrid.numEdges ; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(cGrid.edges[ cGrid.mapU2G[ i ] ]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		//Calculate the value of the function at the mid-point of the edge
		cFunc[i] = sin( PI * midPoint[0] / 200.0 ) *
			sin( PI * midPoint[1] / 200.0 ) ;
	}
	
	printf( "\nCoarse grid function created\n" ) ;
	
	//Now that the function is set up properly prolongate it to the fine grid
	prolong_approx( &cInterp, cFunc, fFunc ) ;
	printf( "Coarse grid function interpolated to fine grid\n" ) ;
	
	//Write out the coarse and fine functions to file to compare them
	write_interior_edge_func_paraview_format( "/tmp/testPCoarse.vtk", &cGrid,
		cFunc ) ;
	printf( "Coarse grid function written to file /tmp/testPCoarse.vtk\n" ) ;
	write_interior_edge_func_paraview_format( "/tmp/testPFine.vtk", &fGrid,
		fFunc ) ;
	printf( "Interpolated fine grid function written to file " ) ;
	printf( "/tmp/testPFine.vtk\n" ) ;
	
	//Free the memory assigned on the heap
	free_grid( &fGrid ) ;
	free_grid( &cGrid ) ;
	free_interp_ops( &fInterp ) ;
	free_interp_ops( &cInterp ) ;
	free( fFunc ) ;
	free( cFunc ) ;
}

//Tests that the inverse of the local stiffness matrix is calculated correctly
void test_calc_loc_stiff_mat_inv( Params *params )
{
	unsigned int i, j, k ; //Loop counters
	Grid grid ; //A grid on which to operate
	const Element *el ; //An element on the grid
	double localMat[9] ; //The local stiffness matrix
	double locMatInv[9] ; //The inverse of the local stiffness matrix
	const Node *points[3] ; //The nodes on the element
	double val ; //Intermediate value used in calculations
	unsigned int ind ; //Index into a matrix
	double e12, e13, e23 ;
	double edgeMagSquare[3] ; //The square of the magnitudes of the edges
	double fact ; //A placeholder used in the calculations
	
	//Set the grid to NULL initially
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	
	//Get a handle on an element in the grid
	el = &(grid.els[3]) ;
	
	//Print out some information about the element
	write_el_info_to_console( "Element:", el ) ;
	
	//Now calculate the inverse of the local stiffness matrix
	calc_loc_mat_inv( el, locMatInv ) ;
	
	//Write the local stiffness matrix to console
	printf( "\nInverse of local stiffness matrix:\n" ) ;
	for( i=0 ; i < 3 ; i++ ){
		for( j=0 ; j < 3 ; j++ )
			printf( "%.6lf ", locMatInv[3*i+j] ) ;
		printf( "\n" ) ;
	}
	
	//Get the nodes on the element	
	get_el_nodes( el, points ) ;
	//Store the value of the inner products of the edges
	e12 = (points[1]->x - points[0]->x) * (points[2]->x - points[1]->x) +
		(points[1]->z - points[0]->z) * (points[2]->z - points[1]->z) ;
	e13 = (points[1]->x - points[0]->x) * (points[0]->x - points[2]->x) +
		(points[1]->z - points[0]->z) * (points[0]->z - points[2]->z) ;
	e23 = (points[2]->x - points[1]->x) * (points[0]->x - points[2]->x) +
		(points[2]->z - points[1]->z) * (points[0]->z - points[2]->z) ;
		
	for( i=0 ; i < 3 ; i++ )
		edgeMagSquare[i] = pow( el->edges[i]->mag, 2 ) ;
		
	//We now construct the local matrix. This is ordered by columns, but seeing
	//as the matrix is symmetric the row ordering would give the same
	//Set the diagonal entries
	localMat[0] = edgeMagSquare[0] - 3 * e23 ;
	localMat[4] = edgeMagSquare[1] - 3 * e13 ;
	localMat[8] = edgeMagSquare[2] - 3 * e12 ;
	
	//Set the off diagonal entries
	localMat[1] = localMat[3] = edgeMagSquare[0] - edgeMagSquare[2] + e13 ;
	localMat[2] = localMat[6] = edgeMagSquare[2] - edgeMagSquare[1] + e23 ;
	localMat[5] = localMat[7] = edgeMagSquare[2] - edgeMagSquare[0] + e13 ;
	
	//Scale the matrix appropriately
	fact = 24 * el->area ;
	for( i=0 ; i < 9 ; i++ )
		localMat[i] /= fact ;
		
	//Print out the local matrix
	printf( "\nLocal stiffness matrix:\n" ) ;
	for( i=0 ; i < 3 ; i++ ){
		for( j=0 ; j < 3 ; j++ ){
			printf( "%.6lf ", localMat[3*i+j] ) ;
		}
		printf( "\n" ) ;
	}
	
	//Peform the product of the two matrices
	printf( "\nProduct of the two matrices\n" ) ;
	for( i=0 ; i < 3 ; i++ ){
		//Perform the multiplication of one row and one column
		for( j=0 ; j < 3 ; j++ ){
			ind = 3*i ;
			val =0.0 ;
			for( k=0 ; k < 3 ; k++ )
				val += localMat[ind+k] * locMatInv[j+3*k] ;
			printf( "%.6lf ", val ) ;
		}
		printf( "\n" ) ;
	}
	
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
}

//Writes a 3x3 matrix to console
void write_3x3_mat_to_console( const double *mat )
{
	unsigned int i, j ;
	
	for( i=0 ; i < 3 ; i++ ){
		for( j=0 ; j < 3 ; j++ )
			printf( "%.15lf ", mat[3*i +j] ) ;
		printf( "\n" ) ;
	}
}

//Tests that the inverses of the local stiffness matrices are set up when a grid
//is read in from file
void test_loc_mat_inv_grid_setup( Params *params )
{
	Grid grid ; //A grid to read in from file
	const Element *el ; //An element on the grid
	
	//Set the grid to NULL initially
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	
	//Show the inverse matrix for several elements on the grid
	el = &(grid.els[0]) ;
	printf( "Element 0:\n" );
	write_3x3_mat_to_console( el->invStiffMat ) ;
	el = &(grid.els[1]) ;
	printf( "\nElement 1:\n" ) ;
	write_3x3_mat_to_console( el->invStiffMat ) ;
	el = &(grid.els[grid.numElements-2]) ;
	printf( "\nElement %u:\n", grid.numElements-2 ) ;
	write_3x3_mat_to_console( el->invStiffMat ) ;
	el = &(grid.els[grid.numElements-1]) ;
	printf( "\nElement %u:\n", grid.numElements-1 ) ;
	write_3x3_mat_to_console( el->invStiffMat ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;	
}

//Tests that the local function defining the relationship between the pressure
//and the Lagrange multipliers is calculated properly if values are calculated
//before calling the method
void test_calc_el_press_func_deriv_with_data( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	Grid grid ; //A grid on which to operate
	const Element *el ; //An element on the grid
	double *funcOld ; //The function calculated using a tested method
	double *funcNew ; //The function calculated using pre-calculated values
	double *press ; //The value of the pressure to use
	double prevTheta ; //The volumetric wetness at the previous time-step
	
	unsigned int numPoints = 10000; //The number of points to use
	double minP = -30 ; //The minimum pressure
	double stepSize ; //This size of the step between one pressure value and the
					  //next
	
	double hCond, hCondDeriv ; 
	double volWet, volWetDeriv ;
	
	//Set the grid to NULL initially
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ 0 ], &grid ) ;
	//Set the soil types
	set_soil_type( params, data, &grid ) ;
	
	//Select an element on the grid
	el = &(grid.els[0]) ;
	
	//Set the memory for the functions
	funcOld = (double*)calloc( numPoints, sizeof( double ) ) ;
	funcNew = (double*)calloc( numPoints, sizeof( double ) ) ;
	press = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//Set the spacing
	stepSize = 1.0 / (numPoints-1) ;
	prevTheta = calc_theta( data, el->soilType,
		minP + stepSize * (numPoints/2) ) ;
	
	//Set the values for the pressure
	for( i=0 ; i < numPoints ; i++ )
		press[i] = minP + i * stepSize ;
		
	//For all of the points calculate the value of the function given the
	//pressure
	for( i=0 ; i < numPoints ; i++ )
		funcOld[i] = el_press_func_deriv( params, data, press[i], prevTheta,
			el ) ;
			
	//Now repeat the process but pre-calculate some of the information
	for( i=0 ; i < numPoints ; i++ ){
		calc_hydr_cond_vol_wet_deriv( data, el->soilType, press[i], &hCond,
			&hCondDeriv, &volWet, &volWetDeriv ) ;
		funcNew[i] = el_press_func_deriv_from_data( params, data, hCond,
			hCondDeriv, volWet, volWetDeriv, prevTheta, el ) ;
	}
	
	//Write out the maximum difference in the entries of the functions
	printf( "Maximum difference in function values: %.18lf\n",
		dvec_diff_infty_norm( funcOld, funcNew, numPoints ) ) ;
		
	//Check that the function value is not zero
	printf( "Function value at index 5000: %.18lf\n", funcOld[5000] ) ;
	
	//Free the memory assigned on the heap
	free( funcOld ) ;
	free( funcNew ) ;
	free( press ) ;
	free_grid( &grid ) ;
}

//Tests that the correct index into a matrix is returned for a given row and
//column
void test_get_mat_ind( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	Grid grid ; //A grid on which to set up a matrix
	LinApproxVars vars ; //Required to set up a matrix on the grid
	const Element *el ; //An element on the grid
	const SparseMatrix *mat ; //The matrix defined on the grid
	unsigned int rowNum, colNum ;
	
	//Set the grid to NULL initially
	set_grid_to_null( &grid ) ;
	set_lin_approx_vars_to_null( &vars ) ;
	
	//Read the grid in from file
	read_grid_from_file( "../testing/mesh2.out", &grid ) ;
	set_soil_type( params, data, &grid ) ;
	
	//Initialise the linear approximation variables, including setting up the
	//structure of the matrix
	init_lin_approx_vars_on_grid( &grid, &vars ) ;
	
	//Set a placeholder for the matrix
	mat = &(vars.op) ;
	
	//Get an element on the grid
	el = &(grid.els[2]) ;
	rowNum = el->edges[0]->id ;
	
	//Print out the current row in the matrix
	printf( "Row number: %u\n", rowNum ) ;
	printf( "Starting index in the matrix: %u\n",
		mat->rowStartInds[ rowNum ] ) ;
	printf( "Entries in the row:" ) ;
	for( i=mat->rowStartInds[rowNum] ; i < mat->rowStartInds[rowNum+1] ; i++ )
		printf( " %u", mat->colNums[i] ) ;
	printf( "\n\n" ) ;
	
	//Loop through the edges on the element
	for( i=0 ; i < 3 ; i++ ){
		colNum = el->edges[i]->id ;
		if( colNum >= grid.numUnknowns ) continue ;
		printf( "Column number: %u\n", colNum ) ;
		printf( "Index in matrix: %u\n",
			get_matrix_ind( mat, rowNum, colNum ) ) ;
		printf( "\n" ) ;
	}
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_lin_approx_vars( &vars ) ;
}

//Tests that the residual and the Jacobian matrix are calculated correctly for 
//the Richards equation
void test_calc_richards_resid_jac( Params *params, REData *data )
{
//	unsigned int i, j ; //Loop counters
	unsigned int i ; //Loop counter
	Grid grid ; //A grid on which to calculate the Jacobian
	REApproxVars vars ; //Variables associated with the approximation to the
						//mixed finite element formulation of the Richards
						//equation
	LinApproxVars linVars ; //Variables associated with the Newton linearisation
							//of a problem
	double *jacEntries ; //The entries in the Jacobian matrix
//	unsigned int edgeNum = 5 ;
//	const Edge *edge ; //An edge on the grid
	
	//Initalise the grid to null
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &vars ) ;
	set_lin_approx_vars_to_null( &linVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[0], &grid ) ;
	//Set the soil type
	set_soil_type( params, data, &grid ) ;
	
	//Setup the approximation variables on the grid
	setup_re_vars_on_grid( params, &grid, &vars ) ;
	
	//Setup the linear approximation variables on the grid
	init_lin_approx_vars_on_grid( &grid, &linVars ) ;
	
	//Set the memory to store the Jacobian entries
	jacEntries = (double*)calloc( linVars.op.numNonZeros, sizeof( double ) ) ;
	
	//Set the boundary data
	set_lagrange_mult_bnd_data( params, &grid, vars.lM ) ;

	//Set the lagrange multipliers to an initial value
	set_richards_init_lagrange_mult( params, data, &grid, &vars ) ;	
		
	//Write out the Lagrange multipliers to file
	write_edge_func_paraview_format( "/tmp/lM.vtk", &grid, vars.lM ) ;
	
	//Set an initial approximation for the pressure
	set_richards_init_pressure( params, data, &grid, &vars ) ;
		
	//Set the volumetric wetness for the current pressure
	for( i=0 ; i < grid.numElements ; i++ ){
		vars.prevTheta[i] = calc_theta( data, grid.els[i].soilType,
			vars.press[i] ) ;
	}
		
	//Now recover the actual pressure
	recover_pressure( params, data, &grid, &vars ) ;
	
	//Write the pressure out to file
	write_element_func_paraview_format( "/tmp/press.vtk", &grid, vars.press ) ;
	
	//Make sure that the right hand side is set to zero
	set_const_dvec( grid.numUnknowns, 0, vars.rhs ) ;
	
	//Now calculate the residual using the functions set above
	calc_richards_resid( params, &grid, data, &vars ) ;
	
	//Write the residual out to file
	write_interior_edge_func_paraview_format( "/tmp/testResid.vtk", &grid,
		vars.resid ) ;
		
	//Now calculate the Jacobian matrix
	calc_richards_jacobian( params, &grid, data, &vars, &(linVars.op) ) ;
	
	//Copy the values from the global entries in the Jacobian into a temporary
	//variable
	copy_dvec( linVars.op.numNonZeros, linVars.op.globalEntry, jacEntries ) ;
	
	//Now calculate the Jacobian matrix using numerical differentiation
	calc_richards_numeric_jacobian( params, &grid, data, &vars,
		&(linVars.op) ) ;
		
	//Compare the entries in the jacobian matrices, and output the maximum
	//difference
	printf( "Maximum difference in numeric and exact Jacobian values: %.18lf\n",
		dvec_diff_infty_norm( jacEntries, linVars.op.globalEntry,
			linVars.op.numNonZeros ) ) ;
			
	//Print to consoel the maximum values in the Jacobian matrices
	printf( "Maximum value in exact Jacobian: %.18lf\n",
		dvec_infty_norm( jacEntries, linVars.op.numNonZeros ) ) ;
	printf( "Maximum value in numeric Jacobian: %.18lf\n",
		dvec_infty_norm( linVars.op.globalEntry, linVars.op.numNonZeros ) ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &vars ) ;
	free_lin_approx_vars( &linVars ) ;
	free( jacEntries ) ;
}

//Tests that when the problem is linear that the Jacobian matrix is the same as
//the operator matrix. We make a note that the residuals calculated will be
//translated from one another by a function determined by the sum of the
//z-factors over the elements
void test_linear_jac_set_up( Params *params, REData *data )
{
	unsigned int i, j ; //Loop counters
	Grid grid ; //The grid on which the problem is to be set up
	REApproxVars reVars ; //Variables associated with the approximation of the
				//mixed finite element formulation of the Richards equation
	LinApproxVars linVars ; //Variables required to perform a linear solve
	double *resid ; //Stores the value of what should be the residual in
					//approximation
	unsigned int edgeNum = 0 ;
	const Edge *edge ; //An edge on the grid
	
	//Set the pointers in the data structures to zero initially
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &reVars ) ;
	set_lin_approx_vars_to_null( &linVars ) ;
	
	//Read the grid in from file
//	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	read_grid_from_file( gridFiles[0], &grid ) ;
	//Set the soil types
	set_soil_type( params, data, &grid ) ;
	
	//Set the memory for the nonlinear approximation variables
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	
	//Set the memory for the linear approximation variables
	init_lin_approx_vars_on_grid( &grid, &linVars ) ;
	
	//Initialise the approximation to the Richards equation such that we are in
	//the linear regime
	set_lagrange_mult_hydrostatic_eq( &grid, &reVars, 0.0 ) ;
	
	write_edge_func_paraview_format( "/tmp/lM.vtk", &grid,
		reVars.lM ) ;
	
	//Set the pressure
	set_richards_init_pressure( params, data, &grid, &reVars ) ;
	
	write_element_func_paraview_format( "/tmp/press.vtk", &grid,
		reVars.press ) ;
	
	//Calculate the appropriate volumetric wetness
	for( i=0 ; i < grid.numElements ; i++ )
		reVars.prevTheta[i] = calc_theta( data, grid.els[i].soilType,
			reVars.press[i] ) ;
	
	//Calculate the residual and the Jacobian matrix
	calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
		&(linVars.op) ) ;
		
	//Write the matrix out to file
	write_sparse_matrix_to_file( "/tmp/sparseJac.txt", &(linVars.op) ) ;
	
	edge = &(grid.edges[edgeNum]) ;
	write_row_nonzero_cols_to_console( &(linVars.op), edgeNum ) ;
	
	for( i=0 ; i < 2 ; i++ ){
		//Print out the order of the edges on the current element
		for( j=0 ; j < 3 ; j++ )
			printf( "%u ", edge->parentEls[i]->edges[j]->id ) ;
		printf( "\n" ) ;
		write_3x3_mat_to_console( edge->parentEls[i]->invStiffMat ) ;
		printf( "Row sum: %.18lf\n\n", edge->parentEls[i]->invMatRowSum ) ;
	}
	
	printf( "Value in the Lagrange multipliers at indices 0 and 5: " ) ;
	printf( "%.18lf, %.18lf\n", reVars.lM[0], reVars.lM[5] ) ;
		
	//Set the memory for the residual
	resid = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Apply the Jacobian matrix to the value of the Lagrange multipliers. The
	//result is stored in 'resid', and the value should be the same as is stored
	//in the nonlinear residual
	sparse_matrix_vec_mult( &(linVars.op), reVars.lM, resid ) ;
	
	//Check that the residual values are the same
	printf( "Maximum difference in residual calculated from the Jacobian " ) ;
	printf( "matrix and the nonlinear operator: %.18e\n",
		dvec_diff_infty_norm( resid, reVars.resid, grid.numUnknowns ) ) ;
		
	//Write the residual out to file
	write_edge_func_paraview_format( "/tmp/origResid.vtk", &grid,
		reVars.resid ) ;
	//Write the value of the Lagrange multipliers times the Jacobian matrix to
	//file
	write_edge_func_paraview_format( "/tmp/linResid.vtk", &grid, resid ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &reVars ) ;
	free_lin_approx_vars( &linVars ) ;
	free( resid ) ;
}

//Tests that the hierarchy of linear variables can be set up on a hierarchy of
//grids
void test_setup_lin_vars_hierarchy( Params *params, REData *data )
{
	Grid **gridHierarchy ; //A hierarchy of grids
	LinApproxVars **linHierarchy ; //Hierarchy storing information required to
					//solve a linear problem on each grid in the grid hierarchy
					
	//Set the grid hierarchy by reading the data in from file
	gridHierarchy = read_grid_hierarchy( params, data ) ;
	linHierarchy = set_lin_approx_hierarchy( params, gridHierarchy ) ;
	
	//Now output some information that makes sure that the linear approximation
	//variables have been set up appropriately. In particular we are interested
	//in knowning the number of rows and columns in the matrices
	write_lin_approx_hierarchy_info_to_console( params, gridHierarchy,
		linHierarchy ) ;
	
	//Free the memory assigned on the heap
	gridHierarchy = free_grid_hierarchy( params, gridHierarchy ) ;
	linHierarchy = free_lin_approx_hierarchy( params, linHierarchy ) ;
}

//Useful for writing out debugging information regarding a hierarchy of
//variables required to perform a linear solve
void write_lin_approx_hierarchy_info_to_console( const Params *params,
	Grid **gridHierarchy, LinApproxVars **linHierarchy )
{
	unsigned int i ;
	//Output useful information regarding the LinApproxVars structures in the
	//hierarchy
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		printf( "Level %u:\n", i+1 ) ;
		printf( "  Number of unknowns on grid: %u\n",
			gridHierarchy[i]->numUnknowns ) ;
		printf( "  Number of rows in matrix: %u\n",
			linHierarchy[i]->op.numRows ) ;
		printf( "  Number of columns in matrix: %u\n",
			linHierarchy[i]->op.numCols ) ;
		if( linHierarchy[i]->approx == NULL ){
			printf( "  Memory not set for approximation\n" ) ;
			exit( 0 ) ;
		}
		if( linHierarchy[i]->rhs == NULL ){
			printf( "  Memory not set for rhs\n" ) ;
			exit( 0 ) ;
		}
		if( linHierarchy[i]->resid == NULL ){
			printf( "  Memory not set for residual\n" ) ;
			exit( 0 ) ;
		}
	}
}

//Tests that the hierarchy of interpolation operators is set up properly on a
//hierarchy of grids
void test_setup_interp_hierarchy( Params *params, REData *data )
{
	Grid **gridHierarchy ; //A hierarchy of grids on which to set up
						   //interpolation operators
	Interpolation **interpHierarchy ; //The hierarchy of interpolation operators
					//to set up on the grid
					
	//Read the grid hierarchy in from file
	gridHierarchy = read_grid_hierarchy( params, data ) ;
	//Set the hierarchy of interpolation operators on the hierarchy of grids
	interpHierarchy = setup_interp_hierarchy( params, gridHierarchy ) ;
	
	//Print out some useful information about the interpolation operators
	//on each grid
	write_interp_hierarchy_info_to_console( params, gridHierarchy,
		interpHierarchy ) ;
	
	//Free the memory assigned on the heap
	gridHierarchy = free_grid_hierarchy( params, gridHierarchy ) ;
	interpHierarchy = free_interp_hierarchy( params, interpHierarchy ) ;
}

//Useful for writing out debugging information regarding a hierarchy of 
//interpolation operators
void write_interp_hierarchy_info_to_console( const Params *params,
	Grid **gridHierarchy, Interpolation **interpHierarchy )
{
	unsigned int i ;
	//Loop through the grids that are set up
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		printf( "Level %u:\n", i+1 ) ;
		printf( "  Number of unknowns on grid: %u\n",
			gridHierarchy[i]->numUnknowns ) ;
		if( interpHierarchy[i]->r.numNonZeros == 0 )
			printf( "  Restriction not set up\n" ) ;
		else{
			printf( "  Number of rows in restriction matrix: %u\n",
				interpHierarchy[i]->r.numRows ) ;
			printf( "  Number of columns in restriction matrix: %u\n",
				interpHierarchy[i]->r.numCols ) ;
		}
		if( interpHierarchy[i]->p.numNonZeros == 0 )
			printf( "  Prolongation not set up\n"  );
		else{
			printf( "  Number of rows in prolongation matrix: %u\n",
				interpHierarchy[i]->p.numRows ) ;
			printf( "  Number of columns in prolongation matrx: %u\n",
				interpHierarchy[i]->p.numCols ) ;
		}
	}
}

//Tests that the appropriate memory is set for the variables required to perform
//a Newton-Multigrid iteration
void test_setup_newton_ctxt( Params *params, REData *data )
{
	NewtonCtxt ctxt ; //The context to set up
	char str[50] ; //String used to print out information
	
	assert( params->linIt == IT_LIN_MG || params->linIt == IT_PGMRES ) ;
	
	//Initialise the context to NULL
	set_newton_ctxt_to_null( &ctxt ) ;
	
	//Set up the Newton context depending on the parameters passed in
	setup_newton_ctxt( params, data, &ctxt ) ;
	
	lin_it_str_rep( params, str ) ;
	printf( "Linear iteration: %s\n", str ) ;
	
	//Print out some information to show that the context has been set up
	//appropriately
	if( params->linIt == IT_LIN_MG ){
		LinMGCtxt *mgCtxt ;
		mgCtxt = (LinMGCtxt*)ctxt.linItCtxt ;
		printf( "Grid Info:\n" ) ;
		write_grid_hierarchy_info_to_console( params, mgCtxt->gridHierarchy ) ;
		printf( "\nLinear Operator Contexts:\n" ) ;
		write_lin_approx_hierarchy_info_to_console( params,
			mgCtxt->gridHierarchy, mgCtxt->linHierarchy ) ;
		printf( "\nInterpolation Operators:\n" ) ;
		write_interp_hierarchy_info_to_console( params, mgCtxt->gridHierarchy,
			mgCtxt->interpHierarchy ) ;
	}
	else if( params->linIt == IT_PGMRES ){
		PrecGMRESCtxt *gmresCtxt ;
		gmresCtxt = (PrecGMRESCtxt*)ctxt.linItCtxt ;
		printf( "Grid Info:\n" ) ;
		write_grid_hierarchy_info_to_console( params, gmresCtxt->gridHierarchy ) ;
		printf( "\nLinear Operator Contexts:\n" ) ;
		write_lin_approx_hierarchy_info_to_console( params,
			gmresCtxt->gridHierarchy, gmresCtxt->linHierarchy ) ;
		printf( "\nInterpolation Operators:\n" ) ;
		write_interp_hierarchy_info_to_console( params,
			gmresCtxt->gridHierarchy, gmresCtxt->interpHierarchy ) ;
	}
	
	//Free the memory assigned on the grid
	free_newton_ctxt( params, &ctxt ) ;
}

//Tests that the residual and the Jacobian matrix are set up properly when they
//are populated simultaneously
void test_calc_richards_resid_and_jac( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	double midPoint[2] ; //Mid point of an edge
	const Edge *edge ;
	Grid grid ; //A grid on which to calculate the residual
	REApproxVars reVars ; //Variables associated with the approximation of the
				//mixed finite element formulation of the Richards equation
	LinApproxVars linVars ; //Variables required for the solution of a linear
				//system of equations
	double constPress = -30.0 ;
	double *oldJac, *oldResid ; //The Jacobian and the residual calculated using
				//separately
				
	//Set all pointers to NULL initially
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &reVars ) ;
	set_lin_approx_vars_to_null( &linVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	//Set the soil types on the elements
	set_soil_type( params, data, &grid ) ;
	
	//Set up the linear and nonlinear approximation variables on the grid
	init_lin_approx_vars_on_grid( &grid, &linVars ) ;
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	
	//Set an initial approximation for the lagrange multipliers
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		//Get the current edge on the grid
		edge = &(grid.edges[ grid.mapU2G[i] ]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		//Set the value of the Lagrange multipliers
		reVars.lM[i] = -20 * sin( PI * midPoint[0] ) * sin( PI * midPoint[1] ) ;
	}
	
	//Set the boundary data
	set_lagrange_mult_bnd_data( params, &grid, reVars.lM ) ;
	
	//Initialise the pressure on the grid
	for( i=0 ; i < grid.numElements ; i++ ){
		reVars.press[i] = constPress ;
		reVars.prevTheta[i] = calc_theta( data, grid.els[i].soilType,
			reVars.press[i] ) ;
	}
		
	//Recover the current pressure
	recover_pressure( params, data, &grid, &reVars ) ;
	
	//Calculate the residual and the Jacobian using the old methods
	calc_richards_resid( params, &grid, data, &reVars ) ;
	calc_richards_jacobian( params, &grid, data, &reVars, &(linVars.op ) ) ;
	
	//Set variables to store the Jacobian and the residual
	oldResid = (double*)malloc( grid.numUnknowns * sizeof( double ) ) ;
	oldJac = (double*)malloc( linVars.op.numNonZeros * sizeof( double ) ) ;
	
	//Copy the residual into the old residual variable
	copy_dvec( grid.numUnknowns, reVars.resid, oldResid ) ;
	copy_dvec( linVars.op.numNonZeros, linVars.op.globalEntry, oldJac ) ;
	
	//Now calculate the residual and the Jacobian simultaneously
	calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
		&(linVars.op) ) ;
		
	//Write out some useful information about the results
	printf( "Maxmimum value in residual calculated independently: %.18lf\n",
		dvec_infty_norm( oldResid, grid.numUnknowns ) ) ;
	printf( "Maximum value in residual calculated with the Jacobian: %.18lf\n",
		dvec_infty_norm( reVars.resid, grid.numUnknowns ) ) ;
	printf( "Maximum difference in residual values: %.18lf\n",
		dvec_diff_infty_norm( oldResid, reVars.resid, grid.numUnknowns ) ) ;
		
	printf( "Maxmimum mvalue in Jacobian calculated independently: %.18lf\n",
		dvec_infty_norm( oldJac, linVars.op.numNonZeros ) ) ;
	printf( "Maxmimum value in Jacobian calculated with the residual: %.18lf\n",
		dvec_infty_norm( linVars.op.globalEntry, linVars.op.numNonZeros ) ) ;
	printf( "Maximum difference in Jacobian values: %.18lf\n",
		dvec_diff_infty_norm( oldJac, linVars.op.globalEntry,
			linVars.op.numNonZeros ) ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &reVars ) ;
	free_lin_approx_vars( &linVars ) ;
	free( oldResid ) ;
	free( oldJac ) ;
}

//Tests that a Newton context is properly initialised
void test_set_newton_ctxt( Params *params, REData *data )
{
	unsigned int i ; //A loop counter
	const Edge *edge ; //An edge on a grid
	double midPoint[2] ; //The mid-point of an edge
	Grid *grid ; //Placeholder for a grid
	NewtonCtxt ctxt ; //The context to set up
	REApproxVars *reVars ;
	double constPress = -30 ; //A constant pressure to initialise the pressure
							  //function with
	LinApproxVars **linHierarchy ;
	Grid **gridHierarchy ;
	Interpolation **interpHierarchy ;
	
	//Set the context to NULL initially
	set_newton_ctxt_to_null( &ctxt ) ;
	reVars = &(ctxt.reVars) ;
	
	//Set up the Newton context on a hierarchy depending on the parameters
	//passed in
	setup_newton_ctxt( params, data, &ctxt ) ;
	
	//Set a pointer to the finest grid
	grid = ctxt.grid ;
	linHierarchy = get_newton_lin_hierarchy( params, &ctxt ) ;
	gridHierarchy = get_newton_grid_hierarchy( params, &ctxt ) ;
	interpHierarchy = get_newton_interp_hierarchy( params, &ctxt ) ;
	
	//Initialise an approximation on the finest grid
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		//Get a handle for the current edge on the grid
		edge = &(grid->edges[ grid->mapU2G[ i ] ]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		//Set the value of the Lagrange multiplier
		reVars->lM[i] = -20 * sin( PI * midPoint[0] ) *
			sin( PI * midPoint[1] ) ;
	}
	
	//Set the initial pressure
	for( i=0 ; i < grid->numElements ; i++ ){
		reVars->press[i] = constPress ;
		reVars->prevTheta[i] = calc_theta( data, grid->els[i].soilType,
			reVars->press[i] ) ;
	}
		
	//Recover the present value for the pressure
	recover_pressure( params, data, grid, reVars ) ;
	
	//Calculate the Newton variables
	set_newton_mg_iteration_vars( params, data, &ctxt, gridHierarchy,
		linHierarchy, interpHierarchy ) ;
	
	//Output some information regarding the hierarchy of operators
	for( i=params->fGridLevel ; i > params->cGridLevel ; i-- ){
		printf( "Operator on level %u:\n", i ) ;
		write_sparse_matrix_rows_to_console( &(linHierarchy[i]->op), 30 ) ;
		printf( "\n" ) ;
	}
	printf( "Operator on level 0:\n" ) ;
	write_sparse_matrix_rows_to_console(
		&(linHierarchy[params->cGridLevel]->op), 30 ) ;
	
	//Free the memory assigned on the heap
	free_newton_ctxt( params, &ctxt ) ;
}

//Writes out the first 'numRows' rows of a sparse matrix. This is useful for
//debugging purposes
void write_sparse_matrix_rows_to_console( const SparseMatrix *mat,
	unsigned int numRows )
{
	unsigned int i, j ; //Loop counters
	
	//Make sure that we don't try and write more rows than there are
	numRows = ( mat->numRows < numRows ) ? mat->numRows : numRows ;
	
	//Loop through the rows
	for( i=0 ; i < numRows ; i++ ){
		printf( "Row %u:", i+1 ) ;
		for( j=mat->rowStartInds[i] ; j < mat->rowStartInds[i+1] ; j++ )
			printf( " %.7lf", mat->globalEntry[j] ) ;
		printf( "\n" ) ;
	}
}

//Tests that the initial data is appropriately set for the Richards equation
void test_set_richards_initial_data( Params *params, REData *data )
{
	Grid grid ; //The grid on which to initialise the data
	REApproxVars vars ; //Variables associated with the mixed finite element
				//approximation of the Richards equation
	
	//Set pointers in structures to NULL initially
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &vars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	//Set up the soil types on the current grid
	set_soil_type( params, data, &grid ) ;
	
	//Initialise the memory for the variables associated with the Richards
	//equation
	setup_re_vars_on_grid( params, &grid, &vars ) ;
	
	//Now set the initial approximation on the grid
	set_richards_initial_data( params, data, &grid, &vars ) ;
	
	//Print out the functions that were intialised
	write_edge_func_paraview_format( "/tmp/lM.vtk", &grid, vars.lM ) ;
	write_element_func_paraview_format( "/tmp/press.vtk", &grid, vars.press ) ;
	write_element_func_paraview_format( "/tmp/prevTheta.vtk", &grid,
		vars.prevTheta ) ;
		
	//Calculate the residual in approximation
	calc_richards_resid( params, &grid, data, &vars ) ;
	
	//Calculate the residual norm
	printf( "Residual norm for initial set-up: %.18e\n",
		discrete_l2_norm( grid.numUnknowns, vars.resid ) ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &vars ) ;
}

//Tests that a Newton method with linear smoothing applied will converge for a
//Richards equation type problem
void test_newton_smooth( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	unsigned int newtCnt = 0 ; //Loop counter
	unsigned int numSmooths = 100 ; //The number of smooths to perform
	Grid grid ; //The grid on which to perform the iteration
	REApproxVars reVars ; //Variables associated with the approximation of the
				//mixed finite element formulation of the Richards equation
	LinApproxVars linVars ; //Variables required to solve a linear system of
				//equations
	double residNorm ; //The scaled L2 norm of the residual
	double linResid, oldLinResid ; //The scaled L2 norm of the linear residual
				//and the previous linear residual
	char outFName[20] ; //File name to write output to

	//Set the appropriate pointers to NULL in the structures to be used
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &reVars ) ;
	set_lin_approx_vars_to_null( &linVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	//Set the soil types
	set_soil_type( params, data, &grid ) ;
	//Set the memory for an array that may be used when needed in the code. This
	//removes the need for repeated allocation and deallocation of memory
	set_param_spare_memory( grid.numEdges, params ) ;
	
	//Initialise the variables required for the approximation of the nonlinear
	//and linear problems
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	init_lin_approx_vars_on_grid( &grid, &linVars ) ;
	
	//Initialise the approximation on the grid
	set_richards_initial_data( params, data, &grid, &reVars ) ;
	
	//Recover the actual pressure
	recover_pressure( params, data, &grid, &reVars ) ;
	
	//Calculate the residual and the Jacobian matrix
	calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
		&(linVars.op) ) ;
		
	//Calculate the residual norm
	residNorm = discrete_l2_norm( grid.numUnknowns, reVars.resid ) ;
	
	//Perform Newton iterations until some stopping criterion is met
	while( residNorm > 1e-6 && newtCnt < 1000 ){
		//Initialise the approximation to the Newton system
		init_newt_approx_on_grid( &reVars, &grid, &linVars ) ;
		
		printf( "Newton iteration %u:\nResidual norm: %.18lf\n", newtCnt + 1,
			residNorm ) ;
		//Calculate the residual in the linear system
		calculate_lin_resid( &linVars ) ;
		linResid = discrete_l2_norm( grid.numUnknowns, linVars.resid ) ;
		printf( "  Linear residual: %.18lf\n", linResid ) ;
		//Perform smoothing
		for( i=0 ; i < numSmooths ; i++ ){
			oldLinResid = linResid ;
			//Perform a single smoothing iteration
			lin_smooth( params, 1, &linVars ) ;
			//Calculate the residual
			calculate_lin_resid( &linVars ) ;
			linResid = discrete_l2_norm( grid.numUnknowns, linVars.resid ) ;
			printf( "  Linear residual: %.18lf\tRatio: %.18lf\n", linResid,
				linResid / oldLinResid ) ;
		}
		
		//Update the value of the approximation
		update_re_approx_from_newt_correct( params, &grid, &linVars, &reVars ) ;
		
		//Update the value of the pressure
		recover_pressure( params, data, &grid, &reVars ) ;
		
		//Calculate the residual and the Jacobian matrix
		calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
			&(linVars.op) ) ;
			
		//Update the residual norm
		residNorm = discrete_l2_norm( grid.numUnknowns, reVars.resid ) ;
		
		//Increment the counter of the Newton iteration
		newtCnt++ ;
		
		//Write the Lagrange multipliers and the pressure out to file
		sprintf( outFName, "/tmp/lM%u.vtk", newtCnt ) ;
		write_edge_func_paraview_format( outFName, &grid, reVars.lM ) ;
		sprintf( outFName, "/tmp/press%u.vtk", newtCnt ) ;
		write_element_func_paraview_format( outFName, &grid, reVars.press ) ;
		sprintf( outFName, "/tmp/theta%u.vtk", newtCnt ) ;
		write_element_func_paraview_format( outFName, &grid,
			reVars.prevTheta ) ;
	}
	
	//Print out the residual norm at the final time
	printf( "Residual norm: %.18lf\n", residNorm ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &reVars ) ;
	free_lin_approx_vars( &linVars ) ;
}

//Tests that smoothing converges in each Newton iteration
void test_newton_smooth_to_converge( Params *params, REData *data )
{
	unsigned int newtCnt = 0 ; //Loop counter
	Grid grid ; //The grid on which to perform the iteration
	REApproxVars reVars ; //Variables associated with the approximation of the
				//mixed finite element formulation of the Richards equation
	LinApproxVars linVars ; //Variables required to solve a linear system of
				//equations
	double residNorm ; //The scaled L2 norm of the residual
	char outFName[20] ; //File name to write output to

	//Set the appropriate pointers to NULL in the structures to be used
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &reVars ) ;
	set_lin_approx_vars_to_null( &linVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	//Set the soil types
	set_soil_type( params, data, &grid ) ;
	//Set the memory for an array that may be used when needed in the code. This
	//removes the need for repeated allocation and deallocation of memory
	set_param_spare_memory( grid.numEdges, params ) ;
	
	//Initialise the variables required for the approximation of the nonlinear
	//and linear problems
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	init_lin_approx_vars_on_grid( &grid, &linVars ) ;
	
	//Initialise the approximation on the grid
	set_richards_initial_data( params, data, &grid, &reVars ) ;
	
	//Recover the actual pressure
	recover_pressure( params, data, &grid, &reVars ) ;
	
	//Calculate the residual and the Jacobian matrix
	calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
		&(linVars.op) ) ;
		
	//Calculate the residual norm
	residNorm = discrete_l2_norm( grid.numUnknowns, reVars.resid ) ;
	
	//Perform Newton iterations until some stopping criterion is met
	while( residNorm > 1e-6 && newtCnt < 10 ){
		//Initialise the approximation to the Newton system
		init_newt_approx_on_grid( &reVars, &grid, &linVars ) ;
		
		printf( "After Newton iteration %u:\nResidual norm: %.18lf\n", newtCnt,
			residNorm ) ;
		//Perform smoothing until convergence
		lin_smooth_to_converge( params, &linVars ) ;
		
		//Update the value of the approximation
		update_re_approx_from_newt_correct( params, &grid, &linVars, &reVars ) ;
		
		//Update the value of the pressure
		recover_pressure( params, data, &grid, &reVars ) ;
		
		//Calculate the residual and the Jacobian matrix
		calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
			&(linVars.op) ) ;
			
		//Update the residual norm
		residNorm = discrete_l2_norm( grid.numUnknowns, reVars.resid ) ;
		
		//Increment the counter of the Newton iteration
		newtCnt++ ;
		
		//Write the Lagrange multipliers and the pressure out to file
		sprintf( outFName, "/tmp/lM%u.vtk", newtCnt ) ;
		write_edge_func_paraview_format( outFName, &grid, reVars.lM ) ;
		sprintf( outFName, "/tmp/press%u.vtk", newtCnt ) ;
		write_element_func_paraview_format( outFName, &grid, reVars.press ) ;
		sprintf( outFName, "/tmp/theta%u.vtk", newtCnt ) ;
		write_element_func_paraview_format( outFName, &grid,
			reVars.prevTheta ) ;
	}
	
	//Print out the residual norm at the final time
	printf( "After Newton iteration %u:\nResidual norm: %.18lf\n", newtCnt,
		residNorm ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &reVars ) ;
	free_lin_approx_vars( &linVars ) ;
}

//Tests that multigrid can be used as part of a Newton iteration to solve a
//nonlinear system of equations at a single time
void test_single_newton_mg( Params *params, REData *data )
{
	unsigned int newtCnt = 0 ; //Counts the number of Newton iterations
							   //performed
	NewtonCtxt ctxt ; //Stores all the data structures necessary to perform a
					  //Newton-Multigrid iteration
	REApproxVars reVars ; //Variables associated with the approximation of a
				//mixed finite element formulation of the Richards equation
	const Grid *fGrid ; //Placeholder for the finest grid in the hierarchy of
						//grids
	LinApproxVars *fVars ; //Placeholder for the variables required to perform
						   //a linear solve on the finest grid
	double residNorm ; //The norm of the residual for the Richards equation
	
	char outFName[20] ; //Filename to write output to
					  
	//Set the pointers in the data structures to NULL initially
	set_newton_ctxt_to_null( &ctxt ) ;
	set_re_vars_to_null( &reVars ) ;
	
	//Initialise all of the memory required to perform a Newton-Multigrid
	//iteration
	setup_newton_ctxt( params, data, &ctxt ) ;
	
	//Set the memory for the nonlinear approximation on the finest grid
	fGrid = ctxt.grid ;
	setup_re_vars_on_grid( params, fGrid, &reVars ) ;
	//Set the memory for an array that may be used when needed in the code. This
	//removes the need for repeated allocation and deallocation of memory
	set_param_spare_memory( fGrid->numEdges, params ) ;
	
	//Initialise the approximation on the grid
	set_richards_initial_data( params, data, fGrid, &reVars ) ;
	//Recover the actual pressure
	recover_pressure( params, data, fGrid, &reVars ) ;
	
	//Calculate the nonlinear residual and Jacobian
	fVars = ctxt.linVars ;
	calc_richards_resid_and_jacobian( params, fGrid, data, &reVars,
		&(fVars->op) ) ;
	//Calculate the norm of the residual
	residNorm = discrete_l2_norm( fGrid->numUnknowns, reVars.resid ) ;
	
	//Perform Newton iterations until some stopping criterion is met
	while( residNorm > 1e-6 && newtCnt < 10 ){
		//Perform the Newton iteration. We first calculate the appropriate
		//linear operators on the grid levels
		set_newton_lin_vars( params, &ctxt ) ;
			
		//Set up the initial approximation for the Newton iteration
		init_newt_approx_on_grid( &reVars, fGrid, fVars ) ;
		
		printf( "After Newton iteration %u:\nResidual norm: %.18lf\n", newtCnt,
			residNorm ) ;
			
		//Perform the inner iterations
		perform_newton_inner_it( params, &ctxt ) ;
				
		//Update the value of the approximation
		update_re_approx_from_newt_correct( params, fGrid, fVars, &reVars ) ;
		
		//Update the value of the pressure
		recover_pressure( params, data, fGrid, &reVars ) ;
		
		//Calculate the residual and the Jacobian matrix
		calc_richards_resid_and_jacobian( params, fGrid, data, &reVars,
			&(fVars->op) ) ;
			
		//Update the residual norm
		residNorm = discrete_l2_norm( fGrid->numUnknowns, reVars.resid ) ;
		
		//Increment the counter of the Newton iteration
		newtCnt++ ;
		
		//Write the Lagrange multipliers and the pressure out to file
		sprintf( outFName, "/tmp/lM%u.vtk", newtCnt ) ;
		write_edge_func_paraview_format( outFName, fGrid, reVars.lM ) ;
		sprintf( outFName, "/tmp/press%u.vtk", newtCnt ) ;
		write_element_func_paraview_format( outFName, fGrid, reVars.press ) ;
		sprintf( outFName, "/tmp/theta%u.vtk", newtCnt ) ;
		write_element_func_paraview_format( outFName, fGrid,
			reVars.prevTheta ) ;
	}
	
	printf( "After Newton iteration %u:\nResidual norm: %.18lf\n", newtCnt,
			residNorm ) ;
	
	//Free the memory assigned on the heap
	free_newton_ctxt( params, &ctxt ) ;
	free_re_vars( &reVars ) ;
}

//Tests that the flux is recovered appropriately from the data passed in
void test_recover_flux( Params *params, REData *data )
{
	unsigned int i, elCnt ; //Loop counters
	Grid grid ; //A grid on which to set up the flux
	REApproxVars reVars ; //Variables associated with the mixed finite element
						  //approximation of the Richards equation
	const Edge *edge ; //An edge on the grid
	const Element *el ; //An element on the grid
	double midPoint[2] ; //The mid-point of an edge
	double *fluxMags ; //The magnitude of the normal fluxes over the edges
	
	//Set the pointers in the data structures to NULL initially
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &reVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	//set the soil types on the grid
	set_soil_type( params, data, &grid ) ;
	
	//Initialise the variables associated with the Richards equation on the
	//grid passed in
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	
	//Set the appropriate memory
	fluxMags = (double*)calloc( grid.numEdges, sizeof( double ) ) ;
	
	//Loop over the edges of the grid
	for( i=0 ; i < grid.numEdges ; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(grid.edges[i]) ;
		//Calculate the value at the centre of the edge
		get_edge_mid_point( edge, midPoint ) ;
		//Set the value of the lagrange multiplier
		reVars.lM[ edge->id ] = sin( PI * midPoint[0] ) *
			sin( PI * midPoint[1] ) ;
	}
	
	//Set the value of the pressure
	set_dvec_to_zero( grid.numElements, reVars.press ) ;
	//Sum over the elements on the grid
	for( elCnt = 0 ; elCnt < grid.numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(grid.els[elCnt]) ;
		//Loop over the edges of the element
		for( i=0 ; i < 3 ; i++ )
			reVars.press[elCnt] += reVars.lM[ el->edges[i]->id ] ;
		
		reVars.press[elCnt] /= 3.0 ;
	}
	
	//Now recover the flux
	recover_flux( params, data, &grid, &reVars, fluxMags ) ;
	
	write_edge_flux_func_paraview_format( "/tmp/flux.vtk", &grid, fluxMags ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &reVars ) ;
	free( fluxMags ) ;
}

//Tests that a Newton iteration with smoothing as the linear solver works as
//expected
void test_solve_richards_with_smooths( Params *params, REData *data )
{
	newton_smooth_solve_richards_eq( params, data ) ;
}

//Tests that a factor required in the calculation of the residual and the
//Jacobian matrix is set up properly
void test_setup_zfact( Params *params, REData *data )
{
	unsigned int i, elCnt ; //Loop counter
	Grid grid ; //The grid to use
	const Element *el ; //An element on the grid
	const Node *nodes[3] ; //Pointers to the nodes on an element
	double midPoint[2] ; //Mid-point of an edge
	
	//Initialise the pointers in the structures to NULL
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	//Set the soil types
	set_soil_type( params, data, &grid ) ;
	
	//Now check that the correct values are stored for the first and second
	//elements
	for( elCnt = 0 ; elCnt < 2 ; elCnt++ ){
		el = &(grid.els[elCnt]) ;
	
		//Print out the coordinates of the nodes on the element
		get_el_nodes( el, nodes ) ;
		printf( "Element %u:\n", elCnt ) ;
		print_indent( 1, "Nodes:\n" ) ;
		for( i=0 ; i < 3 ; i++ )
			print_indent( 2, "Node %u: %.18lf %.18lf\n", i, nodes[i]->x,
				nodes[i]->z ) ;
		//Print out the edge-mid points
		print_indent( 1, "Edge mid-points:\n" ) ;
		for( i=0 ; i < 3 ; i++ ){
			get_edge_mid_point( el->edges[i], midPoint ) ;
			print_indent( 2, "Point %u: %.18lf %.18lf\n", i, midPoint[0],
				midPoint[1] ) ;
		}
		//Now print out the factors to be used in the calculation of the
		//residual and the Jacobian matrix
		print_indent( 1, "Z-Factors:\n" ) ;
		for( i=0 ; i < 3 ; i++ )
			print_indent( 2, "Edge %u: %.18lf\n", i, el->zfacts[i] ) ;
	}
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
}

//Tests that a bounding box of an element is correctly returned
void test_get_el_bnd_box( Params *params, REData *data )
{
	Grid grid ; //A grid from which to select elements
	const Element *el ; //An element on the grid
	unsigned int elInds[] = { 5, 12, 34 } ;
	unsigned int i ; //Loop counter
	BndBox bounds ;
	
	//Initialise pointers in the data structures to NULL
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	
	//Select some elements to look at
	for( i=0 ; i < 3 ; i++ ){
		//Get the current element on the grid
		el = &(grid.els[ elInds[i] ]) ;
		printf( "Element %u:\n", elInds[i] ) ;
		write_el_nodes_to_console( el ) ;
		//Get the bounding box
		get_el_bnd_box( el, &bounds ) ;
		print_indent( 1, "Bounds: %.10lf, %.10lf, %.10lf, %.10lf\n",
			bounds.left, bounds.bottom, bounds.right, bounds.top ) ;
		printf( "\n" ) ;
	}
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
}

//Tests that the soil types are allocated correctly. I am working under the
//assumption that the soil geometry is fully resolved by all grids
void test_set_soil_type( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	Grid grid ; //The grid on which the soils are to be set up
	double *soilType ; //Function representing the soil types
	
	//Set the pointers in the data structures to NULL initially
	set_grid_to_null( &grid ) ;
	
	//Read in the grid data from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	//Set the soil types
	set_soil_type( params, data, &grid ) ;
	
	//Set the appropriate memory of the soil types
	soilType = (double*)calloc( grid.numElements, sizeof( double ) ) ;
	//Set the soil type function
	for( i=0 ; i < grid.numElements ; i++ )
		soilType[i] = (double)grid.els[i].soilType ;
		
	//Now write the soil type out to file
	write_element_func_paraview_format( "/tmp/soils.vtk", &grid, soilType ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free( soilType ) ;
}

//Tests that the hierarchy of variables required to solve a Richards equation is
//set up properly
void test_setup_re_vars_hierarchy( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	FASCtxt ctxt ; //Context storing information required for an FAS solve of
				   //the Richards equation
	const Grid *grid ;
	const REApproxVars *vars ;
				   
	//Set up the hierarchy of grids
	ctxt.gridHierarchy = read_grid_hierarchy( params, data ) ;
	
	//Set up only the hierarchy of approximation variables
	ctxt.reHierarchy = setup_re_vars_hierarchy( params, ctxt.gridHierarchy ) ;
	
	//Write out some information to show that the hierarchy has been set up
	//properly
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		//All the pointers should be set up to zero, so we check that this is
		//the case
		printf( "Level %u:\n", i ) ;
		grid = ctxt.gridHierarchy[i] ;
		vars = ctxt.reHierarchy[i] ;
		print_indent( 1, "lM[0]: %.5lf\tlM[%u]: %.5lf\n", vars->lM[0],
			grid->numEdges-1, vars->lM[ grid->numEdges - 1 ] ) ;
		print_indent( 1, "press[0]: %.5lf\tpress[%u]: %.5lf\n",
			vars->press[0], grid->numElements-1,
			vars->press[ grid->numElements-1 ] ) ;
		print_indent( 1, "prevTheta[0]: %.5lf\tprevTheta[%u]: %.5lf\n",
			vars->prevTheta[0], grid->numElements-1,
			vars->prevTheta[ grid->numElements-1 ] ) ;
		print_indent( 1, "rhs[0]: %.5lf\trhs[%u]: %.5lf\n",
			vars->rhs[0], grid->numUnknowns-1,
			vars->rhs[ grid->numUnknowns-1 ] ) ;
		print_indent( 1, "resid[0]: %.5lf\tresid[%u]: %.5lf\n",
			vars->resid[0], grid->numUnknowns-1,
			vars->resid[ grid->numUnknowns-1 ] ) ;
		printf( "\n" ) ;
	}
	
	//Free the memory assigned on the heap
	ctxt.gridHierarchy = free_grid_hierarchy( params, ctxt.gridHierarchy ) ;
	ctxt.reHierarchy = free_re_vars_hierarchy( params, ctxt.reHierarchy ) ;
}

//Tests that the final rows of a sparse matrix structure can be deleted
void test_delete_sparse_matrix_rows( Params *params, REData *data )
{
	unsigned int i ; //Loop counter
	unsigned int ind ; //An index into a matrix
	SparseMatrix fOp ; //A matrix to delete rows from
	
	//Set the matrices to NULL initially
	set_sparse_mat_to_null( &fOp ) ;
	
	//Explicitly construct the fine grid operator
	fOp.numRows = 16 ;
	fOp.numCols = 16 ;
	fOp.numNonZeros = 3*(fOp.numRows-2) + 4 ;
	fOp.globalEntry = (double*)calloc( fOp.numNonZeros, sizeof( double ) ) ;
	fOp.colNums = (unsigned int*)malloc( fOp.numNonZeros *
		sizeof( unsigned int ) ) ;
	fOp.rowStartInds = (unsigned int*)malloc( (fOp.numRows+1) *
		sizeof( unsigned int ) ) ;
		
	//Set the row start indices
	fOp.rowStartInds[0] = 0 ;
	fOp.rowStartInds[1] = 2 ;
	for( i=2 ; i < fOp.numRows ; i++ )
		fOp.rowStartInds[i] = fOp.rowStartInds[i-1] + 3 ;
	fOp.rowStartInds[fOp.numRows] = fOp.numNonZeros ;
	
	//For the first and last rows set the columns and the values
	fOp.globalEntry[0] = 2 ;
	fOp.colNums[0] = 0 ;
	fOp.globalEntry[1] = -1 ;
	fOp.colNums[1] = 1 ;
	ind = fOp.rowStartInds[fOp.numRows-1] ;
	fOp.globalEntry[ind] = 2 ;
	fOp.colNums[ind] = fOp.numRows-1 ;
	fOp.globalEntry[ind+1] = -1 ;
	fOp.colNums[ind+1] = fOp.numRows-2 ;
	
	//Set the column numbers and entries in the remaining rows
	for( i=1 ; i < fOp.numRows-1 ; i++ ){
		ind = fOp.rowStartInds[i] ;
		fOp.globalEntry[ind] = 2 ;
		fOp.colNums[ind++] = i ;
		fOp.globalEntry[ind] = -1 ;
		fOp.colNums[ind++] = i-1 ;
		fOp.globalEntry[ind] = -1 ;
		fOp.colNums[ind] = i+1 ;
	}
	
	//Print the matrix to file
	write_sparse_matrix_to_file( "/tmp/fullMat.txt", &fOp ) ;
	//Now delete the last 3 rows
	delete_sparse_matrix_last_n_rows( &fOp, 3 ) ;
	
	//Write the matrix to file
	write_sparse_matrix_to_file( "/tmp/trimMat.txt", &fOp ) ;
	
	//Free the memory assigned on the heap
	free_sparse_mat( &fOp ) ;
}

//Tests that the prolongation of a function from a coarse to a fine grid and the
//restriction from a fine to a coarse grid works when using the operators for
//an FAS iteration
void test_fas_prolong_restrict( Params *params )
{
	unsigned int i ; //Loop counter
	Interpolation fInterp ; //Matrices representing the interpolation operators
							//on a fine grid
	Interpolation cInterp ; //Matrices representing the interpolation operators
							//on a coarse grid
	double *fFunc ; //A function to restrict from the fine to the coarse grid
	double *cFunc ; //The function that is restricted from the fine grid
	Grid fGrid ; //A fine grid
	Grid cGrid ; //A coarse grid
	
	const Edge *edge ; //An edge on the grid
	double midPoint[2] ; //The coordinate at the mid-point of an edge
	
	//Set the grids to NULL initially
	set_grid_to_null( &fGrid ) ;
	set_grid_to_null( &cGrid ) ;
	//Set the interpolation operators to NULL initially
	set_interp_ops_to_null( &fInterp ) ;
	set_interp_ops_to_null( &cInterp ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &fGrid ) ;
	read_grid_from_file( gridFiles[ params->fGridLevel -1 ], &cGrid ) ;
	//Set the coarse grid edges that are the parents of the fine grid edges
	set_edge_parents( &cGrid, &fGrid ) ;
	
	set_output_colour( "red" ) ;
	write_grid_info_to_console( "Fine grid:", &fGrid ) ;
	printf( "\n" ) ;
	set_output_colour( "blue" ) ;
	write_grid_info_to_console( "Coarse grid:", &cGrid ) ;
	reset_output_style() ;
	
	//Set up the restriction operator on the grid
	set_fas_restrict_prolong_mat_on_grid( &fGrid, &cGrid, &fInterp, &cInterp ) ;
	
	printf( "Size of the restriction matrix: %u x %u\n",
		fInterp.r.numRows, fInterp.r.numCols ) ;
	printf( "Size of the prolongation matrix: %u x %u\n",
		cInterp.p.numRows, cInterp.p.numCols ) ;
	
	//Now that I have got a restriction operator I need a function to restrict
	//Set the appropriate memory for the functions
	fFunc = (double*)calloc( fGrid.numEdges, sizeof( double ) ) ;
	cFunc = (double*)calloc( cGrid.numEdges, sizeof( double ) ) ;
	
	//Loop through the edges on the fine grid
	for( i=0 ; i < fGrid.numEdges ; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(fGrid.edges[ fGrid.mapU2G[ i ] ]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		//Calculate the value of the function at the mid-point of the edge
		fFunc[i] = sin( PI * midPoint[0] / 200.0 ) *
			sin( PI * midPoint[1] / 200.0 ) - 0.1 ;
	}
	printf( "Fine grid function created\n" ) ;
	
	//Set the boundary value in the coarse approximation
	for( i=cGrid.numUnknowns ; i < cGrid.numEdges; i++ )
		cFunc[i] = -0.1 ;
	
	//Now that the function is set up we restrict it to the coarse grid
	restrict_approx( &fInterp, fFunc, cFunc ) ;
	printf( "Fine grid function restricted to coarse grid\n" ) ;
	
	//We write out the fine and coarse functions to file to compare them
	write_edge_func_paraview_format( "/tmp/testRFine.vtk", &fGrid,
		fFunc ) ;
	printf( "Fine grid function written to file /tmp/testRFine.vtk\n" ) ;
	write_edge_func_paraview_format( "/tmp/testRCoarse.vtk", &cGrid,
		cFunc ) ;
	printf( "Restricted coarse grid function written to file " ) ;
	printf( "/tmp/testRCoarse.vtk\n" ) ;
	
	//Now perform the prolongation the other way around
	//Loop through the unknown edges on the coarse grid
	for( i=0 ; i < cGrid.numEdges ; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(cGrid.edges[ cGrid.mapU2G[ i ] ]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		//Calculate the value of the function at the mid-point of the edge
		cFunc[i] = sin( PI * midPoint[0] / 200.0 ) *
			sin( PI * midPoint[1] / 200.0 ) - 0.1 ;
	}
	
	printf( "\nCoarse grid function created\n" ) ;
	
	//Now that the function is set up properly prolongate it to the fine grid
	prolong_approx( &cInterp, cFunc, fFunc ) ;
	printf( "Coarse grid function interpolated to fine grid\n" ) ;
	
	//Write out the coarse and fine functions to file to compare them
	write_edge_func_paraview_format( "/tmp/testPCoarse.vtk", &cGrid,
		cFunc ) ;
	printf( "Coarse grid function written to file /tmp/testPCoarse.vtk\n" ) ;
	write_edge_func_paraview_format( "/tmp/testPFine.vtk", &fGrid,
		fFunc ) ;
	printf( "Interpolated fine grid function written to file " ) ;
	printf( "/tmp/testPFine.vtk\n" ) ;
	
	//Free the memory assigned on the heap
	free_grid( &fGrid ) ;
	free_grid( &cGrid ) ;
	free_interp_ops( &fInterp ) ;
	free_interp_ops( &cInterp ) ;
	free( fFunc ) ;
	free( cFunc ) ;
}

//Tests that the calculation of the diagonals of the Jacobian matrix is
//performed as expected
void test_calc_jac_diags( Params *params, REData *data )
{
	unsigned int i, elCnt ; //Loop counters
	Grid grid ; //A grid on which to calculate the Jacobian diagonals
	double midPoint[2] ; //The mid-point of an edge on the grid
	const Edge *edge ; //An edge on the grid
	const Element *el ; //An element on the grid
	SparseMatrix jac ; //The Jacobian matrix. I know that this is calculated
					   //correctly so that I can verify whether the diagonals
					   //I calculate are from the Jacobian matrix
	double *jacDiags ; //The diagonals of the Jacobian matrix
	double *oldJacDiags ; //The diagonals of the old Jacobian matrix
	REApproxVars reVars ; //Variables required for the nonlinear solve of a
						  //Richards equation
	double avLM ; //The average value of the Lagrange multipliers on an element
	
	//Set the pointers in the data structures to NULL initially
	set_grid_to_null( &grid ) ;
	set_sparse_mat_to_null( &jac ) ;
	set_re_vars_to_null( &reVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &grid ) ;
	//Set the soil type on the current grid
	set_soil_type( params, data, &grid ) ;
	
	//Set the appropriate memory for the matrix
	set_sparse_op_structure( &grid, &jac ) ;
	
	//set the appropriate memory for the diagonals of the Jacobian matrix
	jacDiags = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	oldJacDiags = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Set up an approximation on the current grid
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	
	//Set a non-trivial approximation
	for( i=0 ; i < grid.numEdges ; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(grid.edges[i]) ;
		//Get the edge mid-point
		get_edge_mid_point( edge, midPoint ) ;
		
		//Set the value of the Lagrange multiplier
		reVars.lM[edge->id] = -20 * sin( PI * midPoint[0] ) *
			sin( PI * midPoint[1] ) - 0.1 ;
	}
	
	//Set the value of the pressure to be the average of the Lagrange
	//multipliers
	for( elCnt=0 ; elCnt < grid.numElements ; elCnt++ ){
		//Get a handle to the current element on the grid
		el = &(grid.els[elCnt]) ;
		//Calculate the average of the Lagrange multipliers on the element
		avLM = 0.0 ;
		//Loop through the edges on the element
		for( i=0 ; i < 3 ; i++ )
			avLM += reVars.lM[ el->edges[i]->id ] ;
		avLM /= 3.0 ;
		reVars.press[elCnt] = avLM ;
		reVars.prevTheta[elCnt] = calc_theta( data, el->soilType, avLM ) ;
	}
	
	//Recover the actual value of the pressure
	recover_pressure( params, data, &grid, &reVars ) ;
	
	//Calculate the Jacobian matrix
	calc_richards_jacobian( params, &grid, data, &reVars, &jac ) ;
	//Store the diagonals of the matrix in a vector
	for( i=0 ; i < grid.numUnknowns ; i++ )
		oldJacDiags[i] = jac.globalEntry[ jac.rowStartInds[i] ] ;
	
	//Calculate the diagonals of the Jacobian matrix
	calc_richards_jacobian_diags( params, &grid, data, &reVars, jacDiags ) ;
	
	//Print out the maximum values and the maximum difference in values to
	//console
	printf( "Maximum value in the diagonals of the calculated Jacobian " ) ;
	printf( "matrix: %.18lf\n",
		dvec_infty_norm( oldJacDiags, grid.numUnknowns ) ) ;
	printf( "Maximum value in the caluclated diagonals: %.18lf\n",
		dvec_infty_norm( jacDiags, grid.numUnknowns ) ) ;
	printf( "Maximum difference in the values calculated: %.18lf\n",
		dvec_diff_infty_norm( oldJacDiags, jacDiags, grid.numUnknowns ) ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_sparse_mat( &jac ) ;
	free_re_vars( &reVars ) ;
	free( jacDiags ) ;
	free( oldJacDiags ) ;
}

//Tests that the residual and the Jacobian diagonals calculated together are
//as expected
void test_calc_richards_resid_and_jac_diags( Params *params, REData *data )
{
	unsigned int i, elCnt ; //Loop counters
	Grid grid ; //A grid on which to calculate the residual and the Jacobian
				//diagonals
	const Edge *edge ; //An edge on the grid
	double midPoint[2] ; //The mid-point of an edge
	const Element *el ; //An element on the grid
	REApproxVars reVars ; //Variables required to solve a Richards equation on
						  //a grid
	double *oldJacDiags, *jacDiags ; //Diagonals of the Jacobian matrix
	double *oldResid ; //The residual in approximation calculated on its own
	double avLM ; //The average value of the Lagrange multipliers on an element
	
	//Set the pointers in data structures to NULL initially
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &reVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	//Set the soil on the grid
	set_soil_type( params, data, &grid ) ;
	
	//Set the variables required for the Richards equation on the grid
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	
	//Set the appropriate memory for the arrays to be used
	oldJacDiags = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	jacDiags = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	oldResid = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Set the approximation for the Lagrange multipliers
	for( i=0 ; i < grid.numEdges; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(grid.edges[i]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		
		//Set the value of the Lagrange multiplier to be something non-trivial
		reVars.lM[ edge->id ] = -20 * sin( PI * midPoint[0] ) *
			sin( PI * midPoint[1] ) - 5.0 ;
	}
	
	//Set the value of the pressure to be the average of the Lagrange
	//multipliers
	for( elCnt = 0 ; elCnt < grid.numElements ; elCnt++ ){
		el = &(grid.els[elCnt]) ;
		//Calculate the average value of the Lagrange multiplier
		avLM = 0.0 ;
		for( i=0 ; i < 3 ; i++ )
			avLM += reVars.lM[ el->edges[i]->id ] ;
		avLM /= 3.0 ;
		reVars.press[elCnt] = avLM ;
		reVars.prevTheta[elCnt] = calc_theta( data, el->soilType, avLM ) ;
	}
	
	//Recover the pressure
	recover_pressure( params, data, &grid, &reVars ) ;
	
	//Calculate the residual
	calc_richards_resid( params, &grid, data, &reVars ) ;
	//Copy the residual into the old residual
	copy_dvec( grid.numUnknowns, reVars.resid, oldResid ) ;
	
	//Calculate the diagonals of the Jacobian matrix on their own
	calc_richards_jacobian_diags( params, &grid, data, &reVars, oldJacDiags ) ;
	
	//Calculate the residual and the Jacobian diagonals together
	calc_richards_resid_and_jacobian_diags( params, &grid, data, &reVars,
		jacDiags ) ;
		
	//Write out the maximum values in the residual and the maximum difference
	printf( "Maximum value in the residual calculated on its own: %.18lf\n",
		dvec_infty_norm( oldResid, grid.numUnknowns ) ) ;
	printf( "Maximum value in residual calculated with Jacobian diagonals: " ) ;
	printf( "%.18lf\n", dvec_infty_norm( reVars.resid, grid.numUnknowns ) ) ;
	printf( "Maximum difference in residual values: %.18lf\n",
		dvec_diff_infty_norm( oldResid, reVars.resid, grid.numUnknowns ) ) ;
	printf( "\n" ) ;
	
	printf( "Maximum value in the Jacobian diagonals calculated on their " ) ;
	printf( "own: %.18lf\n", dvec_infty_norm( oldJacDiags,
		grid.numUnknowns ) ) ;
	printf( "Maximum value in Jacobian diagonals calculated with residual " ) ;
	printf( "%.18lf\n", dvec_infty_norm( jacDiags, grid.numUnknowns ) ) ;
	printf( "Maximum difference in Jacobian diagonals: %.18lf\n",
		dvec_diff_infty_norm( oldJacDiags, jacDiags, grid.numUnknowns ) ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &reVars ) ;
	free( oldJacDiags ) ;
	free( jacDiags ) ;
	free( oldResid ) ;
}

//Tests that the nonlinear smoother converges
void test_nonlinear_smooth( Params *params, REData *data )
{
	unsigned int tCnt, sCnt ; //Loop counters
	Grid grid ; //A grid on which to solve the Richards equation
	REApproxVars vars ; //Variables required to approximate the Richards
						//equation
	double *jacDiags ; //Diagonals of the Jacobian matrix
	double resid, origResid ; //Current and original norm of the residual
	unsigned int numSmooths = 10 ;
	char outFName[20] ; //The name of a file to write output to
						
	//Set the pointers in structures to NULL initially
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &vars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[params->cGridLevel], &grid ) ;
	//Set the soil on the grid
	set_soil_type( params, data, &grid ) ;
	
	//Set the memory for the variables used in the approximation of the Richards
	//equation
	setup_re_vars_on_grid( params, &grid, &vars ) ;
	
	//Set the memory required for the diagonals of the Jacobian matrix
	jacDiags = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Set up the approximation on the grid
	set_richards_initial_data( params, data, &grid, &vars ) ;
	
	//Perform the desired number of time steps
	for( tCnt = 0 ; tCnt < params->numTSteps ; tCnt++ ){
		//Recover the pressure and calculate the residual
		recover_pressure( params, data, &grid, &vars ) ;
		//Calculate the residual and Jacobian diagonals
		calc_richards_resid_and_jacobian_diags( params, &grid, data, &vars,
			jacDiags ) ;
			
		//Write the pressure and the Lagrange multipliers out to file
		sprintf( outFName, "/tmp/smoothLM%u.vtk", tCnt ) ;
		write_edge_func_paraview_format( outFName, &grid, vars.lM ) ;
		sprintf( outFName, "/tmp/smoothPress%u.vtk", tCnt ) ;
		write_element_func_paraview_format( outFName, &grid, vars.press ) ;
			
		//Calculate the norm of the residual
		origResid = resid = discrete_l2_norm( grid.numUnknowns, vars.resid ) ;
		printf( "Time step: %u\n", tCnt ) ;
		
		sCnt = 0 ;
		//Solve the system at the current time step
		while( resid / origResid > 1e-6 && resid > SOLVED_TOL ){
			print_indent( 1, "Smooth: %u\tResidual Norm: %.18lf\n", sCnt,
				resid ) ;
			//Perform some smooths
			nonlin_smooth( params, data, &grid, numSmooths, jacDiags, &vars ) ;
			sCnt += numSmooths ;
			//Calculate the residual
			recover_pressure( params, data, &grid, &vars ) ;
			calc_richards_resid_and_jacobian_diags( params, &grid, data, &vars,
				jacDiags ) ;
			resid = discrete_l2_norm( grid.numUnknowns, vars.resid ) ;
		}
		
		if( tCnt != params->numTSteps-1 )
			set_richards_prev_theta( &grid, data, &vars ) ;
	}
	
	//Write the pressure and the Lagrange multipliers out to file
	sprintf( outFName, "/tmp/smoothLM%u.vtk", tCnt ) ;
	write_edge_func_paraview_format( outFName, &grid, vars.lM ) ;
	sprintf( outFName, "/tmp/smoothPress%u.vtk", tCnt ) ;
	write_element_func_paraview_format( outFName, &grid, vars.press ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &vars ) ;
	free( jacDiags ) ;
}

//Tests that the nonlinear smoother converges
void test_nonlinear_smooth_to_converge( Params *params, REData *data )
{
	unsigned int tCnt ; //Loop counters
	Grid grid ; //A grid on which to solve the Richards equation
	REApproxVars vars ; //Variables required to approximate the Richards
						//equation
	double *jacDiags ; //Diagonals of the Jacobian matrix
	double resid ; //Current norm of the residual
	char outFName[20] ; //The name of a file to write output to
						
	//Set the pointers in structures to NULL initially
	set_grid_to_null( &grid ) ;
	set_re_vars_to_null( &vars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[params->cGridLevel], &grid ) ;
	//Set the soil on the grid
	set_soil_type( params, data, &grid ) ;
	
	//Set the memory for the variables used in the approximation of the Richards
	//equation
	setup_re_vars_on_grid( params, &grid, &vars ) ;
	
	//Set the memory required for the diagonals of the Jacobian matrix
	jacDiags = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Set up the approximation on the grid
	set_richards_initial_data( params, data, &grid, &vars ) ;
	
	//Perform the desired number of time steps
	for( tCnt = 0 ; tCnt < params->numTSteps ; tCnt++ ){
		//Recover the pressure and calculate the residual
		recover_pressure( params, data, &grid, &vars ) ;
		//Calculate the residual and Jacobian diagonals
		calc_richards_resid_and_jacobian_diags( params, &grid, data, &vars,
			jacDiags ) ;
			
		//Write the pressure and the Lagrange multipliers out to file
		sprintf( outFName, "/tmp/smoothLM%u.vtk", tCnt ) ;
		write_edge_func_paraview_format( outFName, &grid, vars.lM ) ;
		sprintf( outFName, "/tmp/smoothPress%u.vtk", tCnt ) ;
		write_element_func_paraview_format( outFName, &grid, vars.press ) ;
			
		//Calculate the norm of the residual
		resid = discrete_l2_norm( grid.numUnknowns, vars.resid ) ;
		printf( "Time step: %u\n", tCnt ) ;
		print_indent( 1, "Initial residual: %.18lf\n", resid ) ;
		
		nonlin_smooth_to_converge( params, data, &grid, jacDiags, &vars ) ;
		
		resid = discrete_l2_norm( grid.numUnknowns, vars.resid ) ;
		print_indent( 1, "Final residual: %.18lf\n", resid ) ;
		
		if( tCnt != params->numTSteps-1 )
			set_richards_prev_theta( &grid, data, &vars ) ;
	}
	
	//Write the pressure and the Lagrange multipliers out to file
	sprintf( outFName, "/tmp/smoothLM%u.vtk", tCnt ) ;
	write_edge_func_paraview_format( outFName, &grid, vars.lM ) ;
	sprintf( outFName, "/tmp/smoothPress%u.vtk", tCnt ) ;
	write_element_func_paraview_format( outFName, &grid, vars.press ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_re_vars( &vars ) ;
	free( jacDiags ) ;
}

//Tests that a piecewise constant function can be restricted from a fine to a
//coarse grid
void test_restrict_pc_func( Params *params, REData *data )
{
	unsigned int elCnt, i ; //Loop counters
	Grid **gridHierarchy ; //A hierarchy of grids
	const Grid *fGrid ; //The finest grid in the hierarchy
	const Element *el ; //An element on a grid
	double cog[2] ; //The centre of gravity of an element
	double **pcFuncs ; //Piecewise constant functions on a grid
	double *pcFunc ; //A piecewise constant function on a grid
	char outFName[20] ; //The name of a file to write output to
	
	//Read in the grid Hierarchy
	gridHierarchy = read_grid_hierarchy( params, data ) ;
	
	//Set the required memory for the functions
	pcFuncs = (double**)calloc( params->fGridLevel+1, sizeof( double* ) ) ;
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ ){
		pcFuncs[i] = (double*)calloc( gridHierarchy[i]->numElements,
			sizeof( double ) ) ;
	}
	
	//Get a handle the finest grid in the hierarchy
	fGrid = gridHierarchy[params->fGridLevel] ;
	pcFunc = pcFuncs[params->fGridLevel] ;
	
	//Loop through the elements on the fine grid
	for( elCnt = 0 ; elCnt < fGrid->numElements ; elCnt++ ){
		//Get a handle to the current element on the fine grid
		el = &(fGrid->els[elCnt]) ;
		//Get the centre of gravity of the element
		el_center_of_gravity( el, cog ) ;
		//Set the value of the function on the fine grid
		pcFunc[elCnt] = 20 * sin( PI * cog[0] / 200 ) *
			sin( PI * cog[1] / 200 ) - 10.0 ;
	}
	
	//Write the function out to file
	sprintf( outFName, "/tmp/pcFunc%u.vtk", params->fGridLevel ) ;
	write_element_func_paraview_format( outFName, fGrid, pcFunc ) ;
	
	//Now restrict the function from one grid to the other
	for( i=params->fGridLevel ; i > params->cGridLevel ; i-- ){
		//Restrict the function to the next coarsest grid
		restrict_const_el_func( gridHierarchy[i], gridHierarchy[i-1],
			pcFuncs[i], pcFuncs[i-1] ) ;
		//Write the function out to file
		sprintf( outFName, "/tmp/pcFunc%u.vtk", i-1 ) ;
		write_element_func_paraview_format( outFName, gridHierarchy[i-1],
			pcFuncs[i-1] ) ;
	}
	
	//Free the memory assigned on the heap
	gridHierarchy = free_grid_hierarchy( params, gridHierarchy ) ;
	for( i=params->cGridLevel ; i <= params->fGridLevel ; i++ )
		free( pcFuncs[i] ) ;
	free( pcFuncs ) ;
}

//Tests that the perturbed right hand side for the FAS iteration is calculated
//correctly on a coarse grid
void test_fas_coarse_rhs_calc( Params *params, REData *data )
{
	unsigned int i, elCnt ; //Loop counters
	Grid fGrid, cGrid ; //A fine and coarse grid
	const Element *el ; //An element on the grid
	const Edge *edge ; //An edge on the grid
	double midPoint[2] ; //The mid-point of an edge
	REApproxVars fVars, cVars ; //Variables required for the solution of the
				//Richards equation on a fine and coarse grid
	Interpolation fInterp, cInterp ; //Interpolation operators on the fine and
				//coarse grid
	double avLM ; //The average of the Lagrange multipliers on an element
				
	//Set the pointers in the structures to NULL initially
	set_grid_to_null( &fGrid ) ;
	set_grid_to_null( &cGrid ) ;
	set_re_vars_to_null( &fVars ) ;
	set_re_vars_to_null( &cVars ) ;
	set_interp_ops_to_null( &fInterp ) ;
	set_interp_ops_to_null( &cInterp ) ;
	
	//Read the grids in from file
	read_grid_from_file( gridFiles[ params->fGridLevel ], &fGrid ) ;
	set_soil_type( params, data, &fGrid ) ;
	read_grid_from_file( gridFiles[ params->fGridLevel-1], &cGrid ) ;
	set_soil_type( params, data, &cGrid ) ;
	//Set up the connections between the coarse and fine grids
	set_edge_parents( &cGrid, &fGrid ) ;
	
	//Set the restriction operators on the grid
	set_fas_restrict_prolong_mat_on_grid( &fGrid, &cGrid, &fInterp, &cInterp ) ;
	
	//Set the memory for the approximations on the fine and coarse grids
	setup_re_vars_on_grid( params, &fGrid, &fVars ) ;
	setup_re_vars_on_grid( params, &cGrid, &cVars ) ;
	
	//Set the Lagrange multipliers on the fine grid
	for( i=0 ; i < fGrid.numEdges ; i++ ){
		//Get a handle to the current edge on the grid
		edge = &(fGrid.edges[i]) ;
		//Get the mid-point of the edge
		get_edge_mid_point( edge, midPoint ) ;
		//Set the value in the Lagrange multiplier
		fVars.lM[edge->id] = 20 * sin( PI * midPoint[0] / 200 ) *
			sin( PI * midPoint[1] / 200 ) - 10.0 ;
	}
	
	//Set the value of the pressure to be the average of the Lagrange
	//multipliers on an element
	for( elCnt=0 ; elCnt < fGrid.numElements ; elCnt++ ){
		//Get a handle to the current element
		el = &(fGrid.els[elCnt]) ;
		avLM = 0.0 ;
		//Loop over the edges of the element
		for( i=0 ; i < 3 ; i++ )
			avLM += fVars.lM[ el->edges[i]->id ] ;
		avLM /= 3.0 ;
		//Set the approximation to the pressure
		fVars.press[elCnt] = avLM ;
		fVars.prevTheta[elCnt] = calc_theta( data, el->soilType, avLM ) ;
	}
	
	//Recover the actual value of the pressure
	recover_pressure( params, data, &fGrid, &fVars ) ;
	
	//Calculate the residual
	calc_richards_resid( params, &fGrid, data, &fVars ) ;
	
	//Write out the fine grid functions to file
	write_edge_func_paraview_format( "/tmp/testLM0.vtk", &fGrid, fVars.lM ) ;
	write_interior_edge_func_paraview_format( "/tmp/testResid0.vtk", &fGrid,
		fVars.resid ) ;
	write_interior_edge_func_paraview_format( "/tmp/testRhs0.vtk", &fGrid,
		fVars.rhs ) ;
	write_element_func_paraview_format( "/tmp/testPress0.vtk", &fGrid,
		fVars.press ) ;
	write_element_func_paraview_format( "/tmp/testPrevTheta0.vtk", &fGrid,
		fVars.prevTheta ) ;
		
	//Set the value on the coarse Dirichlet boundary
	for( i=cGrid.numUnknowns ; i < cGrid.numEdges ; i++ ){
		//Get a handle to the current edge
		edge = &(cGrid.edges[i]) ;
		//Get the edge mid-point
		get_edge_mid_point( edge, midPoint ) ;
		cVars.lM[edge->id] = 20 * sin( PI * midPoint[0] / 200 ) *
			sin( PI * midPoint[1] / 200 ) - 10.0 ;
	}
		
	//Calculate the coarse grid right hand side. This also restricts the
	//approximation of the Lagrange multipliers to the fine grid
	calc_richards_fas_coarse_rhs( params, data, &fInterp, &fGrid, &cGrid,
		&fVars, &cVars ) ;
		
	//Write out the coarse grid functions to file
	write_edge_func_paraview_format( "/tmp/testLM1.vtk", &cGrid, cVars.lM ) ;
	write_interior_edge_func_paraview_format( "/tmp/testResid1.vtk", &cGrid,
		cVars.resid ) ;
	write_interior_edge_func_paraview_format( "/tmp/testRhs1.vtk", &cGrid,
		cVars.rhs ) ;
	write_element_func_paraview_format( "/tmp/testPress1.vtk", &cGrid,
		cVars.press ) ;
	write_element_func_paraview_format( "/tmp/testPrevTheta1.vtk", &cGrid,
		cVars.prevTheta ) ;
	
	//Free the memory assigned on the heap
	free_grid( &fGrid ) ;
	free_grid( &cGrid ) ;
	free_re_vars( &fVars ) ;
	free_re_vars( &cVars ) ;
	free_interp_ops( &fInterp ) ;
	free_interp_ops( &cInterp ) ;
}

//Tests that the FAS multigrid iteration is working as expected
void test_fas_multigrid_iteration( Params *params, REData *data )
{
	if( params->algorithm != ALG_FAS ){
		printf( "FAS iteration must be specified to run test " ) ;
		printf( "test_fas_multigrid_iteration\n" ) ;
		exit( 0 ) ;
	}
	fas_multigrid_solve_write_info( params, data ) ;
}

//Tests that the modified Gram-Schmidt process produces orthogonal vectors
void test_modified_gram_schmidt( Params *params, REData *data )
{
	unsigned int i, j ; //Loop counters
	unsigned int numVecs = 5 ; //The size of the basis
	
	Grid grid ; //The grid on which to set up a problem
	double **basis ; //The basis to make orthonormal
	
	//Set the pointers in structures to zero initially
	set_grid_to_null( &grid ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	//Set the soil type on the grid
	set_soil_type( params, data, &grid ) ;
	
	//Set the memory for the basis
	basis = (double**)calloc( numVecs, sizeof( double* ) ) ;
	for( i=0 ; i < numVecs ; i++ )
		basis[i] = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
		
	//Set the vectors in the basis to be random
	//Seed the random number generator
	srand( time(NULL) ) ;
	for( i=0 ; i < numVecs ; i++ ){
		for( j=0 ; j < grid.numUnknowns ; j++ )
			basis[i][j] = (double)rand() / RAND_MAX ;
	}
		
	//Now orthogonalise the basis
	modified_gram_schmidt( numVecs, grid.numUnknowns, basis ) ;
	
	//Test that the vector is orthogonal to the basis
	for( i=0 ; i < numVecs ; i++ ){
		printf( "Dot product of first basis vector and %uth basis vector: ",
			i+1 ) ;
		printf( " %.18lf\n", discrete_l2_inner_prod( grid.numUnknowns,
			basis[i], basis[0] ) ) ;
	}
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	for( i=0 ; i < numVecs ; i++ )
		free( basis[i] ) ;
	free( basis ) ;
}

//Tests that an upper Hessenberg matrix can be transformed into an upper
//triangular matrix
void test_hess_to_upper( Params *params, REData *data )
{
	unsigned int i, j ; //Loop counters
	unsigned int size = 5 ; //The number of columns in the upper Hessenberg
							//matrix
	
	LeastSqCtxt ctxt ; //A least squares context in which to store different
					   //variables
					   
	//Set the pointers in the data structure to NULL intially
	set_least_squares_ctxt_to_null( &ctxt ) ;
	
	//Set the appropriate memory for the least squares context
	init_least_squares_ctxt( &ctxt ) ;
	
	//Seed the random number generator
	srand( time(NULL) ) ;
	
	ctxt.dim = size+1 ;
	//Set the values in the upper Hessenberg matrix for each row
	for( j=0 ; j < size ; j++ )
		ctxt.hessMat[0][j] = (double)rand() / RAND_MAX ;
	for( i=1 ; i < size+1 ; i++ ){
		for( j=i-1 ; j < size ; j++ )
			ctxt.hessMat[i][j] = (double)rand() / RAND_MAX ;
	}
	
	//Transform the upper Hessenberg matrix into an upper triangular matrix
	hess_to_upper( &ctxt ) ;
	
	//Print out the upper Hessenberg matrix
	printf( "Upper Hessenberg matrix:\n" ) ;
	for( i=0 ; i < size+1 ; i++ ){
		printf( "  " ) ;
		for( j=0 ; j < size ; j++ )
			printf( "%.5lf ", ctxt.hessMat[i][j] ) ;
		printf( "\n" ) ;
	}
	
	//Print out the upper triangular matrix
	printf( "\nUpper triangular matrix:\n" ) ;
	for( i=0 ; i < size ; i++ ){
		printf( "  " ) ;
		for( j=0 ; j < size ; j++ )
			printf( "%.5lf ", ctxt.upTMat[i][j] ) ;
		printf( "\n" ) ;
	}
	
	//Free the memory assigned on the heap
	free_least_squares_ctxt( &ctxt ) ;
}

//Tests that the back-substitution works
void test_back_substitute( Params *params, REData *data )
{
	unsigned int i, j ; //Loop counters
	double matCnt = 1.0 ;
	unsigned int size = 5 ; //The number of columns in the upper Hessenberg
							//matrix
	double *res ; //The result of multiplying the upper triangular system
				  //times the solution of the back-substitution
	
	LeastSqCtxt ctxt ; //A least squares context in which to store different
					   //variables
					   
	//Set the pointers in the data structure to NULL intially
	set_least_squares_ctxt_to_null( &ctxt ) ;
	
	//Set the appropriate memory for the least squares context
	init_least_squares_ctxt( &ctxt ) ;
	
	//Seed the random number generator
	srand( time(NULL) ) ;
	
	ctxt.dim = size+1 ;
	//Set the values in the upper triangular matrix
	for( i=0 ; i < size ; i++ ){
		for( j=i ; j < size ; j++ ){
			ctxt.upTMat[i][j] = matCnt++ ;
		}
	}
	
	//Print out the upper triangular matrix
	printf( "\nUpper triangular matrix:\n" ) ;
	for( i=0 ; i < size ; i++ ){
		printf( "  " ) ;
		for( j=0 ; j < size ; j++ )
			printf( "%.5lf ", ctxt.upTMat[i][j] ) ;
		printf( "\n" ) ;
	}
	
	//Set up the right hand side
	for( i=0 ; i < size ; i++ )
		ctxt.rhs[i] = (double)(i+1) ;
		
	printf( "\nRight hand side:\n" ) ;
	printf( "  " ) ;
	for( i=0 ; i < size ; i++ )
		printf( "%.5lf ", ctxt.rhs[i] ) ;
	printf( "\n" ) ;
	
	///Perform the back-substitution
	back_substitute( &ctxt ) ;
	
	//Print out the solution
	printf( "\nSolution:\n" ) ;
	printf( "  " ) ;
	for( i=0 ; i < size ; i++ )
		printf( "%.5lf ", ctxt.rhs[i] ) ;
	printf( "\n" ) ;
	
	//Perform the matrix-vector product of the solution times the upper
	//triangular matrix
	res = (double*)calloc( size, sizeof( double ) ) ;
	for( i=0 ; i < size ; i++ ){
		for( j=i ; j < size; j++ ){
			res[i] += ctxt.upTMat[i][j] * ctxt.rhs[j] ;
		}
	}
	
	//Print out the result of the matrix-vector multiplication
	printf( "\nMatrix times solution:\n" ) ;
	printf( "  " ) ;
	for( i=0 ; i < size ; i++ )
		printf( "%.5lf ", res[i] ) ;
	printf( "\n" ) ;
	
	//Free the memory assigned on the heap
	free_least_squares_ctxt( &ctxt ) ;
	free( res ) ;
}

//Tests that GMRES as a linear iteration will converge
void test_gmres_solve( Params *params, REData *data )
{
	Grid grid ; //A grid on which to operate
	REApproxVars reVars ; //Variables associated with the approximation on the
						  //coarse grid
	GMRESCtxt ctxt ; //Information required to perform a GMRES iteration
	
	//Set the pointers in the data structures to NULL initially
	set_grid_to_null( &grid ) ;
	set_gmres_ctxt_to_null( &ctxt ) ;
	set_re_vars_to_null( &reVars ) ;
	
	//Read the grid in from file
	read_grid_from_file( gridFiles[params->fGridLevel], &grid ) ;
	//Set the soil types on the grid
	set_soil_type( params, data, &grid ) ;
	
	//Set the appropriate memory for the nonlinear approximation
	setup_re_vars_on_grid( params, &grid, &reVars ) ;
	
	//Set the memory required for the GMRES iteration
	setup_gmres_ctxt_on_grid( &grid, &ctxt ) ;
	
	//Initialise the nonlinear approximation
	set_richards_initial_data( params, data, &grid, &reVars ) ;
	
	//Now calculate the Jacobian matrix
	calc_richards_resid_and_jacobian( params, &grid, data, &reVars,
		&(ctxt.vars.op) ) ;
		
	//Copy the nonlinear residual into the linear right hand side
	copy_dvec( grid.numUnknowns, reVars.resid, ctxt.vars.rhs ) ;
	
	//Now perform a GMRES iteration
	do{
		gmres_iteration( params, &grid, &ctxt, 4 ) ;
		printf( "Resid norm: %.18lf\n", ctxt.residNorm ) ;
	}while( ctxt.residNorm > SOLVED_TOL ) ;
	
	//Free the memory assigned on the heap
	free_grid( &grid ) ;
	free_gmres_ctxt( &ctxt ) ;
	free_re_vars( &reVars ) ;
}

//Tests that a preconditioned GMRES iteration is performed correctly
void test_prec_gmres_solve( Params *params, REData *data )
{
	NewtonCtxt newtCtxt ; //Variables required to perform a Newton-multilevel
					  //iteration
	const Grid *grid ; //Placeholder for a grid in the hierarchy of grids
	PrecGMRESCtxt *gmresCtxt ;
	
	if( params->linIt != IT_PGMRES ){
		printf( "Set the inner iteration to preconditioned GMRES to run " ) ;
		printf( "test test_prec_gmres_solve\n" ) ;
		exit( 0 ) ;
	}
					
	//Set the pointers in the data structures to NULL initially
	set_newton_ctxt_to_null( &newtCtxt ) ;
	
	//Set the memory required for the newton iteration
	setup_newton_ctxt( params, data, &newtCtxt ) ;
	
	gmresCtxt = (PrecGMRESCtxt*)newtCtxt.linItCtxt ;
	
	//Set a placeholder for the finest grid in the hierarchy
	grid = newtCtxt.grid ;
	set_param_spare_memory( grid->numUnknowns, params ) ;
	
	//Initialise the approximation to the nonlinear system of equations
	set_richards_initial_data( params, data, grid, &(newtCtxt.reVars) ) ;
	
	//Set the data required to solve the linear system of equations associated
	//with the Jacobian
	set_newton_mg_iteration_vars( params, data, &newtCtxt,
		gmresCtxt->gridHierarchy, gmresCtxt->linHierarchy,
		gmresCtxt->interpHierarchy ) ;
	
	//Write the Jacobian to file
//	write_sparse_matrix_to_file( "/tmp/newtJac.txt",
//		&(newtCtxt.linHierarchy[params->fGridLevel]->op) ) ;
	
	//Initialise the approximation and right hand side for the linear system of
	//equations
	init_newt_approx_on_grid( &(newtCtxt.reVars), grid, newtCtxt.linVars ) ;
		
	//Now perform a GMRES iteration
	do{
		preconditioned_gmres_iteration( params, gmresCtxt, 3 ) ;
		printf( "Residual norm: %.18lf\n", gmresCtxt->residNorm ) ;
	}while( gmresCtxt->residNorm > SOLVED_TOL ) ;
	
	//Free the memory assigned on the heap
	free_newton_ctxt( params, &newtCtxt ) ;
}

#endif














