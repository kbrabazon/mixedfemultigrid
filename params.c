#include "params.h"
#include "string.h"
#include "richardsFunctions.h"

/** Populates the Params structure using information read in from file
 *	\param [in] fname	The name of the file to read the parameters from
 *	\param [out] params	The Params structure to populate
 *	\param [out] data	Soil characteristics for the soils used in the current
 *						problem
 */
void read_params_from_file( const char *fname, Params *params, REData *data )
{
	FILE *paramsFile ; //File pointer to the file to read from
	size_t strlen = 100 ; //Maximum length of a line to be read from the file
	char *line ; //A line read from the file
	char *paramName ; //Name of a parameter read from file
	char *paramValue ; //String value of a parameter read from file
	
	//Open the file for reading
	paramsFile = fopen( fname, "r" ) ;
	
	if( paramsFile == NULL ){
		printf( "Unable to open file %s for reading\n", fname ) ;
		exit( 0 ) ;
	}
	
	//Set the pointers to be NULL initially
	params->sparePtr = NULL ;
	params->approxFilePrefix[0] = '\0' ;
	
	//First set up the variable to read into
	line = (char*)calloc( strlen, sizeof( char ) ) ;
	
	//Read the file line by line, until we reach the end of the file
	while( !feof( paramsFile ) ){
		//Read a maximum of 100 characters, or until newline character
		fgets( line, strlen * sizeof( char ), paramsFile ) ;
		
		//Check that the line doesn't start with a hash, and that it isn't empty
		if( *(line) != '#' && *(line) != '\n' ){
			//Split the string about the smi-colon, or a newling
			paramName = strtok( line, ":\n" ) ;
			paramValue = strtok( NULL, ":\n" ) ;
			
			//Update the parameter name with the given parameter value
			set_param_value( params, data, paramName, paramValue ) ;
		}
	}
	
	//Clean up
	//Close the file
	fclose( paramsFile ) ;
	//Free any memory on the heap
	free( line ) ;
}

/** Sets the memory specified by size for an array to be used when required to
 *	avoid the repeated allocation and deallocation of memory
 *	\param [in] size	The amount of memory to set
 *	\param [out] params	The parameters structure for which to set the memory
 */
void set_param_spare_memory( unsigned int size, Params *params )
{
	params->sparePtr = (double*)calloc( size, sizeof( double ) ) ;
}

/** Frees the memory assigned on the heap for the parameters structure passed in
 *	\param [out] params	The parameters structure for which to free the memory
 */
void free_param_memory( Params *params )
{
	if( params->sparePtr != NULL ){
		free( params->sparePtr ) ;
		params->sparePtr = NULL ;
	}
}


/** Updates the Params structure using the parameter name and parameter value
 *	passed in
 *	\param [out] params	The Params structure to update
 *	\param [out] data	Soil characterisitics for the soils used in the current
 *						problem
 *  \param [in] paramName	The string identifier of a field in the Params
 *							structure to update
 *	\param [in] paramValue	String value of the parameter to update. It is not
 *							checked whether the parameter value is in the
 *							correct format. A simple cast is performed to
 *							convert it to the right data type
 */
void set_param_value( Params * params, REData * data,
	const char * paramName, char * paramValue )
{
	if( strcmp( paramName, "TEST_CASE" ) == 0 )
		params->testCase = atoi( paramValue ) ;
	else if( strcmp( paramName, "ALGORITHM" ) == 0 )
		params->algorithm = (enum ALG_TYPE)atoi( paramValue ) ;
	else if( strcmp( paramName, "MAX_ITS" ) == 0 )
		params->maxIts = (unsigned int)atoi( paramValue ) ;
	else if( strcmp( paramName, "FINEST_GRID_LEVEL" ) == 0 )
		params->fGridLevel = (unsigned int)(atoi( paramValue ) - 1) ;
	else if( strcmp( paramName, "COARSEST_GRID_LEVEL" ) == 0 )
		params->cGridLevel = (unsigned int)(atoi( paramValue ) - 1) ;
	else if( strcmp( paramName, "DIR_BND_CONST" ) == 0 )
		params->dirConst = atof( paramValue ) ;
	else if( strcmp( paramName, "APPROX_FILE_PREFIX" ) == 0 ){
		if( paramValue != NULL )
			sprintf( params->approxFilePrefix, "%s", paramValue ) ;
	}
	else if( strcmp( paramName, "PRESMOOTH" ) == 0 )
		params->preSmooth = (unsigned int)atoi( paramValue ) ;
	else if( strcmp( paramName, "POSTSMOOTH" ) == 0 )
		params->postSmooth = (unsigned int)atoi( paramValue ) ;
	else if( strcmp( paramName, "SMOOTH_METHOD" ) == 0 )
		params->smoother = (enum SMOOTH_TYPE)atoi( paramValue ) ;
	else if( strcmp( paramName, "SMOOTH_WEIGHT" ) == 0 )
		params->smoothWeight = atof( paramValue ) ;
	else if( strcmp( paramName, "MG_CYCLES" ) == 0 )
		params->numMGCycles = (unsigned int)atoi( paramValue ) ;
	else if( strcmp( paramName, "NEWT_WEIGHT" ) == 0 )
		params->newtWeight = atof( paramValue ) ;
	else if( strcmp( paramName, "GLOBAL_NEWT" ) == 0 )
		params->globalNewton = (bool)(atoi(paramValue) == 1) ;
	else if( strcmp( paramName, "LIN_IT" ) == 0 )
		params->linIt = (enum LIN_IT)atoi( paramValue ) ;
	else if( strcmp( paramName, "PREC_SYMM" ) == 0 )
		params->precSymm = (bool)(atoi(paramValue) == 1 ) ;
	else if( strcmp( paramName, "NUM_LIN_IT" ) == 0 )
		params->numLinIts = (unsigned int)atoi( paramValue ) ;
	else if( strcmp( paramName, "ADAPT_T_STEP" ) == 0 )
		params->adaptTStep = (bool)(atoi( paramValue ) == 1 ) ;
	else if( strcmp( paramName, "TIME_STEP" ) == 0 )
		params->timeStep = atof( paramValue ) ;
	else if( strcmp( paramName, "START_TIME" ) == 0 )
		params->startTime = atof( paramValue ) ;
	else if( strcmp( paramName, "NUM_TIME_STEPS" ) == 0 )
		params->numTSteps = (unsigned int)atoi( paramValue ) ;
	else if( strcmp( paramName, "MIN_T_STEP" ) == 0 )
		params->minTStep = atof( paramValue ) ;
	else if( strcmp( paramName, "MAX_T_STEP" ) == 0 )
		params->maxTStep = atof( paramValue ) ;
	else if( strcmp( paramName, "NUM_SOILS" ) == 0 ){
		data->numSoils = (unsigned int)atoi( paramValue ) ;
		//Once we know how many different soil types are defined on the grid we
		//assign the appropriate amount of memory for them
		assign_re_data_mem( data ) ;
	}
	else if( strcmp( paramName, "THETA_R" ) == 0 ){
		assert( data->numSoils != 0 ) ;
		assign_dvect_values( data->thetaR, paramValue ) ;
	}
	else if( strcmp( paramName, "THETA_S" ) == 0 ){
		assert( data->numSoils != 0 ) ;
		assign_dvect_values( data->thetaS, paramValue ) ;
	}
	else if( strcmp( paramName, "RE_ALPHA" ) == 0 ){
		assert( data->numSoils != 0 ) ;
		assign_dvect_values( data->alpha, paramValue ) ;
	}
	else if( strcmp( paramName, "RE_N" ) == 0 ){
		assert( data->numSoils != 0 ) ;
		assign_dvect_values( data->n, paramValue ) ;
	}
	else if( strcmp( paramName, "SAT_COND" ) == 0 ){
		assert( data->numSoils != 0 ) ;
		assign_dvect_values( data->sCond, paramValue ) ;
	}
	else if( strcmp( paramName, "P_SAT" ) == 0 ){
		assert( data->numSoils != 0 ) ;
		assign_dvect_values( data->pSat, paramValue ) ;
	}
	else if( strcmp( paramName, "TESTING" ) == 0 )
		params->testing = (bool)(atoi( paramValue ) == 1) ;
	else if( strcmp( paramName, "WRITE_TO_FILE" ) == 0 )
		params->writeToFile = (bool)(atoi( paramValue ) == 1) ;
	else{
		set_output_colour( "red" ) ;
		printf( "No action defined for parameter with name %s\n", paramName ) ;
		reset_output_style() ;
	}
}

/** Converts a character string to a vector of double values. This assumes that
 *	the string contains valid double numbersd separated by commas
 *	\param [out] vect	The vector of doubles to populate
 *	\param [in] values	The string representation of the double vector to
 *						populate
 */
void assign_dvect_values( double * vect, char * values )
{
	char *val ; //Substring of the assigned value
	unsigned int i = 0 ; //Loop counter
	
	//Get the first value in the string
	val = strtok( values, ",\n" ) ;
	
	//Check that a value has been read in and put this in the correct entry in
	//the vector of doubles
	while( val != NULL ){
		vect[i++] = atof( val ) ;
		val = strtok( NULL, ",\n" ) ;
	}
}

/** Prints the values stored in the Params structure
 *	\param [in] params	The params structure to print out
 */
void print_params( const Params *params )
{
	if( params->algorithm == ALG_FAS )
		printf( "Performing FAS\n" ) ;
	else
		printf( "Performing Newton's method\n" ) ;
		
	if( params->smoother == SMOOTH_JAC )
		printf( "Performing Jacobi smoothing\n" ) ;
	else
		printf( "Performing Gauss-Seidel smoothing\n" ) ;
		
	printf( "Linear iteration: " ) ;
	if( params->linIt == IT_LIN_MG )
		printf( "Multigrid\n" ) ;
	else if( params->linIt == IT_PCG )
		printf( "Preconditioned CG\n" ) ;
	else
		printf( "Preconditioned GMRES\n" ) ;
		
	printf( "Number of pre-smoothing iterations: %u\n", params->preSmooth ) ;
	printf( "Number of post-smoothing iterations: %u\n", params->postSmooth ) ;
	printf( "Number of Multigrid cycles: %u\n", params->numMGCycles ) ;
	printf( "Smooth weight: %.3lf\n", params->smoothWeight ) ;
	printf( "Newton weight factor: %.3lf\n", params->newtWeight ) ;
}

/** Takes the Params object and makes sure that the parameter combinations are
 *	valid. Values are changed where appropriate and the user is notified of the
 *	changes made
 *	\param [in,out] params	The Params object to make valid
 */
void make_params_valid( Params * params, REData * data )
{
	unsigned int maxGridLevel = 8 ; //The maximum grid level
	make_output_bold() ;
	printf( "Validating parameters\n" ) ;
	reset_output_style() ;
	//Perform checks in the case that the algorithm being run is FAS
	if( params->algorithm == ALG_FAS){
		if( params->smoother != SMOOTH_JAC ){
			params->smoother = SMOOTH_JAC ;
			printf( "  Changing smoother to Jacobi for FAS\n" ) ;
		}
	}
	else{
		//Perform checks in the case that Newton's method is being used
/*		if( params->linIt != IT_LIN_MG && params->smoother != SMOOTH_JAC ){*/
/*			params->smoother = SMOOTH_JAC ;*/
/*			printf( "  Changing smoother to Jacobi for preconditioned " ) ;*/
/*			printf( "Krylov subspace method\n" ) ;*/
/*		}*/
		
		if( params->numLinIts < 1 || params->numLinIts > (unsigned int)-100 ){
			params->numLinIts = 3 ;
			printf( "  Changing number of linear iterations per Newton " ) ;
			printf( "iteration to default number of 3\n" ) ;
		}
		else if( params->numLinIts > MAX_INNER_ITS ){
			params->numLinIts = MAX_INNER_ITS ;
			print_indent( 1, "Changing number of linear iterations to " ) ;
			printf( "maximum of %u\n", MAX_INNER_ITS ) ;
		}
	}
	
	//Check that the fine grid level is in an appropriate range
	if( params->fGridLevel < 1 || params->fGridLevel > (unsigned int)-100 ){
		params->fGridLevel = 1 ;
		printf( "  Changing finest grid level to level 2\n" ) ;
	}
	else if( params->fGridLevel > maxGridLevel ){
		params->fGridLevel = maxGridLevel ;
		printf( "  Changing finest grid level to maximum level %u\n",
			maxGridLevel+1 ) ;
	}
	
	//Check that the coarse grid is in an appropriate range
	if( params->cGridLevel > (unsigned int)(-100) ){
		params->cGridLevel = 0 ;
		printf( "  Changing coarsest grid level to 1\n" ) ;
	}
	else if( params->cGridLevel >= params->fGridLevel ){
		params->cGridLevel = params->fGridLevel - 1 ;
		printf( "  Changing the coarsest grid level to be coarser than " ) ;
		printf( "the fine grid level\n" ) ;
		printf( "  Fine level: %u,  Coarse level: %u\n", params->fGridLevel+1,
			params->cGridLevel+1 ) ;
	}
	
	//Make sure that the time step is positive
	params->timeStep = fabs( params->timeStep ) ;
	
	if( params->numTSteps == 0 || params->numTSteps > (unsigned int)-100 ){
		params->numTSteps = 1 ;
		printf( "  Number of time-steps given is invalid. Changing this to " ) ;
		printf( "default value of one\n" ) ;
	}
	
	//Set the current time to be the start time
	params->currTime = params->startTime ;
	
	//Set up the dependent soil characteristics
	set_re_dependent_data( data ) ;
	
	//Print out a message to say that the parameters are now valid
	make_output_bold() ;
	printf( "Parameters are valid\n\n" ) ;
	reset_output_style() ;
}















