#ifndef _PARAMS_H
#define _PARAMS_H
/** \file params.h
 *	\brief Functions for the set-up, maintenance and destruction of parameters
	defining how the algorithm is to be executed
 */

#include "definitions.h"

//Reads parameters from a file
void read_params_from_file( const char *fname, Params *params, REData *data ) ;

//Sets the value of the parameter identified by 'paramName'
void set_param_value( Params *params, REData *data, const char *paramName,
	char *paramValue ) ;
	
//Converts a string representation of a double vector into a double vector
void assign_dvect_values( double *vect, char *values ) ;
	
//Print out the parameters for debugging purposes
void print_params( const Params *params ) ;

//Makes sure that a valid combination of parameters is used
void make_params_valid( Params *params, REData *data ) ;

//Sets the memory of the specified size fo an array to be used when required
//throughout the algorithm. The extra memory is assigned here to avoid repeated
//allocation and deallocation of memory
void set_param_spare_memory( unsigned int size, Params *params ) ;

//Frees the memory assigned on the heap for the parameters structure passed in
void free_param_memory( Params *params ) ;

#endif
